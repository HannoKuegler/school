#include <Arduino.h>

void setup()
{
  DDRD = 0b01100000; // enable color pins
  DDRB = 0b0000100;  // enable common
}

void manageStates();
void wait(unsigned long nextTime, int nextState);

int state = 0;
unsigned long nextRed, nextBlue;
void loop()
{
  manageStates();
}

void manageStates()
{
  switch (state)
  {
  case 0:
    PORTD = 0b01000000;
    state++;
    nextRed = millis() + 1000l;
    break;
  case 1:
    wait(nextRed, 2);
    break;
  case 2:
    PORTD = 0b00000000;
    state++;
    nextRed = millis() + 1000l;
    break;
  case 3:
    wait(nextRed, 4);
    break;
  case 4:
    PORTD = 0b00100000;
    state++;
    nextBlue = millis() + 3000l;
    break;
  case 5:
    wait(nextBlue, 6);
    break;
  case 6:
    PORTD = 0b00000000;
    state++;
    nextBlue = millis() + 3000l;
    break;
  case 7:
    wait(nextBlue, 0);
    break;
  }
}

void wait(unsigned long nextTime, int nextState)
{
  if (millis() > nextTime)
  {
    state = nextState;
  }
}