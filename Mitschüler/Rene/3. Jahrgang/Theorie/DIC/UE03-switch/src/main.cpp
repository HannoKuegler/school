#include <Arduino.h>

void setup1()
{
  DDRD |= 0b01000000;
}

void setup2()
{
  DDRD |= 0b00100000;
}

int state1 = 0;
int sw2;
unsigned long delta1;
void loop1()
{
  switch (state1)
  {
  case 0:
    // read switch input
    sw2 = PINC & 0b00001000;
    if (sw2 == 8) // read pinc (switch 2) on the 4th bit
      state1 = 1;
    break;
  case 1:
    // turn led on
    PORTD |= 0b01000000;
    state1 = 2;
    delta1 = millis();
    break;
  case 2:
    // wait and turn led off
    if (delta1 + 100 < millis())
    {
      PORTD &= 0b10111111;
      state1 = 0;
    }
    break;
  }
}

int state2 = 0;
int sw1;
unsigned long delta2;

void loop2()
{
  switch (state2)
  {
  case 0:
    // read switch input
    sw1 = PINC & 0b00000100; // read pinc (switch 1) on the 3rd bit
    if (sw1 == 4)
      state2 = 1;
    break;
  case 1:
    // turn led on
    PORTD |= 0b00100000;
    state2 = 2;
    delta2 = millis();
    break;
  case 2:
    // wait and turn led off
    if (delta2 + 100 < millis())
    {
      PORTD &= 0b11011111;
      state2 = 0;
    }
    break;
  }
}

void setup()
{
  DDRB |= 0b00000100;  // set common to input
  PORTB &= 0b11111011; // set common to low
  PORTD &= 0b10011111; // turn leds off
  DDRC &= 0b11110011;
  setup1();
  setup2();
}

void loop()
{
  loop1();
  loop2();
}