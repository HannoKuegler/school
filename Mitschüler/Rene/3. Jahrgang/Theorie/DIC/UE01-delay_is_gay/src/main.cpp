#include <Arduino.h>

void setup()
{
  // put your setup code here, to run once:
  DDRD = 0b00001000;

  DDRB = 0b0000100;
}

/* die gaye Methode:
void loop()
{
  // put your main code here, to run repeatedly:
  PORTD = 0b00000100;
  delay(100); // gay
  PORTD = 0b00000000;
  delay(100); // gay
}
*/

// die Methode für die, die zu cool für die Schule sind
int state = 0;
unsigned int nextTimeToExecute = 0;
void loop()
{
  switch (state)
  {
  case 0:
    PORTD = 0b00001000; // set led high
    state++;
    nextTimeToExecute = millis() + 500; // next time to execute is 100ms after now
    break;
  case 1:
    // wait 100ms
    if (millis() > nextTimeToExecute)
    {
      // waited long enough
      state++;
    }
    break;
  case 2:
    PORTD = 0b00000000; // set led low
    state++;
    nextTimeToExecute = millis() + 500; // next time to execute is 100ms after now
    break;
  case 3:
    if (millis() > nextTimeToExecute)
    {
      // waited long enough
      state = 0;
    }
    break;
  }
}