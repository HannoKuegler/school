#include <Arduino.h>

/*
DDR - data direction register | define directory from pin (0 - input | 1 - output)
PORT - set pin to 1 - high | 0 - low

DDRX / PORTX --> X - pin area (8 bit - pin 7 to pin 0 | 00010000 --> pin 4 high)
*/

void tuerkis();
void lila();
void gelb();
void weiss();
void lauflicht();

void setup()
{
  // set common to output - PIN10 | PB02
  DDRB = 0b00000100;
  // set common to low - PIN10 | PB02
  PORTB = 0b00000000; // PORTB = 0

  // set led to output - PIN5 | PB5
  DDRD = 0b01101000;
}

void loop()
{
  lauflicht();
}

void tuerkis()
{
  PORTD = 0b00101000;
  delay(1000);
  PORTD = 0b00000000;
  delay(1000);
}

void lila()
{
  PORTD = 0b01100000;
  delay(1000);
  PORTD = 0b00000000;
  delay(1000);
}

void gelb()
{
  PORTD = 0b01001000;
  delay(1000);
  PORTD = 0b00000000;
  delay(1000);
}

void weiss()
{
  PORTD = 0b01101000;
  delay(1000);
  PORTD = 0b00000000;
  delay(1000);
}

void lauflicht()
{

  // set led red to high
  PORTD = 0b01000000;
  delay(1000);
  // set led red to low
  PORTD = 0b00000000;
  delay(1000);

  gelb();

  // set led green to high
  PORTD = 0b00001000;
  delay(1000);
  // set led green to low
  PORTD = 0b00000000;
  delay(1000);

  tuerkis();

  // set led blue to high
  PORTD = 0b00100000;
  delay(1000);
  // set led blue to low
  PORTD = 0b00000000;
  delay(1000);

  lila();

  weiss();
}