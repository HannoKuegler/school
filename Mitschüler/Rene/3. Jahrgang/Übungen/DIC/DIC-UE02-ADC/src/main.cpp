#include <Arduino.h>

void prozess1Setup();
void prozess1Loop();
void prozess2Setup();
void prozess2Loop();
void prozess3Setup();
void prozess3Loop();

int prozess1State = 0, prozess2State = 0, prozess3State = 0;
unsigned long prozess1Time = 0, prozess2Time = 0, prozess3Time = 0;

void setup()
{
  prozess1Setup();
  prozess2Setup();
  prozess3Setup();
}

void loop()
{
  prozess1Loop();
  prozess2Loop();
  prozess3Loop();
}

void prozess1Setup()
{
  DDRB |= 0b00000100; // set common to high
  DDRD |= 0b00001000; // set green led pin to output
}

/*
* switch the green led on the myShield on / off every 500ms
*/
void prozess1Loop()
{
  switch (prozess1State)
  {
  case 0:
    if (prozess1Time + 500 <= millis())
    {
      PORTD |= 0b00001000;
      prozess1State++;
      prozess1Time = millis();
    }
    break;
  case 1:
    if (prozess1Time + 500 <= millis())
    {
      PORTD &= 0b11110111;
      prozess1State = 0;
      prozess1Time = millis();
    }
    break;
  }
}

void prozess2Setup()
{
  DDRB |= 0b00000100; // set common pin (D10) to high
  DDRD |= 0b00100000; // set red led pin to output
}

/*
* switch the red led on the myShield on / off every 1200ms
*/
void prozess2Loop()
{
  switch (prozess1State)
  {
  case 0:
    if (prozess2Time + 1200 <= millis())
    {
      PORTD |= 0b00100000;
      prozess2State++;
      prozess2Time = millis();
    }
    break;
  case 1:
    if (prozess2Time + 1200 <= millis())
    {
      PORTD &= 0b11011111;
      prozess2State = 0;
      prozess2Time = millis();
    }
    break;
  }
}

void prozess3Setup()
{
  Serial.begin(9600);
  DDRC &= 0b11111110; // set poti pin (A0) to input
  DDRB |= 0b00001000; // set buzzer pin (D11) to output

  ADMUX = 0b01000000;  // enable adc
  ADCSRA = 0b10000111; // enable mux and define the input channel
}

void prozess3Loop()
{
  ADCSRA |= 1 << ADSC; // set the nth (value of ADSC) bit of ADCSRA to 1

  int wert1 = ADCL;
  int wert2 = ADCH;

  int potiWert = wert1 + wert2 * 256;
  if (potiWert != 0)
    tone(11, potiWert, 100);
}