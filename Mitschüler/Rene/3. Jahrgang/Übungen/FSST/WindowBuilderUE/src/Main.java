public class Main {

    public static void main(String[] args) {
        Gui gui = new Gui();
    }

    public static int calcFactorial(int n) {
        int x = 1;
        for (int i = n; i > 0; i--) {
            x *= i;
        }
        return x;
    }

    public static double calcPi(int n) {
        double pi = 1;
        double accuracy = 5; // Genauigkeit in Nachkommastellen

        accuracy = Math.pow(10, accuracy);
        for (float i = 1; i <= n; i++) {
            pi += i % 2 == 1 ? - 1 / Math.pow(i + 1, 2) : + 1 / Math.pow(i + 1, 2);
        }
        return Math.floor(Math.sqrt(pi * 12) * accuracy) / accuracy;
    }

}
