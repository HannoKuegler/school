public class ArrayOperations {

    public static int[] toIntegerArray(String s, char separator) {
        String[] helper = s.split("" + separator);
        int[] output = new int[helper.length];
        for (int i = 0; i < helper.length; i++) {
            output[i] = Integer.parseInt(helper[i]);
        }
        return output;
    }

    public static int[] reverse(int[] a) {
        int[] helper = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            helper[i] = a[a.length - i - 1];
        }
        return helper;
    }

    public static String generateString(int[] a) {
        String output = "";
        for (int i = 0; i < a.length; i++) {
            output += a[i] + " ";
        }
        return output;
    }

}
