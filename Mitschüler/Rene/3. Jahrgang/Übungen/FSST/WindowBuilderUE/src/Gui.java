import javax.swing.*;

public class Gui extends JFrame {

    public Gui() {
        setTitle("Übung01 | Schaar Rene - 3BHEL");
        setSize(430, 160);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);

        addComponents();

        setVisible(true);
    }

    public void addComponents() {
        setContentPane(new Panel());
    }
}

