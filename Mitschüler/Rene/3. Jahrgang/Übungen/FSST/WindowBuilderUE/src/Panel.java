import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Panel extends JPanel {

    Graphics2D g;

    public Panel() {
        requestFocus();
        setBorder(new EmptyBorder(5, 5, 5, 5));
        setLayout(null);
        setVisible(true);
        addChilds();
    }

    @Override
    protected void paintComponent(Graphics arg0) {
        super.paintComponent(arg0);
        g = (Graphics2D) arg0;
    }

    public void addChilds() {
        // Setup
        JLabel lblInputAufgabe1 = new JLabel("n");
        lblInputAufgabe1.setBounds(10, 10, 100, 20);
        add(lblInputAufgabe1);

        JLabel lblInputAufgabe3 = new JLabel("Zahlen");
        lblInputAufgabe3.setBounds(10, 70, 100, 20);
        add(lblInputAufgabe3);

        JLabel lblOutput = new JLabel("Ergebnis:");
        lblOutput.setBounds(300, 10, 100, 20);
        add(lblOutput);

        JLabel lblActions = new JLabel("Action:");
        lblActions.setBounds(150, 10, 100, 20);
        add(lblActions);

        // Aufgabe 1
        JTextField txtFactorialInput = new JTextField();
        txtFactorialInput.setBounds(10, 30, 100, 20);
        add(txtFactorialInput);

        JTextField txtFactorialOutput = new JTextField();
        txtFactorialOutput.setBounds(300, 30, 100, 20);
        add(txtFactorialOutput);

        JButton btnFactorial = new JButton("n!");
        btnFactorial.setBounds(150, 30, 100, 20);
        btnFactorial.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtFactorialOutput.setText(
                        String.valueOf(Main.calcFactorial(Integer.parseInt(txtFactorialInput.getText())))
                );
            }
        });
        add(btnFactorial);

        // Aufgabe 2
        JTextField txtPiOutput = new JTextField();
        txtPiOutput.setBounds(300, 60, 100, 20);
        add(txtPiOutput);

        JButton btnPi = new JButton("pi");
        btnPi.setBounds(150, 60, 100, 20);
        btnPi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double pi = Main.calcPi(Integer.parseInt(txtFactorialInput.getText()));
                txtPiOutput.setText(String.valueOf(pi));
            }
        });
        add(btnPi);

        // Aufgabe 3
        JTextField txtReverseInput = new JTextField();
        txtReverseInput.setBounds(10, 90, 100, 20);
        add(txtReverseInput);

        JTextField txtReverseOutput = new JTextField();
        txtReverseOutput.setBounds(300, 90, 100, 20);
        add(txtReverseOutput);

        JButton btnReverse = new JButton("reverse");
        btnReverse.setBounds(150, 90, 100, 20);
        btnReverse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                txtReverseOutput.setText(
                        ArrayOperations.generateString(
                                ArrayOperations.reverse(
                                        ArrayOperations.toIntegerArray(
                                                txtReverseInput.getText(), ' '
                                        )
                                )
                        )
                );
            }
        });
        add(btnReverse);
    }
}
