package at.klu.htl.fsst;

public class Main {

	public static void main(String[] args) {
		
		Box[] boxes = new Box[50];
		
		for (int i = 0; i < boxes.length; i++) {
			boxes[i] = new Box((int) (Math.random() * 100));
			System.out.println(i + ". " + boxes[i]);
		}
		
		int highestVolume = 0;
		
		for (int i = 0; i < boxes.length; i++) {
			if (boxes[i].calcVolume() > boxes[highestVolume].calcVolume())
				highestVolume = i;
		}
		
		System.out.println("\nDie Box mit dem gr��ten Volumen hat den Index " + highestVolume);
	}
	
}
