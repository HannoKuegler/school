package at.klu.htl.fsst;

public class Box {

	private int length, width, height;

	public Box(int size) {
		this.length = size;
		this.width = size;
		this.height = size;
	}

	public Box(int length, int width, int height) {
		super();
		this.length = length;
		this.width = width;
		this.height = height;
	}

	public int calcVolume() {
		return length * width * height;
	}

	public int calcSurface() {
		return (2 * length * height) + (2 * length * width) + (2 * height * width);
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "Box: {	" + length + "	x	" + width + "	x	" + height + "	|	Volumen: " + calcVolume()
				+ "		|	Surface: " + calcSurface() + "		}";
	}

}
