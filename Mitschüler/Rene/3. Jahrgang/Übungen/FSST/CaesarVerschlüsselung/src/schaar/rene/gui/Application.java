package schaar.rene.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollBar;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.awt.event.ActionEvent;

public class Application extends JFrame {

	private JPanel contentPane;
	private JTextField txtKey;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application frame = new Application();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Application() {
		initComponents();
	}
	
	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblKey = new JLabel("Key:");
		lblKey.setBounds(10, 418, 46, 13);
		contentPane.add(lblKey);
		
		txtKey = new JTextField();
		txtKey.setBounds(40, 415, 96, 19);
		contentPane.add(txtKey);
		txtKey.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 10, 466, 401);
		contentPane.add(scrollPane);
		
		JTextPane txtInput = new JTextPane();
		scrollPane.setViewportView(txtInput);
		
		JButton btnDecode = new JButton("Decode");
		btnDecode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				char[] input = txtInput.getText().toCharArray();
				int key;
				try {
					key = Integer.parseInt(txtKey.getText());
				} catch (Exception ex){
					txtKey.setText("Invalid");
					return;
				}
				txtInput.setText("");
				for (int i = 0; i < input.length; i++) {
					input[i] -= key;
					txtInput.setText(txtInput.getText() + input[i]);
				}
			}
		});
		btnDecode.setBounds(391, 414, 85, 21);
		contentPane.add(btnDecode);
		
		JButton btnEncode = new JButton("Encode");
		btnEncode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				char[] input = txtInput.getText().toCharArray();
				int key;
				try {
					key = Integer.parseInt(txtKey.getText());
				} catch (Exception ex){
					txtKey.setText("Invalid");
					return;
				}
				txtInput.setText("");
				for (int i = 0; i < input.length; i++) {
					input[i] += key;
					txtInput.setText(txtInput.getText() + input[i]);
				}
			}
		});
		btnEncode.setBounds(296, 414, 85, 21);
		contentPane.add(btnEncode);
		
		JMenuItem mntmClear = new JMenuItem("Clear");
		mntmClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtInput.setText("");
				txtKey.setText("");
			}
		});
		mnFile.add(mntmClear);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		mnFile.add(mntmExit);
	}
}
