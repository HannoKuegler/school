package obj;

import java.util.ArrayList;
import java.util.List;

public class City {

	private List<Street> streets = new ArrayList<Street>();
	private String name;

	public City(String name) {
		this.name = name;
	}
	
	public void addStreet(Street street) {
		streets.add(street);
	}
	
	public void addStreet(Street street, int randomHouseCount) {
		street.addRandomHouses(randomHouseCount);
		addStreet(street);
	}
	
	public void removeStreet(Street street) {
		streets.remove(streets.indexOf(street));
	}
	
	public void removeStreet(int index) {
		streets.remove(index);
	}

	public List<Street> getStreets() {
		return streets;
	}

	public void setStreets(List<Street> streets) {
		this.streets = streets;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {

		String s = this.name + ":\n";

		for (Street street : streets) {
			s += street.toString();
		}

		return s;
	}

}
