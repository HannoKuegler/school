package obj;

public class House {

	private int houseNumber;
	private int area;
	private String color;
	private String owner;

	public House(int houseNumber, int area, String color, String owner) {
		if (houseNumber < 1 || houseNumber > 20) {
			System.out.println("Error: house number out of range 20");
			return;
		}
		this.houseNumber = houseNumber;
		this.area = area;
		this.color = color;
		this.owner = owner;
	}

	public int getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "Haus Nr." + this.houseNumber + ": <Fl�che: " + this.area + ", Farbe: " + color + ", Besitzer: " + this.owner + ">";
	}

}