package obj;

import java.util.ArrayList;
import java.util.List;

public class Street {

	public String name;
	private List<House> houses = new ArrayList<House>();

	public Street(String name) {
		this.name = name;
	}

	public void addHouse(House house) {
		if (houses.size() == 19) {
			System.out.println("ERROR: Du erh�lst f�r dieses Haus keine Baugenehmigung - Die Stra�e ist voll :(");
			return;
		}
		houses.add(house);
	}

	public void addNewRandomHouse() {
		int index = houses.size() + 1;
		addHouse(new House(index, (int) (Math.random() * 1000), randomColor(), randomOwner()));
	}

	public void addRandomHouses(int amount) {
		for (int i = 0; i < amount; i++) {
			addNewRandomHouse();
		}
	}

	public String randomOwner() {
		int r = (int) (Math.random() * 24) + 1;

		switch (r) {
		case 1:
			return "Borislav Bogdanovic";
		case 2:
			return "Sandro Homer";
		case 3:
			return "Paul Kleinlercher";
		case 4:
			return "Raphael Kollmann";
		case 5:
			return "Tommy Krainz";
		case 6:
			return "Pascal Krobath";
		case 7:
			return "David Kurath";
		case 8:
			return "Hanno K�gler";
		case 9:
			return "Tobias Kuss";
		case 10:
			return "Mathias M�ller";
		case 11:
			return "Tarik Music";
		case 12:
			return "Mutlu Orhan";
		case 13:
			return "Fabian Persche";
		case 14:
			return "Justin Primasch";
		case 15:
			return "Sandro Riepl";
		case 16:
			return "Rene Schaar";
		case 17:
			return "Senad Siljic";
		case 18:
			return "Luca Streussnig";
		case 19:
			return "Marvin Stroj";
		case 20:
			return "Sven Str�mpl";
		case 21:
			return "Karim Tayari";
		case 22:
			return "Raphael Uran";
		case 23:
			return "Daniel Wriessnig";
		case 24:
			return "Patrick Zechner";
		}

		return "null";
	}

	public String randomColor() {
		int r = (int) (Math.random() * 6) + 1;

		switch (r) {
		case 1:
			return "Rot";
		case 2:
			return "Gelb";
		case 3:
			return "Gr�n";
		case 4:
			return "T�rkis";
		case 5:
			return "Blau";
		case 6:
			return "Lila";
		}

		return "null";
	}

	public void removeHouse(House house) {
		removeHouse(house.getHouseNumber());
	}

	public void removeHouse(int houseNumber) {
		houses.remove(houseNumber);
	}

	public List<House> getHouses() {
		return this.houses;
	}

	@Override
	public String toString() {
		String s = "	" + name + ":\n";
		for (House house : getHouses()) {
			if (house != null)
				s += "		" + house.toString() + "\n";
		}
		return s;
	}

}
