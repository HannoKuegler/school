package core;

import obj.City;
import obj.Street;

public class Main {

	public static void main(String[] args) {
		
		City klagenfurt = new City("Klagenfurt");
		klagenfurt.addStreet(new Street("10. Oktober Stra�er"), 3);
		klagenfurt.addStreet(new Street("Burggasse"), 5);
		klagenfurt.addStreet(new Street("Bahnhofstra�e"), 6);
		
		System.out.println(klagenfurt);

	}

}