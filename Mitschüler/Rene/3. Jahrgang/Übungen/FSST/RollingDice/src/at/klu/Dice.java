package at.klu;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.RoundRectangle2D;

public class Dice {

	private Point position;
	private int number;

	public Dice(Point position, int number) {
		this.position = position;
		this.number = number;
	}

	public Dice(Point position) {
		this.position = position;
		this.number = (int) (Math.random() * 6) + 1;
	}

	public void roll() {
		this.number = (int) (Math.random() * 6) + 1;
	}

	public void paint(Graphics2D g, int a) {
		g.setColor(Color.BLACK);
		g.draw(new RoundRectangle2D.Double((double) (position.x - 1 - a/2), (double) (position.y - 1 - a/2), (double) (a + 1), (double) (a + 1), 10, 10)); // border
		g.setColor(Color.WHITE);
		g.fill(new RoundRectangle2D.Double((double) (position.x - a/2), (double) (position.y - a/2), (double) a, (double) a, 10, 10));  // area
		
		g.setColor(Color.RED);
		switch (this.number) {
		case 6:
			drawDotFour(g, position, a);
			drawDotSix(g, position, a);
		case 4:
			drawDotOne(g, position, a);
			drawDotNine(g, position, a);
		case 2:
			drawDotThree(g, position, a);
			drawDotSeven(g, position, a);
			break;
		case 5:
			drawDotOne(g, position, a);
			drawDotNine(g, position, a);
		case 3:
			drawDotThree(g, position, a);
			drawDotSeven(g, position, a);
		case 1:
			drawDotFive(g, position, a);
			break;
		}
	}

	/*
	 * 1 2 3
	 * 4 5 6
	 * 7 8 9
	 */
	private void drawDotOne(Graphics2D g, Point diceOrigin, int a) {
		g.fillOval(diceOrigin.x + a / 4 - a/2 - a/8, diceOrigin.y + a / 4 - a/2  - a/8, a / 4, a / 4);
	}

	private void drawDotThree(Graphics2D g, Point diceOrigin, int a) {
		g.fillOval(diceOrigin.x + a / 4 * 3 - a/2 - a/8, diceOrigin.y + a / 4 - a/2 - a/8, a / 4, a / 4);
	}

	private void drawDotFour(Graphics2D g, Point diceOrigin, int a) {
		g.fillOval(diceOrigin.x + a / 4 - a/2 - a/8, diceOrigin.y + a / 2 - a/2 - a/8, a / 4, a / 4);
	}

	private void drawDotFive(Graphics2D g, Point diceOrigin, int a) {
		g.fillOval(diceOrigin.x + a / 2 - a/2 - a/8, diceOrigin.y + a / 2 - a/2 - a/8, a / 4, a / 4);
	}

	private void drawDotSix(Graphics2D g, Point diceOrigin, int a) {
		g.fillOval(diceOrigin.x + a / 4 * 3 - a/2 - a/8, diceOrigin.y + a / 2 - a/2 - a/8, a / 4, a / 4);
	}

	private void drawDotSeven(Graphics2D g, Point diceOrigin, int a) {
		g.fillOval(diceOrigin.x + a / 4 - a/2 - a/8, diceOrigin.y + a / 4 * 3 - a/2 - a/8, a / 4, a / 4);
	}

	private void drawDotNine(Graphics2D g, Point diceOrigin, int a) {
		g.fillOval(diceOrigin.x + a / 4 * 3 - a/2 - a/8, diceOrigin.y + a / 4 * 3 - a/2 - a/8, a / 4, a / 4);
	}

	public Point getPosition() {
		return position;
	}

	public void setPosition(Point position) {
		this.position = position;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
