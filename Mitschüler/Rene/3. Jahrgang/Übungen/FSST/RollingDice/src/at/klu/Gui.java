package at.klu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

public class Gui extends JFrame {

	public static JPanel contentPane;
	Timer t;
	int runs = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setTitle("Rolling Dice | Rene Schaar");

		List<Dice> dices = new ArrayList<>();

		contentPane = new JPanel() {
			@Override
			protected void paintComponent(Graphics arg0) {
				Graphics2D g = (Graphics2D) arg0;
				g.setColor(Color.GREEN);
				g.fillRect(0, 0, getWidth(), getHeight());
				dices.forEach((Dice dice) -> {
					dice.paint(g, 40);
				});
			}
		};
		contentPane.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mousePressed(MouseEvent e) {
				switch (e.getButton()) {
				case 1:
					dices.add(new Dice(new Point(e.getX(), e.getY())));
					repaint();
					break;
				case 2:
					dices.removeAll(dices);
					repaint();
					break;
				case 3:

					t = new Timer(100, new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							if (runs != 0) {
								dices.forEach((Dice dice) -> {
									dice.roll();
									if (++runs == 15) {
										runs = 0;
										t.stop();
									}
								});
								repaint();
							} else {
								runs = 0;
								t.stop();
							}
						}
					});
					t.start();

					repaint();
					break;
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		contentPane.requestFocus();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
