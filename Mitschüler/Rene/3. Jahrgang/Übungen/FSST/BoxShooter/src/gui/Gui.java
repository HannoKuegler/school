package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import obj.Square;

public class Gui extends JFrame {

	private JPanel contentPane;
	private boolean correctPositions = true;
	private int accuracy;

	public static JLabel lblSquareCount;
	public static JLabel lblPositionCorrection;
	
	public static Square[] squares;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public Gui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		setLocationRelativeTo(null);
		setResizable(false);

		contentPane = new JPanel();
		contentPane.setLayout(null);
		setContentPane(contentPane);

		accuracy = 1;

		Game game = new Game();
		game.setBounds(10, 50, 765, 475);
		contentPane.add(game);

		JLabel lblCorrectPosition = new JLabel("Positions Korrektur: " + correctPositions);

		JMenuItem ctrlClear = new JMenuItem("Clear");
		ctrlClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				squares = null;
			}
		});
		JMenuItem ctrlExit = new JMenuItem("Exit");
		ctrlExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		JMenu ctrlAccuracy = new JMenu("Genauigkeit");
		
		JMenuItem ctrlCorrectPositions = new JMenuItem("Toggle Correct Positions");
		ctrlCorrectPositions.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				correctPositions = !correctPositions;
				lblCorrectPosition.setText("Positions Korrektur: " + correctPositions);
				ctrlAccuracy.setVisible(correctPositions);
				squares = null;
			}
		}); 
		
		JMenuItem accOne = new JMenuItem("1x");
		accOne.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				accuracy = 1;
			}
		});
		JMenuItem accTen = new JMenuItem("10x");
		accOne.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				accuracy = 10;
			}
		});
		JMenuItem accHundred = new JMenuItem("100x");
		accOne.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				accuracy = 100;
			}
		});
		JMenuItem accThousand = new JMenuItem("1.000x");
		accOne.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				accuracy = 1000;
			}
		});
		JMenuItem accTenThousand = new JMenuItem("10.000x");
		accOne.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				accuracy = 10000;
			}
		});
		ctrlAccuracy.add(accOne);
		ctrlAccuracy.add(accTen);
		ctrlAccuracy.add(accHundred);
		ctrlAccuracy.add(accThousand);
		ctrlAccuracy.add(accTenThousand);

		JMenu controlMenu = new JMenu("Game");
		controlMenu.add(ctrlCorrectPositions);
		controlMenu.add(ctrlAccuracy);
		controlMenu.add(ctrlClear);
		controlMenu.add(ctrlExit);

		JMenuBar controlPanel = new JMenuBar();
		controlPanel.add(controlMenu);
		setJMenuBar(controlPanel);

		JLabel lblAnzahl = new JLabel("Anzahl:");
		lblAnzahl.setBounds(10, 0, 250, 50);
		contentPane.add(lblAnzahl);

		lblSquareCount = new JLabel();
		lblSquareCount.setBounds(250, 0, 250, 50);
		contentPane.add(lblSquareCount);

		lblCorrectPosition.setBounds(500, 0, 250, 50);
		contentPane.add(lblCorrectPosition);

		JTextField txtAnzahl = new JTextField();
		txtAnzahl.setBounds(60, 15, 50, 20);
		contentPane.add(txtAnzahl);

		JButton btnStart = new JButton("Go!");
		btnStart.setBounds(700, 5, 75, 25);
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int n = Integer.parseInt(txtAnzahl.getText());
				squares = new Square[n];
				for (int i = 0; i < n; i++) {
					squares[i] = Square.newRandomSquare();
				}
				Thread validnessChecker = new Thread(new Runnable() {

					@Override
					public void run() {
						if (!correctPositions)
							return;
						for (Square square : squares) {
							for (int i = 0; i < 1000 * accuracy; i++) {
								square.checkValidness(squares);
							}
						}
					}
				});
				validnessChecker.run();

			}
		});
		contentPane.add(btnStart);

	}
	
}
