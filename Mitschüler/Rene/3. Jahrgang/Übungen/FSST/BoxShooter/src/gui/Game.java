package gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

import listener.MouseListener;
import obj.Square;

public class Game extends JPanel implements ActionListener {

	Graphics2D g;
	MouseListener mouseListener;
	int blueCount, redCount;

	public Game() {
		mouseListener = new MouseListener();
		addMouseListener(mouseListener);
		setFocusable(true);
		requestFocus();
		Timer t = new Timer(1 / 60, this);
		t.start();
	}

	@Override
	protected void paintComponent(Graphics arg0) {
		g = (Graphics2D) arg0;

		int blue = 0, red = 0;

		drawBackground();
		drawBorder(2f);

		if (Gui.squares != null) {
			for (Square square : Gui.squares) {
				// Abz�hlen der Quadrate mit Farben
				if (square.getColor() == Color.blue)
					blue++;
				else if (square.getColor() == Color.red)
					red++;
//				square.print();
				square.draw(g);
			}
			blueCount = blue;
			redCount = red;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (Gui.squares != null)
			Gui.lblSquareCount
					.setText("Quadrate: " + Gui.squares.length + " | Blaue: " + blueCount + " | Rote: " + redCount);
		else
			Gui.lblSquareCount.setText("");
		
		repaint();
	}

	public void drawBorder(float width) {
		Stroke s = g.getStroke();

		g.setColor(Color.black);
		g.setStroke(new BasicStroke(width));
		g.drawLine(0, 0, getWidth(), 0);

		g.drawLine(getWidth(), 0, getWidth(), getHeight());
		g.drawLine(getWidth(), getHeight(), 0, getHeight());
		g.drawLine(0, getHeight(), 0, 0);

		g.setStroke(s);
	}

	public void drawBackground() {
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());
	}

}
