package obj;

public class Vec2f {

	public float x, y;

	public Vec2f(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public Vec2f() {
		this(0, 0);
	}
	
	public void print() {
		System.out.println("[ x: " + x + ", y: " + y + " ]");
	}

	public void add(Vec2f v) {
		this.x += v.x;
		this.y += v.y;
	}

	public void add(float x, float y) {
		this.x += x;
		this.y += y;
	}

	public void sub(Vec2f v) {
		this.x -= v.x;
		this.y -= v.y;
	}

	public void sub(float x, float y) {
		this.x -= x;
		this.y -= y;
	}

	public void mul(Vec2f v) {
		this.x *= v.x;
		this.y *= v.y;
	}

	public void mul(float x, float y) {
		this.x *= x;
		this.y *= y;
	}

	public void div(Vec2f v) {
		this.x /= v.x;
		this.y /= v.y;
	}

	public void div(float x, float y) {
		this.x /= x;
		this.y /= y;
	}

	public Vec2f add(Vec2f v, Vec2f blank) {
		return new Vec2f(x + v.x, y + v.y);
	}

	public Vec2f add(float x, float y, Vec2f blank) {
		return new Vec2f(this.x + x, this.y + y);
	}

	public Vec2f sub(Vec2f v, Vec2f blank) {
		return new Vec2f(x - v.x, y - v.y);
	}

	public Vec2f sub(float x, float y, Vec2f blank) {
		return new Vec2f(this.x - x, this.y - y);
	}

	public Vec2f mul(Vec2f v, Vec2f blank) {
		return new Vec2f(x * v.x, y * v.y);
	}

	public Vec2f mul(float x, float y, Vec2f blank) {
		return new Vec2f(this.x * x, this.y * y);
	}

	public Vec2f div(Vec2f v, Vec2f blank) {
		return new Vec2f(x / v.x, y / v.y);
	}

	public Vec2f div(float x, float y, Vec2f blank) {
		return new Vec2f(this.x / x, this.y / y);
	}

	public float getLength() {
		return this.x * this.x + this.y * this.y;
	}

	public Vec2f getNormalVec() {
		return new Vec2f(this.y, -this.x);
	}

}
