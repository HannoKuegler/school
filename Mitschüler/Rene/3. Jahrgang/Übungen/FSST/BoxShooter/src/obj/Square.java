package obj;

import java.awt.Color;
import java.awt.Graphics2D;

import gui.Gui;

public class Square extends Rectangle {

	private boolean isRightClicked, isLeftClicked;
	private Color color;

	public Square(int x, int y, int width, int height) {
		super(x, y, width, height);
		this.isRightClicked = false;
		this.isLeftClicked = false;
		this.color = Color.black;
	}

	public void draw(Graphics2D g) {
		if (color == Color.white) {
			g.setColor(Color.black);
			g.drawRect((int) x, (int) y, 10, 10);
		} else {
			g.setColor(color);
			g.fillRect(getX(), getY(), getWidth(), getHeight());
		}
	}

	public void print() {
		System.out.println("[");
		System.out.println("	x:		" + getX());
		System.out.println("	y:		" + getY());
		System.out.println("	width:		" + getWidth());
		System.out.println("	height:		" + getHeight());
		System.out.println("	color:		" + color);
		System.out.println("	isRightClicked:	" + isRightClicked);
		System.out.println("	isLeftClicked: 	" + isLeftClicked);
		System.out.println("]");
		System.out.println();
	}

	public static Square newRandomSquare() {
		int x = (int) (Math.random() * 755);
		int y = (int) (Math.random() * 465);
		int width = 10;
		int height = 10;
		Square square = new Square(x, y, width, height);
		square.setColor(Color.white);
		return square;
	}

	public void checkValidness(Square[] squares) {
		for (Square square : squares) {
			if (square == null)
				return;
			if (this != square) {
				if (this.isIntersecting(square))
						square.correctPosition();
			}
		}

	}

	public boolean isIntersecting(int x, int y) {
		return ((x > getLeft() && x < getRight()) && (y < getBottom() && y > getTop()));
	}

	public boolean isIntersecting(Square square) {
		Vec2f distance = getOrigin().sub(square.getOrigin(), new Vec2f());
		distance = new Vec2f(Math.abs(distance.x), Math.abs(distance.y));
		return (distance.x > 0 && distance.x < 10 && distance.y > 0 && distance.y < 10);
	}

	public void correctPosition() {
		x = (int) (Math.random() * 755);
		y = (int) (Math.random() * 465);
	}

	public boolean isRightClicked() {
		return isRightClicked;
	}

	public void setRightClicked(boolean isRightClicked) {
		this.isRightClicked = isRightClicked;
	}

	public boolean isLeftClicked() {
		return isLeftClicked;
	}

	public void setLeftClicked(boolean isLeftClicked) {
		this.isLeftClicked = isLeftClicked;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

}
