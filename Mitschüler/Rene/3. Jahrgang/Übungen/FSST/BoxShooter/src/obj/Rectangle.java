package obj;

public class Rectangle extends Vec2f{

	private int width, height;
	private Vec2f origin, size;

	public Rectangle(int x, int y, int width, int height) {
		super(x, y);
		this.width = width;
		this.height = height;
		this.origin = new Vec2f(x + width / 2, y + height / 2);
		this.size = new Vec2f(width, height);
	}

	public Rectangle(Vec2f origin, Vec2f size) {
		this((int) (origin.x - size.x / 2), (int) (origin.y - size.y / 2),(int)size.x,(int)size.y);
	}

	public int getRight() {
		return (int) x + width;
	}

	public int getBottom() {
		return (int) y + height;
	}
	
	public int getLeft() {
		return (int) x;
	}
	
	public int getTop() {
		return (int) y;
	}
	
	public Vec2f getRightBottom() {
		return new Vec2f(getRight(), getBottom());
	}
	
	public Vec2f getLeftBottom() {
		return new Vec2f(getLeft(), getBottom());
	}
	
	public Vec2f getRightTop() {
		return new Vec2f(getRight(), getTop());
	}
	
	public Vec2f getLeftTop() {
		return new Vec2f(getLeft(), getTop());
	}

	public int getX() {
		return (int) x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return (int) this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Vec2f getOrigin() {
		return origin;
	}

	public void setOrigin(Vec2f origin) {
		this.origin = origin;
	}

	public Vec2f getSize() {
		return size;
	}

	public void setSize(Vec2f size) {
		this.size = size;
	}

}
