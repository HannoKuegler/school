package listener;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import gui.Gui;
import obj.Square;

public class MouseListener implements java.awt.event.MouseListener {

	public MouseListener() {
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// Setzen der Farbe eines Quadrates
		for (Square square : Gui.squares) {
			if (square.isIntersecting(e.getX(), e.getY())) {
				switch (e.getButton()) {
				case 1:
					square.setColor(square.isLeftClicked() ? Color.white : Color.blue);
					square.setLeftClicked(!square.isLeftClicked());
					square.setRightClicked(false);
					break;
				case 3:
					square.setColor(square.isRightClicked() ? Color.white : Color.red);
					square.setRightClicked(!square.isRightClicked());
					square.setLeftClicked(false);
					break;
				}
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
