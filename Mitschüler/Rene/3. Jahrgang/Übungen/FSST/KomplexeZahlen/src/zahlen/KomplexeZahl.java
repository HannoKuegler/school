package zahlen;

import static java.lang.Math.*;


/**
 * Komplexe Zahl
 * @author Ren� Schaar
 *
 */
public class KomplexeZahl {

	private double realTeil, imaginaerTeil;

	/**
	 * Erstellen einer neuen komplexen Zahl anhand von einen realTeil und einen Imagin�rteil
	 * @param realTeil Der reale Anteil der komplexen Zahl
	 * @param imaginaerTeil der imagin�re Anteil der komplexen Zahl
	 */
	public KomplexeZahl(double realTeil, double imaginaerTeil) {
		this.realTeil = realTeil;
		this.imaginaerTeil = imaginaerTeil;
	}
	
	/**
	 * Initialisierung einer komplexen Zahl per Betrag und Winkel
	 * @param betrag Betrag einer komplexen Zahl
	 * @param winkel Winkel phi in Grad
	 * @return neue komplexe Zahl mit angegebenen Betrag und Winkel
	 */
	public static KomplexeZahl getInstanzVonPolarWert(double betrag, double winkel) {
		KomplexeZahl komplexeZahl = new KomplexeZahl(0, 0);
		komplexeZahl.setPolar(betrag, winkel);
		return komplexeZahl;		
	}
	
	/**
	 * Addieren der komplexen Zahl mit einer komplexen Zahl
	 * @param zahl zu addierende komplexe Zahl
	 * @return die neue komplexe Zahl
	 */
	public KomplexeZahl add(KomplexeZahl zahl) {
		this.realTeil += zahl.getrealTeil();
		this.imaginaerTeil += zahl.getimaginaerTeil();
		return this;
	}
	
	/**
	 * Subtrahieren der komplexen Zahl mit einer komplexen Zahl
	 * @param zahl zu subtrahierende komplexe Zahl
	 * @return die neue komplexe Zahl
	 */
	public KomplexeZahl subtract(KomplexeZahl zahl) {
		this.realTeil -= zahl.getrealTeil();
		this.imaginaerTeil -= zahl.getimaginaerTeil();
		return this;
	}
	
	/**
	 * Multiplizieren der komplexen Zahl mit einer komplexen Zahl
	 * @param zahl zu mulitplizierende komplexe Zahl
	 * @return die neue komplexe Zahl
	 */
	public KomplexeZahl multiply(KomplexeZahl zahl) {
		this.realTeil = this.realTeil * zahl.realTeil - this.imaginaerTeil * zahl.imaginaerTeil;
		this.imaginaerTeil = this.realTeil * zahl.imaginaerTeil + this.imaginaerTeil * zahl.realTeil;
		return this;
	}
	
	/**
	 * Division der komplexen Zahl mit einer komplexen Zahl
	 * @param zahl zu dividierende komplexe Zahl
	 * @return die neue komplexe Zahl
	 */
	public KomplexeZahl divide(KomplexeZahl zahl) {
		if (zahl.realTeil == 0 || zahl.imaginaerTeil == 0)
			throw new UnsupportedOperationException("Division durch 0 ist undefiniert");
		this.realTeil = (this.realTeil * zahl.realTeil + this.imaginaerTeil * zahl.imaginaerTeil) / (zahl.realTeil * zahl.realTeil + zahl.imaginaerTeil * zahl.imaginaerTeil);
		this.imaginaerTeil = (-this.realTeil * zahl.imaginaerTeil + this.imaginaerTeil * zahl.realTeil) / (zahl.realTeil * zahl.realTeil + zahl.imaginaerTeil * zahl.imaginaerTeil);
		return this;
	}
	
	/**
	 * Vergleichen zweier Zahlen anhand ihrer Betr�ge
	 * @param zahl zu vergleichende Zahl
	 * @return delta Betrag
	 */
	public double getDeltaBetrag(KomplexeZahl zahl) {
		return this.getBetrag() - zahl.getBetrag();
	}
	
	/**
	 * Setzten des Real- und Imagin�rteils einer komplexen Zahl �ber den Betrag und Winkel
	 * @param betrag Betrag einer komplexen Zahl
	 * @param winkel Winkel phi in Grad
	 */
	public void setPolar(double betrag, double winkel) {
		winkel /= (180*PI);
		this.realTeil = betrag * cos(winkel);
		this.imaginaerTeil = betrag * sin(winkel);
	}
	
	/**
	 * R�ckliefern des Winkels einer komplexen Zahl
	 * @return	der Winkel in Grad
	 */
	public double getWinkel() {
		return atan2(imaginaerTeil, realTeil) * 180 * PI;
	}

	/**
	 * R�ckliefern des Betrages einer komplexen Zahl
	 * @return Betrag der komplexen Zahl
	 */
	public double getBetrag() {
		return sqrt(realTeil * realTeil + imaginaerTeil * imaginaerTeil);
	}

	/**
	 * R�ckliefern des realen Anteils einer komplexen Zahl
	 * @return Realanteil
	 */
	public double getrealTeil() {
		return realTeil;
	}

	/**
	 * Setzten des realen Anteils einer komplexen Zahl
	 * @param realTeil neuer Realanteil
	 */
	public void setrealTeil(double realTeil) {
		this.realTeil = realTeil;
	}

	/**
	 * R�ckliefern des imagin�ren Anteiles einer komplexen Zahl
	 * @return Imagin�rteil
	 */
	public double getimaginaerTeil() {
		return imaginaerTeil;
	}

	/**
	 * Setzen des imagin�ren Anteils einer komplexen Zahl
	 * @param imaginaerTeil neuer Imagin�rteil
	 */
	public void setimaginaerTeil(double imaginaerTeil) {
		this.imaginaerTeil = imaginaerTeil;
	}

	@Override
	public String toString() {
		return realTeil + " + j" + imaginaerTeil;
	}
	
}
