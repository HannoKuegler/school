package schaar.rene.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import schaar.rene.obj.Schueler;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JSeparator;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;

import com.toedter.calendar.JDateChooser;

public class Application extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtIndex;
	private JTextField txtVorname;
	private JTextField txtNachname;
	private JTextField txtBetrag;
	private JTextPane txtAnmerkung;
	private JDateChooser dateChooser;

	// 946688000000l is the time in millis from 01.01.1970 to 01.01.2000 (30 years)
	private Date defaultDate = new Date(946688000000l);

	private Schueler[] students;
	private int index = 0;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application frame = new Application();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Application() {
		students = new Schueler[20];
		init();
	}

	private void init() {
		setTitle("Sch�lerverwaltung | Ren� Schaar");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 410, 500);
		setResizable(false);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu menu = new JMenu("File");
		menuBar.add(menu);

		JMenuItem mntmClear = new JMenuItem("Clear");
		mntmClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				students[index] = null;
				update();
			}
		});
		menu.add(mntmClear);

		JMenuItem mntmClearAll = new JMenuItem("Clear All");
		mntmClearAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				students = new Schueler[20];
				update();
			}
		});
		menu.add(mntmClearAll);

		JSeparator separator_1 = new JSeparator();
		menu.add(separator_1);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		menu.add(mntmExit);

		JMenu mnExtras = new JMenu("Extras");
		menuBar.add(mnExtras);

		JMenuItem menuItem = new JMenuItem("Sch\u00FClerliste");
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (Schueler student : students) {
					if (student != null) {
						System.out.println(student.toString());
					}
				}
			}
		});
		mnExtras.add(menuItem);

		JMenu menu_1 = new JMenu("Betrag");
		mnExtras.add(menu_1);

		JMenuItem menuItem_1 = new JMenuItem("Summe");
		menuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double summe = 0;
				for (Schueler student : students) {
					if (student != null)
						summe += Double.parseDouble(student.getBetrag());
				}
				System.out.println("Summe aller Betr�ge:	" + summe);
				System.out.println("");
			}
		});
		menu_1.add(menuItem_1);

		JMenuItem menuItem_2 = new JMenuItem("Durchschnitt");
		menuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double durchschnitt = 0;
				int counter = 0;
				for (Schueler student : students) {
					if (student != null) {
						durchschnitt += Double.parseDouble(student.getBetrag());
						counter++;
					}
				}
				durchschnitt /= counter;
				System.out.println("Durchschnittsbetrag aller Betr�ge:	" + durchschnitt);
			}
		});
		menu_1.add(menuItem_2);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JSeparator separator = new JSeparator();
		separator.setBounds(10, 374, 376, 2);
		contentPane.add(separator);

		JButton btnFirst = new JButton("|<");
		btnFirst.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				index = 0;
				txtIndex.setText(Integer.toString(index + 1));
				update();
			}
		});
		btnFirst.setBounds(10, 400, 50, 21);
		contentPane.add(btnFirst);

		JButton btnShiftLeft = new JButton("<<");
		btnShiftLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				index = index > 0 ? index - 1 : students.length - 1;
				txtIndex.setText(Integer.toString(index + 1));
				update();
			}
		});
		btnShiftLeft.setBounds(70, 400, 50, 21);
		contentPane.add(btnShiftLeft);

		txtIndex = new JTextField();
		txtIndex.setHorizontalAlignment(SwingConstants.CENTER);
		txtIndex.setText("1");
		txtIndex.setBounds(130, 401, 50, 19);
		contentPane.add(txtIndex);
		txtIndex.setColumns(10);

		JButton btnShiftRight = new JButton(">>");
		btnShiftRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				index = index < students.length - 1 ? index + 1 : 0;
				txtIndex.setText(Integer.toString(index + 1));
				update();
			}
		});
		btnShiftRight.setBounds(190, 400, 50, 21);
		contentPane.add(btnShiftRight);

		JButton btnLast = new JButton(">|");
		btnLast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = students.length - 1; i > 0; i--) {
					if (students[i] != null) {
						index = i;
						break;
					}
				}
				txtIndex.setText(Integer.toString(index + 1));
				update();
			}
		});
		btnLast.setBounds(250, 400, 50, 21);
		contentPane.add(btnLast);

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				students[index] = new Schueler(txtVorname.getText(), txtNachname.getText(), dateChooser.getDate(),
						txtBetrag.getText(), txtAnmerkung.getText());
				update();
			}
		});
		btnSave.setBounds(310, 400, 76, 21);
		contentPane.add(btnSave);

		JLabel lblVorname = new JLabel("Vorname");
		lblVorname.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblVorname.setBounds(10, 10, 75, 13);
		contentPane.add(lblVorname);

		JLabel lblNachname = new JLabel("Nachname");
		lblNachname.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNachname.setBounds(10, 60, 75, 13);
		contentPane.add(lblNachname);

		JLabel lblGeburtsdatum = new JLabel("Geburtsdatum");
		lblGeburtsdatum.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblGeburtsdatum.setBounds(10, 110, 75, 13);
		contentPane.add(lblGeburtsdatum);

		JLabel lblBetrag = new JLabel("Betrag");
		lblBetrag.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblBetrag.setBounds(10, 160, 75, 13);
		contentPane.add(lblBetrag);

		JLabel lblAnmerkung = new JLabel("Anmerkung");
		lblAnmerkung.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblAnmerkung.setBounds(10, 210, 75, 13);
		contentPane.add(lblAnmerkung);

		txtVorname = new JTextField();
		txtVorname.setBounds(130, 8, 256, 19);
		contentPane.add(txtVorname);
		txtVorname.setColumns(10);

		txtNachname = new JTextField();
		txtNachname.setBounds(130, 58, 256, 19);
		contentPane.add(txtNachname);
		txtNachname.setColumns(10);

		txtBetrag = new JTextField();
		txtBetrag.setBounds(130, 158, 256, 19);
		contentPane.add(txtBetrag);
		txtBetrag.setColumns(10);

		txtAnmerkung = new JTextPane();
		txtAnmerkung.setBounds(130, 210, 256, 140);
		contentPane.add(txtAnmerkung);

		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd.MM.yyyy");
		dateChooser.setDate(defaultDate);
		dateChooser.setBounds(130, 103, 256, 20);
		contentPane.add(dateChooser);
	}

	private void update() {
		if (students[index] == null) {
			txtVorname.setText("");
			txtNachname.setText("");
			dateChooser.setDate(defaultDate);
			txtBetrag.setText("");
			txtAnmerkung.setText("");
		} else {
			txtVorname.setText(students[index].getVorname());
			txtNachname.setText(students[index].getNachname());
			dateChooser.setDate(students[index].getGeburtsdatum());
			txtBetrag.setText(students[index].getBetrag());
			txtAnmerkung.setText(students[index].getAnmerkung());
		}
	}
}
