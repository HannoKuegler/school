package schaar.rene.obj;

import java.util.Date;

public class Schueler {

	private String vorname, nachname, betrag, anmerkung;
	private Date geburtsdatum;

	public Schueler(String vorname, String nachname, Date geburtsdatum, String betrag, String anmerkung) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsdatum = geburtsdatum;
		try {
			this.betrag = Double.parseDouble(betrag) < 0 ? "0" : betrag;
		} catch (Exception e) {
			this.betrag = "0";
		}
		this.anmerkung = anmerkung;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public Date getGeburtsdatum() {
		return geburtsdatum;
	}

	public void setGeburtsdatum(Date geburtsdatum) {
		this.geburtsdatum = geburtsdatum;
	}

	public String getBetrag() {
		return betrag;
	}

	public void setBetrag(String betrag) {
		this.betrag = betrag;
	}

	public String getAnmerkung() {
		return anmerkung;
	}

	public void setAnmerkung(String anmerkung) {
		this.anmerkung = anmerkung;
	}

	@Override
	public String toString() {
		String gebDate = getGeburtsdatum().toString();
		String[] splitted = gebDate.split(" ");
		gebDate = splitted[0] + " " + splitted[1] + " " + splitted[2] + " " + splitted[5]; // remove time and timezone

		return "{\n" + "	Vorname:		" + getVorname() + "\n" + "	Nachname:		" + getNachname() + "\n"
				+ "	Geburtsdatum:		" + gebDate + "\n" + "	Betrag:			" + getBetrag() + "\n"
				+ "	Anmerkung:		" + getAnmerkung() + "\n" + "}\n";
	}

}
