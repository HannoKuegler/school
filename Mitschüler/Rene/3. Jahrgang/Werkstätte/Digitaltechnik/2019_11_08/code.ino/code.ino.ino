void setup() {
  // put your setup code here, to run once:
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(11, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (analogRead(A0) < 512) {
    digitalWrite(11, HIGH);
    return;
  }
  if (analogRead(A1) > 512) {
    digitalWrite(11, LOW);
    return;
  }
}
