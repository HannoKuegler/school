<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="e-klemmen">
<description>&lt;b&gt;Klemmen für Elektropläne&lt;/b&gt;&lt;p&gt;

Diese Bibliothek enthält Klemmen sowie Devices für Einspeisung und Erdung. Folgendes ist zu
beachten: &lt;p&gt;

&lt;b&gt;Einspeisungs-Devices&lt;/b&gt;&lt;p&gt;

Enthalten kein Package, da kein entsprechendes Bauteil existiert, das in einer Materialliste erscheinen sollte. &lt;p&gt;

&lt;b&gt;Erdungs-Devices&lt;/b&gt;&lt;p&gt;

Enthalten ein Package, da in einer Materialliste zumindest ein Bauteil mit Klemmmöglichkeit
erscheinen muss.&lt;p&gt;

&lt;b&gt;Klemmen&lt;/b&gt;&lt;p&gt;

Klemmennamen müssen im Schaltplan mit &lt;i&gt;X&lt;/i&gt; beginnen, damit Klemmenplan und Brückenplan richtig erzeugt werden. Deshalb ist ihr Prefix im Device auf X gesetzt. Bitte auch im Schaltplan keine
anderen Namen verwenden. Siehe auch: User-Language-Programm e-klemmenplan.ulp.&lt;p&gt;

&lt;b&gt;Brückenklemmen&lt;/b&gt;&lt;p&gt;

Brückenklemmen sind Klemmen, die zusätzlich zu den Drahtanschlüssen die Möglichkeit bieten, mit einem Brückenkamm eine Reihe von Klemmen zu verbinden. Mit dem User-Language-Programm
e-brueckenverwaltung.ulp werden solche Brücken definiert und als Liste ausgegeben. In dessen
Hilfe erfahren Sie Details. Dieses Programm setzt einige Dinge bei den verwendeten Bauteilen
voraus (nur wichtig, wenn Sie eigene Brückenklemmen definieren wollen):&lt;p&gt;

Der Device-Name muss &lt;i&gt;BRUECKE&lt;/i&gt; enthalten, andere Klemmen dürfen  &lt;i&gt;BRUECKE&lt;/i&gt;
nicht als Namensbestandteil enthalten.&lt;p&gt;

Die Pin-Namen der Klemmensymbole müssen 1 und 2 sein. Die damit verbundenen Pad-Namen
des Package müssen 1.1, 1.2; 2.1, 2.2 usw. sein. Dabei entspricht die Zahl vor dem Punkt dem
Gate-Namen (1, 2, 3..).&lt;p&gt;

Jedes Klemmensymbol muss gesondert definiert sein, da es für den Referenz-Text zwei Attribute verwendet, deren Platzhalter je Symbol unterschiedlich sind. Der Name der Attribute ist 51 und 52,
wenn es sich um Klemme 5 handelt. Der Parameter &lt;i&gt;display&lt;/i&gt; für diese Attribute (der im
Attribut-Bearbeitungsmenü im Feld &lt;i&gt;Anzeige&lt;/i&gt; eingestellt wird) muss im Schaltplan auf
&lt;i&gt;Off&lt;/i&gt; stehen, sonst werden die Referenz-Texte nicht an der richtigen Stelle dargestellt.
Wenn Sie das ULP zur Brückenverwaltung verwenden, geschieht das automatisch.&lt;p&gt;

&lt;p&gt;&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="KLEMME_1-1">
<description>Dummy</description>
<pad name="1.1" x="0" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="1.2" x="0" y="-2.54" drill="0.5" diameter="1" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="EINSPEISUNG_GLEICHSPANNUNG">
<wire x1="-7.62" y1="5.08" x2="8.89" y2="5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="5.08" x2="8.89" y2="-5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-6.35" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-7.62" y="-7.62" size="1.778" layer="94">EINSPEISUNG</text>
<text x="-2.921" y="1.651" size="1.778" layer="94">+</text>
<text x="1.651" y="2.794" size="1.778" layer="94" rot="R270">0V</text>
<pin name="+-EXT" x="-2.54" y="7.62" visible="off" length="short" direction="sup" rot="R270"/>
<pin name="0V-EXT" x="2.54" y="7.62" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="KLEMME_1-1">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.016" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="1.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="1.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EINSPEISUNG_GLEICHSPANNUNG" prefix="ESP" uservalue="yes">
<description>Einspeisung für Gleichspannung</description>
<gates>
<gate name="G$1" symbol="EINSPEISUNG_GLEICHSPANNUNG" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KLEMME_1-1" prefix="X" uservalue="yes">
<description>Klemme 1 Pol.</description>
<gates>
<gate name="1" symbol="KLEMME_1-1" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="KLEMME_1-1">
<connects>
<connect gate="1" pin="1.1" pad="1.1"/>
<connect gate="1" pin="1.2" pad="1.2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-schalter">
<description>&lt;b&gt;Schalter für Elektropläne&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SCHLIESSER_RASTSCHALTER">
<description>Dummy</description>
<pad name="14" x="0" y="-5.08" drill="0.2" diameter="0.6" shape="square"/>
<pad name="13" x="0" y="5.08" drill="0.2" diameter="0.6" shape="square"/>
<text x="0" y="2.54" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="SCHLIESSER_HANDBETAETIGT">
<description>Dummy</description>
<pad name="14" x="0" y="-5.08" drill="0.2" diameter="0.6" shape="square"/>
<pad name="13" x="0" y="5.08" drill="0.2" diameter="0.6" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="SCHLIESSER_RASTSCHALTER">
<wire x1="-4.9164" y1="1.2192" x2="-4.9164" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.9164" y1="0" x2="-4.9164" y2="-1.2192" width="0.1524" layer="94"/>
<wire x1="-3.2908" y1="0" x2="-2.478" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-2.478" y1="-1.016" x2="-1.665" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.665" y1="0" x2="-0.8524" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.9164" y1="0" x2="-3.2908" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="1.524" x2="0" y2="2.54" width="0.1524" layer="94"/>
<text x="-3.81" y="3.81" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-3.81" y="-2.54" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-3.81" y="-10.16" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-3.81" y="-5.08" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-3.81" y="-7.62" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="4" x="0" y="-5.08" visible="pad" length="short" direction="in" rot="R90"/>
<pin name="3" x="0" y="5.08" visible="pad" length="short" direction="in" rot="R270"/>
</symbol>
<symbol name="SCHLIESSER_HANDBETAETIGT">
<wire x1="-4.9164" y1="1.2192" x2="-4.9164" y2="0" width="0.1836" layer="94"/>
<wire x1="-4.9164" y1="0" x2="-4.9164" y2="-1.2192" width="0.1836" layer="94"/>
<wire x1="-3.4938" y1="0" x2="-2.2746" y2="0" width="0.1836" layer="94"/>
<wire x1="-4.9164" y1="0" x2="-4.1034" y2="0" width="0.1836" layer="94"/>
<wire x1="-0.8524" y1="0" x2="-1.665" y2="0" width="0.1836" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="1.524" x2="0" y2="2.54" width="0.1524" layer="94"/>
<text x="-3.81" y="3.81" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-3.81" y="-2.54" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-3.81" y="-10.16" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-3.81" y="-5.08" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-3.81" y="-7.62" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="4" x="0" y="-5.08" visible="pad" length="short" direction="in" rot="R90"/>
<pin name="3" x="0" y="5.08" visible="pad" length="short" direction="in" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SCHLIESSER_RASTSCHALTER" prefix="S" uservalue="yes">
<description>Rastschalter, Schließer</description>
<gates>
<gate name="G$1" symbol="SCHLIESSER_RASTSCHALTER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SCHLIESSER_RASTSCHALTER">
<connects>
<connect gate="G$1" pin="3" pad="13"/>
<connect gate="G$1" pin="4" pad="14"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SCHLIESSER_HANDBETAETIGT" prefix="S" uservalue="yes">
<description>Schließer, handbetätigt</description>
<gates>
<gate name="G$1" symbol="SCHLIESSER_HANDBETAETIGT" x="0" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="SCHLIESSER_HANDBETAETIGT">
<connects>
<connect gate="G$1" pin="3" pad="13"/>
<connect gate="G$1" pin="4" pad="14"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-schuetze-relais">
<description>&lt;b&gt;Schütze und Relais für Elektropläne&lt;/b&gt;&lt;p&gt;

Wenn das von Ihnen gewünschte Schütz in der vorliegenden Bibliothek nicht vorhanden ist, laden Sie
die Bibliothek &lt;u&gt;e-symbole.lbr&lt;/u&gt;, und starten Sie das User-Languag-Programm  &lt;u&gt;e-bauteil-erstellen.ulp&lt;/u&gt;.&lt;p&gt; 

&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MULTIFUNKTIONSRELAIS">
<description>Dummy</description>
<pad name="15" x="10.16" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="18" x="12.7" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="A1" x="0" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="A2" x="0" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="B1" x="2.54" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="A3" x="2.54" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="16" x="7.62" y="-12.7" drill="0.5" diameter="1" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="MULTIFUNKTIONSRELAIS">
<wire x1="5.715" y1="1.651" x2="2.54" y2="1.651" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.651" x2="-2.54" y2="1.651" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.651" x2="-3.175" y2="1.651" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.651" x2="-3.175" y2="-1.651" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.651" x2="-2.54" y2="-1.651" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.651" x2="2.54" y2="-1.651" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.651" x2="5.715" y2="-1.651" width="0.254" layer="94"/>
<wire x1="5.715" y1="-1.651" x2="5.715" y2="1.651" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="1.651" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.651" width="0.1524" layer="94"/>
<wire x1="-6.477" y1="1.651" x2="-3.175" y2="1.651" width="0.254" layer="94"/>
<wire x1="-6.477" y1="-1.651" x2="-6.477" y2="1.651" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.651" x2="-6.477" y2="-1.651" width="0.254" layer="94"/>
<text x="-7.62" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-7.62" y="0" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="A1" x="-2.54" y="5.08" visible="pad" length="short" direction="in" rot="R270"/>
<pin name="A2" x="-2.54" y="-5.08" visible="pad" length="short" direction="in" rot="R90"/>
<pin name="B1" x="2.54" y="5.08" visible="pad" length="short" direction="in" rot="R270"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.651" width="0.1524" layer="94"/>
<pin name="A3" x="2.54" y="-5.08" visible="pad" length="short" direction="in" rot="R90"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.651" width="0.1524" layer="94"/>
<rectangle x1="-6.35" y1="-1.524" x2="-3.302" y2="1.524" layer="94"/>
</symbol>
<symbol name="MF_WECHSLER">
<wire x1="-2.54" y1="-1.524" x2="-1.27" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="-1.651" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.524" width="0.1524" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="15" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="18" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="16" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MULTIFUNKTIONSRELAIS" prefix="K" uservalue="yes">
<description>Zeitrelais, 1-pol.</description>
<gates>
<gate name="G$1" symbol="MULTIFUNKTIONSRELAIS" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="MF_WECHSLER" x="17.78" y="0"/>
</gates>
<devices>
<device name="" package="MULTIFUNKTIONSRELAIS">
<connects>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="B1" pad="B1"/>
<connect gate="G$2" pin="15" pad="15"/>
<connect gate="G$2" pin="16" pad="16"/>
<connect gate="G$2" pin="18" pad="18"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-lampen-signalisation">
<description>&lt;b&gt;Signalgeber für Elektropläne&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="LAMPE">
<description>Dummy</description>
<pad name="1" x="0" y="5.08" drill="0.2" diameter="0.6" shape="square"/>
<pad name="2" x="0" y="-5.08" drill="0.2" diameter="0.6" shape="square"/>
<text x="0" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="LAMPE">
<wire x1="0" y1="2.54" x2="0" y2="2.034" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.034" width="0.1524" layer="94"/>
<wire x1="-1.4352" y1="-1.4352" x2="1.4352" y2="1.4352" width="0.1524" layer="94"/>
<wire x1="1.4352" y1="-1.4352" x2="-1.4352" y2="1.4352" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="2.034" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-2.54" y="-7.62" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="X1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="X2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SIGNALLAMPE" prefix="P" uservalue="yes">
<description>Lampe</description>
<gates>
<gate name="G$1" symbol="LAMPE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LAMPE">
<connects>
<connect gate="G$1" pin="X1" pad="1"/>
<connect gate="G$1" pin="X2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="ESP1" library="e-klemmen" deviceset="EINSPEISUNG_GLEICHSPANNUNG" device=""/>
<part name="X1/10" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="X1/9" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="X5/7" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="X5/10" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="X5/8" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="X5/9" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="X4/3" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="X4/2" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="X4/1" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="X2/10" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="X2/7" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="S1" library="e-schalter" deviceset="SCHLIESSER_RASTSCHALTER" device=""/>
<part name="S2" library="e-schalter" deviceset="SCHLIESSER_HANDBETAETIGT" device=""/>
<part name="K1" library="e-schuetze-relais" deviceset="MULTIFUNKTIONSRELAIS" device=""/>
<part name="H1" library="e-lampen-signalisation" deviceset="SIGNALLAMPE" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="ESP1" gate="G$1" x="-17.78" y="86.36" rot="R270"/>
<instance part="X1/10" gate="1" x="2.54" y="73.66"/>
<instance part="X1/9" gate="1" x="15.24" y="73.66"/>
<instance part="X5/7" gate="1" x="27.94" y="73.66"/>
<instance part="X5/10" gate="1" x="33.02" y="73.66" rot="R180"/>
<instance part="X5/8" gate="1" x="27.94" y="48.26"/>
<instance part="X5/9" gate="1" x="33.02" y="48.26" rot="R180"/>
<instance part="X4/3" gate="1" x="50.8" y="73.66"/>
<instance part="X4/2" gate="1" x="55.88" y="73.66" rot="R180"/>
<instance part="X4/1" gate="1" x="53.34" y="48.26"/>
<instance part="X2/10" gate="1" x="73.66" y="48.26" rot="R180"/>
<instance part="X2/7" gate="1" x="73.66" y="73.66"/>
<instance part="S1" gate="G$1" x="0" y="60.96"/>
<instance part="S2" gate="G$1" x="5.08" y="60.96" rot="R180"/>
<instance part="K1" gate="G$2" x="53.34" y="60.96" rot="R180"/>
<instance part="K1" gate="G$1" x="30.48" y="60.96"/>
<instance part="H1" gate="G$1" x="73.66" y="60.96"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="3"/>
<pinref part="X1/10" gate="1" pin="1.1"/>
<wire x1="0" y1="66.04" x2="0" y2="71.12" width="0.1524" layer="91"/>
<wire x1="0" y1="71.12" x2="2.54" y2="71.12" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="4"/>
<wire x1="5.08" y1="66.04" x2="5.08" y2="71.12" width="0.1524" layer="91"/>
<wire x1="5.08" y1="71.12" x2="2.54" y2="71.12" width="0.1524" layer="91"/>
<junction x="2.54" y="71.12"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="4"/>
<wire x1="0" y1="55.88" x2="0" y2="50.8" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="3"/>
<wire x1="0" y1="50.8" x2="2.54" y2="50.8" width="0.1524" layer="91"/>
<wire x1="2.54" y1="50.8" x2="5.08" y2="50.8" width="0.1524" layer="91"/>
<wire x1="5.08" y1="50.8" x2="5.08" y2="55.88" width="0.1524" layer="91"/>
<wire x1="2.54" y1="50.8" x2="2.54" y2="45.72" width="0.1524" layer="91"/>
<junction x="2.54" y="50.8"/>
<wire x1="2.54" y1="45.72" x2="15.24" y2="45.72" width="0.1524" layer="91"/>
<wire x1="15.24" y1="45.72" x2="15.24" y2="71.12" width="0.1524" layer="91"/>
<pinref part="X1/9" gate="1" pin="1.1"/>
</segment>
</net>
<net name="+-EXT" class="0">
<segment>
<pinref part="ESP1" gate="G$1" pin="+-EXT"/>
<pinref part="X1/10" gate="1" pin="1.2"/>
<wire x1="-10.16" y1="88.9" x2="2.54" y2="88.9" width="0.1524" layer="91"/>
<wire x1="2.54" y1="88.9" x2="2.54" y2="76.2" width="0.1524" layer="91"/>
<wire x1="2.54" y1="88.9" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
<pinref part="X4/3" gate="1" pin="1.2"/>
<wire x1="50.8" y1="76.2" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="X1/9" gate="1" pin="1.2"/>
<pinref part="X5/7" gate="1" pin="1.2"/>
<wire x1="15.24" y1="76.2" x2="27.94" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="X5/7" gate="1" pin="1.1"/>
<pinref part="K1" gate="G$1" pin="A1"/>
<wire x1="27.94" y1="71.12" x2="27.94" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="X5/10" gate="1" pin="1.2"/>
<pinref part="K1" gate="G$1" pin="B1"/>
<wire x1="33.02" y1="71.12" x2="33.02" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="K1" gate="G$1" pin="A2"/>
<pinref part="X5/8" gate="1" pin="1.2"/>
<wire x1="27.94" y1="55.88" x2="27.94" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="K1" gate="G$1" pin="A3"/>
<pinref part="X5/9" gate="1" pin="1.1"/>
<wire x1="33.02" y1="55.88" x2="33.02" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="0V-EXT" class="0">
<segment>
<pinref part="ESP1" gate="G$1" pin="0V-EXT"/>
<wire x1="-10.16" y1="83.82" x2="-10.16" y2="35.56" width="0.1524" layer="91"/>
<pinref part="X5/8" gate="1" pin="1.1"/>
<wire x1="-10.16" y1="35.56" x2="27.94" y2="35.56" width="0.1524" layer="91"/>
<wire x1="27.94" y1="35.56" x2="27.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="73.66" y1="35.56" x2="27.94" y2="35.56" width="0.1524" layer="91"/>
<junction x="27.94" y="35.56"/>
<pinref part="X2/10" gate="1" pin="1.2"/>
<wire x1="73.66" y1="35.56" x2="73.66" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="K1" gate="G$2" pin="18"/>
<pinref part="X4/3" gate="1" pin="1.1"/>
<wire x1="50.8" y1="71.12" x2="50.8" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="X4/2" gate="1" pin="1.2"/>
<pinref part="K1" gate="G$2" pin="16"/>
<wire x1="55.88" y1="71.12" x2="55.88" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="K1" gate="G$2" pin="15"/>
<pinref part="X4/1" gate="1" pin="1.2"/>
<wire x1="53.34" y1="55.88" x2="53.34" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="X4/1" gate="1" pin="1.1"/>
<wire x1="53.34" y1="45.72" x2="66.04" y2="45.72" width="0.1524" layer="91"/>
<wire x1="66.04" y1="45.72" x2="66.04" y2="76.2" width="0.1524" layer="91"/>
<pinref part="X2/7" gate="1" pin="1.2"/>
<wire x1="66.04" y1="76.2" x2="73.66" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="X2/7" gate="1" pin="1.1"/>
<pinref part="H1" gate="G$1" pin="X1"/>
<wire x1="73.66" y1="71.12" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="X2/10" gate="1" pin="1.1"/>
<pinref part="H1" gate="G$1" pin="X2"/>
<wire x1="73.66" y1="50.8" x2="73.66" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
