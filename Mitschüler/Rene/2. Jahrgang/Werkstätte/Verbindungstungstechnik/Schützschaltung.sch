<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="dots" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="e-elektro-zeichnungsrahmen">
<description>&lt;b&gt;Rahmen für Elektropläne&lt;/b&gt;&lt;p&gt;
Die vordefinierten Rahmen eignen sich für &lt;u&gt;Kontakt- und Signalreferenzen&lt;/u&gt;, die ab Version 5.0 möglich sind.&lt;p&gt;

Wird ein Rahmen mit vordefinerten &lt;u&gt;Attributen&lt;/u&gt;, z. B. RAHMEN_A4_8Z-19S, in eine neue Schaltung
geholt, dann sind zunächst die Platzhaltertexte (&gt;ATTRIBUTNAME) sichtbar, weil diese Attribute noch nicht
angelegt sind. Verwenden Sie das User-Language-Programm &lt;u&gt;e-attributverwaltung.ulp&lt;/u&gt;, um auf
bequeme Weise die noch nicht definierten Attribute zu generieren.&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="RAHMEN_A4_8Z-19S_OHNE_DOKUMENTENFELD">
<wire x1="0" y1="-7.62" x2="2.54" y2="-7.62" width="0.254" layer="95" style="shortdash"/>
<text x="1.27" y="-6.35" size="1.524" layer="95">Papierrand</text>
<text x="128.27" y="-7.62" size="1.524" layer="95">Heftrand</text>
<frame x1="0" y1="0" x2="297.18" y2="182.88" columns="19" rows="8" layer="94" border-right="no" border-bottom="no"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RAHMEN_A4_8Z-19S_OHNE_DOKUMENTENFELD" prefix="RAHMEN">
<description>Zeichnungsrahmen DIN A4, 8 Zeilen, 19 Spalten ohne Dokumentenfeld</description>
<gates>
<gate name="G$1" symbol="RAHMEN_A4_8Z-19S_OHNE_DOKUMENTENFELD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-klemmen">
<description>&lt;b&gt;Klemmen für Elektropläne&lt;/b&gt;&lt;p&gt;

Diese Bibliothek enthält Klemmen sowie Devices für Einspeisung und Erdung. Folgendes ist zu
beachten: &lt;p&gt;

&lt;b&gt;Einspeisungs-Devices&lt;/b&gt;&lt;p&gt;

Enthalten kein Package, da kein entsprechendes Bauteil existiert, das in einer Materialliste erscheinen sollte. &lt;p&gt;

&lt;b&gt;Erdungs-Devices&lt;/b&gt;&lt;p&gt;

Enthalten ein Package, da in einer Materialliste zumindest ein Bauteil mit Klemmmöglichkeit
erscheinen muss.&lt;p&gt;

&lt;b&gt;Klemmen&lt;/b&gt;&lt;p&gt;

Klemmennamen müssen im Schaltplan mit &lt;i&gt;X&lt;/i&gt; beginnen, damit Klemmenplan und Brückenplan richtig erzeugt werden. Deshalb ist ihr Prefix im Device auf X gesetzt. Bitte auch im Schaltplan keine
anderen Namen verwenden. Siehe auch: User-Language-Programm e-klemmenplan.ulp.&lt;p&gt;

&lt;b&gt;Brückenklemmen&lt;/b&gt;&lt;p&gt;

Brückenklemmen sind Klemmen, die zusätzlich zu den Drahtanschlüssen die Möglichkeit bieten, mit einem Brückenkamm eine Reihe von Klemmen zu verbinden. Mit dem User-Language-Programm
e-brueckenverwaltung.ulp werden solche Brücken definiert und als Liste ausgegeben. In dessen
Hilfe erfahren Sie Details. Dieses Programm setzt einige Dinge bei den verwendeten Bauteilen
voraus (nur wichtig, wenn Sie eigene Brückenklemmen definieren wollen):&lt;p&gt;

Der Device-Name muss &lt;i&gt;BRUECKE&lt;/i&gt; enthalten, andere Klemmen dürfen  &lt;i&gt;BRUECKE&lt;/i&gt;
nicht als Namensbestandteil enthalten.&lt;p&gt;

Die Pin-Namen der Klemmensymbole müssen 1 und 2 sein. Die damit verbundenen Pad-Namen
des Package müssen 1.1, 1.2; 2.1, 2.2 usw. sein. Dabei entspricht die Zahl vor dem Punkt dem
Gate-Namen (1, 2, 3..).&lt;p&gt;

Jedes Klemmensymbol muss gesondert definiert sein, da es für den Referenz-Text zwei Attribute verwendet, deren Platzhalter je Symbol unterschiedlich sind. Der Name der Attribute ist 51 und 52,
wenn es sich um Klemme 5 handelt. Der Parameter &lt;i&gt;display&lt;/i&gt; für diese Attribute (der im
Attribut-Bearbeitungsmenü im Feld &lt;i&gt;Anzeige&lt;/i&gt; eingestellt wird) muss im Schaltplan auf
&lt;i&gt;Off&lt;/i&gt; stehen, sonst werden die Referenz-Texte nicht an der richtigen Stelle dargestellt.
Wenn Sie das ULP zur Brückenverwaltung verwenden, geschieht das automatisch.&lt;p&gt;

&lt;p&gt;&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="KLEMME_1-10">
<description>Dummy</description>
<pad name="1.1" x="0" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="1.2" x="0" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="2.1" x="2.54" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="2.2" x="2.54" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="3.1" x="5.08" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="3.2" x="5.08" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="4.1" x="7.62" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="4.2" x="7.62" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="5.1" x="10.16" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="5.2" x="10.16" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="6.1" x="12.7" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="6.2" x="12.7" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="7.1" x="15.24" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="7.2" x="15.24" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="8.1" x="17.78" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="8.2" x="17.78" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="9.1" x="20.32" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="9.2" x="20.32" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="10.1" x="22.86" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="10.2" x="22.86" y="-2.54" drill="0.5" diameter="1" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="EINSPEISUNG_GLEICHSPANNUNG">
<wire x1="-7.62" y1="5.08" x2="8.89" y2="5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="5.08" x2="8.89" y2="-5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-6.35" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-7.62" y="-7.62" size="1.778" layer="94">EINSPEISUNG</text>
<text x="-2.921" y="1.651" size="1.778" layer="94">+</text>
<text x="1.651" y="2.794" size="1.778" layer="94" rot="R270">0V</text>
<pin name="+-EXT" x="-2.54" y="7.62" visible="off" length="short" direction="sup" rot="R270"/>
<pin name="0V-EXT" x="2.54" y="7.62" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="KLEMME_1-1">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="1.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="1.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE </text>
</symbol>
<symbol name="KLEMME_2">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="2.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="2.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_3">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.524" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="3.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="3.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.524" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_4">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="4.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="4.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_5">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="5.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_6">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="6.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_7">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="7.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="7.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_8">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="8.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="8.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_9">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.016" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="9.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="9.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.524" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_10">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="10.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="10.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="EINSPEISUNG_GLEICHSPANNUNG" prefix="ESP" uservalue="yes">
<description>Einspeisung für Gleichspannung</description>
<gates>
<gate name="G$1" symbol="EINSPEISUNG_GLEICHSPANNUNG" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KLEMME_1-10" prefix="X" uservalue="yes">
<description>Klemme 10 Pol.</description>
<gates>
<gate name="1" symbol="KLEMME_1-1" x="-48.26" y="0" swaplevel="1"/>
<gate name="2" symbol="KLEMME_2" x="-35.56" y="0" swaplevel="2"/>
<gate name="3" symbol="KLEMME_3" x="-22.86" y="0" swaplevel="3"/>
<gate name="4" symbol="KLEMME_4" x="-10.16" y="0" swaplevel="4"/>
<gate name="5" symbol="KLEMME_5" x="2.54" y="0" swaplevel="5"/>
<gate name="6" symbol="KLEMME_6" x="15.24" y="0" swaplevel="6"/>
<gate name="7" symbol="KLEMME_7" x="27.94" y="0" swaplevel="7"/>
<gate name="8" symbol="KLEMME_8" x="40.64" y="0" swaplevel="8"/>
<gate name="9" symbol="KLEMME_9" x="53.34" y="0" swaplevel="9"/>
<gate name="10" symbol="KLEMME_10" x="66.04" y="0" swaplevel="10"/>
</gates>
<devices>
<device name="" package="KLEMME_1-10">
<connects>
<connect gate="1" pin="1.1" pad="1.1"/>
<connect gate="1" pin="1.2" pad="1.2"/>
<connect gate="10" pin="10.1" pad="10.1"/>
<connect gate="10" pin="10.2" pad="10.2"/>
<connect gate="2" pin="2.1" pad="2.1"/>
<connect gate="2" pin="2.2" pad="2.2"/>
<connect gate="3" pin="3.1" pad="3.1"/>
<connect gate="3" pin="3.2" pad="3.2"/>
<connect gate="4" pin="4.1" pad="4.1"/>
<connect gate="4" pin="4.2" pad="4.2"/>
<connect gate="5" pin="5.1" pad="5.1"/>
<connect gate="5" pin="5.2" pad="5.2"/>
<connect gate="6" pin="6.1" pad="6.1"/>
<connect gate="6" pin="6.2" pad="6.2"/>
<connect gate="7" pin="7.1" pad="7.1"/>
<connect gate="7" pin="7.2" pad="7.2"/>
<connect gate="8" pin="8.1" pad="8.1"/>
<connect gate="8" pin="8.2" pad="8.2"/>
<connect gate="9" pin="9.1" pad="9.1"/>
<connect gate="9" pin="9.2" pad="9.2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-lampen-signalisation">
<description>&lt;b&gt;Signalgeber für Elektropläne&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="LAMPE">
<description>Dummy</description>
<pad name="1" x="0" y="5.08" drill="0.2" diameter="0.6" shape="square"/>
<pad name="2" x="0" y="-5.08" drill="0.2" diameter="0.6" shape="square"/>
<text x="0" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="LAMPE">
<wire x1="0" y1="2.54" x2="0" y2="2.034" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.034" width="0.1524" layer="94"/>
<wire x1="-1.4352" y1="-1.4352" x2="1.4352" y2="1.4352" width="0.1524" layer="94"/>
<wire x1="1.4352" y1="-1.4352" x2="-1.4352" y2="1.4352" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="2.034" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-2.54" y="-7.62" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="X1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="X2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SIGNALLAMPE" prefix="P" uservalue="yes">
<description>Lampe</description>
<gates>
<gate name="G$1" symbol="LAMPE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LAMPE">
<connects>
<connect gate="G$1" pin="X1" pad="1"/>
<connect gate="G$1" pin="X2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-schalter">
<description>&lt;b&gt;Schalter für Elektropläne&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="EINTASTER">
<description>Dummy</description>
<pad name="14" x="0" y="-1.905" drill="0.8" shape="square"/>
<pad name="13" x="0" y="1.905" drill="0.8" shape="square"/>
<text x="0" y="2.54" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="AUSTASTER">
<description>Dummy</description>
<pad name="12" x="0" y="-7.62" drill="0.2" diameter="0.6" shape="square"/>
<pad name="11" x="0" y="7.62" drill="0.2" diameter="0.6" shape="square"/>
<text x="0" y="2.54" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TASTER_SCHLIESSER">
<wire x1="-0.762" y1="0" x2="-0.889" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="-1.143" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="0" x2="-3.302" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.143" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="-4.191" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.937" y1="1.143" x2="-5.08" y2="1.143" width="0.1524" layer="94"/>
<wire x1="-3.937" y1="-1.143" x2="-5.08" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="1.524" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.651" width="0.254" layer="94"/>
<text x="-3.81" y="3.81" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-3.81" y="-2.54" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-3.81" y="-10.16" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-3.81" y="-5.08" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-3.81" y="-7.62" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="14" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="13" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="TASTER_OEFFNER">
<wire x1="-1.143" y1="0" x2="-2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.556" y1="1.143" x2="-3.556" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.556" y1="0" x2="-3.556" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="-2.667" y1="0" x2="-3.556" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.413" y1="1.143" x2="-3.556" y2="1.143" width="0.1524" layer="94"/>
<wire x1="-2.413" y1="-1.143" x2="-3.556" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="2.032" y1="1.778" x2="0" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="1.778" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.3768" y2="2.1384" width="0.254" layer="94"/>
<wire x1="0.381" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="3.81" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-2.54" y="-10.16" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-2.54" y="-7.62" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TASTER_SCHLIESSER" prefix="S" uservalue="yes">
<description>Ein-Taster (Arbeitskontakt)</description>
<gates>
<gate name="G$1" symbol="TASTER_SCHLIESSER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EINTASTER">
<connects>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TASTER_OEFFNER" prefix="S" uservalue="yes">
<description>Austaster (Ruhekontakt)</description>
<gates>
<gate name="G$1" symbol="TASTER_OEFFNER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AUSTASTER">
<connects>
<connect gate="G$1" pin="1" pad="11"/>
<connect gate="G$1" pin="2" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-schuetze-relais">
<description>&lt;b&gt;Schütze und Relais für Elektropläne&lt;/b&gt;&lt;p&gt;

Wenn das von Ihnen gewünschte Schütz in der vorliegenden Bibliothek nicht vorhanden ist, laden Sie
die Bibliothek &lt;u&gt;e-symbole.lbr&lt;/u&gt;, und starten Sie das User-Languag-Programm  &lt;u&gt;e-bauteil-erstellen.ulp&lt;/u&gt;.&lt;p&gt; 

&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="HAUPTSCHUETZ_3-POL_HILFSKONTAKT_3Ö-3S">
<description>Dummy</description>
<pad name="A1" x="-10.16" y="2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="A2" x="-10.16" y="-7.62" drill="0.5" diameter="1" shape="square"/>
<pad name="1" x="2.54" y="2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="2" x="2.54" y="-7.62" drill="0.5" diameter="1" shape="square"/>
<pad name="3" x="7.62" y="2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="4" x="7.62" y="-7.62" drill="0.5" diameter="1" shape="square"/>
<pad name="5" x="12.7" y="2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="6" x="12.7" y="-7.62" drill="0.5" diameter="1" shape="square"/>
<pad name="13" x="17.78" y="2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="14" x="17.78" y="-7.62" drill="0.5" diameter="1" shape="square"/>
<pad name="21" x="22.86" y="2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="22" x="22.86" y="-7.62" drill="0.5" diameter="1" shape="square"/>
<pad name="53" x="27.94" y="2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="54" x="27.94" y="-7.62" drill="0.5" diameter="1" shape="square"/>
<pad name="61" x="33.02" y="2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="62" x="33.02" y="-7.62" drill="0.5" diameter="1" shape="square"/>
<pad name="71" x="38.1" y="2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="72" x="38.1" y="-7.62" drill="0.5" diameter="1" shape="square"/>
<pad name="83" x="43.18" y="2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="84" x="43.18" y="-7.62" drill="0.5" diameter="1" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="SPULE">
<wire x1="3.175" y1="1.651" x2="0" y2="1.651" width="0.254" layer="94"/>
<wire x1="0" y1="1.651" x2="-3.175" y2="1.651" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.651" x2="-3.175" y2="-1.651" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.651" x2="0" y2="-1.651" width="0.254" layer="94"/>
<wire x1="0" y1="-1.651" x2="3.175" y2="-1.651" width="0.254" layer="94"/>
<wire x1="3.175" y1="-1.651" x2="3.175" y2="1.651" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.651" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.651" width="0.1524" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-3.81" y="0" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="A2" x="0" y="-5.08" visible="pad" length="short" direction="in" rot="R90"/>
<pin name="A1" x="0" y="5.08" visible="pad" length="short" direction="in" rot="R270"/>
</symbol>
<symbol name="SCHLIESSER_HAUPTSCHUETZ_3-POL">
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.143" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.397" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="2.413" width="0.1524" layer="94" curve="-157.380135"/>
<wire x1="-5.08" y1="-2.54" x2="-6.35" y2="1.524" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="1.778" x2="0" y2="2.413" width="0.1524" layer="94" curve="-157.380135"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.778" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.778" x2="5.08" y2="2.413" width="0.1524" layer="94" curve="-157.380135"/>
<wire x1="5.08" y1="-2.54" x2="3.81" y2="1.524" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="1.778" width="0.1524" layer="94"/>
<text x="-7.62" y="1.27" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="2" x="-5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="-5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="3" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="4" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="6" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="OEFFNER">
<wire x1="0" y1="1.778" x2="1.778" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="2.286" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.778" width="0.1524" layer="94"/>
<text x="-2.54" y="1.27" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="SCHLIESSER">
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.778" width="0.1524" layer="94"/>
<text x="-2.54" y="1.27" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="3" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="4" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HAUPTSCHUETZ_3-POL_HILFSKONTAKT_3Ö-3S" prefix="Q" uservalue="yes">
<description>Hauptschütz mit Öffner und Schließer</description>
<gates>
<gate name="G$1" symbol="SPULE" x="-10.16" y="5.08" addlevel="must"/>
<gate name="G$2" symbol="SCHLIESSER_HAUPTSCHUETZ_3-POL" x="7.62" y="5.08" addlevel="always"/>
<gate name="G$3" symbol="OEFFNER" x="27.94" y="5.08" addlevel="can"/>
<gate name="G$4" symbol="SCHLIESSER" x="22.86" y="5.08" addlevel="can"/>
<gate name="G$5" symbol="SCHLIESSER" x="33.02" y="5.08" addlevel="can"/>
<gate name="G$6" symbol="OEFFNER" x="38.1" y="5.08" addlevel="can"/>
<gate name="G$7" symbol="OEFFNER" x="43.18" y="5.08" addlevel="can"/>
<gate name="G$8" symbol="SCHLIESSER" x="48.26" y="5.08" addlevel="can"/>
</gates>
<devices>
<device name="" package="HAUPTSCHUETZ_3-POL_HILFSKONTAKT_3Ö-3S">
<connects>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$2" pin="1" pad="1"/>
<connect gate="G$2" pin="2" pad="2"/>
<connect gate="G$2" pin="3" pad="3"/>
<connect gate="G$2" pin="4" pad="4"/>
<connect gate="G$2" pin="5" pad="5"/>
<connect gate="G$2" pin="6" pad="6"/>
<connect gate="G$3" pin="1" pad="21"/>
<connect gate="G$3" pin="2" pad="22"/>
<connect gate="G$4" pin="3" pad="13"/>
<connect gate="G$4" pin="4" pad="14"/>
<connect gate="G$5" pin="3" pad="53"/>
<connect gate="G$5" pin="4" pad="54"/>
<connect gate="G$6" pin="1" pad="61"/>
<connect gate="G$6" pin="2" pad="62"/>
<connect gate="G$7" pin="1" pad="71"/>
<connect gate="G$7" pin="2" pad="72"/>
<connect gate="G$8" pin="3" pad="83"/>
<connect gate="G$8" pin="4" pad="84"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="RAHMEN1" library="e-elektro-zeichnungsrahmen" deviceset="RAHMEN_A4_8Z-19S_OHNE_DOKUMENTENFELD" device=""/>
<part name="ESP1" library="e-klemmen" deviceset="EINSPEISUNG_GLEICHSPANNUNG" device=""/>
<part name="+T-X1" library="e-klemmen" deviceset="KLEMME_1-10" device=""/>
<part name="P1" library="e-lampen-signalisation" deviceset="SIGNALLAMPE" device=""/>
<part name="S1" library="e-schalter" deviceset="TASTER_SCHLIESSER" device=""/>
<part name="S2" library="e-schalter" deviceset="TASTER_OEFFNER" device=""/>
<part name="+T-X2" library="e-klemmen" deviceset="KLEMME_1-10" device=""/>
<part name="+R-X1" library="e-klemmen" deviceset="KLEMME_1-10" device=""/>
<part name="+R-X3" library="e-klemmen" deviceset="KLEMME_1-10" device=""/>
<part name="+R-K1" library="e-schuetze-relais" deviceset="HAUPTSCHUETZ_3-POL_HILFSKONTAKT_3Ö-3S" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="RAHMEN1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="ESP1" gate="G$1" x="27.94" y="147.32" smashed="yes" rot="R270">
<attribute name="VALUE" x="24.13" y="153.67" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+T-X1" gate="1" x="50.8" y="104.14" smashed="yes">
<attribute name="PART" x="49.022" y="105.664" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="49.022" y="103.124" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="P1" gate="G$1" x="106.68" y="55.88" smashed="yes">
<attribute name="PART" x="104.14" y="58.42" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="104.14" y="55.88" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="104.14" y="48.26" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="104.14" y="53.34" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="104.14" y="50.8" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="S1" gate="G$1" x="50.8" y="124.46" smashed="yes">
<attribute name="PART" x="46.99" y="128.27" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="46.99" y="121.92" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="46.99" y="114.3" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="46.99" y="119.38" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="46.99" y="116.84" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="S2" gate="G$1" x="50.8" y="93.98" smashed="yes">
<attribute name="PART" x="48.26" y="97.79" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="48.26" y="91.44" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="48.26" y="83.82" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="48.26" y="88.9" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="48.26" y="86.36" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+T-X1" gate="2" x="50.8" y="83.82" smashed="yes">
<attribute name="PART" x="49.022" y="85.344" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="49.022" y="82.804" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X1" gate="5" x="50.8" y="134.62" smashed="yes">
<attribute name="PART" x="49.022" y="136.144" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="49.022" y="133.604" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X1" gate="6" x="50.8" y="114.3" smashed="yes">
<attribute name="PART" x="49.022" y="115.824" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="49.022" y="113.284" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X2" gate="8" x="106.68" y="66.04" smashed="yes">
<attribute name="PART" x="104.902" y="67.564" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="104.902" y="65.024" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X2" gate="10" x="106.68" y="45.72" smashed="yes">
<attribute name="PART" x="104.902" y="47.244" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="104.902" y="44.704" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-X1" gate="8" x="50.8" y="66.04" smashed="yes">
<attribute name="PART" x="49.022" y="67.564" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="49.022" y="65.024" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-X1" gate="9" x="50.8" y="45.72" smashed="yes">
<attribute name="PART" x="49.022" y="46.736" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="49.022" y="44.196" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-X3" gate="4" x="73.66" y="134.62" smashed="yes">
<attribute name="PART" x="71.882" y="136.144" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="71.882" y="133.604" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-X3" gate="7" x="99.06" y="134.62" smashed="yes">
<attribute name="PART" x="97.282" y="136.144" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="97.282" y="133.604" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-K1" gate="G$1" x="50.8" y="55.88" smashed="yes">
<attribute name="PART" x="46.99" y="58.42" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="46.99" y="55.88" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+R-K1" gate="G$2" x="17.78" y="63.5" smashed="yes">
<attribute name="PART" x="10.16" y="64.77" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-K1" gate="G$4" x="73.66" y="124.46" smashed="yes">
<attribute name="PART" x="71.12" y="125.73" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-K1" gate="G$5" x="76.2" y="78.74" smashed="yes">
<attribute name="PART" x="73.66" y="80.01" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-K1" gate="G$6" x="86.36" y="78.74" smashed="yes">
<attribute name="PART" x="83.82" y="80.01" size="1.778" layer="95" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="+R-X1" gate="9" pin="9.2"/>
<wire x1="50.8" y1="48.26" x2="50.8" y2="50.8" width="0.1524" layer="91"/>
<pinref part="+R-K1" gate="G$1" pin="A2"/>
</segment>
</net>
<net name="+-EXT" class="0">
<segment>
<pinref part="ESP1" gate="G$1" pin="+-EXT"/>
<pinref part="+R-X3" gate="7" pin="7.2"/>
<wire x1="35.56" y1="149.86" x2="50.8" y2="149.86" width="0.1524" layer="91"/>
<wire x1="50.8" y1="149.86" x2="73.66" y2="149.86" width="0.1524" layer="91"/>
<wire x1="73.66" y1="149.86" x2="99.06" y2="149.86" width="0.1524" layer="91"/>
<wire x1="99.06" y1="149.86" x2="99.06" y2="137.16" width="0.1524" layer="91"/>
<pinref part="+R-X3" gate="4" pin="4.2"/>
<wire x1="73.66" y1="137.16" x2="73.66" y2="149.86" width="0.1524" layer="91"/>
<junction x="73.66" y="149.86"/>
<pinref part="+T-X1" gate="5" pin="5.2"/>
<wire x1="50.8" y1="137.16" x2="50.8" y2="149.86" width="0.1524" layer="91"/>
<junction x="50.8" y="149.86"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="+T-X1" gate="5" pin="5.1"/>
<pinref part="S1" gate="G$1" pin="13"/>
<wire x1="50.8" y1="132.08" x2="50.8" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="+R-X3" gate="4" pin="4.1"/>
<wire x1="73.66" y1="132.08" x2="73.66" y2="129.54" width="0.1524" layer="91"/>
<pinref part="+R-K1" gate="G$4" pin="3"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="+R-X3" gate="7" pin="7.1"/>
<wire x1="99.06" y1="132.08" x2="99.06" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="+T-X2" gate="8" pin="8.2"/>
<wire x1="106.68" y1="111.76" x2="106.68" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="+T-X2" gate="8" pin="8.1"/>
<pinref part="P1" gate="G$1" pin="X1"/>
<wire x1="106.68" y1="63.5" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="+T-X1" gate="6" pin="6.1"/>
<pinref part="+T-X1" gate="1" pin="1.2"/>
<wire x1="50.8" y1="111.76" x2="50.8" y2="109.22" width="0.1524" layer="91"/>
<wire x1="50.8" y1="109.22" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
<wire x1="50.8" y1="109.22" x2="73.66" y2="109.22" width="0.1524" layer="91"/>
<wire x1="73.66" y1="109.22" x2="73.66" y2="111.76" width="0.1524" layer="91"/>
<junction x="50.8" y="109.22"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="+T-X1" gate="6" pin="6.2"/>
<pinref part="S1" gate="G$1" pin="14"/>
<wire x1="50.8" y1="116.84" x2="50.8" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="+T-X1" gate="1" pin="1.1"/>
<pinref part="S2" gate="G$1" pin="1"/>
<wire x1="50.8" y1="101.6" x2="50.8" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="2"/>
<pinref part="+T-X1" gate="2" pin="2.2"/>
<wire x1="50.8" y1="88.9" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="+T-X1" gate="2" pin="2.1"/>
<pinref part="+R-X1" gate="8" pin="8.2"/>
<wire x1="50.8" y1="81.28" x2="50.8" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="+R-X1" gate="8" pin="8.1"/>
<wire x1="50.8" y1="63.5" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
<pinref part="+R-K1" gate="G$1" pin="A1"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="X2"/>
<pinref part="+T-X2" gate="10" pin="10.2"/>
<wire x1="106.68" y1="50.8" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="0V-EXT" class="0">
<segment>
<pinref part="+T-X2" gate="10" pin="10.1"/>
<wire x1="106.68" y1="43.18" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<wire x1="106.68" y1="40.64" x2="50.8" y2="40.64" width="0.1524" layer="91"/>
<pinref part="+R-X1" gate="9" pin="9.1"/>
<wire x1="50.8" y1="40.64" x2="50.8" y2="43.18" width="0.1524" layer="91"/>
<wire x1="50.8" y1="40.64" x2="38.1" y2="40.64" width="0.1524" layer="91"/>
<wire x1="38.1" y1="40.64" x2="38.1" y2="144.78" width="0.1524" layer="91"/>
<junction x="50.8" y="40.64"/>
<pinref part="ESP1" gate="G$1" pin="0V-EXT"/>
<wire x1="38.1" y1="144.78" x2="35.56" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
