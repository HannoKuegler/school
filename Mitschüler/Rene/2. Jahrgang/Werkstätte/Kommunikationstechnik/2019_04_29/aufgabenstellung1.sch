<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="100" unitdist="mil" unit="mil" style="lines" multiple="1" display="yes" altdistance="25" altunitdist="mil" altunit="mil"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="no" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="no" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="no" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="no" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="no" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="rs-kommunikationstechnik">
<packages>
<package name="PATCHPANEL">
<pad name="P$1" x="-2.54" y="0" drill="0.6" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.6" shape="octagon"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
</package>
<package name="DATENDOSE">
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="0.6" shape="octagon"/>
</package>
</packages>
<symbols>
<symbol name="PATCHPANEL">
<pin name="P$1" x="7.62" y="0" length="short" rot="R180"/>
<pin name="P$2" x="-7.62" y="0" length="short"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<rectangle x1="-2.54" y1="-2.54" x2="2.54" y2="2.54" layer="94"/>
</symbol>
<symbol name="DATENDOSE">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<rectangle x1="-2.54" y1="-2.54" x2="2.54" y2="2.54" layer="94"/>
<pin name="P$1" x="-7.62" y="0" length="short"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PATCHPANEL">
<gates>
<gate name="G$1" symbol="PATCHPANEL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PATCHPANEL">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DATENDOSE">
<gates>
<gate name="G$1" symbol="DATENDOSE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DATENDOSE">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="rs-kommunikationstechnik" deviceset="PATCHPANEL" device=""/>
<part name="U$2" library="rs-kommunikationstechnik" deviceset="PATCHPANEL" device=""/>
<part name="U$3" library="rs-kommunikationstechnik" deviceset="PATCHPANEL" device=""/>
<part name="U$4" library="rs-kommunikationstechnik" deviceset="PATCHPANEL" device=""/>
<part name="U$5" library="rs-kommunikationstechnik" deviceset="PATCHPANEL" device=""/>
<part name="U$6" library="rs-kommunikationstechnik" deviceset="PATCHPANEL" device=""/>
<part name="U$7" library="rs-kommunikationstechnik" deviceset="PATCHPANEL" device=""/>
<part name="U$8" library="rs-kommunikationstechnik" deviceset="PATCHPANEL" device=""/>
<part name="U$10" library="rs-kommunikationstechnik" deviceset="DATENDOSE" device=""/>
<part name="U$9" library="rs-kommunikationstechnik" deviceset="PATCHPANEL" device=""/>
<part name="U$11" library="rs-kommunikationstechnik" deviceset="PATCHPANEL" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<polygon width="0.1524" layer="91">
<vertex x="-12.7" y="5.08"/>
<vertex x="-7.62" y="5.715"/>
<vertex x="-7.62" y="4.445"/>
</polygon>
<text x="-20.32" y="5.08" size="1.778" layer="91">Serverraum
DA1</text>
<text x="1.905" y="10.795" size="1.778" layer="91">PPL/1</text>
<text x="22.225" y="10.795" size="1.778" layer="91">PPK/7</text>
<text x="42.545" y="10.795" size="1.778" layer="91">PPJ/7</text>
<text x="62.865" y="10.795" size="1.778" layer="91">PPI/7</text>
<text x="83.185" y="10.795" size="1.778" layer="91">PPH/7</text>
<text x="103.505" y="10.795" size="1.778" layer="91">PPG/7</text>
<text x="123.825" y="10.795" size="1.778" layer="91">PPF/7</text>
<text x="144.145" y="10.795" size="1.778" layer="91">PPE/7</text>
<text x="164.465" y="10.795" size="1.778" layer="91">PPD/7</text>
<text x="184.785" y="10.795" size="1.778" layer="91">PPB/5</text>
<text x="205.105" y="10.795" size="1.778" layer="91">DDB/5</text>
<text x="-7.62" y="0" size="1.778" layer="91">LAN
CAT5e</text>
<text x="33.02" y="0" size="1.778" layer="91">LAN
CAT5e</text>
<text x="73.66" y="0" size="1.778" layer="91">LAN
CAT5e</text>
<text x="114.3" y="0" size="1.778" layer="91">LAN
CAT5e</text>
<text x="154.94" y="0" size="1.778" layer="91">LAN
CAT5e</text>
<text x="195.58" y="0" size="1.778" layer="91">LAN
CAT5e</text>
<text x="12.7" y="0" size="1.778" layer="91">CAT3</text>
<text x="53.34" y="0" size="1.778" layer="91">CAT3</text>
<text x="93.98" y="0" size="1.778" layer="91">CAT3</text>
<text x="134.62" y="0" size="1.778" layer="91">CAT3</text>
<text x="175.26" y="0" size="1.778" layer="91">CAT3</text>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="5.08" y="5.08" smashed="yes"/>
<instance part="U$2" gate="G$1" x="25.4" y="5.08" smashed="yes"/>
<instance part="U$3" gate="G$1" x="45.72" y="5.08" smashed="yes"/>
<instance part="U$4" gate="G$1" x="66.04" y="5.08" smashed="yes"/>
<instance part="U$5" gate="G$1" x="86.36" y="5.08" smashed="yes"/>
<instance part="U$6" gate="G$1" x="106.68" y="5.08" smashed="yes"/>
<instance part="U$7" gate="G$1" x="127" y="5.08" smashed="yes"/>
<instance part="U$8" gate="G$1" x="147.32" y="5.08" smashed="yes"/>
<instance part="U$10" gate="G$1" x="208.28" y="5.08" smashed="yes"/>
<instance part="U$9" gate="G$1" x="167.64" y="5.08" smashed="yes"/>
<instance part="U$11" gate="G$1" x="187.96" y="5.08" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="P$2"/>
<pinref part="U$6" gate="G$1" pin="P$1"/>
<wire x1="119.38" y1="5.08" x2="114.3" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="P$2"/>
<pinref part="U$4" gate="G$1" pin="P$1"/>
<wire x1="78.74" y1="5.08" x2="73.66" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="P$2"/>
<pinref part="U$2" gate="G$1" pin="P$1"/>
<wire x1="38.1" y1="5.08" x2="33.02" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="P$2"/>
<pinref part="U$1" gate="G$1" pin="P$1"/>
<wire x1="17.78" y1="5.08" x2="12.7" y2="5.08" width="0.1524" layer="91" curve="-180"/>
<pinref part="U$4" gate="G$1" pin="P$2"/>
<wire x1="53.34" y1="5.08" x2="58.42" y2="5.08" width="0.1524" layer="91" curve="180"/>
<pinref part="U$3" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="P$1"/>
<pinref part="U$6" gate="G$1" pin="P$2"/>
<wire x1="93.98" y1="5.08" x2="99.06" y2="5.08" width="0.1524" layer="91" curve="180"/>
<pinref part="U$8" gate="G$1" pin="P$2"/>
<wire x1="134.62" y1="5.08" x2="139.7" y2="5.08" width="0.1524" layer="91" curve="180"/>
<pinref part="U$7" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$2"/>
<wire x1="-2.54" y1="5.08" x2="-12.7" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="P$1"/>
<pinref part="U$9" gate="G$1" pin="P$2"/>
<wire x1="154.94" y1="5.08" x2="160.02" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="P$1"/>
<pinref part="U$10" gate="G$1" pin="P$1"/>
<wire x1="195.58" y1="5.08" x2="200.66" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$9" gate="G$1" pin="P$1"/>
<pinref part="U$11" gate="G$1" pin="P$2"/>
<wire x1="175.26" y1="5.08" x2="180.34" y2="5.08" width="0.1524" layer="91" curve="180"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
