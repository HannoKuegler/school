<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="2" unitdist="mm" unit="mm" style="lines" multiple="1" display="yes" altdistance="0.001" altunitdist="mm" altunit="mm"/>
<layers>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
</parts>
<sheets>
<sheet>
<plain>
<polygon width="0.1524" layer="91">
<vertex x="100" y="66"/>
<vertex x="97" y="65"/>
<vertex x="100" y="64"/>
</polygon>
<text x="102" y="64" size="1.778" layer="91">Triggerlevel</text>
<polygon width="0.1524" layer="91">
<vertex x="51" y="100"/>
<vertex x="50" y="97"/>
<vertex x="49" y="100"/>
</polygon>
<text x="45" y="102" size="1.778" layer="91">Ursprung</text>
<text x="50" y="-3" size="1.778" layer="91">2ms</text>
<circle x="48.167" y="-2.372" radius="1.4142125" width="0.1524" layer="91"/>
<text x="47.169" y="-3.184" size="1.778" layer="91">M</text>
<circle x="0" y="-3" radius="1.4142125" width="0.1524" layer="91"/>
<text x="-0.717" y="-3.803" size="1.778" layer="91">1</text>
<text x="2" y="-4" size="1.778" layer="91">⚎ 5V</text>
<polygon width="0.1524" layer="91">
<vertex x="0" y="50"/>
<vertex x="-3" y="51"/>
<vertex x="-3" y="49"/>
</polygon>
<text x="-12" y="49" size="1.778" layer="91">Nullinie</text>
<polygon width="0.1524" layer="91">
<vertex x="-3" y="-2"/>
<vertex x="-4" y="3"/>
<vertex x="-6" y="2"/>
</polygon>
<text x="-18" y="9" size="1.778" layer="91">Channel 1</text>
<polygon width="0.1524" layer="91">
<vertex x="53" y="66"/>
<vertex x="54" y="69"/>
<vertex x="55" y="68"/>
</polygon>
<text x="77" y="108" size="1.778" layer="91">Triggerpunkt</text>
<polygon width="0.1524" layer="91">
<vertex x="7" y="-5"/>
<vertex x="6" y="-9"/>
<vertex x="8" y="-9"/>
</polygon>
<text x="9" y="-14" size="1.778" layer="91">VOLT/DIV</text>
<polygon width="0.1524" layer="91">
<vertex x="2" y="-5"/>
<vertex x="-3" y="-7"/>
<vertex x="-1" y="-9"/>
</polygon>
<text x="-21" y="-11" size="1.778" layer="91">DC Kopplung</text>
<text x="59" y="-13" size="1.778" layer="91">TIME/DIV</text>
<polygon width="0.1524" layer="91">
<vertex x="52" y="-9"/>
<vertex x="55" y="-8"/>
<vertex x="52" y="-4"/>
</polygon>
<text x="81" y="-4" size="1.778" layer="91">Triggerbedingung</text>
<text x="85" y="-9" size="1.778" layer="91">Frequenz</text>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<wire x1="100" y1="90" x2="0" y2="90" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="100" y1="10" x2="0" y2="10" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="0" y1="20" x2="100" y2="20" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="100" y1="30" x2="0" y2="30" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="0" y1="40" x2="100" y2="40" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="0" y1="50" x2="20" y2="50" width="0.6096" layer="91"/>
<wire x1="20" y1="50" x2="40" y2="50" width="0.6096" layer="91"/>
<wire x1="40" y1="50" x2="60" y2="50" width="0.6096" layer="91"/>
<wire x1="60" y1="50" x2="80" y2="50" width="0.6096" layer="91"/>
<wire x1="80" y1="50" x2="100" y2="50" width="0.6096" layer="91"/>
<wire x1="10" y1="0" x2="10" y2="70" width="0.1524" layer="91"/>
<wire x1="10" y1="70" x2="10" y2="100" width="0.1524" layer="91"/>
<wire x1="0" y1="50" x2="10" y2="70" width="0.254" layer="91"/>
<wire x1="20" y1="100" x2="20" y2="50" width="0.1524" layer="91"/>
<wire x1="20" y1="50" x2="20" y2="0" width="0.1524" layer="91"/>
<wire x1="10" y1="70" x2="20" y2="50" width="0.254" layer="91"/>
<wire x1="30" y1="0" x2="30" y2="30" width="0.1524" layer="91"/>
<wire x1="30" y1="30" x2="30" y2="100" width="0.1524" layer="91"/>
<wire x1="20" y1="50" x2="30" y2="30" width="0.254" layer="91"/>
<wire x1="40" y1="100" x2="40" y2="50" width="0.1524" layer="91"/>
<wire x1="40" y1="50" x2="40" y2="0" width="0.1524" layer="91"/>
<wire x1="30" y1="30" x2="40" y2="50" width="0.254" layer="91"/>
<wire x1="50" y1="100" x2="50" y2="70" width="0.8128" layer="91"/>
<wire x1="50" y1="70" x2="50" y2="0" width="0.8128" layer="91"/>
<wire x1="40" y1="50" x2="48" y2="66" width="0.254" layer="91"/>
<wire x1="48" y1="66" x2="50" y2="70" width="0.254" layer="91"/>
<wire x1="60" y1="0" x2="60" y2="50" width="0.1524" layer="91"/>
<wire x1="60" y1="50" x2="60" y2="100" width="0.1524" layer="91"/>
<wire x1="50" y1="70" x2="52" y2="66" width="0.254" layer="91"/>
<wire x1="52" y1="66" x2="60" y2="50" width="0.254" layer="91"/>
<wire x1="70" y1="100" x2="70" y2="30" width="0.1524" layer="91"/>
<wire x1="70" y1="30" x2="70" y2="0" width="0.1524" layer="91"/>
<wire x1="60" y1="50" x2="70" y2="30" width="0.254" layer="91"/>
<wire x1="80" y1="0" x2="80" y2="50" width="0.1524" layer="91"/>
<wire x1="80" y1="50" x2="80" y2="100" width="0.1524" layer="91"/>
<wire x1="70" y1="30" x2="80" y2="50" width="0.254" layer="91"/>
<wire x1="90" y1="100" x2="90" y2="70" width="0.1524" layer="91"/>
<wire x1="90" y1="70" x2="90" y2="0" width="0.1524" layer="91"/>
<wire x1="80" y1="50" x2="90" y2="70" width="0.254" layer="91"/>
<wire x1="0" y1="100" x2="100" y2="100" width="1.27" layer="91"/>
<wire x1="100" y1="100" x2="100" y2="65" width="1.27" layer="91"/>
<wire x1="100" y1="65" x2="100" y2="50" width="1.27" layer="91"/>
<wire x1="100" y1="50" x2="100" y2="0" width="1.27" layer="91"/>
<wire x1="100" y1="0" x2="0" y2="0" width="1.27" layer="91"/>
<wire x1="0" y1="0" x2="0" y2="100" width="1.27" layer="91"/>
<wire x1="90" y1="70" x2="100" y2="50" width="0.254" layer="91"/>
<wire x1="52.578" y1="65.024" x2="100" y2="65" width="0.1524" layer="91" style="shortdash"/>
<wire x1="48" y1="66" x2="52" y2="66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="0" y1="60" x2="100" y2="60" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="100" y1="70" x2="0" y2="70" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="0" y1="80" x2="100" y2="80" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="-4.951" y1="2.608" x2="-7" y2="8" width="0.1524" layer="91"/>
<wire x1="-7" y1="8" x2="-18" y2="8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="52.616" y1="65.057" x2="52.516" y2="65.057" width="1.016" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="54.509" y1="68.565" x2="76" y2="107" width="0.1524" layer="91"/>
<wire x1="76" y1="107" x2="91" y2="107" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="7" y1="-9" x2="7" y2="-15" width="0.1524" layer="91"/>
<wire x1="7" y1="-15" x2="20" y2="-15" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="-2" y1="-8" x2="-6" y2="-12" width="0.1524" layer="91"/>
<wire x1="-6" y1="-12" x2="-22" y2="-12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="53.594" y1="-8.445" x2="56" y2="-14" width="0.1524" layer="91"/>
<wire x1="56" y1="-14" x2="71" y2="-14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="101" y1="-1" x2="80" y2="-1" width="0.1524" layer="91" style="shortdash"/>
<wire x1="80" y1="-1" x2="80" y2="-5" width="0.1524" layer="91" style="shortdash"/>
<wire x1="80" y1="-5" x2="101" y2="-5" width="0.1524" layer="91" style="shortdash"/>
<wire x1="101" y1="-5" x2="101" y2="-1" width="0.1524" layer="91" style="shortdash"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="80" y1="-6" x2="101" y2="-6" width="0.1524" layer="91" style="shortdash"/>
<wire x1="101" y1="-6" x2="101" y2="-10" width="0.1524" layer="91" style="shortdash"/>
<wire x1="101" y1="-10" x2="80" y2="-10" width="0.1524" layer="91" style="shortdash"/>
<wire x1="80" y1="-10" x2="80" y2="-6" width="0.1524" layer="91" style="shortdash"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="2" y1="52" x2="2" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="4" y1="52" x2="4" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="6" y1="52" x2="6" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="8" y1="52" x2="8" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="12" y1="52" x2="12" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="14" y1="52" x2="14" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="16" y1="52" x2="16" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="18" y1="52" x2="18" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="22" y1="52" x2="22" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="24" y1="52" x2="24" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<wire x1="26" y1="52" x2="26" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<wire x1="28" y1="52" x2="28" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<wire x1="32" y1="52" x2="32" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="34" y1="52" x2="34" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<wire x1="36" y1="52" x2="36" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="38" y1="52" x2="38" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<wire x1="42" y1="52" x2="42" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<wire x1="44" y1="48" x2="44" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<wire x1="46" y1="52" x2="46" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<wire x1="48" y1="48" x2="48" y2="52" width="0.1524" layer="91"/>
<wire x1="52" y1="52" x2="52" y2="48" width="0.1524" layer="91"/>
<wire x1="48" y1="48" x2="52" y2="48" width="0.1524" layer="91"/>
<wire x1="48" y1="52" x2="52" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<wire x1="54" y1="48" x2="54" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<wire x1="56" y1="52" x2="56" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<wire x1="58" y1="48" x2="58" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<wire x1="62" y1="48" x2="62" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<wire x1="64" y1="52" x2="64" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<wire x1="66" y1="48" x2="66" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<wire x1="68" y1="52" x2="68" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<wire x1="72" y1="48" x2="72" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<wire x1="74" y1="52" x2="74" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<wire x1="76" y1="48" x2="76" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<wire x1="78" y1="52" x2="78" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<wire x1="82" y1="48" x2="82" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<wire x1="84" y1="52" x2="84" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<wire x1="86" y1="48" x2="86" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<wire x1="88" y1="52" x2="88" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<wire x1="92" y1="48" x2="92" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="94" y1="52" x2="94" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<wire x1="96" y1="48" x2="96" y2="52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<wire x1="98" y1="52" x2="98" y2="48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<wire x1="48" y1="54" x2="52" y2="54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<wire x1="48" y1="56" x2="52" y2="56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<wire x1="48" y1="58" x2="52" y2="58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<wire x1="48" y1="62" x2="52" y2="62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<wire x1="48" y1="64" x2="52" y2="64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<wire x1="48" y1="68" x2="52" y2="68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<wire x1="48" y1="72" x2="52" y2="72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<wire x1="48" y1="74" x2="52" y2="74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<wire x1="48" y1="76" x2="52" y2="76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<wire x1="48" y1="78" x2="52" y2="78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<wire x1="48" y1="82" x2="52" y2="82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<wire x1="48" y1="84" x2="52" y2="84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<wire x1="48" y1="86" x2="52" y2="86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<wire x1="48" y1="88" x2="52" y2="88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<wire x1="48" y1="92" x2="52" y2="92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<wire x1="48" y1="94" x2="52" y2="94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<wire x1="48" y1="96" x2="52" y2="96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<wire x1="48" y1="98" x2="52" y2="98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<wire x1="48" y1="46" x2="52" y2="46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<wire x1="48" y1="44" x2="52" y2="44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<wire x1="48" y1="42" x2="52" y2="42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<wire x1="48" y1="38" x2="52" y2="38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<wire x1="48" y1="36" x2="52" y2="36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<wire x1="48" y1="34" x2="52" y2="34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<wire x1="48" y1="32" x2="52" y2="32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<wire x1="48" y1="28" x2="52" y2="28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<wire x1="48" y1="26" x2="52" y2="26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<wire x1="48" y1="24" x2="52" y2="24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<wire x1="48" y1="22" x2="52" y2="22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<wire x1="48" y1="18" x2="52" y2="18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<wire x1="48" y1="16" x2="52" y2="16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<wire x1="48" y1="14" x2="52" y2="14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<wire x1="48" y1="12" x2="52" y2="12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<wire x1="48" y1="8" x2="52" y2="8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<wire x1="48" y1="6" x2="52" y2="6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<wire x1="48" y1="4" x2="52" y2="4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<wire x1="48" y1="2" x2="52" y2="2" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
