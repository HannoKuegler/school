<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="1" unitdist="mm" unit="mm" style="lines" multiple="1" display="yes" altdistance="0.001" altunitdist="mm" altunit="mm"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="9" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-wago" urn="urn:adsk.eagle:library:197">
<description>&lt;b&gt;Wago Connectors&lt;/b&gt;&lt;p&gt;
Types:&lt;p&gt;
 233&lt;p&gt;
 234&lt;p&gt;
 255 - 5.0; 5.08; 7.5; 7.62, 10.0; 10.16 mm&lt;p&gt;
 254&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="255-401-5" library_version="1">
<description>&lt;b&gt;WAGO Cage Clamp grid 5.0 mm&lt;/b&gt;</description>
<wire x1="-6.05" y1="6.05" x2="-5.7" y2="6.05" width="0.2032" layer="21"/>
<wire x1="-5.7" y1="6.05" x2="-5.7" y2="-0.55" width="0.2032" layer="21"/>
<wire x1="-5.7" y1="-0.55" x2="-6.05" y2="-0.55" width="0.2032" layer="21"/>
<wire x1="-6.05" y1="-0.55" x2="-6.05" y2="-2.35" width="0.2032" layer="21"/>
<wire x1="-6.05" y1="-2.35" x2="-5.7" y2="-2.35" width="0.2032" layer="21"/>
<wire x1="-5.7" y1="-2.35" x2="-5.7" y2="-4.35" width="0.2032" layer="21"/>
<wire x1="-5.7" y1="-4.35" x2="-6.05" y2="-4.35" width="0.2032" layer="21"/>
<wire x1="-6.05" y1="-4.35" x2="-6.05" y2="-5.35" width="0.2032" layer="21"/>
<wire x1="-6.05" y1="-5.35" x2="-5.7" y2="-5.35" width="0.2032" layer="21"/>
<wire x1="-5.7" y1="-5.35" x2="-5.7" y2="-6.2" width="0.2032" layer="21"/>
<wire x1="-5.7" y1="-6.2" x2="-5.05" y2="-6.2" width="0.2032" layer="21"/>
<wire x1="-5.05" y1="-6.2" x2="-4.85" y2="-6.2" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="-6.2" x2="-0.1" y2="-6.2" width="0.2032" layer="51"/>
<wire x1="-0.1" y1="-6.2" x2="1.65" y2="-6.2" width="0.2032" layer="21"/>
<wire x1="1.65" y1="-6.2" x2="1.65" y2="-5.35" width="0.2032" layer="21"/>
<wire x1="1.65" y1="-4.35" x2="1.65" y2="-3.65" width="0.2032" layer="21"/>
<wire x1="1.65" y1="-3.65" x2="1.65" y2="-2.35" width="0.2032" layer="21"/>
<wire x1="1.65" y1="-0.55" x2="1.65" y2="6.05" width="0.2032" layer="21"/>
<wire x1="-5.7" y1="-3.65" x2="-5" y2="-3.65" width="0.2032" layer="21"/>
<wire x1="-5" y1="-3.65" x2="-0.1" y2="-3.65" width="0.2032" layer="51"/>
<wire x1="-0.05" y1="-3.65" x2="1.65" y2="-3.65" width="0.2032" layer="21"/>
<wire x1="-3.45" y1="0.75" x2="-3.45" y2="-3.05" width="0.2032" layer="51"/>
<wire x1="-3.45" y1="-3.05" x2="-1.5" y2="-3.05" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="-3.05" x2="-1.5" y2="0.75" width="0.2032" layer="51"/>
<wire x1="-1.5" y1="0.75" x2="-3.45" y2="0.75" width="0.2032" layer="51"/>
<wire x1="1.3" y1="6.05" x2="1.65" y2="6.05" width="0.2032" layer="21"/>
<wire x1="1.65" y1="-0.55" x2="1.3" y2="-0.55" width="0.2032" layer="21"/>
<wire x1="1.3" y1="-0.55" x2="1.3" y2="-2.35" width="0.2032" layer="21"/>
<wire x1="1.3" y1="-2.35" x2="1.65" y2="-2.35" width="0.2032" layer="21"/>
<wire x1="1.65" y1="-4.35" x2="1.3" y2="-4.35" width="0.2032" layer="21"/>
<wire x1="1.3" y1="-4.35" x2="1.3" y2="-5.35" width="0.2032" layer="21"/>
<wire x1="1.3" y1="-5.35" x2="1.65" y2="-5.35" width="0.2032" layer="21"/>
<wire x1="-5.7" y1="7.575" x2="-5.7" y2="7.05" width="0.2032" layer="21"/>
<wire x1="-5.7" y1="7.05" x2="-6.05" y2="7.05" width="0.2032" layer="21"/>
<wire x1="-6.05" y1="7.05" x2="-6.05" y2="6.05" width="0.2032" layer="21"/>
<wire x1="1.65" y1="7.05" x2="1.65" y2="7.55" width="0.2032" layer="21"/>
<wire x1="-4.85" y1="7.55" x2="-4.85" y2="-6.2" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="11.85" x2="-4.8" y2="9.35" width="0.2032" layer="21"/>
<wire x1="-4.8" y1="9.35" x2="-3.1" y2="9.35" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="9.35" x2="-1.9" y2="9.35" width="0.2032" layer="21"/>
<wire x1="-1.9" y1="9.35" x2="-0.2" y2="9.35" width="0.2032" layer="21"/>
<wire x1="-0.2" y1="9.35" x2="-0.2" y2="11.85" width="0.2032" layer="21"/>
<wire x1="-0.2" y1="11.85" x2="-4.8" y2="11.85" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="7.55" x2="-0.1" y2="2.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="2.65" x2="-0.1" y2="-6.2" width="0.2032" layer="21"/>
<wire x1="1.65" y1="7.05" x2="1.3" y2="7.05" width="0.2032" layer="21"/>
<wire x1="1.3" y1="7.05" x2="1.3" y2="6.05" width="0.2032" layer="21"/>
<wire x1="-3.85" y1="11.35" x2="-3.85" y2="9.85" width="0.2032" layer="21"/>
<wire x1="-3.85" y1="9.85" x2="-1.15" y2="9.85" width="0.2032" layer="21"/>
<wire x1="-1.15" y1="9.85" x2="-1.15" y2="11.35" width="0.2032" layer="21"/>
<wire x1="-1.15" y1="11.35" x2="-3.85" y2="11.35" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="9.35" x2="-3.1" y2="6.7" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="6.7" x2="-1.9" y2="6.7" width="0.2032" layer="21"/>
<wire x1="-1.9" y1="6.7" x2="-1.9" y2="9.35" width="0.2032" layer="21"/>
<wire x1="-5.7" y1="7.575" x2="-3.125" y2="7.575" width="0.2032" layer="21"/>
<wire x1="-1.875" y1="7.575" x2="1.625" y2="7.575" width="0.2032" layer="21"/>
<wire x1="0.1" y1="7.55" x2="0.1" y2="-6.2" width="0.2032" layer="21"/>
<wire x1="-5.05" y1="7.55" x2="-5.05" y2="-6.2" width="0.2032" layer="21"/>
<wire x1="-5.65" y1="2.65" x2="-0.1" y2="2.65" width="0.2032" layer="21"/>
<wire x1="-0.1" y1="2.65" x2="1.6" y2="2.65" width="0.2032" layer="21"/>
<pad name="A1" x="-2.5" y="0" drill="1.2" shape="long" rot="R90"/>
<pad name="B1" x="-2.5" y="-5" drill="1.2" shape="long" rot="R90"/>
<text x="-2.75" y="1.35" size="0.8128" layer="21">1</text>
<text x="-6.535" y="-5.855" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.9" y="-5.75" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="KL-1V" library_version="1">
<wire x1="0" y1="0.254" x2="0" y2="-0.254" width="0.254" layer="94"/>
<wire x1="0" y1="-0.254" x2="1.524" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.254" x2="1.524" y2="0.254" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.254" x2="0" y2="0.254" width="0.254" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-3.556" y="1.524" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="B1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WAGO1" prefix="X" library_version="1">
<description>&lt;b&gt;WAGO Cage Clamp&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL-1V" x="0" y="0"/>
</gates>
<devices>
<device name="255-401-5" package="255-401-5">
<connects>
<connect gate="-1" pin="A1" pad="A1"/>
<connect gate="-1" pin="B1" pad="B1"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="255-401" constant="no"/>
<attribute name="OC_FARNELL" value="4015319" constant="no"/>
<attribute name="OC_NEWARK" value="28K9763" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-wago-500" urn="urn:adsk.eagle:library:195">
<description>&lt;b&gt;Wago Screw Clamps&lt;/b&gt;&lt;p&gt;
Grid 5.00 mm&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="W237-112" urn="urn:adsk.eagle:footprint:10674/1" library_version="1">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<wire x1="1.488" y1="-2.261" x2="3.469" y2="-0.254" width="0.254" layer="51"/>
<wire x1="-4.989" y1="-5.461" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-5.461" x2="-4.989" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="4.993" y1="-3.073" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="1.615" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="1.615" y1="-3.073" x2="3.393" y2="-3.073" width="0.1524" layer="51"/>
<wire x1="3.393" y1="-3.073" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<circle x="-2.5" y="-1.27" radius="1.4986" width="0.1524" layer="21"/>
<circle x="-2.5" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<circle x="2.5038" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="2.5038" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<text x="-5.04" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.8462" y="-4.9784" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.532" y="0.635" size="1.27" layer="51" ratio="10">1</text>
<text x="0.421" y="0.635" size="1.27" layer="51" ratio="10">2</text>
</package>
</packages>
<packages3d>
<package3d name="W237-112" urn="urn:adsk.eagle:package:10689/1" type="box" library_version="1">
<description>WAGO SCREW CLAMP</description>
<packageinstances>
<packageinstance name="W237-112"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="KL+V" urn="urn:adsk.eagle:symbol:10673/1" library_version="1">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-3.683" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="W237-112" urn="urn:adsk.eagle:component:10699/1" prefix="X" uservalue="yes" library_version="1">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL+V" x="0" y="0"/>
</gates>
<devices>
<device name="" package="W237-112">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10689/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="237-112" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="76R2974" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="docu-dummy" urn="urn:adsk.eagle:library:215">
<description>Dummy symbols</description>
<packages>
</packages>
<symbols>
<symbol name="CAPACITOR" urn="urn:adsk.eagle:symbol:13164/1" library_version="1">
<wire x1="0" y1="-5.08" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.508" x2="0" y2="2.54" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C" urn="urn:adsk.eagle:component:13175/1" prefix="C" library_version="1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="X1" library="con-wago" library_urn="urn:adsk.eagle:library:197" deviceset="WAGO1" device="255-401-5"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="X2" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-112" device="" package3d_urn="urn:adsk.eagle:package:10689/1"/>
<part name="X3" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-112" device="" package3d_urn="urn:adsk.eagle:package:10689/1"/>
<part name="X4" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-112" device="" package3d_urn="urn:adsk.eagle:package:10689/1"/>
<part name="X5" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-112" device="" package3d_urn="urn:adsk.eagle:package:10689/1"/>
<part name="C1" library="docu-dummy" library_urn="urn:adsk.eagle:library:215" deviceset="C" device=""/>
<part name="X6" library="con-wago-500" library_urn="urn:adsk.eagle:library:195" deviceset="W237-112" device="" package3d_urn="urn:adsk.eagle:package:10689/1"/>
</parts>
<sheets>
<sheet>
<plain>
<polygon width="0.1524" layer="91">
<vertex x="71" y="60.993"/>
<vertex x="71" y="61.95"/>
<vertex x="71.978" y="61.449"/>
<vertex x="71.005" y="60.999"/>
</polygon>
<polygon width="0.1524" layer="91">
<vertex x="58" y="63"/>
<vertex x="58" y="60"/>
<vertex x="61.055" y="61.544"/>
</polygon>
<circle x="56.116" y="73.003" radius="1" width="0.1524" layer="91"/>
<circle x="62.598" y="72.974" radius="1" width="0.1524" layer="91"/>
<text x="50.034" y="68.087" size="1.778" layer="91" rot="R270">Y-Verstärker</text>
<text x="23" y="69" size="1.778" layer="91">AC</text>
<text x="23" y="63" size="1.778" layer="91">DC</text>
<text x="22" y="57" size="1.778" layer="91">GND</text>
<text x="43" y="72" size="1.778" layer="91">VOLT/DIV</text>
<text x="65" y="72" size="1.778" layer="91">INV</text>
<text x="-8" y="54" size="1.778" layer="91">BNC-Buchse</text>
<text x="-5" y="68" size="1.778" layer="91">Eingang</text>
<text x="-7" y="65" size="1.778" layer="91">1MΩ / 20pF</text>
<text x="18" y="76" size="1.778" layer="91">AC/DC Schalter</text>
<text x="67" y="63" size="1.778" layer="91">Sample &amp; Hold bei
Digitaloszilloskop</text>
<text x="67" y="56" size="1.778" layer="91">Endstufe bei
Analogoszilloskop</text>
</plain>
<instances>
<instance part="X1" gate="-1" x="6.604" y="61.468" smashed="yes" rot="R180"/>
<instance part="GND1" gate="1" x="6.096" y="56.896" smashed="yes"/>
<instance part="GND2" gate="1" x="59.374" y="52.357" smashed="yes"/>
<instance part="GND3" gate="1" x="15.498" y="52.454" smashed="yes"/>
<instance part="X2" gate="-1" x="16.734" y="61.456" smashed="yes" rot="R180">
<attribute name="VALUE" x="19.274" y="65.139" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="X3" gate="-1" x="25.987" y="67.012" smashed="yes" rot="R180">
<attribute name="VALUE" x="28.527" y="70.695" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="X4" gate="-1" x="26.056" y="61.433" smashed="yes" rot="R180">
<attribute name="VALUE" x="28.596" y="65.116" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="X5" gate="-1" x="26.259" y="55.488" smashed="yes" rot="R180">
<attribute name="VALUE" x="28.799" y="59.171" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="18.092" y="67.011" smashed="yes" rot="R90"/>
<instance part="X6" gate="-1" x="36.007" y="61.4" smashed="yes">
<attribute name="VALUE" x="33.467" y="57.717" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<wire x1="5" y1="64" x2="5" y2="59" width="0.1524" layer="91" curve="-157.380135"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="15.545" y1="62.654" x2="15.545" y2="67" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="16.721" y1="61.449" x2="20.957" y2="61.449" width="0.1524" layer="91"/>
<wire x1="20.957" y1="61.449" x2="20.957" y2="61.447" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="X5" gate="-1" pin="KL"/>
<wire x1="15.498" y1="54.994" x2="15.498" y2="55.488" width="0.1524" layer="91"/>
<wire x1="15.498" y1="55.488" x2="21.179" y2="55.488" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="35.968" y1="61.386" x2="35.968" y2="61.41" width="0.1524" layer="91"/>
<wire x1="35.968" y1="61.41" x2="26.044" y2="61.41" width="0.1524" layer="91" style="shortdash"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="36.016" y1="61.434" x2="26.068" y2="67.031" width="0.1524" layer="91" style="shortdash"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="35.897" y1="61.481" x2="26.283" y2="55.477" width="0.1524" layer="91" style="shortdash"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="X6" gate="-1" pin="KL"/>
<wire x1="41.087" y1="61.4" x2="52.667" y2="61.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="52.607" y1="61.402" x2="52.607" y2="68.091" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="52.596" y1="61.489" x2="52.596" y2="55.071" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="65.631" y1="61.373" x2="65.631" y2="68.062" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="65.649" y1="61.402" x2="65.649" y2="54.984" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="59.287" y1="55.024" x2="52.598" y2="55.024" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="59.2" y1="55.013" x2="65.618" y2="55.013" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="59.287" y1="68.143" x2="52.598" y2="68.143" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="59.2" y1="68.132" x2="65.618" y2="68.132" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="65.63" y1="61.429" x2="70.951" y2="61.429" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="72.051" y1="61.46" x2="75.948" y2="61.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="56.057" y1="68.156" x2="56.081" y2="71.936" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="62.595" y1="68.13" x2="62.619" y2="71.91" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="55.449" y1="72.366" x2="56.761" y2="73.629" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="61.875" y1="72.483" x2="63.32" y2="73.579" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="12" y1="75" x2="12" y2="48" width="0.1524" layer="91" style="longdash"/>
<wire x1="12" y1="48" x2="40" y2="48" width="0.1524" layer="91" style="longdash"/>
<wire x1="40" y1="48" x2="40" y2="75" width="0.1524" layer="91" style="longdash"/>
<wire x1="40" y1="75" x2="12" y2="75" width="0.1524" layer="91" style="longdash"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
