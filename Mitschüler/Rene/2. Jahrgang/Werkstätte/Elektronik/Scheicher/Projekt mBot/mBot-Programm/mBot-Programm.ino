/*
   mBot Programm

   v2.0
   © 2019 René Schaar 2BHEL
          rene.schaar@htl-klu.at
*/

// Libraries
#include <Servo.h>

// Pin setting
const int OPTO1 = A0;
const int PIN_BUCHSE_6 = A0;
const int OPTO2 = A1;
const int OPTO3 = A2;
const int PIN_BUCHSE_5 = A4;
const int PIN_BUCHSE_3 = A5;
const int BUMPER_RECHTS = 2;
const int BUMPER_LINKS = 3;
const int LED_BLINK_RECHTS = 4;
const int LED_FRONT = 5;
const int OPTOS = 6;
const int LED_HINTEN = 7;
const int LED_BLINK_LINKS = 8;
const int SERVO_RECHTS = 9;
const int PIN_BUCHSE_4 = 9;
const int SERVO_LINKS = 10;
const int BUZZER = 11;

// Servo Setup
Servo servo_links;
Servo servo_rechts;
// Nullpunkte der Servos
int SERVO_RECHTS_NULL = 1480;
int SERVO_LINKS_NULL = 1450;
int lastSpeed;

// Musikalisches Setup
int bpm = 120;
int frequenz;
int dauer = 50;
int pause = 0;
int finalDelay;
int bogenDauer;
boolean punkt = false, bogen = false;

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void setup() {
  Serial.begin(9600);
  Serial.println("mBot Program v2.0 © 2019 René Schaar 2BHEL");
  Serial.println("------------------------------------------");

  // Pin Mode Setup
  Serial.println("Pin Mode Setup...");
  pinMode(OPTOS, OUTPUT);
  pinMode(OPTO1, INPUT);
  pinMode(OPTO2, INPUT);
  pinMode(OPTO3, INPUT);
  pinMode(BUMPER_RECHTS, INPUT_PULLUP);
  pinMode(BUMPER_LINKS, INPUT_PULLUP);
  pinMode(LED_BLINK_RECHTS, OUTPUT);
  pinMode(LED_BLINK_LINKS, OUTPUT);
  pinMode(LED_FRONT, OUTPUT);
  pinMode(LED_HINTEN, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  Serial.println("Done");
  Serial.println("------------------------------------------");
  /*
    // Testen der LEDs
    Serial.println("LED testing...");
    Serial.println("LED Blink Rechts");
    testLed(LED_BLINK_RECHTS);
    Serial.println("LED Blink Links");
    testLed(LED_BLINK_LINKS);
    Serial.println("LED Front");
    testLed(LED_FRONT);
    Serial.println("LED Hinten");
    testLed(LED_HINTEN);
    Serial.println("Done");
    Serial.println("------------------------------------------");

    // Testen des Buzzers
    Serial.println("BUZZER testing...");
    playNote("c2", 8);
    playNote("d2", 8);
    playNote("e2", 8);
    playNote("f2", 8);
    playNote("g2", 8);
    playNote("a2", 8);
    playNote("h2", 8);
    playNote("c3", 8);
    playNote("h2", 8);
    playNote("a2", 8);
    playNote("g2", 8);
    playNote("f2", 8);
    playNote("e2", 8);
    playNote("d2", 8);
    playNote("c1", 8);
    Serial.println("Done");
    Serial.println("------------------------------------------");
  */
  // Servo Pin Zuweisung
  Serial.println("Servo Pin Zuweisung...");
  servo_links.attach(SERVO_LINKS, SERVO_LINKS_NULL - 500, SERVO_LINKS_NULL + 500);
  servo_rechts.attach(SERVO_RECHTS, SERVO_RECHTS_NULL + 500, SERVO_RECHTS_NULL - 500);
  Serial.println("Done");
  Serial.println("------------------------------------------");
  /*
    // Testen der Servos
    Serial.println("Testen der Servos...");
    drive(50);
    delay(500);
    changeDirection("drive", -50);
    delay(500);
    drive(-50);
    delay(1000);
    resetServo("both");
    Serial.println("Done");
    Serial.println("------------------------------------------");

    // Testern der Bumper
    Serial.println("Testen der Bumper...");
    int bumperVar = 1;
    do {
      Serial.println("Bitte rechten Bumper drücken!  ");
      delay(300);
      bumperVar = digitalRead(BUMPER_RECHTS);
    } while (bumperVar == 1);
    Serial.print("Wert : ");
    Serial.println(bumperVar);
    Serial.println("------------------------------------------");
    delay(500);
    bumperVar = 1;
    do {
      Serial.println("Bitte linken Bumper drücken!  ");
      delay(300);
      bumperVar = digitalRead(BUMPER_LINKS);
    } while (bumperVar == 1);
    Serial.print("Wert : ");
    Serial.println(bumperVar);
    delay(500);
    Serial.println("Done");
    Serial.println("------------------------------------------");

    // Testen der OPTOs
    Serial.println("Testen der Optos...");
    digitalWrite(OPTOS, HIGH);
    int optoVar = 0;
    optoVar = analogRead(OPTO1);
    Serial.print("LICHTSCHRANKE1 = ");
    Serial.println(optoVar);
    Serial.println("------------------------------------------");
    delay(500);
    optoVar = analogRead(OPTO2);
    Serial.print("LICHTSCHRANKE2 = ");
    Serial.println(optoVar);
    Serial.println("------------------------------------------");
    delay(500);
    optoVar = analogRead(OPTO3);
    Serial.print("LICHTSCHRANKE3 = ");
    Serial.println(optoVar);
    Serial.println("------------------------------------------");
    delay(500);
    digitalWrite(OPTOS, LOW);
    Serial.println("Done");
    Serial.println("------------------------------------------");

    Serial.println("Das Setup und alle Tests wurden erfolgreich durchgeführt!");
  */
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void loop() {
    // Normal Mode
    drive(100);
    delay(30);
    while (digitalRead(BUMPER_RECHTS) == 0) {
    turn(-50);
    delay(500);
    }
    while (digitalRead(BUMPER_LINKS) == 0) {
    turn(50);
    delay(500);
    }
  // Line Follow Mode
  //checkOptos();
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
   Umwandeln von Prozent werten in MicroSecond Werte

   int servoPin - Pin, auf den der Servo zugewiesen wurde
   int speedInPercent - Prozentuale Geschwindigkeit (-100% --> +100%)
*/
int getMicroSeconds(int servoPin, int speedInPercent) {
  int speed;
  // Diese Umwandlung ist nur für den mBot von René Schaar funktional. Bei Verwendung anderer mBots wird keine einwandfreie Funktion gewährleistet.
  switch (servoPin) {
    case SERVO_RECHTS:
      speed = 500 * speedInPercent / 100;
      lastSpeed = SERVO_RECHTS_NULL - speed;
      return SERVO_RECHTS_NULL - speed;
    case SERVO_LINKS:
      speed = 500 * speedInPercent / 100;
      lastSpeed = SERVO_RECHTS_NULL + speed;
      return SERVO_LINKS_NULL + speed;
    default:
      return 0;
  }
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
   Konstantes fahren nach vorne, oder hinten (+,-)

   int speedInPercent - Prozentuale Geschwindigkeit (-100% bis +100%)
*/
void drive(int speedInPercent) {
  resetLeds();
  if (speedInPercent > 0) {
    digitalWrite(LED_FRONT, HIGH);
  } else {
    digitalWrite(LED_HINTEN, HIGH);
  }
  servo_rechts.writeMicroseconds(getMicroSeconds(SERVO_RECHTS, speedInPercent));
  servo_links.writeMicroseconds(getMicroSeconds(SERVO_LINKS, speedInPercent));
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
   Drehung in, oder gegen den Uhrzeigersinn (+ --> Im Uhrzeigersinn; - --> Gegen den Uhrzeigersinn)

   int speedInPercent - Prozentuale Geschwindigkeit (-100% bis +100%)
*/
void turn(int speedInPercent) {
  servo_rechts.writeMicroseconds(getMicroSeconds(SERVO_RECHTS, -speedInPercent));
  servo_links.writeMicroseconds(getMicroSeconds(SERVO_LINKS, speedInPercent));
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
   Ansteuern des Rechten Servos

   int speedInPercent - Prozentuale Geschwindigkeit (-100% bis +100%)
*/
void servoRight(int speedInPercent) {
  resetLeds();
  digitalWrite(LED_BLINK_RECHTS, HIGH);
  servo_rechts.writeMicroseconds(getMicroSeconds(SERVO_RECHTS, speedInPercent));
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
   Ansteuern des Linken Servos

   int speedInPercent - Prozentuale Geschwindigkeit (-100% bis +100%)
*/
void servoLeft(int speedInPercent) {
  resetLeds();
  digitalWrite(LED_BLINK_LINKS, HIGH);
  servo_links.writeMicroseconds(getMicroSeconds(SERVO_LINKS, speedInPercent));
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
   Zurücksetzen der Servo Geschwindigkeit auf 0

   char servo - Der zurückzusetzende Servo ("rechts", "links", oder "both")
*/
void resetServo(String servo) {
  if (servo == "rechts") {
    servo_rechts.writeMicroseconds(getMicroSeconds(SERVO_RECHTS, 0));
  } else if (servo == "links") {
    servo_links.writeMicroseconds(getMicroSeconds(SERVO_LINKS, 0));
  } else if (servo == "both") {
    servo_rechts.writeMicroseconds(getMicroSeconds(SERVO_RECHTS, 0));
    servo_links.writeMicroseconds(getMicroSeconds(SERVO_LINKS, 0));
  }
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/* Diese Methode ermöglicht das fahren in entgegengesetzte Richtung durch langsames, lineare ändern der Richtung

  String mode - Art der aktuellen Bewegung (drive, turn, rechts, links)
  int newSpeed - gewünschte neue Richtung
*/
void changeDirection(String mode, int newSpeed) {
  if (mode == "rechts") {
    if (lastSpeed > newSpeed) {
      for (int i = newSpeed; i > 0; i--) {
        servoRight(i);
        delay(30);
      }
    } else {
      for (int i = 0; i < newSpeed; i++) {
        servoRight(i);
      }
    }
  } else if (mode == "links") {
    if (lastSpeed > newSpeed) {
      for (int i = -newSpeed; i > 0; i--) {
        servoLeft(i);
        delay(30);
      }
    }
    else {
      for (int i = 0; i < newSpeed; i++) {
        servoLeft(i);
        delay(30);
      }
    }
  } else if (mode == "drive") {
    if (lastSpeed > newSpeed) {
      for (int i = -newSpeed; i > 0; i--) {
        drive(i);
        delay(30);
      }
    } else {
      for (int i = 0; i < newSpeed; i++) {
        drive(i);
        delay(30);
      }
    }
  } else if (mode == "turn") {
    if (lastSpeed > newSpeed) {
      for (int i = newSpeed; i > 0; i--) {
        turn(i);
        delay(30);
      }
    } else {
      for (int i = 0; i < newSpeed; i++) {
        turn(i);
        delay(30);
      }
    }
  }
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void testLed(int led) {
  digitalWrite(led, HIGH);
  delay(1000);
  digitalWrite(led, LOW);
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void resetLeds() {
  digitalWrite(LED_FRONT, LOW);
  digitalWrite(LED_HINTEN, LOW);
  digitalWrite(LED_BLINK_RECHTS, LOW);
  digitalWrite(LED_BLINK_LINKS, LOW);
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void checkOptos() {
  digitalWrite(OPTOS, HIGH);
  if (analogRead(OPTO1) > 500 && analogRead(OPTO2) < 500) {
    resetLeds();
    digitalWrite(LED_BLINK_RECHTS, HIGH);
    turn(50);
    delay(500);
    drive(100);
    delay(500);
  }
  if (analogRead(OPTO3) > 500 && analogRead(OPTO2) < 500) {
    resetLeds();
    digitalWrite(LED_BLINK_LINKS, HIGH);
    turn(-50);
    delay(500);
    drive(100);
    delay(500);
  }
  if (analogRead(OPTO2) > 500) {
    drive(500);
    delay(500);
  }
  if (analogRead(OPTO1) < 500 && analogRead(OPTO2) < 500 && analogRead(OPTO3) < 500) {
    servoRight(50);
    delay(500);
    servoLeft(50);
    delay(500);
  }
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
   Umwandlung von musikalischen Tönen in Frequenzen

   String name - Name des musikkalischen Tones
   double wer - Notenwert
*/
void playNote(String name, int wert) {

  // Berechnung der Frequenz
  if (name == "c") {
    frequenz = 131;
  } else if (name == "d") {
    frequenz = 147;
  } else if (name == "e") {
    frequenz = 165;
  } else if (name == "f") {
    frequenz = 175;
  } else if (name == "fis") {
    frequenz = 185;
  } else if (name == "g") {
    frequenz = 196;
  } else if (name == "gis") {
    frequenz = 208;
  } else if (name == "a") {
    frequenz = 220;
  } else if (name == "b") {
    frequenz = 233;
  } else if (name == "h") {
    frequenz = 247;
  } else if (name == "c1") {
    frequenz = 262;
  } else if (name == "d1") {
    frequenz = 294;
  } else if (name == "e1") {
    frequenz = 330;
  } else if (name == "f1") {
    frequenz = 350;
  } else if (name == "fis1") {
    frequenz = 370;
  } else if (name == "g1") {
    frequenz = 392;
  } else if (name == "as1") {
    frequenz = 415;
  } else if (name == "a1") {
    frequenz = 440;
  } else if (name == "b1") {
    frequenz = 466;
  } else if (name == "h1") {
    frequenz = 494;
  } else if (name == "c2") {
    frequenz = 523;
  } else if (name == "d2") {
    frequenz = 587;
  } else if (name == "es2" || name == "dis2") {
    frequenz = 622;
  } else if (name == "e2") {
    frequenz = 659;
  } else if (name == "f2") {
    frequenz = 698;
  } else if (name == "fis2") {
    frequenz = 740;
  } else if (name == "g2") {
    frequenz = 784;
  } else if (name == "gis2") {
    frequenz = 831;
  } else if (name == "a2") {
    frequenz = 880;
  } else if (name == "b2" || name == "ais2") {
    frequenz = 932;
  } else if (name == "h2") {
    frequenz = 988;
  } else if (name == "c3") {
    frequenz = 1046;
  } else if (name == "des3" || name == "cis3") {
    frequenz = 1109;
  } else if (name == "d3") {
    frequenz = 1175;
  } else if (name == "e3") {
    frequenz = 1319;
  } else if (name == "es3" || name == "dis3") {
    frequenz = 1245;
  } else if (name == "fis3") {
    frequenz = 1480;
  } else if (name == "g3") {
    frequenz = 1568;
  } else if (name == "a3") {
    frequenz = 1760;
  } else if (name == "h3") {
    frequenz = 1976;
  } else if (name == "cis4") {
    frequenz = 2217;
  } else if (name == "d4") {
    frequenz = 2349;
  } else if (name == "pause") {
    frequenz = 0;
  }

  // Berechnung der Verzögerung
  if (wert != 3 && wert != 6) {
    pause = (int) (60000 / (bpm / 4) / wert);
  } else if (wert == 3) {
    pause = ((60000 / (bpm / 4)) / 6);
  } else if (wert == 6) {
    pause = ((60000 / (bpm / 4)) / 12);
  }
  // Erweiterung des Delays falls die Note punktiert ist
  if (punkt) {
    finalDelay = (pause + pause / 2) + dauer;
  } else {
    finalDelay = pause + dauer;
  }
  bogenDauer = finalDelay + dauer;

  // Ausgabe
  if (bogen == false) {
    if (frequenz != 0) {
      tone(BUZZER, frequenz, dauer);
    }
    delay(finalDelay);
  } else {
    if (frequenz != 0) {
      tone(BUZZER, frequenz, bogenDauer);
    }
    delay(bogenDauer);
  }
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void wiederholen() {
  // Ausgabe
  if (bogen == false) {
    if (frequenz != 0) {
      tone(BUZZER, frequenz, dauer);
    }
    delay(finalDelay);
  } else {
    if (frequenz != 0) {
      tone(BUZZER, frequenz, bogenDauer);
    }
    delay(bogenDauer);
  }
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void megalovania() {
  bpm = 120;

  playNote("d1", 16);
  wiederholen();
  playNote("d2", 8);
  punkt = true;
  playNote("a1", 8);
  punkt = false;
  playNote("as1", 16);
  playNote("pause", 16);
  playNote("g1", 8);
  bogen = true;
  playNote("f1", 16);
  bogen = false;
  playNote("f1", 16);
  playNote("d1", 16);
  playNote("f1", 16);
  playNote("g1", 16);

  playNote("c1", 16);
  wiederholen();
  playNote("d2", 8);
  punkt = true;
  playNote("a1", 8);
  punkt = false;
  playNote("as1", 16);
  playNote("pause", 16);
  playNote("g1", 8);
  bogen = true;
  playNote("f1", 16);
  bogen = false;
  playNote("f1", 16);
  playNote("d1", 16);
  playNote("f1", 16);
  playNote("g1", 16);

  playNote("h", 16);
  wiederholen();
  playNote("d2", 8);
  punkt = true;
  playNote("a1", 8);
  punkt = false;
  playNote("as1", 16);
  playNote("pause", 16);
  playNote("g1", 8);
  bogen = true;
  playNote("f1", 16);
  bogen = false;
  playNote("f1", 16);
  playNote("d1", 16);
  playNote("f1", 16);
  playNote("g1", 16);

  playNote("b", 16);
  wiederholen();
  playNote("d2", 8);
  punkt = true;
  playNote("a1", 8);
  punkt = false;
  playNote("as1", 16);
  playNote("pause", 16);
  playNote("g1", 8);
  bogen = true;
  playNote("f1", 16);
  bogen = false;
  playNote("f1", 16);
  playNote("d1", 16);
  playNote("f1", 16);
  playNote("g1", 16);
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void routeOne() {
  bpm = 120;

  playNote("d3", 16);
  playNote("e3", 16);
  playNote("fis3", 8);
  wiederholen();
  wiederholen();
  playNote("d3", 16);
  playNote("e3", 16);
  playNote("fis3", 8);
  wiederholen();
  wiederholen();
  playNote("d3", 16);
  playNote("e3", 16);
  playNote("fis3", 8);
  wiederholen();
  playNote("g3", 4);
  playNote("fis3", 16);
  playNote("e3", 2);

  playNote("cis3", 16);
  playNote("d3", 16);
  playNote("e3", 8);
  wiederholen();
  wiederholen();
  playNote("cis3", 16);
  playNote("d3", 16);
  playNote("e3", 8);
  wiederholen();
  wiederholen();
  playNote("cis3", 16);
  playNote("d3", 16);
  playNote("e3", 8);
  wiederholen();
  playNote("fis3", 16);
  playNote("e3", 16);
  playNote("e3", 16);
  playNote("fis3", 16);
  playNote("d3", 2);

  playNote("d3", 16);
  playNote("e3", 16);
  playNote("fis3", 8);
  wiederholen();
  wiederholen();
  playNote("d3", 16);
  playNote("e3", 16);
  playNote("fis3", 8);
  wiederholen();
  wiederholen();
  playNote("d3", 16);
  playNote("e3", 16);
  playNote("fis3", 8);
  wiederholen();
  playNote("g3", 4);
  playNote("fis3", 16);
  playNote("e3", 2);

  playNote("cis3", 16);
  playNote("d3", 16);
  playNote("e3", 8);
  playNote("g3", 8);
  playNote("fis3", 8);
  playNote("e3", 8);
  playNote("d3", 8);
  playNote("cis3", 8);
  playNote("h2", 8);
  playNote("cis3", 8);
  playNote("a3", 4);
  playNote("cis3", 4);
  playNote("d3", 2);

  playNote("d3", 16);
  playNote("fis3", 16);
  playNote("a3", 8);
  wiederholen();
  playNote("fis3", 8);
  playNote("d3", 8);
  playNote("d4", 8);
  playNote("cis4", 8);
  playNote("h3", 8);
  playNote("cis4", 8);
  playNote("a3", 8);
  playNote("fis3", 8);
  playNote("d3", 8);
  playNote("pause", 16);
  playNote("fis3", 16);
  playNote("e3", 4);
}

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
