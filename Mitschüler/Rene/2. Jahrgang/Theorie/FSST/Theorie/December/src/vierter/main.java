package vierter;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class main extends JPanel {

	public void TestPaint() {
		setBackground(Color.WHITE);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(Color.BLUE);
		g2d.drawLine(0, 0, 800, 600);
		g2d.fillRect(800 / 2 - 800 / 4, 600 / 2 - 600 / 4, (800) - (800 / 2), (600) - (600 / 2));
		g2d.fillOval(800 / 2 + (400 - 800/25) / 2 + 50, (300 - 600/25) / 2 - 75, 50, 50);
		repaint();

	}

	public JLabel draw() {
		setBackground(Color.WHITE);
		return null;
	}

	public static void main(String[] args) {
		JFrame jf = new JFrame("04.12.2018");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setSize(800, 600);
		jf.setLocationRelativeTo(null);
		jf.requestFocus();
		jf.setResizable(false);
		jf.add(new main());

		jf.setVisible(true);
	}

}
