package achtzehnter;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

public class Zeichenfläche extends JPanel implements MouseListener, MouseMotionListener {

	private int mouseX, mouseY;
	int x = 0, y = 0, i = 0;
	private Graphics2D g;

	@Override
	protected void paintComponent(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paintComponent(arg0);
		g = (Graphics2D) arg0;
		g.fillOval(x - 5, y - 5, 10, 10);

	}

	/**
	 * Create the panel.
	 */
	public Zeichenfläche() {
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		if (i < 5) {
			x = mouseX;
			y = mouseY;
			i++;
		} else {
			i = 0;
			mouseX = e.getX();
			mouseY = e.getY();
			x = mouseX;
			y = mouseY;
		}

		System.out.println(mouseX + ", " + mouseY);
		repaint();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
