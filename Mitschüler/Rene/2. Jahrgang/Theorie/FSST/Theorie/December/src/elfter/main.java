package elfter;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.StrokeBorder;

public class main extends JPanel {

	public void TestPaint() {
		setBackground(Color.WHITE);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		// Zeichenfläche löschen
		g2d.clearRect(0, 0, 800, 600);

		// Linie 1
		g2d.drawLine(400, 0, 400, 600);

		// Linie 2
		g2d.setStroke(new BasicStroke(4));
		g2d.drawLine(0, 300, 800, 300);
		
		// Linie 3
		g2d.setStroke(new BasicStroke(1));
		g2d.drawLine(0, 0, 400, 300);
		
		// Linie 4
		g2d.drawLine(400, 300, 800, 0);
		
		// Kreis 1
		g2d.drawOval(400 - 50, 300 - 50, 100, 100);
		
		// Kreis 2
		g2d.drawOval(400 - 200, 300 - 200, 400, 400);
		
		// String
		g2d.setFont(new Font("Arial", Font.PLAIN, 15));
		g2d.drawString("Grafik", 700, 500);


		repaint();

	}

	public JLabel draw() {
		setBackground(Color.WHITE);
		return null;
	}

	public static void main(String[] args) {
		JFrame jf = new JFrame("11.12.2018");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setSize(800, 600);
		jf.setLocationRelativeTo(null);
		jf.requestFocus();
		jf.setResizable(false);
		jf.add(new main());

		jf.setVisible(true);
	}

}
