package f�nfzehnter;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;


public class Zeichenfl�che extends JPanel implements ActionListener {

	private javax.swing.Timer t;
	private int xPos, yPos, xV, yV;
	private boolean links = true;

	public Zeichenfl�che() {
		xPos = 0;
		yPos = 50;
		xV = 10;
		yV = 10;
		t = new Timer(20, this);
		t.start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		if(xPos < getWidth() - 50 && links) {
			xPos += xV;
			links = true;
		} else if(xPos >= 0){
			links = false;
			xPos -= xV;
		} else {
			links = true;
		}
		
		repaint();
	}

	@Override
	protected void paintComponent(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paintComponent(arg0);
		Graphics2D g = (Graphics2D) arg0;
		g.drawRect(xPos, yPos, 50, 50);
	}

}
