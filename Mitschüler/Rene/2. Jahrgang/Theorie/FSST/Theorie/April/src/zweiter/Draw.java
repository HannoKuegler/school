package zweiter;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

public class Draw extends JPanel implements KeyListener {

	int x = 0, y = 0;
	int speed = 10;

	public Draw() {
		this.addKeyListener(this);
		this.setFocusable(true);
	}

	@Override
	protected void paintComponent(Graphics arg0) {
		// Setup
		super.paintComponent(arg0);
		Graphics2D g = (Graphics2D) arg0;

		g.fillRect(x, y, 100, 50);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

		if (e.getKeyCode() == KeyEvent.VK_UP)
			y -= speed;
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
			x -= speed;
		if (e.getKeyCode() == KeyEvent.VK_DOWN)
			y += speed;
		if (e.getKeyCode() == KeyEvent.VK_RIGHT)
			x += speed;
		;
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
