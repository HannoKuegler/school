
public class Aufgabe1To8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Aufgabe 1");
		repeat("Cool", 5);
		System.out.println("");

		System.out.println("Aufgabe 2");
		int j = charCount("Der liebe Patrick ist ein netter Mensch", 'e');
		System.out.println(j);
		System.out.println("");

		System.out.println("Aufgabe 3");
		double p = prozent(1337, 25);
		System.out.println("Prozentwert = " + p);
		System.out.println("");

		System.out.println("Aufgabe 4");
		boolean b = istTeilbar(30, 3);
		System.out.println(b);
		System.out.println("");

		System.out.println("Aufgabe 5");
		String s = replaceChar("Java", 'a', 'o');
		System.out.println(s);
		System.out.println("");

		System.out.println("Aufgabe 6");
		s = substringDelimiter("Java;ist toll", ';');
		System.out.println(s);
		System.out.println("");

		System.out.println("Aufgabe 7");
		StaticCounter.set(10);
		StaticCounter.inc();
		System.out.println(StaticCounter.get());
		System.out.println("");
		
		System.out.println("Aufgabe 8");
		InstanzCounter ic = new InstanzCounter();
		ic.set(10);
		ic.inc();
		System.out.println(ic.get());
	}

	public static void repeat(String sParam, int iParam) {
		for (int i = 0; i <= iParam; i++) {
			System.out.print(sParam + " ");
		}
		System.out.println("");
	}

	public static int charCount(String sParam, char cParam) {
		int j = 0;
		for (int i = 0; i < sParam.length(); i++) {
			if (sParam.charAt(i) == cParam)
				j++;
		}
		return (j);
	}

	public static double prozent(double wert, double pSatz) {
		double p = 0;
		p = wert * pSatz / 100;
		return p;
	}

	public static boolean istTeilbar(int zahl, int divisor) {
		boolean b = false;
		if ((zahl / divisor) % 2 == 0)
			b = true;
		return b;
	}

	public static String replaceChar(String sParam, char cToRep, char cRep) {
		String s = "";
		for (int i = 0; i < sParam.length(); i++) {
			if (sParam.charAt(i) == cToRep) {
				s += cRep;
			} else {
				s += sParam.charAt(i);
			}
		}
		return s;
	}

	public static String substringDelimiter(String in, char begr) {
		String s = "";
		for (int i = 0; i < in.length(); i++) {
			if (in.charAt(i) != begr) {
				s += in.charAt(i);
			} else {
				break;
			}
		}
		return s;
	}

}
