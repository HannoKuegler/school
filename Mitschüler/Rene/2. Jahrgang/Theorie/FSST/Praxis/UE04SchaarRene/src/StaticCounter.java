
public class StaticCounter {

	private static int counter = 0;

	public static void set(int wert) {
		counter = wert;
	}
	
	public static int get() {
		return counter;
	}
	
	public static void inc() {
		counter++;
	}
	
}
