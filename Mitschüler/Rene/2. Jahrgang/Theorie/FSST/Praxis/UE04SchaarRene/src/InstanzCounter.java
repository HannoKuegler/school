
public class InstanzCounter {
	private static int counter = 0;

	public void set(int wert) {
		counter = wert;
	}
	
	public int get() {
		return counter;
	}
	
	public void inc() {
		counter++;
	}
}
