import java.util.Arrays;
import java.util.Random;

public class Aufgaben1To9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] iArray = { 1, 2, 5, 6, 4 };
		String[] sArray = { "Hallo", "Welt" };
		int[] iIndexTo15 = new int[10];
		int[] iIndexToIndexNumber = new int[8];

		druckeArray(iArray);

		druckeArray(sArray);

		druckeArray(IndexTo15(iIndexTo15));

		druckeArray(IndexToIndexNumber(iIndexToIndexNumber));

		Random rd = new Random();
		int[] randomArray = new int[6];
		for (int i = 0; i < randomArray.length; i++) {
			randomArray[i] = rd.nextInt(21);
		}
		System.out.println("Summe:" + Summe(randomArray));
		System.out.println("Mittelwert:" + Mittelwert(randomArray));
		System.out.println("Kleinster Wert:" + KleinsterWert(randomArray));
		System.out.println(Groe�terWert(randomArray));
		System.out.println("");

		int[] x = new int[20];
		setzeArray(IndexToIndexNumber(x), 3, 9, 13);

		int[] randomArray3 = new int[10];
		for (int i = 0; i < randomArray3.length; i++) {
			randomArray3[i] = rd.nextInt(21);
		}
		int[] sortiert = Sort(randomArray3);
		System.out.println(Arrays.toString(sortiert));
	}

	/*
	 * This method prints the containment of an array out on the console
	 * 
	 * @param Array in the format int[]
	 */
	public static void druckeArray(int[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.println("a[" + i + "] = " + a[i]);
		}
		System.out.println("");
	}

	/*
	 * This method prints the containment of an array out on the console
	 * 
	 * @param Array in the format String[]
	 */
	public static void druckeArray(String[] a) {
		for (int i = 0; i < a.length; i++) {
			System.out.println("a[" + i + "] = " + a[i]);
		}
		System.out.println("");
	}

	/*
	 * This method sets every single index of an array to 15
	 * 
	 * @param Array in the format int[]
	 * 
	 * @return A new array where every index is 15
	 */
	public static int[] IndexTo15(int[] a) {
		for (int i = 0; i < a.length; i++) {
			a[i] = 15;
		}

		return a;
	}

	/*
	 * This method sets the index of an array to it�s position Number
	 * 
	 * @param Array in the format int[]
	 * 
	 * @return A new Array where every index is set to it�s position number
	 */
	public static int[] IndexToIndexNumber(int[] a) {
		for (int i = 0; i < a.length; i++) {
			a[i] = i;
		}
		return a;
	}

	/*
	 * This method calculates the sum of an Array
	 * 
	 * @param Array in the format int[]
	 * 
	 * @return The sum in the format int
	 */
	public static int Summe(int[] a) {
		int x = 0;
		for (int i = 0; i < a.length; i++) {
			x += a[i];
		}
		return x;
	}

	/*
	 * This method calculates the average value of an Array
	 * 
	 * @param Array in the format int[]
	 * 
	 * @return The average value in the format int
	 */
	public static double Mittelwert(int[] a) {
		double x = 0;
		for (int i = 0; i < a.length; i++) {
			x = (double) Summe(a) / a.length;
		}
		return x;
	}

	/*
	 * This method calculates the smallest number of an Array
	 * 
	 * @param Array in the format int[]
	 * 
	 * @return The smallest number in the format int
	 */
	public static int KleinsterWert(int[] a) {
		Arrays.sort(a);
		int klWert = a[0];
		return klWert;
	}

	/*
	 * This method calculates the biggest number of an Array
	 * 
	 * @param Array in the format int[]
	 * 
	 * @return The biggest number in the format int
	 */
	public static int Groe�terWert(int[] a) {
		Arrays.sort(a);
		int grWert = a[a.length - 1];
		return grWert;
	}

	/*
	 * This method sets the values of the indexes from an array inbetween the
	 * defined range to a defined number
	 * 
	 * @param Array in the format int[]
	 * 
	 * @param First index of the range in the format int
	 * 
	 * @param Last index of the range in the format int
	 * 
	 * @param A number to replace the indexes in the range with in the format int
	 * 
	 */
	public static void setzeArray(int[] a, int beginIndex, int endIndex, int wert) {
		for (int i = 0; i < a.length; i++) {
			if (i >= beginIndex && i <= endIndex) {
				a[i] = wert;
			}
		}
		druckeArray(a);
	}

	/*
	 * This method sorts the numbers of an Array by their values
	 * 
	 * @param Array in the format int[]
	 * 
	 * @return A new Array, where the left number of a pair of two numbers is on the
	 * left side, in the format int[]
	 */
	public static int[] Sort(int[] a) {
		boolean �nderung = false;
		while (�nderung == false) {
			for (int i = 0; i < a.length; i++) {
				if (i + 1 < a.length) {
					if (a[i] > a[i + 1]) {
						int b = a[i + 1];
						a[i + 1] = a[i];
						a[i] = b;
						�nderung = true;
					}
				}
				System.out.println("Das aktuelle Array: " + Arrays.toString(a));
			}
			System.out.println("");
			if (�nderung == true) {
				System.out.println("Das Array wurde sortiert");
			}
		}
		return a;
	}

}
