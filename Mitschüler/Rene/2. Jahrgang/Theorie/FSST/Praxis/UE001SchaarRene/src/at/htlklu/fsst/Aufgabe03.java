package at.htlklu.fsst;

import java.util.Scanner;

public class Aufgabe03 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		Scanner in = new Scanner(System.in);
		
			System.out.println("Legen Sie einen Wert fest:");
			double n = in.nextDouble();
		
		in.close();
		
		//for-Schleife
		
		for(double i = 3; i < 2 * n; i++) {
			if(i == 3) {
				i++;
			}
			System.out.println(1 / (2 * i + 1));
		}
		
		Thread.sleep(3000);
		System.out.println();
		
		//While-Schleife
		
		double i = 3;
		
		while (i < 2 * n - 1) {
			i++;
			System.out.println(1 / (2 * i + 1));	
		}
		
	}

}
