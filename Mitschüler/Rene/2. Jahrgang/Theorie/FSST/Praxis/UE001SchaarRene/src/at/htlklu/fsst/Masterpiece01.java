package at.htlklu.fsst;

import java.util.Scanner;

public class Masterpiece01 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		Scanner in = new Scanner(System.in);
		
		boolean pr�fung = false;
		double zn = 0;
		
		do {
			System.out.println("Legen Sie eine ganzahlige, und positive Zahl fest:");
			zn = in.nextDouble();
			
			if(zn % 1 == 0 && zn > 0) {
				pr�fung = true; break;
			}
			
			System.out.println("Das war keine ganzahlige, und positive Zahl");
			Thread.sleep(1500);
			System.out.print("Das Programm wird neugestartet");
			Thread.sleep(500);
			System.out.print(".");
			Thread.sleep(500);
			System.out.print(".");
			Thread.sleep(500);
			System.out.print(".");
			System.out.println();
			Thread.sleep(500);
			System.out.println();
			
		} while (pr�fung == false);
		in.close();
		
		while(zn != 1) {
			if(zn % 2 == 0) {
				zn /= 2;
			} else {
				zn = zn * 3 + 1;
			}
			
			System.out.print((int) zn + " ");
			
		}
		
	}

}
