
public class HUE004 {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(fakultaet(5));
		System.out.println(gerade(5));
	}

	/**
	 * Berechnung der Fakult�t einer beliebigen Zahl
	 * 
	 * @param c Ist eine beliebige Zahl, von der die Fakult�t berechnet werden kann
	 * @return Das Ergebnis der Berechung
	 */
	public static int fakultaet(int c) {
		int e = 1;
		for (int i = 1; i <= c; i++)
			e *= i;
		return e;
	}

	/**
	 * �berpr�fung, ob eine Zahl gerade, oder ungerade ist
	 * 
	 * @param c2 Entspricht entspricht einer beliebigen Zahl
	 * @return Ein boolean mit "true" wenn die Zahl gerade ist und "false" wenn sie
	 *         ungerade ist
	 */
	public static boolean gerade(int c2) {
		boolean g = false;
		if (c2 % 2 == 0)
			g = true;
		return g;
	}

}
