
public class HUE003 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Aufgabe01
		printHTLCareer();

		// Aufgabe02
		nStar(10);

		// Aufgabe03
		mult(2, 3);

	}

	public static void printHTLCareer() {
		System.out.println("Regelkarriere:");
		for (int i = 0; i < 5; i++)
			System.out.println((i + 1) + ". Jahrgang");
		System.out.println("");
	}

	public static void nStar(int amount) {
		for (int i = 0; i < amount; i++)
			System.out.print("*");
		System.out.println("");
	}

	public static void mult(int fak1, int fak2) {
		System.out.println(fak1 + " * " + fak2 + " = " + (fak1 * fak2));
		System.out.println("");
	}

}
