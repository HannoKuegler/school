package GLFS2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;


/**
 * 2 GLFS �bung
 * @author raphael
 *
 */
public class GLFS2Panel_2_4 extends JPanel {

	// Arrays f�r UE 3
	private int[] x = { 250, 250, 300, 375, 375 };
	private int[] y = { 50, 100, 75, 50, 100 };
	private int[] s = { 20, 20, 40, 20, 20 };
	private Color[] c = { Color.GREEN, Color.GREEN, Color.RED, Color.GREEN, Color.GREEN };

	public GLFS2Panel_2_4() {

	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		// Aufgabe 2
		// color Blue
		g2D.setColor(Color.BLUE);
		// 20 mal die Methode drawCircle aufrufen
		for (int i = 0; i < 20; i++) {
			drawCircle(100, 100, i * 5, g2D);
		}

		/*
		 * Aufgabe 3 oben angelete Arrays durchgenen und die rechtecke mit verscheidenen
		 * Koordinaten zeichen. Die Koordinaten stehen in den Arrays sowie die farbe
		 */

		for (int i = 0; i < x.length; i++) {
			g2D.setColor(c[i]);
			g2D.fillRect(x[i], y[i], s[i], s[i]);
		}

		// Aufgabe 4
		// Blaue Farbe
		g2D.setColor(Color.BLUE);

		// Linie con oben links bis unten rechts

		g2D.drawLine(0, 0, getWidth() - 1, getHeight() - 1);

		// Strichst�rle dicker machen

		g2D.setStroke(new BasicStroke(3.0f));

		// Linie von rechts oben nach links unten zeichnen

		g2D.drawLine(getWidth(), 0, 0, getHeight());

		// gr�ne Farbe

		g2D.setColor(Color.GREEN);

		// gef�lltes rechteck zeichen von mitte weg

		g2D.fillRect(getWidth() / 2, getHeight() / 2, getWidth() / 4, getHeight() / 8);

		// neue Schriftart und Schriftstyle sowie dicke der Schrift

		Font f = new Font("Arial", Font.BOLD, getHeight() / 8);

		// neue Schriftart setzen

		g2D.setFont(f);

		// rote farbe

		g2D.setColor(Color.RED);

		// String zeichen Mit der Schriftart

		g2D.drawString("FSST", getWidth() / 16, getHeight() / 2);

		// schwarze Farbe

		g2D.setColor(Color.BLACK);

		// Neue Strichst�rke

		g2D.setStroke(new BasicStroke(1.0f));

		// Linie von rechts mitte nach links mitte

		g2D.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2);
	}

	/**
	 * Zeichnet einen Kreis
	 * 
	 * @param xM     x Mittelpunkt
	 * @param yM     y Mittelpunkt
	 * @param radius Radius
	 * @param g2D    Zeichenstift
	 */
	public void drawCircle(int xM, int yM, int radius, Graphics2D g2D) {
		g2D.drawOval(xM - radius, yM - radius, 2 * radius, 2 * radius);
	}

}
