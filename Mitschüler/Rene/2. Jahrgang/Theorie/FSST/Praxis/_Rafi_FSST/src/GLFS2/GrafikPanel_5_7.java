package GLFS2;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

/**
 * 2 GLFS �bung
 * @author raphael
 *
 */
public class GrafikPanel_5_7 extends JPanel {
	// Aufgabe 6
	// F�r Barcode
	private boolean[] barcode = { true, false, true, false, false, true, true, false, true };

	// Aufgabe 7
	private Color[] farben = { Color.BLACK, Color.WHITE, Color.RED, Color.GREEN, Color.YELLOW, Color.MAGENTA };

	// Aufgabe 5
	// K = Kreis, Q = Quadrat, O = Oval
	char[] grafikObjekte = { 'K', 'Q', 'O', 'K' };
	// Fraben
	Color[] farbe = { Color.RED, Color.BLUE, Color.BLACK, Color.GREEN };

	public GrafikPanel_5_7() {

	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D) g;
		// Aufgabe 6
		for (int i = 0; i < barcode.length; i++) {
			// wenn true dann schwarz
			if (barcode[i]) {
				g2D.setColor(Color.BLACK);
				// wenn false dann wei�
			} else {
				g2D.setColor(Color.WHITE);
			}
			//zeichen 
			g2D.fillRect(i * 10, 0, 10, 100);
		}
		
		// Aufgabe 7
		// Farben durchgehen, dann rechteck zeichen
		for (int i = 0; i < farben.length; i++) {
			g2D.setColor(farben[i]);
			g2D.fillRect(i * 10, 100, 10, 100);
		}
		
		// Aufgabe 5

		for (int i = 0; i < 4; i++) {
			
			//schwarz machen 
			
			g2D.setColor(Color.BLACK);
			
			//rechteck zecihen
			
			g2D.drawRect(i * getWidth() / 4, 200, getWidth() / 4, getWidth() / 4);
			
			// farbe des Arrays an der Stelle i setzen 
			
			g2D.setColor(farbe[i]);
			
			//switch case f�r die chars des arrays an der stelle i 
			
			switch (grafikObjekte[i]) {
			//wenn Q dann zeichene ein Quadrat
			case 'Q':
				g2D.fillRect(getWidth() / 4 * i + getWidth() / 16, 200 + getWidth() / 16, getWidth() / 8,
						getWidth() / 8);
				break;
			// wenn K dann zeiche ein Kreis
			case 'K':
				g2D.fillOval(getWidth() / 4 * i + getWidth() / 16, 200 + getWidth() / 16, getWidth() / 8,
						getWidth() / 8);
				break;
			//wenn Q dann zeichne ein Kreis
			case 'O':
				g2D.fillOval(getWidth() / 4 * i + getWidth() / 16, 200 + getWidth() / 16 + getWidth() / 32,
						getWidth() / 8, getWidth() / 16);
				break;
			}
		}
	}

}
