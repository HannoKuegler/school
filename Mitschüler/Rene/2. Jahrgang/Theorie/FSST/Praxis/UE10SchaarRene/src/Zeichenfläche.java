import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class Zeichenfläche extends JPanel implements ActionListener, MouseListener {

	Graphics2D g;
	int mouseX, mouseY;
	boolean click;
	int aufgabe1X, aufgabe1Y, aufgabe2X, aufgabe2Y;
	Timer aufgabe1T, aufgabe2T;

	public Zeichenfläche() {
		aufgabe1T = new Timer(25, this);
		aufgabe2T = new Timer(20, this);
		addMouseListener(this);
		aufgabe1T.start();
		aufgabe2T.start();
	}

	@Override
	protected void paintComponent(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paintComponent(arg0);
		g = (Graphics2D) arg0;

		aufgabe1Y = 0;
		g.setColor(new Color(0, 0, 255));
		g.fillRect(aufgabe1X, aufgabe1Y, 40, 40);

		if (click) {
			g.setColor(new Color(0, 255, 0));
			g.fillOval(aufgabe2X, aufgabe2Y, 20, 20);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// Aufgabe 1 - Quadrat 40px/s --> rechts
		if (aufgabe1T.isRunning()) {

			if (aufgabe1X <= getWidth() - 40)
				aufgabe1X += 1;
			else
				aufgabe1T.stop();
		}
		// Aufgabe 2 - Kreis von (X|Y) nach oben rechts
		if (aufgabe2T.isRunning()) {
			if (click) {
				if (getWidth() != aufgabe2X + 20)
					aufgabe2X += 1;
				if (getHeight() != getHeight() - aufgabe2Y)
					aufgabe2Y -= 1;

			}
		}

		repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		mouseX = e.getX();
		mouseY = e.getY();
		click = true;
		if (mouseX <= getWidth() - 20)
			aufgabe2X = mouseX;
		else
			aufgabe2X = mouseX + getWidth() - mouseX - 20;
		if (mouseY < getHeight())
			aufgabe2Y = mouseY;
		else
			aufgabe2Y = mouseY + getHeight() - mouseY;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
