package at.htlklu.fsst;

import java.util.Scanner;

public class Aufgabe06 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		char auswahl = '1';
		while (auswahl != '0') {
			System.out.println("Bitte wählen Sie zwischen den Aufgaben a - j (ohne h) (beenden = 0): ");
			auswahl = in.next().charAt(0);

			switch (auswahl) {
			case 'a':
				// Aufgabe 1a
				System.out.println("Aufgabe A\n");

				int a, b;
				System.out.println("Geben Sie bitte Werte für a und b ein.");
				a = in.nextInt();
				b = in.nextInt();

				System.out.println("Addition: " + (a + b));
				System.out.println("Division (a-b): " + (a - b));
				System.out.println("Division (b-a): " + (b - a));
				System.out.println("Multipikation: " + (a * b));
				if (b != 0)
					System.out.println("Divison (a/b): " + (a / b));
				if (a != 0)
					System.out.println("Divison (b/a): " + (b / a));

				System.out.println("");
				break;
			case 'b':
				// Aufgabe 1b
				System.out.println("Aufgabe B\n");

				double d1, d2;
				System.out.println("Geben Sie bitte Werte für d1 und d2 ein.");
				d1 = in.nextDouble();
				d2 = in.nextDouble();

				System.out.println("Addition: " + (d1 + d2));
				System.out.println("Division (d1-d2): " + (d1 - d2));
				System.out.println("Division (d2-d1): " + (d2 - d1));
				System.out.println("Multipikation: " + (d1 * d2));
				if (d2 != 0)
					System.out.println("Divison (d1/d2): " + (d1 / d2));
				if (d1 != 0)
					System.out.println("Divison (d2/d1): " + (d2 / d1));

				System.out.println("");
				break;
			case 'c':
				// Aufgabe 1c
				System.out.println("Aufgabe C\n");

				for (int i = 1; i <= 10; i++) {
					System.out.printf("Das ist die %d. Zeile\n", i);
				}

				System.out.println("");
				break;
			case 'd':
				// Aufgabe 1d
				System.out.println("Aufgabe D\n");

				double rand = 1;
				for (int i = 0; i < 15; i++) {
					rand = Math.random();
					System.out.print(rand + "  ");
					if (rand < 0.5)
						System.out.println("Die Zahl ist kleiner als 0,5");
					else if (rand == 0.5)
						System.out.println("Die Zahl ist 0,5 groß");
					else if (rand > 0.5)
						System.out.println("Die Zahl ist größer als 0,5");
					else {
						System.err.println("ERROR");
						Thread.sleep(333);
					}
				}

				System.out.println("");
				break;
			case 'e':
				// Aufgabe 1e
				System.out.println("Aufgabe E\n");

				for (int i = 1; i <= 10; i++) {
					for (int j = 1; j <= 10; j++) {
						System.out.print(i * j + " ");
					}
					System.out.println();
				}

				System.out.println("");
				break;
			case 'f':
				// Aufgabe 1f
				System.out.println("Aufgabe F\n");

				double notenstand = 0;
				int einser = 0, zweier = 0, dreier = 0, vierer = 0, fünfer = 0;
				System.out.println("Programm wird mit 0 beendet.");
				byte note = 6;
				for (int i = 0; note != 0; i++) {
					System.out.println("Bitte geben Sie ihre Note ein: ");
					note = in.nextByte();

					switch (note) {
					case 1:
						einser++;
						notenstand += note;
						break;
					case 2:
						zweier++;
						notenstand += note;
						break;
					case 3:
						dreier++;
						notenstand += note;
						break;
					case 4:
						vierer++;
						notenstand += note;
						break;
					case 5:
						fünfer++;
						notenstand += note;
						break;
					case 0:
						System.out.println("Ihr Notenstand: " + (notenstand / i));
						System.out.printf("Anzahl an:\nSehr Gut: %d\nGut: %d\nBefriedigend: %d\nGenügend: %d\nNicht Genügend: %d\n",
								einser, zweier, dreier, vierer, fünfer);
						break;
					default:
						System.err.println("Bitte geben Sie eine gültige Note ein");
						i--;
						break;
					}

				}
				System.out.println("");
				break;
			case 'g':
				// Aufgabe 1g
				System.out.println("Aufgabe G\n");

				int inp = -1;
				System.out.println("Programm wird mit 0 beendet.");
				while (inp != 0) {
					System.out.println("Bitte geben Sie eine Zahl ein: ");
					inp = in.nextInt();
					if (inp < 0) {
						System.err.println("Bitte gib eine positive Zahl ein.");
						Thread.sleep(333);
						continue;
					}
					if (inp == 0)
						break;
					if (inp % 2 == 0)
						System.out.println("Die Zahl ist gerade.");
					else
						System.out.println("Die Zahl ist ungerade");

				}
				System.out.println("");
				break;
			case 'i':
				// Aufgabe 1i
				System.out.println("Aufgabe I\n");

				System.out.println("Geben Sie bitte eine Zeichenkette ein: ");
				String input = in.next();
				String output = "";
				for (int i = 1; i <= input.length(); i++) {
					output += input.charAt(input.length() - i);
				}
				System.out.println(output + "\n\n\n");
				break;
			case '0':
				System.out.print("Programm wird beendet");
				Thread.sleep(333);
				System.out.print(".");
				Thread.sleep(333);
				System.out.print(".");
				Thread.sleep(333);
				System.out.print(".");
				break;
			case 'j':
				//Aufgabe J
				System.out.println("Aufgabe J\n");

				for (int i = 1; i <= 10; i++) {
					for (int j = 1; j <= 10; j++) {
						if ((i * j) <= 50)
						System.out.print(i * j + " ");
						else
							continue;
					}
					System.out.println();
				}

				System.out.println("");
				break;
			default:
				System.err.println("Bitte eine von den zur Verfügung stehenden Buchstaben benutzen. Danke.");
			}

			Thread.sleep(2000);
		}
		
		in.close();
	}

}
