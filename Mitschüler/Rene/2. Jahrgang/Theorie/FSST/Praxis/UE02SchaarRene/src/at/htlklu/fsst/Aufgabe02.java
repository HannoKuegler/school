package at.htlklu.fsst;

import java.util.Scanner;

public class Aufgabe02 {

	public static double eingabeA() {
		Scanner in = new Scanner(System.in);
		System.out.println("Bitte geben Sie bitte A ein:");
		double a = in.nextDouble();
		in.close();
		return a;
	}

	public static double eingabeB() {
		Scanner in = new Scanner(System.in);
		System.out.println("Bitte geben Sie bitte B ein:");
		double b = in.nextDouble();
		in.close();
		return b;
	}

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int operatoren = 5;
		double a = 0;
		double b = 0;
		while (operatoren != 0) {
			System.out.println("Bitte w�hlen Sie eine von den untenstehende Zahlen aus");
			System.out.println("0 = Programmende\n1 = Addition\n2 = Subtraktion\n3 = Multiplikation\n4 = Division");

			operatoren = in.nextInt();

			switch (operatoren) {
			case 0:
				System.out.println("Programm wird beendet");
				break;
			case 1:
				a = eingabeA();
				b = eingabeB();
				System.out.printf("%.2f + %.2f = %.2f\n", a, b, a + b);
				break;
			case 2:
				a = eingabeA();
				b = eingabeB();
				System.out.printf("%.2f - %.2f = %.2f\n", a, b, a - b);
				break;
			case 3:
				a = eingabeA();
				b = eingabeB();
				System.out.printf("%.2f * %.2f = %.2f\n", a, b, a * b);
				break;
			case 4:
				a = eingabeA();
				b = eingabeB();
				if (b == 0) {
					System.err.println("Es ist nicht m�glich durch 0 zu dividieren");
					Thread.sleep(333);
				} else
					System.out.printf("%.2f / %.2f = %.2f\n", a, b, a / b);
				break;
			default:
				System.err.println("Falsche Eingabe");
				Thread.sleep(333);
				break;
			}
		}
		in.close();
	}

}
