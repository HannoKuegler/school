import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class Zeichenfläche extends JPanel {

	int[] x2 = { 250, 250, 300, 375, 375 };
	int[] y2 = { 50, 100, 75, 50, 100 };
	int[] s = { 20, 20, 40, 20, 20 };
	Color[] c2 = { Color.GREEN, Color.GREEN, Color.RED, Color.GREEN, Color.GREEN };

	@Override
	protected void paintComponent(Graphics arg) {
		super.paintComponent(arg);
		Graphics2D g = (Graphics2D) arg;

		for (int i = 0; i < 256; i++) {
			g.setColor(new Color(0, i, (255-i)));
			g.drawLine((int)(i * 1.7), 0, (int)(i*1.7), getHeight());

		}

//		for (int i = 0; i < 20; i++) {
//			drawCircle(g, Color.BLUE, 100, 100, i * 5);
//		}
//
//		for (int i = 0; i < 5; i++) {
//			g.setColor(c2[i]);
//			g.fillRect(x2[i], y2[i], s[i], s[i]);
//		}
//
//	}
//
//	void drawCircle(Graphics2D g, Color c, int x, int y, int r) {
//		g.setColor(c);
//		g.drawOval(x - r, y - r, 2 * r, 2 * r);
	}

	/**
	 * Create the panel.
	 */
	public Zeichenfläche() {

	}

}
