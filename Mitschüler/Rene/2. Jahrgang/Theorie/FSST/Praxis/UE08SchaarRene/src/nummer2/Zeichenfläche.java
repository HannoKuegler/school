package nummer2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;

import javax.swing.JPanel;
import javax.swing.border.StrokeBorder;

public class Zeichenfläche extends JPanel {

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2);
		g2d.setColor(new Color(0, 0, 255));
		g2d.setStroke(new BasicStroke(1f));
		g2d.drawLine(0, 0, getWidth(), getHeight());
		g2d.setStroke(new BasicStroke(5f));
		g2d.drawLine(0, getHeight(), getWidth(), 0);
		g2d.setColor(new Color(0, 255, 0));
		g2d.fillRect(getWidth() / 2, getHeight() / 2, 100, 50);
		g2d.setFont(new Font("Arial", Font.BOLD, 25));
		g2d.setColor(new Color(255, 0, 0));
		g2d.drawString("FSST", 20, getHeight() / 2);
	}

	/**
	 * Create the panel.
	 */
	public Zeichenfläche() {

	}

}
