import java.util.Scanner;

public class Aufgabe01 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		Scanner in = new Scanner(System.in);
		boolean neustart = true;

		do {

			System.out.println("Welche Aufgabe wollen Sie abspielen (a-i):");
			String aufgabeStr = in.next().toLowerCase();
			int aufgabeInt = String.valueOf(aufgabeStr).charAt(0);

			if (aufgabeInt >= ((int) 'a') && aufgabeInt <= ((int) 'i')) {

				// Aufgabe a
				if (aufgabeInt == (int) 'a') {

					int a = 5, b = 6;

					System.out.println("Summe :" + (a + b));
					Thread.sleep(333);
					System.out.println("Differenz :" + (a - b));
					Thread.sleep(333);
					System.out.println("Produkt :" + (a * b));
					Thread.sleep(333);
					System.out.println("Quotient: " + ((double) a / (double) b));
					Thread.sleep(333);

					Thread.sleep(1500);
					System.out.println("Wollen Sie das Programm neustarten?");
					char neustartStr = in.next().charAt(0);

					if (neustartStr == 'j' || neustartStr == 'J' || neustartStr == 'Y' || neustartStr == 'y') {
						System.out.println();
					} else {
						neustart = false;
					}

					// Aufgabe b
				} else if (aufgabeInt == (int) 'b') {

					double d1 = 6, d2 = 12;

					System.out.println("Summe :" + (d1 + d2));
					Thread.sleep(333);
					System.out.println("Differenz :" + (d1 - d2));
					Thread.sleep(333);
					System.out.println("Produkt :" + (d1 * d2));
					Thread.sleep(333);
					System.out.println("Quotient: " + (d1 / d2));
					Thread.sleep(333);

					Thread.sleep(1000);
					System.out.println("Wollen Sie das Programm neustarten?");
					char neustartStr = in.next().charAt(0);

					if (neustartStr == 'j' || neustartStr == 'J' || neustartStr == 'Y' || neustartStr == 'y') {
						System.out.println();
					} else {
						neustart = false;
					}

					// Aufgabe c
				} else if (aufgabeInt == (int) 'c') {

					for (int i = 1; i <= 10; i++) {

						System.out.println("Das ist die " + i + "te Zeile");
						Thread.sleep(333);

					}

					Thread.sleep(1500);
					System.out.println("Wollen Sie das Programm neustarten?");
					char neustartStr = in.next().charAt(0);
					Thread.sleep(500);

					if (neustartStr == 'j' || neustartStr == 'J' || neustartStr == 'Y' || neustartStr == 'y') {
						System.out.println();
					} else {
						neustart = false;
					}

					// Aufgabe d
				} else if (aufgabeInt == (int) 'd') {

					byte gr��er = 0, kleiner = 0, gleich = 0;

					for (int i = 0; i < 15; i++) {

						double random = Math.random();
						System.out.print(random + " ");

						if (random > 0.5) {
							System.out.println("> 0,5");
							gr��er++;
						} else if (random == 0.5) {
							System.out.println("= 0,5");
							gleich++;
						} else if (random < 0.5) {
							System.out.println("< 0,5");
							kleiner++;
						}
						Thread.sleep(333);
					}
					Thread.sleep(999);
					System.out.println(gr��er + " Zahlen waren gr��er, " + gleich + " waren gleich und " + kleiner
							+ " waren kleiner als 0,5");

					Thread.sleep(2000);
					System.out.println("Wollen Sie das Programm neustarten?");
					char neustartStr = in.next().charAt(0);
					Thread.sleep(500);

					if (neustartStr == 'j' || neustartStr == 'J' || neustartStr == 'Y' || neustartStr == 'y') {
						System.out.println();
					} else {
						neustart = false;
					}
				} // Aufgabe e
				else if (aufgabeInt == (int) 'e') {

					for (int i = 0; i <= 100; i++) {

						System.out.println("1 mal " + i + " = " + i);
						Thread.sleep(50);
					}

					Thread.sleep(1500);
					System.out.println("Wollen Sie das Programm neustarten?");
					char neustartStr = in.next().charAt(0);
					Thread.sleep(500);

					if (neustartStr == 'j' || neustartStr == 'J' || neustartStr == 'Y' || neustartStr == 'y') {
						System.out.println();
					} else {
						neustart = false;
					}
				} // Aufgabe f
				else if (aufgabeInt == (int) 'f') {

					byte eins = 0, zwei = 0, drei = 0, vier = 0, f�nf = 0;
					byte i = 0, note = 0;

					do {
						System.out.println("Geben Sie ihre Noten ein (0 = Durchschnitt berechnen):");
						note = in.nextByte();
						if (note == 0) {
							break;
						} else if (note == 1) {
							eins++;
						} else if (note == 2) {
							zwei++;
						} else if (note == 3) {
							drei++;
						} else if (note == 4) {
							vier++;
						} else if (note == 5) {
							f�nf++;
						}

						i++;

					} while (note >= 0 && note <= 5);

					System.out.println("Eingegebene Noten:");
					Thread.sleep(333);
					System.out.println("Sehr Gut: " + eins);
					Thread.sleep(333);
					System.out.println("Gut: " + zwei);
					Thread.sleep(333);
					System.out.println("Befriedigend: " + drei);
					Thread.sleep(333);
					System.out.println("Gen�gend: " + vier);
					Thread.sleep(333);
					System.out.println("Nicht Gen�gend: " + f�nf);
					Thread.sleep(500);
					System.out.println("Somit betr�gt ihr Notendurchschnitt: "
							+ ((double) ((eins * 1) + (zwei * 2) + (drei * 3) + (vier * 4) + (f�nf * 5)) / i));

					Thread.sleep(1500);
					System.out.println("Wollen Sie das Programm neustarten?");
					char neustartStr = in.next().charAt(0);
					Thread.sleep(500);

					if (neustartStr == 'j' || neustartStr == 'J' || neustartStr == 'Y' || neustartStr == 'y') {
						System.out.println();
					} else {
						neustart = false;
					}

					// Aufgabe g
				} else if (aufgabeInt == (int) 'g') {

					int gerade = 0, ungerade = 0, zahl = 0;

					do {

						System.out.println("Geben Sie eine ganzzahlige Zahl ein (0 = beenden):");
						zahl = in.nextByte();

						if (zahl % 2 == 0 && zahl != 0) {
							Thread.sleep(333);
							System.out.println("Diese Zahl ist gerade");
							System.out.println();
							gerade++;
							Thread.sleep(333);
						} else if (zahl % 2 != 0 && zahl != 0) {
							Thread.sleep(333);
							System.out.println("Diese Zahl ist ungerade");
							System.out.println();
							ungerade++;
							Thread.sleep(333);
						}
					} while (zahl != 0);

					System.out.println("Es wurden:");
					System.out.println(gerade + " Gerade und " + ungerade + " ungerade Zahlen eingegeben");
					Thread.sleep(666);

					Thread.sleep(1500);
					System.out.println("Wollen Sie das Programm neustarten?");
					char neustartStr = in.next().charAt(0);

					if (neustartStr == 'j' || neustartStr == 'J' || neustartStr == 'Y' || neustartStr == 'y') {
						System.out.println();
					} else {
						neustart = false;
					}
				} else // Aufgabe h
				if (aufgabeInt == (int) 'h') {

					int ergebnis = 1;

					System.out.println("Legen Sie eine nat�rliche Zahl fest (max. 10):");
					int fakult�t = in.nextInt();
					for (int i = 1; i <= fakult�t; i++) {
						ergebnis = ergebnis * i;
						if (i < fakult�t) {
							System.out.print(i + " * ");
						} else {
							System.out.print(i);
						}
					}

					System.out.println(" = " + ergebnis);

					Thread.sleep(1500);
					System.out.println("Wollen Sie das Programm neustarten?");
					char neustartStr = in.next().charAt(0);

					if (neustartStr == 'j' || neustartStr == 'J' || neustartStr == 'Y' || neustartStr == 'y') {
						System.out.println();
					} else {
						neustart = false;
					}
				} else // Aufgabe i
				if (aufgabeInt == (int) 'i') {

					System.out.println("Geben Sie eine Zeichenkette ein:");
					String zeichenkette = in.next();
					int l�nge = zeichenkette.length();
					char invertedAktuell = ' ';
					int aktuell = 0;

					for (int i = 0; i < l�nge; i++) {
						aktuell = l�nge - i - 1;
						invertedAktuell = zeichenkette.charAt(aktuell);
						if (i < l�nge - 1) {
							System.out.print(invertedAktuell);
						} else
							System.out.println(invertedAktuell);
					}

					Thread.sleep(1500);
					System.out.println("Wollen Sie das Programm neustarten?");
					char neustartStr = in.next().charAt(0);

					if (neustartStr == 'j' || neustartStr == 'J' || neustartStr == 'Y' || neustartStr == 'y') {
						System.out.println();
					} else {
						neustart = false;
					}
				}
			} else {
				System.out.println("Geben Sie bitte einen g�ltigen Buchstaben ein!");
				Thread.sleep(1000);
				System.out.println();
			}
		} while (neustart == true);

		System.err.println("Das Programm ist zu Ende");

		in.close();

	}

}
