import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

public class Zeichenfläche extends JPanel {

	/**
	 * Create the panel.
	 */
	public Zeichenfläche() {

	}

	@Override
	protected void paintComponent(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paintComponent(arg0);
		Graphics2D g = (Graphics2D) arg0;

		g.drawImage(Bilder.bg, 0, 0, 1920, 1080, null);
		
		repaint();
	}

}
