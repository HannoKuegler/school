import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Bilder {

	static BufferedImage bg, mj, ms, mr;

	public static void BilderLaden() {
		try {
			bg = ImageIO.read(new File("rsc/bg1.png"));
			mj = ImageIO.read(new File("rsc/mariojump.png"));
			ms = ImageIO.read(new File("rsc/mariostand.png"));
			mr = ImageIO.read(new File("rsc/mariorunning.png"));
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Bilder konnten nicht geladen werden");
		}
	}

}
