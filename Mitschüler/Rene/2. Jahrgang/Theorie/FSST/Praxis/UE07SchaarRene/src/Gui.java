import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Gui extends JFrame {

	private JPanel contentPane;
	private Zeichenfläche zeichenfläche;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Bilder.BilderLaden();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Gui() {
		initComponents();
	}

	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 400, 400);
		setLocationRelativeTo(null);
		setLayout(null);
		setResizable(true);
		setTitle("Mario");
		requestFocus();

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		zeichenfläche = new Zeichenfläche();
		contentPane.add(zeichenfläche, BorderLayout.CENTER);
	}

}
