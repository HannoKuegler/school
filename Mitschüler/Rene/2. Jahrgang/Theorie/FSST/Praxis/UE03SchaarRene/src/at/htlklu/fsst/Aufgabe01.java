package at.htlklu.fsst;

import java.util.Random;

public class Aufgabe01 {

	public static void main(String[] args) {

		// printStar();
		// System.out.println();
		//
		// printIstGleich();
		// System.out.println();
		//
		// slash();
		// System.out.println();
		//
		// printDiamond();
		// System.out.println();
		//
		// printMuster();
		// System.out.println();
		//
		// printRandom(5);
		// System.out.println();
		//
		// printHexToDec("FA");
		//
		// printHexToDec2("FA");
		printDiamond2();
	}

	public static void printStar() {
		for (int i = 0; i < 20; i++)
			System.out.print("*");
		System.out.println("");
	}

	public static void printIstGleich() {
		for (int i = 0; i < 10; i++)
			System.out.print(" ");
		for (int i = 0; i < 10; i++)
			System.out.print("=");
		System.out.println("");
	}

	public static void slash() {
		for (int i = 0; i < 12; i++)
			System.out.print(" ");
		System.out.println("*");
		for (int i = 0; i < 8; i++)
			System.out.print(" ");
		System.out.println("*");
		for (int i = 0; i < 4; i++)
			System.out.print(" ");
		System.out.println("*");
		System.out.println("*");
	}

	public static void printDiamond() {
		for (int i = 0; i < 12; i++)
			System.out.print(" ");
		System.out.println("*");
		for (int i = 0; i < 8; i++)
			System.out.print(" ");
		System.out.print("*");
		for (int i = 0; i < 7; i++)
			System.out.print(" ");
		System.out.println("*");
		for (int i = 0; i < 4; i++)
			System.out.print(" ");
		System.out.print("*");
		for (int i = 0; i < 15; i++)
			System.out.print(" ");
		System.out.println("*");
		System.out.print("*");
		for (int i = 0; i < 23; i++)
			System.out.print(" ");
		System.out.println("*");
		for (int i = 0; i < 4; i++)
			System.out.print(" ");
		System.out.print("*");
		for (int i = 0; i < 15; i++)
			System.out.print(" ");
		System.out.println("*");
		for (int i = 0; i < 8; i++)
			System.out.print(" ");
		System.out.print("*");
		for (int i = 0; i < 7; i++)
			System.out.print(" ");
		System.out.println("*");
		for (int i = 0; i < 12; i++)
			System.out.print(" ");
		System.out.println("*");
	}

	private static void printDiamond2() {

		
		for (int zeilen = 3; zeilen >= 0; zeilen--) {
			for (int spalten = 0; spalten < zeilen; spalten++) {
				System.out.print("	");
			}
			if (zeilen != 3)
				System.out.print("*");
			for (int spalten = 0; spalten < 2 * (3 - zeilen); spalten++) {
				System.out.print("	");
			}
			System.out.print("*\n");
		}
		for (int zeilen = 3; zeilen >= 0; zeilen--) {
			for (int spalten = 0; spalten > zeilen; spalten--) {
				System.out.print("	");
			}
			if (zeilen != 3 || zeilen != 0)
				System.out.print("*");
			for (int spalten = 0; spalten > 2 * (3 - zeilen); spalten--) {
				System.out.print("	");
			}
			if (zeilen != 3)
			System.out.print("*\n");
		}
	}

	public static void printMuster() {
		printStar();

		slash();

		printIstGleich();

		printDiamond();

		printStar();
	}

	public static void printRandom(int amount) {
		Random ran = new Random();
		int rand = 0;

		for (int i = 0; i < amount; i++) {
			rand = ran.nextInt(10);
			if ((i - 2) < amount)
				System.out.print(rand + ", ");
			else
				System.out.print(rand);
		}

	}

	public static void printHexToDec(String hexWert) {
		System.out.println(Integer.parseInt(hexWert, 16));

	}

	public static void printHexToDec2(String hexWert) {
		int dez = 0;
		int multiplier = 16;

		for (int i = 0; i < hexWert.length(); i++) {
			multiplier = (int) Math.pow(multiplier, i);
			switch (hexWert.charAt(hexWert.length() - 1 - i)) {
			case '1':
				dez += 1 * multiplier;
				break;
			case '2':
				dez += 2 * multiplier;
				break;
			case '3':
				dez += 3 * multiplier;
				break;
			case '4':
				dez += 4 * multiplier;
				break;
			case '5':
				dez += 5 * multiplier;
				break;
			case '6':
				dez += 6 * multiplier;
				break;
			case '7':
				dez += 7 * multiplier;
				break;
			case '8':
				dez += 8 * multiplier;
				break;
			case '9':
				dez += 9 * multiplier;
				break;
			case 'A':
				dez += 10 * multiplier;
				break;
			case 'B':
				dez += 11 * multiplier;
				break;
			case 'C':
				dez += 13 * multiplier;
				break;
			case 'D':
				dez += 14 * multiplier;
				break;
			case 'E':
				dez += 15 * multiplier;
				break;
			case 'F':
				dez += 16 * multiplier;
				break;
			default:
				System.err.println("Bitte eine Zahl in Hexadezimal angeben.");
				break;
			}
		}
		System.out.println(dez);
	}

}
