import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;
import java.awt.event.MouseMotionAdapter;

public class Zeichenfl�che extends JPanel implements MouseMotionListener {

	int breite, h�he, xPos, yPos;

	/**
	 * Create the panel.
	 */
	public Zeichenfl�che() {

		initComponents();
	}

	private void initComponents() {
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				thisMouseMoved(e);
			}
		});
	}

	@Override
	protected void paintComponent(Graphics arg) {
		// TODO Auto-generated method stub
		super.paintComponent(arg);
		Graphics2D g = (Graphics2D) arg;
		breite = getWidth();
		h�he = getHeight();
		int quadrate = 10;

		// Hintergrund
		g.setColor(new Color(0, 0, 255));
		g.fillRect(0, 0, breite, h�he);

		// Raster
		raster(quadrate, Color.black, g);

		// Funktion
		g.setColor(new Color(0, 255, 0));
		int a = xPos / (breite / quadrate);
		int b = yPos / (h�he / quadrate);
		g.fillRect(breite / quadrate * a, h�he / quadrate * b, breite / quadrate + 1, h�he / quadrate + 1);
	}

	/**
	 * Diese Methode zeichnet ein Quadratraster belieber Seitenl�nge und Farbe
	 * 
	 * @param quadrate Seitenl�nge
	 * @param c        Farbe
	 * @param g        Grafikvariable
	 */
	public void raster(int quadrate, Color c, Graphics2D g) {
		g.setColor(c);
		for (int j = 0; j < quadrate; j++) {
			for (int i = 0; i < quadrate; i++) {
				g.drawRect(i * breite / quadrate, j * h�he / quadrate, breite / quadrate, h�he / quadrate);
			}
		}
	}

	protected void thisMouseMoved(MouseEvent e) {
		xPos = e.getX();
		yPos = e.getY();
		repaint();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
