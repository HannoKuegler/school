import java.util.Scanner;

public class UE4Aufgabe5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner in = new Scanner(System.in);

			System.out.println("Legen Sie die Punkteanzahl fest:");
			double punkte = in.nextDouble();
	
			if ((punkte < 0)||(punkte > 100)) {
				System.out.println("Syntax Error");
			} else {
				if (punkte <= 50) {
					System.out.println("Nicht Gen�gend :(");
				} else {
					if ((punkte > 50)&&(punkte <= 62)) {
						System.out.println("Gen�gend :|");
					} else {
						if((punkte > 62)&&(punkte <= 75)){
							System.out.println("Befriedigend :|");
						} else {
							if((punkte > 75)&&(punkte <= 87)){
								System.out.println("Gut :)");
							} else {
								if(punkte > 87){
									System.out.println("Sehr Gut :D");
								}
							}
						}
					}
				}
			}

		in.close();

	}

}
