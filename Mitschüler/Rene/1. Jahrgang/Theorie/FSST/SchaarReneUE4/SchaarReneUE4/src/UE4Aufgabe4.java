import java.util.Scanner;

public class UE4Aufgabe4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner in = new Scanner(System.in);

			System.out.println("Legen Sie die Punkteanzahl fest:");
			double punkte = in.nextDouble();
	
			if ((punkte < 0) || (punkte > 100)) {
				System.out.println("Syntax Error");
			} else {
				if (punkte < 50) {
					System.out.println("Nicht bestanden :(");
				} else {
					if (punkte >= 50) {
						System.out.println("Bestanden :)");
					}
				}
			}

		in.close();

	}

}
