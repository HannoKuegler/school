import java.util.Scanner;

public class UE5Aufgabe6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	Scanner in = new Scanner(System.in);
		
		System.out.println("Legen Sie bitte eine natürliche Zahl fest:");
		int number = in.nextInt();
		
		if(number == 1){
			System.out.println("Das ist die Eins");
		} else {
			if(number == 2){
				System.out.println("Das ist die Zwei");
			} else {
				if(number == 3){
					System.out.println("Das ist die Drei");
				} else {
					System.out.println("Diese Zahl kenne ich nicht");
				}
			}
		}
		
	in.close();

	}

}
