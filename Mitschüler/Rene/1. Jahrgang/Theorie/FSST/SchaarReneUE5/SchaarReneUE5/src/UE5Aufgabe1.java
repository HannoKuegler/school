import java.util.Scanner;

public class UE5Aufgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		
			System.out.println("Legen Sie bitte eine natürliche Zahl fest:");
			int number = in.nextInt();
			
			switch (number) 
			{
				case 1:	System.out.println("Das ist die Eins"); break;
				case 2: System.out.println("Das ist die Zwei"); break;
				case 3: System.out.println("Das ist die Drei"); break;
				default: System.out.println("Diese Zahl kenne ich nicht");
			}
		
		in.close();
		

	}

}
