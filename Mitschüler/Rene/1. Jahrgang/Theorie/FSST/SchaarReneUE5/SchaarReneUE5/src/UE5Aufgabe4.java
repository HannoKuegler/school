import java.util.Scanner;

public class UE5Aufgabe4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Legen Sie eine Himmelsrichtung mit den Buchstaben N,O,S und W fest");
		char richtung = in.next().charAt(0);
		
		switch (richtung)
		{
			case 'N': System.out.println("Die Himmelsrichtung lautet: Norden");	break;
			case 'O': System.out.println("Die Himmelsrichtung lautet: Osten");	break;
			case 'S': System.out.println("Die Himmelsrichtung lautet: S�den");	break;
			case 'W': System.out.println("Die Himmelsrichtung lautet: Westen");	break;
			case 'n': System.out.println("Die Himmelsrichtung lautet: Norden");	break;
			case 'o': System.out.println("Die Himmelsrichtung lautet: Osten");	break;
			case 's': System.out.println("Die Himmelsrichtung lautet: S�den");	break;
			case 'w': System.out.println("Die Himmelsrichtung lautet: Westen");	break;
			default: System.out.println("Okey interessante Himmelsrichtung");
		}
	
	in.close();
	
	}

}
