import java.util.Scanner;

public class UE2Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
	Scanner in = new Scanner(System.in);
		
	//Methode 1
	
		System.out.println("Methode 1:");
		
		System.out.println("Geben Sie Ihre erste Note ein:");
		int ersteNote = in.nextInt();
		System.out.println("Geben Sie Ihre zweite Note ein:");
		int zweiteNote = in.nextInt();
		System.out.println("Geben Sie Ihre dritte Note ein:");
		int dritteNote = in.nextInt();
		
		double endNoteA= (ersteNote+zweiteNote+dritteNote)/3.0;
		
		System.out.println("");
		System.out.println("Ihre Endnote ist eine: "+endNoteA);
		System.out.println("");
		System.out.println("Ende der ersten Methode");
		
	//Methode 2
		System.out.println("");
		System.out.println("Methode 2:");
		
		System.out.println("Geben Sie Ihre erste Note ein:");
		double noteEins = in.nextDouble();
		System.out.println("Geben Sie Ihre zweite Note ein:");
		double noteZwei = in.nextDouble();
		System.out.println("Geben Sie Ihre dritte Note ein:");
		double noteDrei = in.nextDouble();
		
		double endNoteB= (noteEins+noteZwei+noteDrei)/3;
		
		System.out.println("Ihre Endnote ist ein: "+endNoteB);
		System.out.println("");
		System.out.println("Ende der zweiten Methode");
	
		
	in.close();
		
		
	}

}
