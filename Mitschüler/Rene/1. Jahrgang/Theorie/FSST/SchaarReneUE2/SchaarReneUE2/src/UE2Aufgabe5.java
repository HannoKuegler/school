import java.util.Scanner;

public class UE2Aufgabe5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		
			System.out.println("Legen Sie fest, wieviele Stunden Sie heute gearbeitet haben:");
			double tagA = in.nextDouble();
			System.out.println("Legen Sie fest, wieviele Stunden Sie morgen arbeiten werden:");
			double tagB = in.nextDouble();
			System.out.println("Legen Sie fest, wieviele Stunden Sie gestern gearbeitet haben:");
			double tagC = in.nextDouble();
		
			double arbeitsZeitDurchschnittlich = (tagA+tagB+tagC)/3;
			
			System.out.println("Legen Sie fest, wieviele Wochen Sie im Jahr arbeiten");
			double hochrechnungsFaktor = in.nextDouble();
			
			double hochrechnung = (arbeitsZeitDurchschnittlich*7)*hochrechnungsFaktor;
			
			System.out.println("Sie arbeiten, hochgerechnet, "+hochrechnung+" Stunden im Jahr arbeiten");
			
			
		in.close();

		}

}
