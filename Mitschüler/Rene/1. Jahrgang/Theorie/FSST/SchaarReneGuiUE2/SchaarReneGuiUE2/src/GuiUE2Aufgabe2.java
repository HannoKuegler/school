import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.naming.BinaryRefAddr;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.function.BinaryOperator;
import java.awt.event.ActionEvent;
import javax.swing.ButtonGroup;

public class GuiUE2Aufgabe2 extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JRadioButton rdbtnBin;
	private JRadioButton rdbtnHex;
	private JRadioButton rdbtnDez;
	private JLabel lblVariableEins;
	private JLabel lblVariableZwei;
	private JTextField txtVariableEins;
	private JTextField txtVariableZwei;
	private JLabel lblErgebnis;
	private JTextField txtErgebnis;
	private JButton btnPlus;
	private JButton btnModulo;
	private JButton btnMinus;
	private JButton btnMal;
	private JButton btnGeteilt;

	boolean bin = false;
	boolean dez = false;
	boolean hex = false;
	private JButton btnClearAll;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuiUE2Aufgabe2 frame = new GuiUE2Aufgabe2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GuiUE2Aufgabe2() {
		initComponents();
	}

	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Zahlensystem", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 564, 83);
		contentPane.add(panel);
		panel.setLayout(null);

		rdbtnBin = new JRadioButton("2 / Bin\u00E4r");
		buttonGroup.add(rdbtnBin);
		rdbtnBin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rdbtnBinActionPerformed(e);
			}
		});
		rdbtnBin.setBounds(6, 30, 109, 23);
		panel.add(rdbtnBin);

		rdbtnHex = new JRadioButton("16 / Hexadezimal");
		buttonGroup.add(rdbtnHex);
		rdbtnHex.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bin = false;
				dez = false;
				hex = true;
			}
		});
		rdbtnHex.setBounds(449, 30, 109, 23);
		panel.add(rdbtnHex);

		rdbtnDez = new JRadioButton("10 / Dezimal");
		buttonGroup.add(rdbtnDez);
		rdbtnDez.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bin = false;
				dez = true;
				hex = false;
			}
		});
		rdbtnDez.setBounds(229, 30, 109, 23);
		panel.add(rdbtnDez);

		panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Input", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 105, 564, 163);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		lblVariableEins = new JLabel("Variable Eins");
		lblVariableEins.setBounds(10, 65, 100, 14);
		panel_1.add(lblVariableEins);

		lblVariableZwei = new JLabel("Variable Zwei");
		lblVariableZwei.setBounds(10, 90, 100, 14);
		panel_1.add(lblVariableZwei);

		txtVariableEins = new JTextField();
		txtVariableEins.setBounds(110, 62, 86, 20);
		panel_1.add(txtVariableEins);
		txtVariableEins.setColumns(10);

		txtVariableZwei = new JTextField();
		txtVariableZwei.setBounds(110, 90, 86, 20);
		panel_1.add(txtVariableZwei);
		txtVariableZwei.setColumns(10);

		panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Output", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 279, 564, 83);
		contentPane.add(panel_2);
		panel_2.setLayout(null);

		lblErgebnis = new JLabel("Ergebnis");
		lblErgebnis.setBounds(10, 36, 100, 14);
		panel_2.add(lblErgebnis);

		txtErgebnis = new JTextField();
		txtErgebnis.setBounds(110, 33, 86, 20);
		panel_2.add(txtErgebnis);
		txtErgebnis.setColumns(10);

		panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Operatoren", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_3.setBounds(10, 373, 564, 77);
		contentPane.add(panel_3);
		panel_3.setLayout(null);

		btnPlus = new JButton("+");
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnPlusActionPerformed(e);
			}
		});
		btnPlus.setBounds(10, 27, 50, 23);
		panel_3.add(btnPlus);

		btnModulo = new JButton("%");
		btnModulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnModuloActionPerformed(e);
			}
		});
		btnModulo.setBounds(465, 27, 60, 23);
		panel_3.add(btnModulo);

		btnMinus = new JButton("-");
		btnMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnMinusActionPerformed(e);
			}
		});
		btnMinus.setBounds(70, 27, 50, 23);
		panel_3.add(btnMinus);

		btnMal = new JButton("*");
		btnMal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnMalActionPerformed(e);
			}
		});
		btnMal.setBounds(130, 27, 50, 23);
		panel_3.add(btnMal);

		btnGeteilt = new JButton("/");
		btnGeteilt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGeteiltActionPerformed(e);
			}
		});
		btnGeteilt.setBounds(190, 27, 50, 23);
		panel_3.add(btnGeteilt);

		btnClearAll = new JButton("Clear all");
		btnClearAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnClearAllActionPerformed(e);
			}
		});
		btnClearAll.setBounds(303, 27, 89, 23);
		panel_3.add(btnClearAll);
	}

	protected void rdbtnBinActionPerformed(ActionEvent e) {
		bin = true;
		dez = false;
		hex = false;
	}

	protected void btnPlusActionPerformed(ActionEvent e) {
		if (dez == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins);
			int iVariableZwei = Integer.parseInt(sVariableZwei);
			int iErgebnis = iVariableEins + iVariableZwei;
			String sErgebnis = "" + iErgebnis;
			txtErgebnis.setText(sErgebnis);
		} else if (bin == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins, 2);
			int iVariableZwei = Integer.parseInt(sVariableZwei, 2);
			int iErgebnis = iVariableEins + iVariableZwei;
			String sErgebnis = Integer.toBinaryString(iErgebnis);
			txtErgebnis.setText(sErgebnis);
		} else if (hex == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins, 16);
			int iVariableZwei = Integer.parseInt(sVariableZwei, 16);
			int iErgebnis = iVariableEins + iVariableZwei;
			String sErgebnis = Integer.toHexString(iErgebnis);
			txtErgebnis.setText(sErgebnis);
		}
	}

	protected void btnClearAllActionPerformed(ActionEvent e) {
		txtErgebnis.setText("");
		txtVariableEins.setText("");
		txtVariableZwei.setText("");
	}

	protected void btnMinusActionPerformed(ActionEvent e) {
		if (dez == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins);
			int iVariableZwei = Integer.parseInt(sVariableZwei);
			int iErgebnis = iVariableEins - iVariableZwei;
			String sErgebnis = "" + iErgebnis;
			txtErgebnis.setText(sErgebnis);
		} else if (bin == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins, 2);
			int iVariableZwei = Integer.parseInt(sVariableZwei, 2);
			int iErgebnis = iVariableEins - iVariableZwei;
			String sErgebnis = Integer.toBinaryString(iErgebnis);
			txtErgebnis.setText(sErgebnis);
		} else if (hex == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins, 16);
			int iVariableZwei = Integer.parseInt(sVariableZwei, 16);
			int iErgebnis = iVariableEins - iVariableZwei;
			String sErgebnis = Integer.toHexString(iErgebnis);
			txtErgebnis.setText(sErgebnis);
		}
	}

	protected void btnMalActionPerformed(ActionEvent e) {
		if (dez == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins);
			int iVariableZwei = Integer.parseInt(sVariableZwei);
			int iErgebnis = iVariableEins * iVariableZwei;
			String sErgebnis = "" + iErgebnis;
			txtErgebnis.setText(sErgebnis);
		} else if (bin == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins, 2);
			int iVariableZwei = Integer.parseInt(sVariableZwei, 2);
			int iErgebnis = iVariableEins * iVariableZwei;
			String sErgebnis = Integer.toBinaryString(iErgebnis);
			txtErgebnis.setText(sErgebnis);
		} else if (hex == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins, 16);
			int iVariableZwei = Integer.parseInt(sVariableZwei, 16);
			int iErgebnis = iVariableEins * iVariableZwei;
			String sErgebnis = Integer.toHexString(iErgebnis);
			txtErgebnis.setText(sErgebnis);
		}
	}

	protected void btnGeteiltActionPerformed(ActionEvent e) {
		String sVariableEins = txtVariableEins.getText();
		String sVariableZwei = txtVariableZwei.getText();
		int iVariableEins = Integer.parseInt(sVariableEins);
		int iVariableZwei = Integer.parseInt(sVariableZwei);
		boolean div = false;
		if(iVariableZwei == 0) {
			System.err.println("Error");
		} else {
			div = true;
		}

		if (dez == true && div == true) {
			
			if (iVariableZwei == 0) {
				double iErgebnis = iVariableEins / iVariableZwei;
				String sErgebnis = "" + iErgebnis;
				txtErgebnis.setText(sErgebnis);
			} else if (bin == true && div == true) {
				int iVariableEins2 = Integer.parseInt(sVariableEins, 2);
				int iVariableZwei2 = Integer.parseInt(sVariableZwei, 2);
				double iErgebnis = iVariableEins2 / iVariableZwei2;
				String sErgebnis = Integer.toBinaryString((int) iErgebnis);
				txtErgebnis.setText(sErgebnis);
			} else if (hex == true && div == true) {
				int iVariableEins3 = Integer.parseInt(sVariableEins, 16);
				int iVariableZwei3 = Integer.parseInt(sVariableZwei, 16);
				double iErgebnis = iVariableEins3 / iVariableZwei3;
				String sErgebnis = Integer.toHexString((int) iErgebnis);
				txtErgebnis.setText(sErgebnis);
			}
		}
	}

	protected void btnModuloActionPerformed(ActionEvent e) {
		if (dez == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins);
			int iVariableZwei = Integer.parseInt(sVariableZwei);
			int iErgebnis = iVariableEins % iVariableZwei;
			String sErgebnis = "" + iErgebnis;
			txtErgebnis.setText(sErgebnis);
		} else if (bin == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins, 2);
			int iVariableZwei = Integer.parseInt(sVariableZwei, 2);
			int iErgebnis = iVariableEins % iVariableZwei;
			String sErgebnis = Integer.toBinaryString(iErgebnis);
			txtErgebnis.setText(sErgebnis);
		} else if (hex == true) {
			String sVariableEins = txtVariableEins.getText();
			String sVariableZwei = txtVariableZwei.getText();
			int iVariableEins = Integer.parseInt(sVariableEins, 16);
			int iVariableZwei = Integer.parseInt(sVariableZwei, 16);
			int iErgebnis = iVariableEins % iVariableZwei;
			String sErgebnis = Integer.toHexString(iErgebnis);
			txtErgebnis.setText(sErgebnis);
		}
	}
}
