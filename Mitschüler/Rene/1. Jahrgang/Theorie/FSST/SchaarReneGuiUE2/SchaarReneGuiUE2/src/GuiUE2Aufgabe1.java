import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GuiUE2Aufgabe1 extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JLabel lblErsteVariable;
	private JLabel lblZweiteVariable;
	private JTextField txtVariableEins;
	private JTextField txtVariableZwei;
	private JLabel lblErgebnis;
	private JTextField txtErgebnis;
	private JButton btnPlus;
	private JButton btnMinus;
	private JButton btnMal;
	private JButton btnGeteilt;
	private JButton btnModulo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuiUE2Aufgabe1 frame = new GuiUE2Aufgabe1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GuiUE2Aufgabe1() {
		initComponents();
	}

	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Input", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 564, 145);
		contentPane.add(panel);
		panel.setLayout(null);

		lblErsteVariable = new JLabel("Erste Variable");
		lblErsteVariable.setBounds(10, 55, 100, 14);
		panel.add(lblErsteVariable);

		lblZweiteVariable = new JLabel("Zweite Variable");
		lblZweiteVariable.setBounds(10, 80, 100, 14);
		panel.add(lblZweiteVariable);

		txtVariableEins = new JTextField();
		txtVariableEins.setBounds(120, 52, 86, 20);
		panel.add(txtVariableEins);
		txtVariableEins.setColumns(10);

		txtVariableZwei = new JTextField();
		txtVariableZwei.setBounds(120, 77, 86, 20);
		panel.add(txtVariableZwei);
		txtVariableZwei.setColumns(10);

		panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Output", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(10, 164, 564, 145);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		lblErgebnis = new JLabel("Ergebnis");
		lblErgebnis.setBounds(10, 64, 100, 14);
		panel_1.add(lblErgebnis);

		txtErgebnis = new JTextField();
		txtErgebnis.setEditable(false);
		txtErgebnis.setBounds(120, 61, 86, 20);
		panel_1.add(txtErgebnis);
		txtErgebnis.setColumns(10);

		panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Operatoren", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(10, 314, 564, 136);
		contentPane.add(panel_2);
		panel_2.setLayout(null);

		btnPlus = new JButton("+");
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnPlusActionPerformed(arg0);
			}
		});
		btnPlus.setBounds(10, 54, 50, 23);
		panel_2.add(btnPlus);

		btnMinus = new JButton("-");
		btnMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnMinusActionPerformed(e);
			}
		});
		btnMinus.setBounds(70, 54, 50, 23);
		panel_2.add(btnMinus);

		btnMal = new JButton("*");
		btnMal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnMalActionPerformed(e);
			}
		});
		btnMal.setBounds(130, 54, 50, 23);
		panel_2.add(btnMal);

		btnGeteilt = new JButton("/");
		btnGeteilt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGeteiltActionPerformed(e);
			}
		});
		btnGeteilt.setBounds(190, 54, 50, 23);
		panel_2.add(btnGeteilt);

		btnModulo = new JButton("%");
		btnModulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnModuloActionPerformed(e);
			}
		});
		btnModulo.setBounds(504, 54, 50, 23);
		panel_2.add(btnModulo);
	}

	protected void btnPlusActionPerformed(ActionEvent arg0) {
		String sVariableEins = txtVariableEins.getText();
		String sVariableZwei = txtVariableZwei.getText();
		int iVariableEins = Integer.parseInt(sVariableEins);
		int iVariableZwei = Integer.parseInt(sVariableZwei);
		int iErgebnis = iVariableEins + iVariableZwei;
		String sErgebnis = "" + iErgebnis;
		txtErgebnis.setText(sErgebnis);

	}

	protected void btnMinusActionPerformed(ActionEvent e) {
		String sVariableEins = txtVariableEins.getText();
		String sVariableZwei = txtVariableZwei.getText();
		int iVariableEins = Integer.parseInt(sVariableEins);
		int iVariableZwei = Integer.parseInt(sVariableZwei);
		int iErgebnis = iVariableEins - iVariableZwei;
		String sErgebnis = "" + iErgebnis;
		txtErgebnis.setText(sErgebnis);
	}

	protected void btnMalActionPerformed(ActionEvent e) {
		String sVariableEins = txtVariableEins.getText();
		String sVariableZwei = txtVariableZwei.getText();
		int iVariableEins = Integer.parseInt(sVariableEins);
		int iVariableZwei = Integer.parseInt(sVariableZwei);
		int iErgebnis = iVariableEins * iVariableZwei;
		String sErgebnis = "" + iErgebnis;
		txtErgebnis.setText(sErgebnis);
	}

	protected void btnGeteiltActionPerformed(ActionEvent e) {
		String sVariableEins = txtVariableEins.getText();
		String sVariableZwei = txtVariableZwei.getText();
		double dVariableEins = Double.parseDouble(sVariableEins);
		Double dVariableZwei = Double.parseDouble(sVariableZwei);
		Double dErgebnis = dVariableEins / dVariableZwei;
		String sErgebnis = "" + dErgebnis;
		txtErgebnis.setText(sErgebnis);
	}

	protected void btnModuloActionPerformed(ActionEvent e) {
		String sVariableEins = txtVariableEins.getText();
		String sVariableZwei = txtVariableZwei.getText();
		int iVariableEins = Integer.parseInt(sVariableEins);
		int iVariableZwei = Integer.parseInt(sVariableZwei);
		int iErgebnis = iVariableEins % iVariableZwei;
		String sErgebnis = "" + iErgebnis;
		txtErgebnis.setText(sErgebnis);
	}
}
