import java.util.Scanner;

public class UE7AufgabeL {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		
			System.out.println("Legen Sie eine Start und eine Endvariable fest:");
			int start = in.nextInt();
			int end = in.nextInt();
			int i = 0;
			int x = start;
			
			if(start < end || start == end || start < 0 || end < 0){
				System.out.println("ERROR");
			} else {
				while(i <= (start-end)){
					System.out.print(x + " ");
					x--;
					i++;
				}
			}
			
		in.close();

	}

}
