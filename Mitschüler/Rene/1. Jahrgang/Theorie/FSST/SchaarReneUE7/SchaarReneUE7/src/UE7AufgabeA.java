import java.util.Scanner;

public class UE7AufgabeA {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Anzahl an Sternen, die augegeben werden soll, ein.");
		int sterne = in.nextInt();
		
		while(sterne > 0){
			System.out.println("*");
			sterne = sterne-1;
		}
		
		if(sterne == 0){
			System.out.println("Jetzt bin ich nutzlos");
		}
		
		in.close();

	}

}
