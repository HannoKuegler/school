import java.util.Scanner;

public class ZusatzUE4Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner in = new Scanner(System.in);

			System.out.println("Legen Sie bitte 2 Zahlen fest:");
			double ersterWert = in.nextDouble();
			double zweiterWert = in.nextDouble();
			System.out.println("Sie haben nun folgende Möglichkeiten:");
			System.out.println("Sie addieren indem Sie 1 eingeben");
			System.out.println("Sie subtrahieren den ersten mit den zweiten Wert indem Sie 2 eingeben");
			System.out.println("Sie dividieren den ersten durch den zweiten Wert indem Sie 3 eingeben");
			System.out.println("Sie multiplizieren indem Sie 4 eingeben");
			System.out.println("Sie mudulorieren indem Sie 5 eingeben");
			System.out.println("Geben Sie nun Ihre Auswahl ein:");
			short answer = in.nextShort();
	
			if (((answer == 2) || (answer == 3) || (answer == 5)) && (zweiterWert < 1)) {
				System.out.println("Syntax Error");
			} else {
				if ((answer <= 0) || (answer >= 6)) {
					System.out.println("Ich habe Ihnen nur Möglichkeiten von 1-5 gegeben");
				} else {
					if (answer == 1) {
						double summe = ersterWert + zweiterWert;
						System.out.printf("Die Summe ist %.3f", summe);
					} else {
						if (answer == 2) {
							double differenz = ersterWert - zweiterWert;
							System.out.printf("Die Differenz ist %3.f", differenz);
						} else {
							if (answer == 3) {
								double quotient = ersterWert / zweiterWert;
								System.out.printf("Der Quotient ist %.3f", quotient);
							} else {
								if (answer == 4) {
									double produkt = ersterWert * zweiterWert;
									System.out.printf("Das Produkt sit %.3f", produkt);
								} else {
									if (answer == 5) {
										double modulo = ersterWert % zweiterWert;
										System.out.printf("Das Modulo ist %.3f", modulo);
	
									}
								}
							}
						}
					}
				}
			}

		in.close();

	}

}