import java.util.Scanner;

public class ZusatzUE4Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		
			System.out.println("Montag = 1 | Dienstag = 2 |... | Sonntag = 7");
			System.out.println("Welchen Wochentag haben wir heute?");
			short day = in.nextShort();
			
			if((day <= 0)||(day >= 8)){
				System.out.println("Gott hat die Welt aber nur in 7 Tagen erschaffen. Den "+day+"ten Tag kenne ich garnicht");
			} else {
				if(day == 1){
					System.out.println("Echt! Erst Montag?");
				} else {
					if(day == 2){
						System.out.println("Ok Dienstag geht ja noch");
					} else {
						if(day == 3){
							System.out.println("Ja mit dem Mittwoch bin ich zufrieden.");
						} else {
							if(day == 4){
								System.out.println("Wenn heute Donnerstag ist, ist Morgen ja schon Freitag");
							} else {
								if(day == 5){
									System.out.println("Huhu Freitag!");
								} else {
									System.out.println("HOCH DIE H�NDE WOCHENENDE!");
								}
							}
						}
					}
				} 
			}
		
		in.close();

	}

}
