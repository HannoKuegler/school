import java.util.Random;
public class UE8Aufgabe4 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		Random rd = new Random();
		
		byte i = 0;
		int gesamtAugenZahl = 0;
		int wuerfelNummerEins = 0;
		int wuerfelNummerZwei = 0;
		
		do {
			
			wuerfelNummerEins = 1 + rd.nextInt(6);
			wuerfelNummerZwei = 1 + rd.nextInt(6);
			gesamtAugenZahl = wuerfelNummerEins+wuerfelNummerZwei;
			System.out.print(" " + wuerfelNummerEins + " + " + wuerfelNummerZwei + " ;");
			i++;
			if(i % 10 == 0) {
				System.out.println();
			}
			
		} while(gesamtAugenZahl != 11);
		
		System.out.println("\nKATZTASTISCH! Wir haben eine 11 gew�rfelt");
		Thread.sleep(1000);
		System.out.println("Es dauerte nur " + i + " Versuche");
		Thread.sleep(500);
		System.out.println("Wie cool ist das denn!");
		
	}

}
