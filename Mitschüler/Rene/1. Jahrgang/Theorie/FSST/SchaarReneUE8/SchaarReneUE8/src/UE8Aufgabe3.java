import java.util.Scanner;

public class UE8Aufgabe3 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		Scanner in = new Scanner(System.in);

		int i = 0;
		byte note = 0;
		double ergebnis = 0;
		double stand = 0;
		boolean fehler = false;
		
// Erkl�rung
		
		System.out.println("Wilkommen");
		Thread.sleep(750);
		System.out.println("Brauchen Sie eine Einf�hrung?");
		char einf�hrung = in.next().charAt(0);
		
		if(einf�hrung == 'j' || einf�hrung == 'J' || einf�hrung == 'y' || einf�hrung == 'Y'){
			Thread.sleep(750);
			System.out.println("Sie k�nnen hier Ihren Notendurchschnitt berechnen lassen.");
			Thread.sleep(2000);
			System.out.println("Wie cool ist das denn!");
			Thread.sleep(1500);
			System.out.println("Hier eine kurze Erkl�rung:");
			Thread.sleep(1500);
				System.out.println(" -Jedes mal wenn Sie aufgefordert werden, geben Sie nur eine Note ein");
				Thread.sleep(3000);
				System.out.println(" -Sollten Sie alle Ihre Noten eingegeben haben, tippen Sie eine 0 ein und Ihr Durschnitt wird berechnet!");
				Thread.sleep(5000);
				System.out.println("Wie cool ist das denn!");
				Thread.sleep(1250);
		}
		
		Thread.sleep(750);
		
// Ausf�hrung
			
			do{
				if(fehler == false){
					System.out.println("\nGeben Sie eine Note ein:");
					note = in.nextByte();
					if(note >= 1 && note <= 5){
						ergebnis = ergebnis + note;
						Thread.sleep(750);
						stand = ergebnis/++i;
						System.out.printf("Aktueller Stand: %.2f ",stand);
						Thread.sleep(750);
					} else if(note != 0){
						fehler = true;
						System.err.println("Error");
					} else {
						stand = ergebnis/++i;
					}
				} 
				
				if(note == 0){
					fehler = false;
				}
				
			} while(note != 0 && fehler == false);

			Thread.sleep(500);
			if(fehler == false){
				System.out.println("\nSo lautet Ihr Durschnitt: " + ergebnis/--i);
			}
		in.close();

	}

}
