package aufgabe1;

import java.util.Random;

public class Aufgabe1H {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Random rd = new Random();
		
		for (int i = 0; i < 30; i++){
			int r = 1 + rd.nextInt(10);
			if(r % 6 == 0){
				System.out.print("‹bersprungen ");
				continue;
			}
			
			System.out.print(r + " ");
		}
		
	}

}
