package aufgabe5;

import javax.swing.JOptionPane;

public class Aufgabe5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String sa = JOptionPane.showInputDialog("Legen Sie eine ganze Zahl fest:");
		int zahl = (int) Double.parseDouble(sa);
		
		for (int out = 1; out <= zahl; out++) {
			for (int i = 1; i <= out; i++) {
				System.out.print(i + " ");
			}
			
			System.out.println();
		}

	}

}
