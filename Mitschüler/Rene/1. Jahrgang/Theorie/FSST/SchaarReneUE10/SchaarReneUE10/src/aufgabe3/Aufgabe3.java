package aufgabe3;

import javax.swing.JOptionPane;

public class Aufgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double x = 0;
		double a = 0;

		String sa = JOptionPane.showInputDialog("Legen Sie eine ganze Zahl fest:");
		int zahl = (int) Double.parseDouble(sa);
		a = zahl;

		for (int i = 1; a > 10; i++) {

			x = a % 10;
			a = a - x;
			a = a / 10;

			if (a <= 10) {
				JOptionPane.showMessageDialog(null, "Eingabe: " + zahl + "\nAusgabe: " + ++i, "Anzahl der Ziffern" ,
						JOptionPane.INFORMATION_MESSAGE);
			}

		}

		if (zahl < 10) {
			JOptionPane.showMessageDialog(null, "Eingabe: " + zahl + "\nAusgabe: 1" , "Anzahl der Ziffern" ,
						JOptionPane.INFORMATION_MESSAGE);
		}
	}

}
