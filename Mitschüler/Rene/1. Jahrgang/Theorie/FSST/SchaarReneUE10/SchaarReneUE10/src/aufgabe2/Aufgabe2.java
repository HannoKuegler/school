package aufgabe2;

import javax.swing.JOptionPane;

public class Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int summe = 0;
		
		String sa = JOptionPane.showInputDialog("Geben Sie eine positive Zahl ein:");
		int a = (int) Double.parseDouble(sa);
		
		if(a >= 0){
			for(int i = 1; i <= a
					; i++){
				summe += i;
				if(i == a){
					System.out.print(i);
				} else 
				System.out.print(i + " + ");
			}
			System.out.print(" = " + summe);
		}
		
	}

}
