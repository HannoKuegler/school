import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class JGuiUE3Aufgabe1 extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JPanel panel_1;
	private JLabel lblTextA;
	private JLabel lblTextB;
	private JTextField txtA;
	private JTextField txtB;
	private JButton btnClear;
	private JButton btnConcat;
	private JTextField txtConcat;
	private JButton btnUppercaseA;
	private JButton btnLowercaseA;
	private JTextField txtUpperLowerA;
	private JButton btnUppercaseB;
	private JButton btnLowercaseB;
	private JTextField txtUpperLowerB;
	private JButton btnLengthOfA;
	private JButton btnLengthOfB;
	private JTextField txtLengthOfA;
	private JTextField txtLengthOfB;
	private JButton btnTrimA;
	private JTextField txtTrimmedA;
	private JButton btnTrimB;
	private JTextField txtTrimmedB;
	private JLabel lblLebngthOfTrimmed;
	private JLabel lblLengthOfTrimmed;
	private JTextField txtLengthOfTrimmedA;
	private JTextField txtLengthOfTrimmedB;
	private JButton btnNewButton;
	private JCheckBox chckbxEquals;
	private JCheckBox chckbxEqualIgnoreCase;
	private JButton btnSubstringA;
	private JLabel lblBegin;
	private JTextField txtBeginA;
	private JLabel lblEnd;
	private JTextField txtEndA;
	private JTextField txtSubstringA;
	private JButton btnSubstringB;
	private JLabel label;
	private JTextField txtBeginB;
	private JLabel label_1;
	private JTextField txtEndB;
	private JTextField txtSubstringB;
	private JButton btnClearAll;
	private JLabel lblCompare;
	private JLabel label_3;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JButton btnCharAtA;
	private JButton btnCharAtB;
	private JTextField txtIndexA;
	private JTextField txtIndexB;
	private JLabel lblIndex;
	private JLabel label_2;
	private JLabel lblChar;
	private JLabel lblChar_1;
	private JTextField txtCharA;
	private JTextField txtCharB;
	private JButton btnisEmptyA;
	private JButton btnIsEmptyB;
	private JRadioButton rdnbtnIsEmptyAYes;
	private JRadioButton rdnbtnIsEmptyANo;
	private JRadioButton rdnbtnIsEmptyBYes;
	private JRadioButton rdnbtnIsEmptyBNo;
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private final ButtonGroup buttonGroup_3 = new ButtonGroup();
	private JButton btnReplaceAIn;
	private JButton btnReplaceBIn;
	private JButton btnContain;
	private JRadioButton rdnbtnContainYes;
	private JRadioButton rdnbtnContainNo;
	private JButton btnHashCodeA;
	private JTextField txtHashCodeA;
	private JButton btnHashCodeB;
	private JTextField txtHashCodeB;
	private JButton btnSplitA;
	private JButton btnSplitB;
	private JTextField txtSplitB;
	private JPanel panel_2;
	private JTextField txtSplitA;
	private JButton btnValueofA;
	private JTextField txtValueOfA;
	private JButton btnValueofB;
	private JTextField txtValueOfB;
	private JButton btnGetbytesOfA;
	private JTextField txtBytesOfA;
	private JButton btnGetbytesOfB;
	private JTextField txtBytesOfB;
	private JTextField txtReplaxeAInB;
	private JTextField txtReplaxeBInA;
	private JTextField txtWithA;
	private JTextField txtWithB;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JGuiUE3Aufgabe1 frame = new JGuiUE3Aufgabe1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JGuiUE3Aufgabe1() {
		initComponents();
	}
	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1200, 730);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Input", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 1164, 82);
		contentPane.add(panel);
		panel.setLayout(null);
		
		lblTextA = new JLabel("Text A");
		lblTextA.setBounds(10, 24, 46, 14);
		panel.add(lblTextA);
		
		lblTextB = new JLabel("Text B");
		lblTextB.setBounds(10, 49, 46, 14);
		panel.add(lblTextB);
		
		txtA = new JTextField();
		txtA.setBounds(66, 21, 160, 20);
		panel.add(txtA);
		txtA.setColumns(10);
		
		txtB = new JTextField();
		txtB.setBounds(66, 46, 160, 20);
		panel.add(txtB);
		txtB.setColumns(10);
		
		btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnClearActionPerformed(e);
			}
		});
		btnClear.setBounds(236, 32, 89, 23);
		panel.add(btnClear);
		
		btnClearAll = new JButton("Clear All");
		btnClearAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnClearAllActionPerformed(arg0);
			}
		});
		btnClearAll.setBounds(1065, 45, 89, 23);
		panel.add(btnClearAll);
		
		panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Vorgegeben Funktionen", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(10, 104, 564, 576);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		btnConcat = new JButton("Concat");
		btnConcat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnConcatActionPerformed(arg0);
			}
		});
		btnConcat.setBounds(10, 25, 130, 23);
		panel_1.add(btnConcat);
		
		txtConcat = new JTextField();
		txtConcat.setBounds(150, 26, 370, 20);
		panel_1.add(txtConcat);
		txtConcat.setColumns(10);
		
		btnUppercaseA = new JButton("Uppercase A");
		btnUppercaseA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUppercaseAActionPerformed(e);
			}
		});
		btnUppercaseA.setBounds(10, 59, 130, 23);
		panel_1.add(btnUppercaseA);
		
		btnLowercaseA = new JButton("Lowercase A");
		btnLowercaseA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnLowercaseAActionPerformed(e);
			}
		});
		btnLowercaseA.setBounds(150, 59, 130, 23);
		panel_1.add(btnLowercaseA);
		
		txtUpperLowerA = new JTextField();
		txtUpperLowerA.setBounds(289, 60, 231, 20);
		panel_1.add(txtUpperLowerA);
		txtUpperLowerA.setColumns(10);
		
		btnUppercaseB = new JButton("Uppercase B");
		btnUppercaseB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUppercaseBActionPerformed(e);
			}
		});
		btnUppercaseB.setBounds(10, 93, 130, 23);
		panel_1.add(btnUppercaseB);
		
		btnLowercaseB = new JButton("Lowercase B");
		btnLowercaseB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnLowercaseBActionPerformed(arg0);
			}
		});
		btnLowercaseB.setBounds(150, 93, 130, 23);
		panel_1.add(btnLowercaseB);
		
		txtUpperLowerB = new JTextField();
		txtUpperLowerB.setColumns(10);
		txtUpperLowerB.setBounds(289, 94, 231, 20);
		panel_1.add(txtUpperLowerB);
		
		btnLengthOfA = new JButton("Length of A");
		btnLengthOfA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnLengthOfAActionPerformed(e);
			}
		});
		btnLengthOfA.setBounds(10, 127, 130, 23);
		panel_1.add(btnLengthOfA);
		
		btnLengthOfB = new JButton("Length of B");
		btnLengthOfB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnLengthOfBActionPerformed(e);
			}
		});
		btnLengthOfB.setBounds(10, 161, 130, 23);
		panel_1.add(btnLengthOfB);
		
		txtLengthOfA = new JTextField();
		txtLengthOfA.setBounds(150, 127, 130, 20);
		panel_1.add(txtLengthOfA);
		txtLengthOfA.setColumns(10);
		
		txtLengthOfB = new JTextField();
		txtLengthOfB.setBounds(150, 162, 130, 20);
		panel_1.add(txtLengthOfB);
		txtLengthOfB.setColumns(10);
		
		btnTrimA = new JButton("Trim A");
		btnTrimA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnTrimAActionPerformed(e);
			}
		});
		btnTrimA.setBounds(10, 195, 130, 23);
		panel_1.add(btnTrimA);
		
		txtTrimmedA = new JTextField();
		txtTrimmedA.setBounds(150, 196, 130, 20);
		panel_1.add(txtTrimmedA);
		txtTrimmedA.setColumns(10);
		
		btnTrimB = new JButton("Trim B");
		btnTrimB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnTrimBActionPerformed(e);
			}
		});
		btnTrimB.setBounds(10, 229, 130, 23);
		panel_1.add(btnTrimB);
		
		txtTrimmedB = new JTextField();
		txtTrimmedB.setColumns(10);
		txtTrimmedB.setBounds(150, 230, 130, 20);
		panel_1.add(txtTrimmedB);
		
		lblLebngthOfTrimmed = new JLabel("Length of Trimmed A:");
		lblLebngthOfTrimmed.setBounds(305, 199, 130, 14);
		panel_1.add(lblLebngthOfTrimmed);
		
		lblLengthOfTrimmed = new JLabel("Length of Trimmed B:");
		lblLengthOfTrimmed.setBounds(305, 233, 130, 14);
		panel_1.add(lblLengthOfTrimmed);
		
		txtLengthOfTrimmedA = new JTextField();
		txtLengthOfTrimmedA.setBounds(434, 196, 86, 20);
		panel_1.add(txtLengthOfTrimmedA);
		txtLengthOfTrimmedA.setColumns(10);
		
		txtLengthOfTrimmedB = new JTextField();
		txtLengthOfTrimmedB.setColumns(10);
		txtLengthOfTrimmedB.setBounds(434, 230, 86, 20);
		panel_1.add(txtLengthOfTrimmedB);
		
		btnNewButton = new JButton("Compare A B");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnNewButtonActionPerformed(e);
			}
		});
		btnNewButton.setBounds(10, 263, 130, 23);
		panel_1.add(btnNewButton);
		
		chckbxEquals = new JCheckBox("equals");
		buttonGroup_1.add(chckbxEquals);
		chckbxEquals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chckbxEqualsActionPerformed(e);
			}
		});
		chckbxEquals.setBounds(150, 263, 70, 23);
		panel_1.add(chckbxEquals);
		
		chckbxEqualIgnoreCase = new JCheckBox("equal ignore case");
		buttonGroup_1.add(chckbxEqualIgnoreCase);
		chckbxEqualIgnoreCase.setBounds(222, 263, 130, 23);
		panel_1.add(chckbxEqualIgnoreCase);
		
		btnSubstringA = new JButton("Substring A");
		btnSubstringA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSubstringAActionPerformed(e);
			}
		});
		btnSubstringA.setBounds(10, 297, 130, 23);
		panel_1.add(btnSubstringA);
		
		lblBegin = new JLabel("Begin:");
		lblBegin.setBounds(149, 301, 35, 14);
		panel_1.add(lblBegin);
		
		txtBeginA = new JTextField();
		txtBeginA.setBounds(185, 298, 20, 20);
		panel_1.add(txtBeginA);
		txtBeginA.setColumns(10);
		
		lblEnd = new JLabel("End:");
		lblEnd.setBounds(209, 301, 35, 14);
		panel_1.add(lblEnd);
		
		txtEndA = new JTextField();
		txtEndA.setColumns(10);
		txtEndA.setBounds(245, 298, 20, 20);
		panel_1.add(txtEndA);
		
		txtSubstringA = new JTextField();
		txtSubstringA.setBounds(275, 298, 255, 20);
		panel_1.add(txtSubstringA);
		txtSubstringA.setColumns(10);
		
		btnSubstringB = new JButton("Substring B");
		btnSubstringB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSubstringBActionPerformed(arg0);
			}
		});
		btnSubstringB.setBounds(10, 331, 130, 23);
		panel_1.add(btnSubstringB);
		
		label = new JLabel("Begin:");
		label.setBounds(149, 335, 35, 14);
		panel_1.add(label);
		
		txtBeginB = new JTextField();
		txtBeginB.setColumns(10);
		txtBeginB.setBounds(185, 332, 20, 20);
		panel_1.add(txtBeginB);
		
		label_1 = new JLabel("End:");
		label_1.setBounds(209, 335, 35, 14);
		panel_1.add(label_1);
		
		txtEndB = new JTextField();
		txtEndB.setColumns(10);
		txtEndB.setBounds(245, 332, 20, 20);
		panel_1.add(txtEndB);
		
		txtSubstringB = new JTextField();
		txtSubstringB.setColumns(10);
		txtSubstringB.setBounds(275, 332, 255, 20);
		panel_1.add(txtSubstringB);
		
		lblCompare = new JLabel("");
		lblCompare.setEnabled(false);
		lblCompare.setBounds(379, 267, 86, 14);
		panel_1.add(lblCompare);
		
		label_3 = new JLabel("");
		label_3.setBounds(350, 267, 46, 14);
		panel_1.add(label_3);
		
		btnCharAtA = new JButton("Char at (A)");
		btnCharAtA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnCharAtAActionPerformed(arg0);
			}
		});
		btnCharAtA.setBounds(10, 365, 130, 23);
		panel_1.add(btnCharAtA);
		
		btnCharAtB = new JButton("Char at (B)");
		btnCharAtB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnCharAtBActionPerformed(arg0);
			}
		});
		btnCharAtB.setBounds(10, 399, 130, 23);
		panel_1.add(btnCharAtB);
		
		txtIndexA = new JTextField();
		txtIndexA.setBounds(185, 366, 20, 20);
		panel_1.add(txtIndexA);
		txtIndexA.setColumns(10);
		
		txtIndexB = new JTextField();
		txtIndexB.setColumns(10);
		txtIndexB.setBounds(185, 400, 20, 20);
		panel_1.add(txtIndexB);
		
		lblIndex = new JLabel("Index:");
		lblIndex.setBounds(150, 369, 46, 14);
		panel_1.add(lblIndex);
		
		label_2 = new JLabel("Index:");
		label_2.setBounds(150, 403, 46, 14);
		panel_1.add(label_2);
		
		lblChar = new JLabel("Char:");
		lblChar.setBounds(209, 369, 46, 14);
		panel_1.add(lblChar);
		
		lblChar_1 = new JLabel("Char:");
		lblChar_1.setBounds(209, 403, 46, 14);
		panel_1.add(lblChar_1);
		
		txtCharA = new JTextField();
		txtCharA.setColumns(10);
		txtCharA.setBounds(245, 366, 20, 20);
		panel_1.add(txtCharA);
		
		txtCharB = new JTextField();
		txtCharB.setColumns(10);
		txtCharB.setBounds(245, 402, 20, 20);
		panel_1.add(txtCharB);
		
		btnisEmptyA = new JButton("isEmpty A");
		btnisEmptyA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnisEmptyAActionPerformed(e);
			}
		});
		btnisEmptyA.setBounds(10, 433, 130, 23);
		panel_1.add(btnisEmptyA);
		
		btnIsEmptyB = new JButton("isEmpty B");
		btnIsEmptyB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnIsEmptyBActionPerformed(e);
			}
		});
		btnIsEmptyB.setBounds(10, 467, 130, 23);
		panel_1.add(btnIsEmptyB);
		
		rdnbtnIsEmptyAYes = new JRadioButton("yes");
		rdnbtnIsEmptyAYes.setBounds(150, 433, 46, 23);
		panel_1.add(rdnbtnIsEmptyAYes);
		
		rdnbtnIsEmptyANo = new JRadioButton("no");
		rdnbtnIsEmptyANo.setBounds(209, 433, 46, 23);
		panel_1.add(rdnbtnIsEmptyANo);
		
		rdnbtnIsEmptyBYes = new JRadioButton("yes");
		rdnbtnIsEmptyBYes.setBounds(150, 467, 46, 23);
		panel_1.add(rdnbtnIsEmptyBYes);
		
		rdnbtnIsEmptyBNo = new JRadioButton("no");
		rdnbtnIsEmptyBNo.setBounds(209, 467, 46, 23);
		panel_1.add(rdnbtnIsEmptyBNo);
		
		btnReplaceAIn = new JButton("Replace A in B");
		btnReplaceAIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnReplaceAInActionPerformed(e);
			}
		});
		btnReplaceAIn.setBounds(10, 501, 130, 23);
		panel_1.add(btnReplaceAIn);
		
		btnReplaceBIn = new JButton("Replace B in A");
		btnReplaceBIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnReplaceBInActionPerformed(e);
			}
		});
		btnReplaceBIn.setBounds(10, 535, 130, 23);
		panel_1.add(btnReplaceBIn);
		
		txtReplaxeAInB = new JTextField();
		txtReplaxeAInB.setBounds(245, 502, 130, 20);
		panel_1.add(txtReplaxeAInB);
		txtReplaxeAInB.setColumns(10);
		
		txtReplaxeBInA = new JTextField();
		txtReplaxeBInA.setColumns(10);
		txtReplaxeBInA.setBounds(245, 536, 130, 20);
		panel_1.add(txtReplaxeBInA);
		
		txtWithA = new JTextField();
		txtWithA.setText("with");
		txtWithA.setBounds(150, 502, 86, 20);
		panel_1.add(txtWithA);
		txtWithA.setColumns(10);
		
		txtWithB = new JTextField();
		txtWithB.setText("with");
		txtWithB.setBounds(150, 536, 86, 20);
		panel_1.add(txtWithB);
		txtWithB.setColumns(10);
		
		panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Eigene Funktionen", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(584, 104, 590, 576);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		btnContain = new JButton("Contains A B");
		btnContain.setBounds(10, 26, 130, 23);
		panel_2.add(btnContain);
		
		rdnbtnContainYes = new JRadioButton("yes");
		rdnbtnContainYes.setBounds(146, 26, 46, 23);
		panel_2.add(rdnbtnContainYes);
		
		rdnbtnContainNo = new JRadioButton("no");
		rdnbtnContainNo.setBounds(194, 26, 46, 23);
		panel_2.add(rdnbtnContainNo);
		
		btnHashCodeA = new JButton("Hash Code A");
		btnHashCodeA.setBounds(10, 60, 130, 23);
		panel_2.add(btnHashCodeA);
		
		txtHashCodeA = new JTextField();
		txtHashCodeA.setBounds(146, 61, 130, 20);
		panel_2.add(txtHashCodeA);
		txtHashCodeA.setColumns(10);
		
		btnHashCodeB = new JButton("Hash Code B");
		btnHashCodeB.setBounds(10, 94, 130, 23);
		panel_2.add(btnHashCodeB);
		
		btnSplitA = new JButton("Split A");
		btnSplitA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSplitAActionPerformed(e);
			}
		});
		btnSplitA.setBounds(10, 128, 130, 23);
		panel_2.add(btnSplitA);
		
		btnSplitB = new JButton("Split B");
		btnSplitB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSplitBActionPerformed(e);
			}
		});
		btnSplitB.setBounds(10, 162, 130, 23);
		panel_2.add(btnSplitB);
		
		txtHashCodeB = new JTextField();
		txtHashCodeB.setBounds(146, 95, 130, 20);
		panel_2.add(txtHashCodeB);
		txtHashCodeB.setColumns(10);
		
		txtSplitB = new JTextField();
		txtSplitB.setBounds(146, 163, 434, 20);
		panel_2.add(txtSplitB);
		txtSplitB.setColumns(10);
		
		txtSplitA = new JTextField();
		txtSplitA.setBounds(146, 128, 434, 20);
		panel_2.add(txtSplitA);
		txtSplitA.setColumns(10);
		
		btnValueofA = new JButton("ValueOf A");
		btnValueofA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnValueofAActionPerformed(e);
			}
		});
		btnValueofA.setBounds(10, 196, 130, 23);
		panel_2.add(btnValueofA);
		
		txtValueOfA = new JTextField();
		txtValueOfA.setBounds(146, 194, 130, 20);
		panel_2.add(txtValueOfA);
		txtValueOfA.setColumns(10);
		
		btnValueofB = new JButton("ValueOf B");
		btnValueofB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnValueofBActionPerformed(e);
			}
		});
		btnValueofB.setBounds(10, 230, 130, 23);
		panel_2.add(btnValueofB);
		
		txtValueOfB = new JTextField();
		txtValueOfB.setColumns(10);
		txtValueOfB.setBounds(146, 231, 130, 20);
		panel_2.add(txtValueOfB);
		
		btnGetbytesOfA = new JButton("GetBytes of A");
		btnGetbytesOfA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGetbytesOfAActionPerformed(e);
			}
		});
		btnGetbytesOfA.setBounds(10, 264, 130, 23);
		panel_2.add(btnGetbytesOfA);
		
		txtBytesOfA = new JTextField();
		txtBytesOfA.setBounds(146, 265, 130, 20);
		panel_2.add(txtBytesOfA);
		txtBytesOfA.setColumns(10);
		
		btnGetbytesOfB = new JButton("GetBytes of B");
		btnGetbytesOfB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGetbytesOfBActionPerformed(e);
			}
		});
		btnGetbytesOfB.setBounds(10, 298, 130, 23);
		panel_2.add(btnGetbytesOfB);
		
		txtBytesOfB = new JTextField();
		txtBytesOfB.setColumns(10);
		txtBytesOfB.setBounds(146, 299, 130, 20);
		panel_2.add(txtBytesOfB);
		btnHashCodeB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHashCodeBActionPerformed(e);
			}
		});
		btnHashCodeA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnHashCodeAActionPerformed(e);
			}
		});
		btnContain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnContainAActionPerformed(e);
			}
		});
	}
	protected void btnConcatActionPerformed(ActionEvent arg0) {
		String textA = txtA.getText();
		String textB = txtB.getText();
		String textC = textA.concat(textB);
		
		txtConcat.setText(textC);
		
		//txtConcat.setText(txtA.getText().concat(txtB.getText()));
	}
	
	protected void btnClearActionPerformed(ActionEvent e) {
		txtA.setText("");
		txtB.setText("");
	}
	protected void btnUppercaseAActionPerformed(ActionEvent e) {
		txtUpperLowerA.setText(txtA.getText().toUpperCase());
	}
	protected void btnLowercaseAActionPerformed(ActionEvent e) {
		txtUpperLowerA.setText(txtA.getText().toLowerCase());
	}
	protected void btnUppercaseBActionPerformed(ActionEvent e) {
		txtUpperLowerB.setText(txtB.getText().toUpperCase());
	}
	protected void btnLowercaseBActionPerformed(ActionEvent arg0) {
		txtUpperLowerB.setText(txtB.getText().toLowerCase());
	}
	protected void btnLengthOfAActionPerformed(ActionEvent e) {
		txtLengthOfA.setText(String.valueOf(txtA.getText().length()));
	}
	protected void btnLengthOfBActionPerformed(ActionEvent e) {
		txtLengthOfB.setText(String.valueOf(txtB.getText().length()));
	}
	protected void btnTrimAActionPerformed(ActionEvent e) {
		txtTrimmedA.setText(txtA.getText().trim());
		txtLengthOfTrimmedA.setText(String.valueOf(txtA.getText().trim().length()));
	}
	protected void btnTrimBActionPerformed(ActionEvent e) {
		txtTrimmedB.setText(txtB.getText().trim());
		txtLengthOfTrimmedB.setText(String.valueOf(txtB.getText().trim().length()));
	}
	protected void btnNewButtonActionPerformed(ActionEvent e) {
		if(chckbxEquals.isSelected() == true) {
			int Compare = txtA.getText().compareTo(txtB.getText());
			if(Compare == 0) {
				lblCompare.setText("Gleich");
			} else {
				lblCompare.setText("Nicht Gleich");
			}
		} else if(chckbxEqualIgnoreCase.isSelected() == true) {
			int Compare2 = txtA.getText().compareToIgnoreCase(txtB.getText());
			if(Compare2 == 0) {
				lblCompare.setText("Gleich");
			} else {
				lblCompare.setText("Nicht Gleich");
			}
		}
		
	}
	protected void btnSubstringAActionPerformed(ActionEvent e) {
		txtSubstringA.setText(txtA.getText().substring(Integer.valueOf(txtBeginA.getText())-1,Integer.valueOf(txtEndA.getText())));
	}
	protected void btnSubstringBActionPerformed(ActionEvent arg0) {
		txtSubstringB.setText(txtB.getText().substring(Integer.valueOf(txtBeginB.getText())-1,Integer.valueOf(txtEndB.getText())));
	}
	protected void btnClearAllActionPerformed(ActionEvent arg0) {
		txtA.setText("");
		txtB.setText("");
		txtBeginA.setText("");
		txtBeginB.setText("");
		txtConcat.setText("");
		txtEndA.setText("");
		txtEndB.setText("");
		txtLengthOfA.setText("");
		txtLengthOfB.setText("");
		txtLengthOfTrimmedA.setText("");
		txtLengthOfTrimmedB.setText("");
		txtSubstringA.setText("");
		txtSubstringB.setText("");
		txtTrimmedA.setText("");
		txtTrimmedB.setText("");
		txtUpperLowerA.setText("");
		txtUpperLowerB.setText("");
		chckbxEquals.setSelected(false);
		chckbxEqualIgnoreCase.setSelected(false);
		txtCharA.setText("");
		txtCharB.setText("");
		txtIndexA.setText("");
		txtIndexB.setText("");
		lblCompare.setText("");
		rdnbtnIsEmptyAYes.setSelected(false);
		rdnbtnIsEmptyANo.setSelected(false);
		rdnbtnIsEmptyBYes.setSelected(false);
		rdnbtnIsEmptyBNo.setSelected(false);
		rdnbtnContainNo.setSelected(false);
		rdnbtnContainYes.setSelected(false);
		txtHashCodeA.setText("");
		txtHashCodeB.setText("");
		txtSplitA.setText("");
		txtSplitB.setText("");
		txtValueOfA.setText("");
		txtValueOfB.setText("");
		txtBytesOfA.setText("");
		txtBytesOfB.setText("");
		txtReplaxeAInB.setText("");
		txtReplaxeBInA.setText("");
		txtWithA.setText("with");
		txtWithB.setText("with");
	}
	protected void btnCharAtAActionPerformed(ActionEvent arg0) {
		int indexA = Integer.parseInt(txtIndexA.getText()) - 1;
		char CharAtA = txtA.getText().charAt(indexA);
		String stringAtA = String.valueOf(CharAtA);
		txtCharA.setText(stringAtA);
	}
	protected void btnCharAtBActionPerformed(ActionEvent arg0) {
		int indexB = Integer.parseInt(txtIndexB.getText()) - 1;
		char CharAtB = txtB.getText().charAt(indexB);
		String stringAtB = String.valueOf(CharAtB);
		txtCharB.setText(stringAtB);
	}
	protected void btnisEmptyAActionPerformed(ActionEvent e) {
		if(txtA.getText().isEmpty() == true) {
			rdnbtnIsEmptyAYes.setSelected(true);
			rdnbtnIsEmptyANo.setSelected(false);
		} else {
			rdnbtnIsEmptyANo.setSelected(true);
			rdnbtnIsEmptyAYes.setSelected(false);
		}
	}
	protected void btnIsEmptyBActionPerformed(ActionEvent e) {
		if(txtB.getText().isEmpty() == true) {
			rdnbtnIsEmptyBYes.setSelected(true);
			rdnbtnIsEmptyBNo.setSelected(false);
		} else {
			rdnbtnIsEmptyBNo.setSelected(true);
			rdnbtnIsEmptyBYes.setSelected(false);
		}
	}
	protected void btnContainAActionPerformed(ActionEvent e) {
		boolean contain = txtA.getText().contains(txtB.getText());
		if(contain == true) {
			rdnbtnContainYes.setSelected(true);
			rdnbtnContainNo.setSelected(false);
		} else {
			rdnbtnContainYes.setSelected(false);
			rdnbtnContainNo.setSelected(true);
		}
	}
	protected void btnHashCodeAActionPerformed(ActionEvent e) {
		int iHashCodeA = txtA.getText().hashCode();
		String sHashCodeA = String.valueOf(iHashCodeA);
		txtHashCodeA.setText(sHashCodeA);
	}
	protected void btnHashCodeBActionPerformed(ActionEvent e) {
		int iHashCodeB = txtB.getText().hashCode();
		String sHashCodeB = String.valueOf(iHashCodeB);
		txtHashCodeB.setText(sHashCodeB);
	}
	protected void btnSplitAActionPerformed(ActionEvent e) {
		String s1 = txtA.getText();
		String[] words = s1.split("\\s");
		for (String w : words) {
			txtSplitA.setText(txtSplitA.getText() + " - " + w + " - ");
		}
	}
	protected void btnSplitBActionPerformed(ActionEvent e) {
		String s2 = txtB.getText();
		String[] words2 = s2.split("\\s");
		for (String w : words2) {
			txtSplitB.setText(txtSplitB.getText() + " - " + w + " - ");
		}
	}
	protected void btnValueofAActionPerformed(ActionEvent e) {
		txtValueOfA.setText(txtA.getText().valueOf(txtA.getText()));
	}
	protected void btnValueofBActionPerformed(ActionEvent e) {
		txtValueOfB.setText(txtB.getText().valueOf(txtB.getText()));
	}
	protected void btnGetbytesOfAActionPerformed(ActionEvent e) {
		txtBytesOfA.setText(txtA.getText().getBytes().toString());
	}
	protected void btnGetbytesOfBActionPerformed(ActionEvent e) {
		txtBytesOfB.setText(txtB.getText().getBytes().toString());

	}
	protected void btnReplaceAInActionPerformed(ActionEvent e) {
		txtReplaxeAInB.setText(txtA.getText().replace(txtB.getText(), txtWithA.getText()));
	}
	protected void btnReplaceBInActionPerformed(ActionEvent e) {
		txtReplaxeBInA.setText(txtB.getText().replace(txtA.getText(), txtWithB.getText()));
	}
}
