# oct/09/2020 11:21:58 by RouterOS 6.46.7
# software id = UDGW-A63Q
#
# model = 951G-2HnD
# serial number = 4184027808DB
/interface bridge
add name=bridge1
/interface wireless
# managed by CAPsMAN
# channel: 2412/20-Ce/gn(17dBm), SSID: AP-HK, CAPsMAN forwarding
set [ find default-name=wlan1 ] ssid=MikroTik
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/tool user-manager customer
set admin access=\
    own-routers,own-users,own-profiles,own-limits,config-payment-gw
/interface bridge port
add bridge=bridge1 interface=ether1
add bridge=bridge1 interface=ether3
add bridge=bridge1 interface=ether4
add bridge=bridge1 interface=ether5
add bridge=bridge1 interface=ether2
/interface wireless cap
# 
set caps-man-addresses=192.168.100.1 enabled=yes interfaces=wlan1
/ip dhcp-client
add disabled=no interface=bridge1
/system clock
set time-zone-name=Europe/Vienna
/system identity
set name=AP-HK
/system lcd
set contrast=0 enabled=no port=parallel type=24x4
/system lcd page
set time disabled=yes display-time=5s
set resources disabled=yes display-time=5s
set uptime disabled=yes display-time=5s
set packets disabled=yes display-time=5s
set bits disabled=yes display-time=5s
set version disabled=yes display-time=5s
set identity disabled=yes display-time=5s
set bridge1 disabled=yes display-time=5s
set wlan1 disabled=yes display-time=5s
set ether1 disabled=yes display-time=5s
set ether2 disabled=yes display-time=5s
set ether3 disabled=yes display-time=5s
set ether4 disabled=yes display-time=5s
set ether5 disabled=yes display-time=5s
/tool user-manager database
set db-path=user-manager
