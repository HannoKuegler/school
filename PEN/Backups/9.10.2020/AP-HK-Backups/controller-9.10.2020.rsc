# oct/09/2020 11:19:11 by RouterOS 6.46.7
# software id = XZZV-07UH
#
# model = RBD52G-5HacD2HnD
# serial number = D7160CD82BD3
/caps-man channel
add band=2ghz-g/n control-channel-width=20mhz extension-channel=disabled \
    frequency=2412 name=channel1 tx-power=10
add band=2ghz-g/n control-channel-width=20mhz extension-channel=disabled \
    frequency=2432 name=channel5 tx-power=10
add band=2ghz-g/n control-channel-width=20mhz extension-channel=disabled \
    frequency=2452 name=channel9 tx-power=10
add band=2ghz-g/n control-channel-width=20mhz extension-channel=disabled \
    frequency=2472 name=channel13 tx-power=10
/caps-man configuration
add country=austria distance=indoors hw-protection-mode=rts-cts hw-retries=5 \
    max-sta-count=50 mode=ap name=config-wlan ssid=AP-HK
/interface bridge
add admin-mac=48:8F:5A:C0:C2:14 auto-mac=no comment=defconf name=bridge
add name=bridge-wlan
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-XX \
    disabled=no distance=indoors frequency=auto installation=indoor mode=\
    ap-bridge ssid=MikroTik-C0C218 station-roaming=enabled wireless-protocol=\
    802.11
set [ find default-name=wlan2 ] band=5ghz-a/n/ac channel-width=\
    20/40/80mhz-XXXX disabled=no distance=indoors frequency=auto \
    installation=indoor mode=ap-bridge ssid=MikroTik-C0C219 station-roaming=\
    enabled wireless-protocol=802.11
/caps-man datapath
add bridge=bridge-wlan client-to-client-forwarding=yes name=datapath1
/caps-man security
add authentication-types=wpa2-psk encryption=aes-ccm name=security1 \
    passphrase=cisco123
/interface list
add comment=defconf name=WAN
add comment=defconf name=LAN
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
/ip hotspot profile
set [ find default=yes ] html-directory=flash/hotspot
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
add name=dhcp_pool1 ranges=192.168.100.20-192.168.100.200
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
add address-pool=dhcp_pool1 disabled=no interface=bridge-wlan name=dhcp1
/user group
set full policy="local,telnet,ssh,ftp,reboot,read,write,policy,test,winbox,pas\
    sword,web,sniff,sensitive,api,romon,dude,tikapp"
/caps-man manager
set enabled=yes package-path=/pub
/caps-man provisioning
add action=create-dynamic-enabled master-configuration=config-wlan
/interface bridge port
add bridge=bridge comment=defconf interface=ether2
add bridge=bridge comment=defconf interface=ether3
add bridge=bridge comment=defconf interface=ether4
add bridge=bridge comment=defconf interface=ether5
add bridge=bridge comment=defconf interface=wlan1
add bridge=bridge comment=defconf interface=wlan2
/ip neighbor discovery-settings
set discover-interface-list=LAN
/interface list member
add comment=defconf interface=bridge list=LAN
add comment=defconf interface=ether1 list=WAN
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
add address=192.168.100.1/24 interface=bridge-wlan network=192.168.100.0
/ip dhcp-client
add comment=defconf disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
add address=192.168.100.0/24 gateway=192.168.100.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 comment=defconf name=router.lan
/ip firewall filter
add action=accept chain=input comment=\
    "defconf: accept established,related,untracked" connection-state=\
    established,related,untracked
add action=drop chain=input comment="defconf: drop invalid" connection-state=\
    invalid
add action=accept chain=input comment="defconf: accept ICMP" protocol=icmp
add action=accept chain=input comment=\
    "defconf: accept to local loopback (for CAPsMAN)" dst-address=127.0.0.1
add action=drop chain=input comment="defconf: drop all not coming from LAN" \
    in-interface-list=!LAN
add action=accept chain=forward comment="defconf: accept in ipsec policy" \
    ipsec-policy=in,ipsec
add action=accept chain=forward comment="defconf: accept out ipsec policy" \
    ipsec-policy=out,ipsec
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add action=accept chain=forward comment=\
    "defconf: accept established,related, untracked" connection-state=\
    established,related,untracked
add action=drop chain=forward comment="defconf: drop invalid" \
    connection-state=invalid
add action=drop chain=forward comment=\
    "defconf: drop all from WAN not DSTNATed" connection-nat-state=!dstnat \
    connection-state=new in-interface-list=WAN
/ip firewall nat
add action=masquerade chain=srcnat comment="defconf: masquerade" \
    ipsec-policy=out,none out-interface-list=WAN
/system clock
set time-zone-name=Europe/Vienna
/system identity
set name=HK-Controller
/system ntp client
set primary-ntp=10.0.0.2
/system package update
set channel=long-term
/tool mac-server
set allowed-interface-list=LAN
/tool mac-server mac-winbox
set allowed-interface-list=LAN
