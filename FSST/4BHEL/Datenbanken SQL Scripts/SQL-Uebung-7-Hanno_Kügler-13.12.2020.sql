USE projekte;

-- A - Welche Projekte sind in der Datenbank gesperichert? Welchen Code haben diese Projekte?
SELECT * FROM projekte;
-- Ausgabe - AusgabeA.html

-- B - Wo wohnt Mitarbeiterin Monika Mori?
SELECT strasse, plz, ort, bundesland 
FROM mitarbeiter 
WHERE vorname = "Monika" 
AND nachname = "Mori";
-- Ausgabe - Kulm 76	9141	Eberndorf	Kärnten

-- C - Welche Vornamen kommen bei den Mitarbeitern öfter als einmal vor? Selektieren Sie nur diese Vornamen und deren Anzahl nach Anzahl absteigend sortiert.
SELECT vorname, COUNT(*) AS anzahl 
FROM mitarbeiter 
GROUP BY vorname 
HAVING anzahl > 1;
-- Ausgabe - Christian	3		Stefan	2		Martin	2

-- D - Wer ist der jüngste Mitarbeiter?
SELECT vorname, nachname, gebdat 
FROM mitarbeiter
WHERE gebdat = (SELECT max(gebdat) FROM mitarbeiter);
-- Ausgabe - Ferdinand	Einig	2009-05-05

-- E - Wie lautet Namen und Geburtsdatum des ältesten Mitarbeiters?
SELECT vorname, nachname, gebdat 
FROM mitarbeiter 
WHERE gebdat = (SELECT min(gebdat) FROM mitarbeiter);
-- Ausgabe - Raphael	Rezibas	1986-09-08

-- F - Selektieren Sie den jüngsten und den ältesten Mitarbeiter mit einer Abfrage.
SELECT vorname, nachname, gebdat 
FROM mitarbeiter
WHERE gebdat = (SELECT max(gebdat) FROM mitarbeiter) OR gebdat = (SELECT min(gebdat) FROM mitarbeiter);
-- Ausgabe - Raphael	Rezibas	1986-09-08		Ferdinand	Einig	2009-05-05

-- G - Welche Mitarbeiter haben einen Vornamen mit 5 Buchstaben?
SELECT vorname, nachname 
FROM mitarbeiter 
WHERE LENGTH(vorname) = 5;
-- Ausgabe - Armin	Gräfin		Jimmy	Lastig		Erich	Alukim		Peter	Guckenbergen

-- H - Welche Mitarbeiter haben die Hausnummer 12?
SELECT vorname, nachname, strasse 
FROM mitarbeiter
WHERE strasse LIKE "%12";
-- Ausgabe - Christoph	Samtitsch	Meiselhofstraße 12		Martin	Plischi	Heinzgasse 12

-- I - Welche 5 Mitarbeiter stehen ihrem Nachnamen nach im Alphabet an letzter Stelle?
SELECT vorname, nachname 
FROM mitarbeiter 
ORDER BY nachname DESC LIMIT 5;
-- Ausgabe - AusgabeI.html

-- J - Welche Arbeitspakete gibt es bei Projekt Nr 3?
SELECT code, Bezeichnung 
FROM arbeitspakete 
WHERE pr_nr = 3;
-- Ausgabe - Ausgabe-J-Hanno_Kügler.html

-- K - Welche Code und welche Bezeichnung haben die Arbeitspakete des Projekts mit dem Code WSTP?
SELECT code, Bezeichnung 
FROM arbeitspakete 
WHERE pr_nr = (SELECT nr FROM projekte WHERE code = "WSTP");
-- Ausgabe - Ausgabe-K-Hanno_Kügler.html

-- L - Selektieren Sie den Titel und den Gesamtaufwand für alle Projekte und sortieren Sie die Datensätze aufsteigend nach der Summe der Stunden.
SELECT p.titel, count(zu.std) AS gesamtaufwand 
FROM projekte p, arbeitspakete a, ma_ap_zuord zu 
WHERE p.nr = a.pr_nr AND a.id = zu.ap_id 
GROUP BY p.nr 
ORDER BY gesamtaufwand ASC;
-- Ergebnis: 'Design und Entwicklung einer Web-Stundenplanverwaltung', '87' ; 'WLAN Griller - Mikrowellen Accesspoints', '95' ; 'HomeWork Robot - Roboter zur Hausübungsautomatisierung', '135'   

-- M - Wie heißen die Projektmanager der Projekte und wie viel Zeit wurde für das Projektmanagement aufgewendet?
-- Es gibt keine Projektmanager in der Datenbankstruktur.

-- N - Wie viele Stunden haben die einzelnen Mitarbeiter gearbeitet ? Sortieren Sie die Liste absteigend nach der Summe der Stunden.
SELECT m.nachname, m.vorname, count(zu.std) AS arbeitsstunden 
FROM mitarbeiter m, ma_ap_zuord zu 
WHERE m.id = zu.ma_id 
GROUP BY m.id ORDER BY arbeitsstunden desc;
-- Ergebnis: Ausgabe-N-Hanno_Kügler.html

-- O) An welchen Projekten arbeitete Carmen Muran wie viel ?
SELECT p.titel, count(zu.std) as arbeitsaufwand 
FROM projekte p, arbeitspakete a, ma_ap_zuord zu, mitarbeiter m
WHERE p.nr = a.pr_nr AND a.id = zu.ap_id AND m.id = zu.ma_id AND m.nachname = 'Muran' AND m.vorname = 'Carmen'
GROUP BY p.nr;

-- Ergebnis: 'Design und Entwicklung einer Web-Stundenplanverwaltung', '1' ; 'HomeWork Robot - Roboter zur Hausübungsautomatisierung', '8' ; 'WLAN Griller - Mikrowellen Accesspoints', '2'
-- P - Welches war das aufwändigste Arbeitspaket mit der höchsten Stundenanzahl?
SELECT ap.code, ap.bezeichnung, SUM(ma_ap_zuord.std) AS hours 
FROM arbeitspakete AS ap LEFT JOIN ma_ap_zuord ON ap.id = ma_ap_zuord.ap_id 
GROUP BY ap.code 
ORDER BY hours DESC LIMIT 1;
-- Ausgabe - C008 	Inbetriebnahme	54h