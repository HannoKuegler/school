use school4ahel2020;

INSERT INTO `schoolclass` (`name`, `room`) VALUES ('4BHEL', 'T243');
INSERT INTO schoolclass VALUES (null, '5BHEL', 'T244');

SELECT * FROM schoolclass;
SELECT * FROM schoolclass_has_teacher;
SELECT * FROM student;
SELECT * FROM teacher;
SELECT * FROM subject;
SELECT * FROM studentgrades;

-- a Vor- und Nachname aller Schüler
SELECT firstname, surname FROM student;
-- b Lehrer "Dr. Daniel Datenpunkt" hinzufügen
INSERT INTO teacher (title, firstname, surname) VALUES ('Dr.', 'Daniel', 'Datenpunkt');
-- c namen auf "Christian Datafunk" ändern
UPDATE teacher SET firstname = "Christian", surname = "Datafunk" WHERE firstname = "Daniel" AND surname = "Datenpunkt";
-- d. Füge Daten ein, dass der Lehrer „Datafunk“ in zumindest 2 Klassen unterrichtet.
INSERT INTO schoolclass_has_teacher (schoolclass_id,teacher_id,subject_id)  
SELECT sc.id, t.id, sub.id
FROM schoolclass sc, teacher t, subject sub
WHERE sc.name = '4BHEL'
AND t.surname='Datafunk'
AND sub.shortname ='MTRS';
INSERT INTO schoolclass_has_teacher (schoolclass_id,teacher_id,subject_id)  
SELECT sc.id, t.id, sub.id
FROM schoolclass sc, teacher t, subject sub
WHERE sc.name = '4BHEL'
AND t.surname='Datafunk'
AND sub.shortname ='FSST';
-- e. Frage alle Schulklassen ab.
SELECT * FROM schoolclass;
-- f. Frage alle Schulklassen ab, die einen Raum im 2. Stock haben
SELECT * FROM schoolclass 
WHERE room LIKE 'T2%'
OR room LIKE 'W2%';
-- g. Frage alle Schüler ab, die im Jahr 2003 geboren sind.
SELECT * FROM student WHERE YEAR(dateOfBirth) = 2003;
-- h. Frage alle Schüler ab, die im Jahr 2005 oder im Jahr 2002 geboren sind
SELECT * FROM student WHERE YEAR(dateOfBirth) = 2002 OR YEAR(dateOfBirth) = 2005;
-- i. Frage alle Lehrer ab, die einen Doktortitel haben.
SELECT * FROM teacher WHERE title LIKE "%Dr.%";
-- j. Frage die Namen aller Lehrer ab und sortiere das Ergebnis nach Nachnamen und Vornamen
SELECT * FROM teacher ORDER BY surname, firstname DESC;
-- k. Frage die Anzahl aller Schüler ab
SELECT COUNT(*) FROM student;
-- l. Frage die Anzahl aller Schüler ab, die 2004 geboren sind
SELECT COUNT(*) FROM student WHERE YEAR(dateOfBirth) = 2004;
-- m. Frage die Anzahl aller Schüler je Jahr ab
SELECT LEFT(dateOfBirth,4), COUNT(*)
FROM student s
GROUP BY LEFT(dateOfBirth,4);
-- n. Frage das minimale und das maximale Geburtsjahr der Schüler ab
SELECT MIN(dateOfBirth), MAX(dateOfBirth) FROM student;
-- o. Frage alle Schüler ab, welche die Buchstabenkombination „ia“ im Vornamen haben (z.B. Florian)
SELECT * FROM student WHERE firstname LIKE "%ia%";
-- p. Sofern noch keine Klasse 5CHEL existiert, ändere den Namen einer Klasse auf 5CHEL.
UPDATE schoolclass SET name='5CHEL' WHERE id=2;
-- q. Frage alle Schüler ab, die in die Klasse 5CHEL gehen.
SELECT * FROM student, schoolclass WHERE student.schoolclasses_id = schoolclass.id AND schoolclass.name = '5CHEL';
-- r. Frage alle Lehrer ab, die den Gegenstand X* unterrichten
SELECT t.surname, t.firstname, s.name FROM teacher t, subject s, schoolclass_has_teacher sht 
WHERE t.id = sht.teacher_id 
AND s.id = sht.subject_id 
AND s.shortname like "FSST";
-- s. Frage den Namen aller Klassen ab, die vom Lehrer X* unterrichtet werden.
SELECT t.surname, t.firstname, c.name 
FROM teacher t, schoolclass c, schoolclass_has_teacher sht
WHERE t.id = sht.teacher_id
AND c.id = sht.schoolclass_id
AND t.surname like 'Rodiga'
AND t.firstname like 'Alexander';
-- t. Frage ab, welcher Lehrer den Gegenstand FSST in der Klasse 4BHEL unterrichtet
SELECT c.name, t.surname, t.firstname, sub.name
FROM schoolclass c, teacher t, schoolclass_has_teacher sht, subject sub
WHERE c.id = sht.schoolclass_id
AND t.id = sht.teacher_id 
AND sub.id = sht.subject_id
AND c.name = '4BHEL'
AND sub.shortname like 'FSST';
-- u. Frage die Namen der Schüler ab, die als Stammraum den Raum 242T haben.
SELECT s.firstname, s.surname, c.room
FROM student s, schoolclass c
WHERE c.id = s.schoolclasses_id
AND c.room like 'T242';
-- v. Frage die Anzahl aller Schüler je Klasse ab und sortiere das Ergebnis absteigend nach der
SELECT c.name, count(*) 
FROM schoolclass c, student s
WHERE c.id = s.schoolclasses_id
GROUP BY c.name
ORDER BY count(*) desc;
-- w. Frage die Noten des Schülers X* ab und gib sie mit dem Fach aufsteigend sortiert aus.
SELECT s.firstname, s.surname, g.grade, sub.name
FROM student s, studentgrades g, subject sub
WHERE s.id = g.student_id
AND sub.id = g.subject_id 
AND s.surname like 'Mrsic'
ORDER BY g.grade;
-- x. Gib die Schüler der Klasse 5CHEL aufsteigend sortiert nach ihrem Notendurchschnitt aus.
SELECT s.firstname, s.surname, c.name, AVG(grade)
FROM student s, schoolclass c, studentgrades g
WHERE c.id = s.schoolclasses_id
AND g.student_id = s.id
AND c.name LIKE '5CHEL'
GROUP BY s.firstname,s.surname, c.name
ORDER BY AVG(grade);
-- y. Gib alle Schüler mit Klassennamen aus, die einen ausgezeichneten Erfolg haben.
SELECT s.firstname, s.surname, c.name, AVG(grade)
FROM student s, schoolclass c, studentgrades g
WHERE c.id = s.schoolclasses_id
AND g.student_id = s.id
GROUP BY s.firstname, s.surname, c.name
HAVING AVG(grade) <= 1.5
ORDER BY AVG(grade);
-- z. Frag alle Schüler ab, die vom Lehrer X* unterrichtet werden und gib die Note aus, welche die Schüler beim Lehrer X* in welchem Fach haben.
SELECT s.firstname, s.surname, sub.name, g.grade, t.surname
FROM student s, subject sub, studentgrades g, teacher t, schoolclass c, schoolclass_has_teacher sht
WHERE s.id = g.student_id 
AND sht.teacher_id =t.id 
AND sub.id = sht.subject_id
AND c.id = s.schoolclasses_id 
AND g.subject_id = sub.id
AND t.surname LIKE 'Rodiga';
