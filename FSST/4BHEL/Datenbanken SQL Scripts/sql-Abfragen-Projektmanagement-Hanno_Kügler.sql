-- SQL Fragen Vorlage
-- A)Welche Projekte sind in der Datenbank gespeichert ? Welchen Code haben diese Projekte?
select titel,code from projekte;
-- Ergebnis : 
WSTP, ROBO, WGR, POA, ATDB;

-- B)Wo wohnt Mitarbeiterin Monika Mori ?
select strasse,plz,ort from mitarbeiter 
where nachname = 'Mori' and vorname = 'Monika';

-- Ergebnis:
Kulm 76	9141	Eberndorf

-- C)Welche Vornamen kommen bei den Mitarbeitern öfter als einmal vor? Selektieren Sie nur diese Vornamen und deren Anzahl nach Anzahl absteigend sortiert.
select
-- Ergebnis:

-- D)Wer ist der jüngste Mitarbeiter?
select
-- Ergebnis:

-- E)Wie lautet Namen und Geburtsdatum des ältesten Mitarbeiters?
select
-- Ergebnis:

-- F)Selektieren Sie den jüngsten und den ältesten Mitarbeiter mit einer Abfrage.
select
-- Ergebnis:

-- G)Welche Mitarbeiter haben einen Vornamen mit 5 Buchstaben?
select
-- Ergebnis:

-- H)Welche Mitarbeiter haben die Hausnummer 12?
select
-- Ergebnis:

-- I)Welche 5 Mitarbeiter stehen ihrem Nachnamen nach im Alphabet an letzter Stelle?
select
-- Ergebnis:

-- J)Welche Arbeitspakete gibt es bei Projekt Nr 3?
select
-- Ergebnis:

-- K)Welche Code und welche Bezeichnung haben die Arbeitspakete des Projekts mit dem Code WSTP?
select
-- Ergebnis:

-- L)Selektieren Sie den Titel und den Gesamtaufwand für alle Projekte und sortieren Sie die Datensätze aufsteigend nach der Summe der Stunden.
select
-- Ergebnis:

-- M)Wie heißen die Projektmanager der Projekte und wie viel Zeit wurde für das Projektmanagement aufgewendet?
select
-- Ergebnis:

-- N)Wie viele Stunden haben die einzelnen Mitarbeiter gearbeitet ? Sortieren Sie die Liste absteigend nach der Summe der Stunden.
select
-- Ergebnis:

-- O)An welchen Projekten arbeitete Carmen Muran wie viel ?
select
-- Ergebnis:

-- P)Welches war das aufwändigste Arbeitspaket mit der höchsten Stundenanzahl?
select
-- Ergebnis:

