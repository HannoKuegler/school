CREATE DATABASE  IF NOT EXISTS `school4ahel2020` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `school4ahel2020`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: school4ahel2020
-- ------------------------------------------------------
-- Server version	5.1.53-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `schoolclass`
--

DROP TABLE IF EXISTS `schoolclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schoolclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(5) NOT NULL,
  `room` varchar(45) NOT NULL COMMENT 'default class room',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schoolclass`
--

LOCK TABLES `schoolclass` WRITE;
/*!40000 ALTER TABLE `schoolclass` DISABLE KEYS */;
INSERT INTO `schoolclass` VALUES (1,'4AHEL','TU23'),(2,'5CHEL','T244'),(3,'5AHEL','T242'),(4,'3AHBG','T122'),(5,'2AHEL','T124'),(6,'1CHEL','T027'),(7,'4AHET','W013');
/*!40000 ALTER TABLE `schoolclass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schoolclass_has_teacher`
--

DROP TABLE IF EXISTS `schoolclass_has_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schoolclass_has_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schoolclass_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_teachsub` (`schoolclass_id`,`teacher_id`,`subject_id`),
  KEY `fk_schoolclasses_has_teachers_teachers1_idx` (`teacher_id`),
  KEY `fk_schoolclasses_has_teachers_schoolclasses_idx` (`schoolclass_id`),
  KEY `fk_schoolclass_has_teacher_subjects1_idx` (`subject_id`),
  CONSTRAINT `fk_schoolclasses_has_teachers_schoolclasses` FOREIGN KEY (`schoolclass_id`) REFERENCES `schoolclass` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_schoolclasses_has_teachers_teachers1` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_schoolclass_has_teacher_subjects1` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schoolclass_has_teacher`
--

LOCK TABLES `schoolclass_has_teacher` WRITE;
/*!40000 ALTER TABLE `schoolclass_has_teacher` DISABLE KEYS */;
INSERT INTO `schoolclass_has_teacher` VALUES (1,1,2,1),(5,1,3,5),(6,1,4,4),(7,2,3,2),(4,2,4,3),(8,2,5,6),(13,2,11,1),(2,3,2,4),(14,3,5,1),(16,3,6,5),(9,4,2,5),(15,4,5,1),(3,5,1,2),(10,5,2,2),(12,6,2,1),(11,6,4,4);
/*!40000 ALTER TABLE `schoolclass_has_teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `sex` varchar(1) NOT NULL COMMENT 'm oder w',
  `height` double NOT NULL COMMENT 'Größe in m\n',
  `schoolclasses_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_students_schoolclasses1_idx` (`schoolclasses_id`),
  CONSTRAINT `fk_students_schoolclasses1` FOREIGN KEY (`schoolclasses_id`) REFERENCES `schoolclass` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES (1,'Johanens','Mlinar','2002-07-15','m',183,1),(2,'David','Steinwidder','2003-07-02','M',177,4),(3,'Michael','Gfrerer','2000-10-10','m',170,2),(4,'Karin','Steinwidder','1970-02-01','s',120,1),(8,'Katarina','Mrsic','2003-03-13','w',180,3),(20,'Josef','Winter','2020-01-01','m',184,5),(21,'Niko','Markitz','2002-09-09','M',184,3),(22,'Bruce','Banner','2002-12-24','M',197,1),(23,'Christopher','Metnitzer','2005-11-06','Y',183,6),(24,'Günter','Naschberger','2010-12-12','w',187,1),(25,'Sebastian','Naschberger','2003-03-20','M',187,1),(26,'Johannes','Müller','2002-04-20','M',174,7),(27,'Seppal','Pavlovic','2002-12-12','m',177,3),(28,'Harald','Manzinger','1978-02-04','S',199,3),(29,'Gabriel','Pasterk','2003-04-25','m',180,1),(30,'Mama','Rauter','1832-02-24','Ü',12,3),(31,'Anton','Horvath','1988-04-03','m',180,1),(32,'Hans','Peter','1988-02-11','m',184,4),(33,'Jesus','Christus','0000-12-24','M',173,1),(34,'Petschnig','Renate','1993-03-02','µ',23,2),(35,'Manziner','Harry','1940-02-11','M',177,6),(36,'Frank','Rosin','1966-07-17','B',180,5),(38,'DARF','VADER','2342-02-13','m',166,3),(39,'Josef','Stalin','1878-12-18','M',178,2),(40,'Rick','Astley','1966-02-06','m',178,3),(41,'Snens','Coleman','2020-10-18','m',156,5),(42,'Protein','Mlinar','2002-07-15','W',183,1);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentgrades`
--

DROP TABLE IF EXISTS `studentgrades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentgrades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `grade` int(11) NOT NULL COMMENT '1 bis 5 , Österreichicher Noteskala\n',
  PRIMARY KEY (`id`),
  KEY `fk_students_has_subjects_students1_idx` (`student_id`),
  KEY `fk_studentgrades_subject1_idx` (`subject_id`),
  CONSTRAINT `fk_students_has_subjects_students1` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_studentgrades_subject1` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=354 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentgrades`
--

LOCK TABLES `studentgrades` WRITE;
/*!40000 ALTER TABLE `studentgrades` DISABLE KEYS */;
INSERT INTO `studentgrades` VALUES (1,20,1,1),(2,8,1,1),(3,8,3,1),(4,25,3,1),(5,3,3,5),(6,28,4,3),(7,1,1,1),(8,38,2,2),(9,20,6,5),(19,34,1,1),(20,34,2,1),(21,34,3,1),(22,34,4,1),(23,31,3,5),(24,31,1,5),(25,31,2,5),(26,31,4,5),(27,31,5,5),(28,31,6,30),(29,34,5,1),(30,34,6,1),(60,39,1,1),(61,39,2,1),(62,39,3,1),(63,39,4,5),(64,39,5,1),(65,23,1,5),(66,2,3,4),(67,2,5,2),(68,20,2,2),(69,40,4,1),(70,20,3,3),(73,21,6,2),(74,23,2,1),(75,23,3,4),(76,23,3,2),(77,23,4,5),(78,23,5,1),(79,23,6,1),(80,23,7,1),(81,8,6,1),(333,35,3,5),(334,33,1,5),(335,4,6,1),(336,33,2,5),(337,33,3,5),(338,33,4,5),(339,33,5,5),(340,33,6,1),(341,33,7,5),(342,36,1,1),(343,36,2,1),(344,36,3,1),(345,36,4,1),(346,36,5,1),(347,36,6,1),(348,36,7,1),(350,4,8,1),(351,23,8,5),(352,36,8,1),(353,33,8,5);
/*!40000 ALTER TABLE `studentgrades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shortname` varchar(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES (1,'FSST','Fachspezifische Software Technik'),(2,'D','Deitsch'),(3,'AM','Mathe'),(4,'REL','Religion'),(5,'HWE','Hardware Entwicklung'),(6,'Drunk','Drinking Education'),(7,'MTRS','Messtechnik'),(8,'SPO','Sport'),(9,'187','187');
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher`
--

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` VALUES (1,'Prof. Mag.','Andrea','Scheinig'),(2,'Prof. ','Alexander','Rodiga'),(3,'Prof. ','Liselotte','Gruber'),(4,'DIPL.ING','Hubert','Lutnik'),(5,'Dipl. Ing.','Josef','Winter'),(6,'Mag. prof.','Anton','Horwath'),(7,'Dr.','Gernot','Opriessnig'),(8,'Prof. Dr. Mag. Dipl.-Ing. Prof. h.c. mult.','Heinz-Christian','Strache'),(9,'Mag.','Simon','Miedl'),(10,'Floschn.','Thomas','Pavlovic'),(11,'DI AV','Harald','Grünanger'),(13,'AV für Elektronik und Elektronik','Johann','Klanschek'),(14,'Dr.','','');
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-19 12:59:08