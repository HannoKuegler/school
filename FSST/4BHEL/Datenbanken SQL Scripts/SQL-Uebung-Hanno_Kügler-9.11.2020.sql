use school4ahel2020;
# a # 
select firstname, surname from student;

# b # 
insert into teacher value (null, "Dr.", "Daniel", "Datenpunk");
select * from teacher;

# c # 
UPDATE teacher SET firstname="Christian", surname="Datafunk" WHERE id='15'; 
select * from teacher;

# d # 
insert into schoolclass_has_teacher values (null, 1, 15, 5);
insert into schoolclass_has_teacher values (null, 2, 15, 5);
select * from schoolclass_has_teacher;
 
 # e # 
 select * from schoolclass;
 
 # f # 
 select * from schoolclass where room >= "T200" and room <= "T299";
 
 # g # 
 select * from student where dateOfBirth >= "2003-01-01" and  dateOfBirth <= "2003-12-31";
 
 # h #
	
 select * from student 
 where dateOfBirth >= "2002-01-01" and  dateOfBirth <= "2002-12-31" 
 or dateOfBirth >= "2005-01-01" and  dateOfBirth <= "2005-12-31";
 
 # i # 
 select * from teacher where title= "Dr."; 
 
 # j # 
 select * from teacher order by surname, firstname DESC; 
 
 # k # 
 select count(*) from student; 
 
 # l # 
	# no stundents born in 2004 i took 2005
 select count(*) from student where dateOfBirth >= "2005-01-01" and  dateOfBirth <= "2005-12-31"; 
 
 # m # 
SELECT LEFT(dateOfBirth), COUNT(*)
FROM student s
GROUP BY LEFT(dateOfBirth,4);

# n # 
SELECT MIN(dateOfBirth), MAX(dateOfBirth) FROM student;

# o #
SELECT * FROM student WHERE firstname LIKE "%ia%";

# p # 
UPDATE schoolclass SET name='5CHEL' WHERE id=2;

# q # 
SELECT * FROM student, schoolclass WHERE student.schoolclasses_id = schoolclass.id AND schoolclass.name = '5CHEL';

# r # 
SELECT t.surname, t.firstname, s.name FROM teacher t, subject s, schoolclass_has_teacher sht 
WHERE t.id = sht.teacher_id 
AND s.id = sht.subject_id 
AND s.shortname like "FSST";

# s # 
SELECT t.surname, t.firstname, c.name 
FROM teacher t, schoolclass c, schoolclass_has_teacher sht
WHERE t.id = sht.teacher_id
AND c.id = sht.schoolclass_id
AND t.surname like 'Rodiga'
AND t.firstname like 'Alexander';

# t #
SELECT c.name, t.surname, t.firstname, sub.name
FROM schoolclass c, teacher t, schoolclass_has_teacher sht, subject sub
WHERE c.id = sht.schoolclass_id
AND t.id = sht.teacher_id 
AND sub.id = sht.subject_id
AND c.name = '4BHEL'
AND sub.shortname like 'FSST';
 
# u # 
SELECT s.firstname, s.surname, c.room
FROM student s, schoolclass c
WHERE c.id = s.schoolclasses_id
AND c.room like 'T242';

# v # 
SELECT c.name, count(*) 
FROM schoolclass c, student s
WHERE c.id = s.schoolclasses_id
GROUP BY c.name
ORDER BY count(*) desc;

# w # 
SELECT s.firstname, s.surname, g.grade, sub.name
FROM student s, studentgrades g, subject sub
WHERE s.id = g.student_id
AND sub.id = g.subject_id 
AND s.surname like 'Mrsic'
ORDER BY g.grade;

# x # 
SELECT s.firstname, s.surname, c.name, AVG(grade)
FROM student s, schoolclass c, studentgrades g
WHERE c.id = s.schoolclasses_id
AND g.student_id = s.id
AND c.name LIKE '5CHEL'
GROUP BY s.firstname,s.surname, c.name
ORDER BY AVG(grade);

# y # 
SELECT s.firstname, s.surname, c.name, AVG(grade)
FROM student s, schoolclass c, studentgrades g
WHERE c.id = s.schoolclasses_id
AND g.student_id = s.id
GROUP BY s.firstname, s.surname, c.name
HAVING AVG(grade) <= 1.5
ORDER BY AVG(grade);

# z # 
SELECT s.firstname, s.surname, sub.name, g.grade, t.surname
FROM student s, subject sub, studentgrades g, teacher t, schoolclass c, schoolclass_has_teacher sht
WHERE s.id = g.student_id 
AND sht.teacher_id =t.id 
AND sub.id = sht.subject_id
AND c.id = s.schoolclasses_id 
AND g.subject_id = sub.id
AND t.surname LIKE 'Rodiga';

 
 

