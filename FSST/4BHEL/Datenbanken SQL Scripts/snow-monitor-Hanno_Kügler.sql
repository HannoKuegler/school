-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema snowmonitor
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema snowmonitor
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `snowmonitor` DEFAULT CHARACTER SET utf8 ;
USE `snowmonitor` ;

-- -----------------------------------------------------
-- Table `snowmonitor`.`skiresort`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `snowmonitor`.`skiresort` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idskiresort_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `snowmonitor`.`snowdepth`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `snowmonitor`.`snowdepth` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `skiresort_id` INT NOT NULL,
  `snowdepth` INT NOT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_snowdepth_skiresort_idx` (`skiresort_id` ASC),
  CONSTRAINT `fk_snowdepth_skiresort`
    FOREIGN KEY (`skiresort_id`)
    REFERENCES `snowmonitor`.`skiresort` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
