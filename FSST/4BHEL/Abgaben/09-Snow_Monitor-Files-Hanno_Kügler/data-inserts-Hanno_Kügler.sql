/*
-- Query: SELECT * FROM `snow-monitor`.snowdepth
LIMIT 0, 1000

-- Date: 2021-01-24 21:28
*/
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (1,1,380,'2021-01-06');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (2,2,240,'2021-01-02');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (3,3,320,'2021-01-12');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (4,4,120,'2021-01-11');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (5,5,240,'2021-01-17');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (6,6,280,'2021-01-08');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (7,7,360,'2021-01-18');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (8,8,400,'2021-01-20');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (9,9,300,'2021-01-07');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (10,2,220,'2021-01-10');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (11,7,200,'2021-01-11');
INSERT INTO `snowdepth` (`id`,`skiresort_id`,`snowdepth`,`date`) VALUES (13,8,100,'2000-12-04');
