package DB;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import data.Temperatur;

public class DAO {

	public static void main(String[] args) {
		try {
			Connection c = DbConnection.getConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// get all Temps

	public static List<Temperatur> getAllTemps() {

		List<Temperatur> temps = new ArrayList<>();

		try {

			Connection con = DbConnection.getConnection();
			Statement stmt = con.createStatement();

			ResultSet rst = stmt.executeQuery("SELECT * FROM temperatur");

			while (rst.next()) {
				Temperatur t = new Temperatur(rst.getString("raumnummer"), rst.getDouble("temp"),
						rst.getString("time"));
				temps.add(t);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return temps;

	}

	// save new temp
	
	public static void insertNewTemp(Temperatur temp) {
		int toRet = -1;
		try {
			Connection con = DbConnection.getConnection();
			String sql = "INSERT INTO temperatur (roomnumber, temp, time) VALUES (?, ?, ?)";
			PreparedStatement pst = DbConnection.getConnection().prepareStatement(sql);
			
			pst.setString(1, temp.getRoomnumber());
			pst.setDouble(2, temp.getTemp());
			pst.setString(3, temp.getldtString());

			toRet = pst.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// get temp by room number

	public static Temperatur getTempsByRoomNumber(String room) {

		Temperatur t = null;
		List<Temperatur> temps = new ArrayList<>();

		try {

			Connection con = DbConnection.getConnection();
			Statement stmt = con.createStatement();

			ResultSet rst = stmt.executeQuery("SELECT * FROM temperatur");

			while (rst.next()) {
				Temperatur ts = new Temperatur(rst.getString("raumnummer"), rst.getDouble("temp"),
						rst.getString("time"));
				temps.add(ts);
			}
			for (Temperatur temperatur : temps) {
				if (temperatur.getRoomnumber().equals(room)) {
					return t;
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

}
