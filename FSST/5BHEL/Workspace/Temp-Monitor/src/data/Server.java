package data;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	private Socket socket; 
	
	public static void main(String[] args) {
		
		Server s = new Server(); 
		s.waitForCon();
		
	}

	private void waitForCon() {
	
		try {
			ServerSocket s = new ServerSocket(4545);
			while(true) {
				socket = s.accept();
				new ServerThread(socket).start(); 
				System.out.println("Connection established");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
	}
	
	
	
	
}
