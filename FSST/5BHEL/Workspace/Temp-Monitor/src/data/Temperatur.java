package data;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Temperatur {

	public static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
	private String roomnumber; 
	private double temp;
	private LocalDateTime ldt;
	
	public Temperatur(String roomnumber, double temp, LocalDateTime ldt) {
		super();
		this.roomnumber = roomnumber;
		this.temp = temp;
		this.ldt = ldt;
	}
	
	public Temperatur(String roomnumber, double temp) {
		super();
		this.roomnumber = roomnumber;
		this.temp = temp;
		this.ldt = LocalDateTime.now();
	}
	
	public Temperatur(String roomnumber, double temp, String time) {
		this.roomnumber = roomnumber;
		this.temp = temp;
		this.ldt = LocalDateTime.parse(time, dtf);
	}

	@Override
	public String toString() {
		return "Room-Temp [roomnumber=" + roomnumber + ", temp=" + temp + ", ldt=" + ldt + "]";
	}

	public String getRoomnumber() {
		return roomnumber;
	}
	public void setRoomnumber(String roomnumber) {
		this.roomnumber = roomnumber;
	}
	public double getTemp() {
		return temp;
	}
	public void setTemp(double temp) {
		this.temp = temp;
	}
	public LocalDateTime getLdt() {
		return ldt;
	}
	public String getldtString() {
		return ldt.format(dtf);
	} 
	public void setLdt(LocalDateTime ldt) {
		this.ldt = ldt;
	} 
	
	
	
	
}
