package data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Trivial client for the date server.
 */
public class DateClient {

	private String serverIp;
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;

	public DateClient(String serverIp) {
		this.serverIp = serverIp;
	}

	public static void main(String[] args) {
		DateClient client = new DateClient("127.0.0.1");
		client.establishConnection();
		client.communicate();
	}

	private void communicate() {
		System.out.println("booting up client ... " );
		Scanner sc = new Scanner(System.in);
		String scan = "";
		String line = "";

		try {
			while (true) {
				line = in.readLine();
				System.out.println("response of server: " + line);
				System.out.print("enter commands: ");
				scan = sc.nextLine();
				out.println(scan);
				out.flush();
				if (scan.equals("exit")) {

					System.out.println("System exit");
					break;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();

		} catch (NullPointerException e) {
			System.out.println("No connection/connection ended");
		} finally {
			sc.close();
			try {
				if (socket != null) {
					in.close();
					out.close();
					socket.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private void establishConnection() {
		Socket socket;
		try {
			socket = new Socket(serverIp, 4545);
			out = new PrintWriter(socket.getOutputStream());
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}