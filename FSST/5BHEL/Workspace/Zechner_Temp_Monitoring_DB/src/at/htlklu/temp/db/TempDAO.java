package at.htlklu.temp.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import at.htlklu.temp.ServerTask;
import at.htlklu.temp.Temperature;

public class TempDAO {

	public static synchronized void getTemps() {
		try {
			Connection con = DbConnection.getConnection();
			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM temptable");
			ServerTask.roomList = new ArrayList<>();

			while (rs.next()) {
				ServerTask.roomList.add(new Temperature(rs.getString("roomnumber"), rs.getDouble("temp"), rs.getString("timestamp")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static Temperature getTempByRoomnumber(String roomnumber) {
		Temperature temp = null;
		try {
			Connection con = DbConnection.getConnection();
			Statement stmt = con.createStatement();

			ResultSet rs = stmt.executeQuery("SELECT * FROM temptable WHERE roomnumber = '" + roomnumber + "'");
			ServerTask.roomList = new ArrayList<>();

			if (rs.next()) {
				 temp = new Temperature(rs.getString("roomnumber"), rs.getDouble("temp"), rs.getString("timestamp"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return temp;
	}
	
	public static int saveTemp(Temperature temp) {
		int toRet = -1;
		try {
			String sql = "INSERT INTO temptable (roomnumber, temp, timestamp) VALUES (?, ?, ?)";
			PreparedStatement pst = DbConnection.getConnection().prepareStatement(sql);
			pst.setString(1, temp.getRoomnumber()); pst.setDouble(2, temp.getTemp()); pst.setString(3, temp.getTimestampString());
			
			if (DbConnection.getConnection().createStatement().executeQuery(String.format(
					"SELECT id FROM temptable WHERE roomnumber = '%s'",
					temp.getRoomnumber(), temp.getTemp(), temp.getTimestamp())).next()) {
				sql = "UPDATE temptable SET temp = ?, timestamp = ? WHERE roomnumber = ?";
				pst = DbConnection.getConnection().prepareStatement(sql);
				pst.setDouble(1, temp.getTemp()); pst.setString(2, temp.getTimestampString()); pst.setString(3, temp.getRoomnumber());
			}
			toRet = pst.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return toRet;
	}

}
