package at.htlklu.temp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	private static final int _PORT = 4431;

	public static void main(String[] args) {
		try {
			@SuppressWarnings("resource")
			ServerSocket ss = new ServerSocket(_PORT);
			System.out.println("Server started...");
			Temperature.getAllTemps();
			
			while (true) {
				System.out.println("Waiting for new Client...");
				Socket s = ss.accept();
				System.out.println("Client connected");
				
				ServerTask st = new ServerTask(s);
				st.start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
