package at.htlklu.temp.db;

import java.sql.*;

public class DbConnection {
	// Defaults
	private static final String DEFAULT_USER = "root";
	private static final String DEFAULT_PASSWORD = "cisco123";

	private static final String DEFAULT_SERVER = "localhost"; // IP-address
	private static final String DEFAULT_PORT = "3306"; // port number
	private static final String DEFAULT_SCHEMA = "temp"; // schema name
	
	private static Connection connection; // database connection

	public static synchronized Connection getConnection() throws SQLException {
		if (connection == null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			// connect to the database
			String url = String.format("%s:%s/%s", DEFAULT_SERVER, DEFAULT_PORT, DEFAULT_SCHEMA); 
			String fullURL = String.format("jdbc:mysql://%s?useSSL=false&useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=Europe/Vienna", url);
			connection = DriverManager.getConnection(fullURL, DEFAULT_USER, DEFAULT_PASSWORD);
		}
		return connection;
	}
	
	public static void close() throws SQLException {
		if (connection != null) {
			connection.close();
			connection = null;
		}
	}
}