package at.htlklu.temp;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import at.htlklu.temp.db.TempDAO;

public class Temperature {

	public static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
	private String roomnumber;
	private double temp;
	private LocalDateTime timestamp;

	public synchronized static Temperature getTempByRoomnumber(String roomnumber) {
		Temperature ret = TempDAO.getTempByRoomnumber(roomnumber);
		return ret;
	}
	
	public synchronized static void getAllTemps() {
		TempDAO.getTemps();
	}

	public synchronized static void addRoom(String roomnumber, double temp) {
		Temperature t = getTempByRoomnumber(roomnumber);
		if (t != null) {
			ServerTask.roomList.remove(t);
		}
		t = new Temperature(roomnumber, temp);
		ServerTask.roomList.add(t);
		TempDAO.saveTemp(t);
	}

	@Override
	public String toString() {
		return "Temperatur in Raum " + roomnumber + " betragt " + temp + " Grad C | " + timestamp.format(dtf);
	}

	public Temperature(String roomnumber, double temp) {
		this.roomnumber = roomnumber;
		this.temp = temp;
		this.timestamp = LocalDateTime.now();
	}
	
	public Temperature(String roomnumber, double temp, String timestamp) {
		this.roomnumber = roomnumber;
		this.temp = temp;
		this.timestamp = LocalDateTime.parse(timestamp, dtf);
	}

	public String getRoomnumber() {
		return roomnumber;
	}

	public void setRoomnumber(String roomnumber) {
		this.roomnumber = roomnumber;
	}

	public double getTemp() {
		return temp;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}
	
	public String getTimestampString() {
		return timestamp.format(dtf);
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

}
