package at.htlklu.temp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public class ServerTask extends Thread{

	public static ArrayList<Temperature> roomList;
	private Socket s;

	public ServerTask(Socket s) {
		this.s = s;
	};
	
	@Override
	public void run() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			PrintWriter pw = new PrintWriter(s.getOutputStream());
			
			pw.println("Temperaturuberwachung");
			pw.flush();
			
			while (true) {
				String msg = br.readLine();
				if (msg == null) {
					continue;
				}
				String[] args = msg.split(" ");
				if (msg.equals("EXIT")) {
					break;
				} else if (args[0].equals("SET")) {
					String[] argParam = args[1].split(":");
					Temperature.addRoom(argParam[0], Double.parseDouble(argParam[1]));
					pw.println("Temperaturwert gespeichert");
					pw.flush();
				} else if (args[0].equals("GET")) {
					Temperature temp = Temperature.getTempByRoomnumber(args[1]);
					pw.println(temp == null ? "Raum nicht vorhanden" : temp.toString());
					pw.flush();
				} else if (args[0].equals("LIST")) {
					pw.println("Verfugbare Raume:");
					Temperature.getAllTemps();
					for (Temperature temp : roomList) {
						pw.println(temp.getRoomnumber());
					}
					pw.flush();
				} else if (args[0].equals("ALL")) {
					pw.println("Alle Temperaturen:");
					Temperature.getAllTemps();
					for (Temperature temp : roomList) {
						pw.println(temp.toString());
					}
					pw.flush();
				} else {
					pw.println("Error: Unknown command");
					pw.flush();
				}
			}
			
			
			br.close();
			pw.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
