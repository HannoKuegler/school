import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class RollerCoasterTest {

	@Test
	public void age() throws InvalidAgeException {
		int age1 = 12;
		boolean result = RollerCoaster.isAllowedToEnterRollerCoaster(age1);
		assertFalse(result);
		
		boolean expected = true; 
		assertEquals(expected, result);

	}
	
	@Test
	public void age2() throws InvalidAgeException {
		int age1 = -1;
		Exception e = assertThrows(InvalidAgeException.class, () 
				-> RollerCoaster.isAllowedToEnterRollerCoaster(age1));
		
		assertEquals(("Ungültiges Alter"), e.getMessage());
	}
}
