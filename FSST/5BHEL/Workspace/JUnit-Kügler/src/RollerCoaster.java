
public class RollerCoaster {

	public static boolean isAllowedToEnterRollerCoaster(int age) throws InvalidAgeException {
		
		if(age < 0 || age > 120) {
			throw new InvalidAgeException("Ungültiges Alter");
		}
	

		if(age > 12 && age < 100) 
			return true;
		else 
			return false;
		
	}
}
