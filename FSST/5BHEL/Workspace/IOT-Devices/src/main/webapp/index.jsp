<%@page import="db.DAOIOT"%>
<%@page import="db.Device"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HTL IOT</title>
<style type="text/css">
@charset "UTF-8";

td, th {
	border: 2px solid #ddd;
	padding: 20px;
}

table, h1 {
	margin-left: 5%;
	margin-top: 5%;
}

#big {
	background-color: blue;
	color: white;
}

#aON {
	background-color: yellow;
	color: orange;
}

#aOFF {
	background-color: grey;
	color: white;
}
</style>
</head>
<body>

	<h1>HTL IOT DEVICES</h1>

	<table>
		<tr id=big>
			<th>ID</th>
			<th>Device Name</th>
			<th>State</th>
			<th>LD</th>
		</tr>
		<%
		String id2 = request.getParameter("id");
		System.out.print(id2);
		if (id2 != null) {
			int id = Integer.parseInt(id2);
			out.println(id);
			DAOIOT.switchState(id);
		}
		
		ArrayList<Device> devices = DAOIOT.getAllDevices();

		for (Device d : devices) {
			out.println("<tr>");

			out.println("<th>" + d.getId() + "</th>");
			out.println("<th>" + d.getName() + "</th>");
			if (d.isState()) {
				out.println("<th id=aON> <a href=index.jsp?id=" + d.getId() + ">" + d.isStateWithString() + "</th>");
			} else {
				out.println("<th id=aOFF> <a href=index.jsp?id=" + d.getId() + ">" + d.isStateWithString() + "</th>");
			}

			out.println("<th>" + d.getLd() + "</th>");

			out.println("</tr>");
		}
		%>



	</table>

</body>
</html>