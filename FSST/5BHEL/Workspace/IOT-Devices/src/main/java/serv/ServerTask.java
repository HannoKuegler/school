package serv;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerTask extends Thread {

	private Socket s;

	public ServerTask(Socket sock) {
		// TODO Auto-generated constructor stub
		s = sock;

	}

	@Override
	public void run() {

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			PrintWriter pw = new PrintWriter(s.getOutputStream(), true);

			String command = "";
			
			while((command = br.readLine())!=null) {
				
				if(command.startsWith("HI")) {
					pw.println("Seas");
					pw.flush();
				}
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
