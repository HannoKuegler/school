package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class DAOIOT {

	public static ArrayList<Device> getAllDevices() {

		try {

			Connection c = DbConnection.getConnection();
			Statement stmt = c.createStatement();
			String s = "SELECT * FROM device";
			ResultSet rs = stmt.executeQuery(s);
			ArrayList<Device> devices = new ArrayList();

			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				boolean state = Device.parseStringtoBool(rs.getString(3));
				Timestamp ts;
				if ((ts = rs.getTimestamp(4)) != null) {

					LocalDateTime ld = ts.toLocalDateTime();
					System.out.println("dev");
					devices.add(new Device(name, state, ld));
				} else
					devices.add(new Device(id, name, state));

			}
			return devices;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("ERRROROROROROR");
		}

		return null;
	}

	public static void switchState(int id) {

		try {
			Connection c = DbConnection.getConnection();
			String sql = "UPDATE device SET state=? WHERE id=?";
			PreparedStatement stmt = c.prepareStatement(sql);

			ArrayList<Device> devices = DAOIOT.getAllDevices();

			boolean state = devices.get(id - 1).isState();
			if (state) {
				state = false;
				stmt.setString(1, "OFF");
				devices.get(id).setState(state);
			} else {
				state = true;

				stmt.setString(1, "ON");
				devices.get(id).setState(state);
			}

			stmt.setInt(2, id);
			stmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void addDevice(Device d) {

		try {
			Connection c = DbConnection.getConnection();
			//INSERT INTO `iot`.`device` (`name`, `state`) VALUES ('keys', 'OFF');
			String sql = "INSERT INTO device (name, state) VALUES (?, ?)";
			PreparedStatement stmt = c.prepareStatement(sql);

			stmt.setString(1, d.getName());
			stmt.setString(2, d.parseBooltoString(d.isState()));

			stmt.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
