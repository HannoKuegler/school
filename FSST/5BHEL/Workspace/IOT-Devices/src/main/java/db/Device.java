package db;

import java.time.LocalDateTime;

public class Device {

	private int id;
	private String name;
	private boolean state;
	private LocalDateTime ld;

	public Device(String name, boolean state, LocalDateTime ld) {
		super();
		this.name = name;
		this.state = state;
		this.ld = ld;
	}

	public Device(String name, String s) {
		super();
		this.name = name;
		setStateWithString(s);
	}

	public Device(int id, String name, boolean state, LocalDateTime ld) {
		super();
		this.id = id;
		this.name = name;
		this.state = state;
		this.ld = ld;
	}
	
	public Device(int id, String name, boolean state) {
		super();
		this.id = id;
		this.name = name;
		this.state = state;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}
	
	public String isStateWithString() {
		return state + "";
	}

	public void setStateWithString(String s) {
		if(s.toUpperCase().equals("ON")) {
			this.state = true;
		}else if(s.toUpperCase().equals("OFF")) {
			this.state = false;
		}
	}

	public static boolean parseStringtoBool(String s) {
		if(s.equalsIgnoreCase("ON"))
			return true;
		else if(s.equalsIgnoreCase("ON"))
			return false;
		return false;
	}
	public static String parseBooltoString(boolean b) {
		if(b)
			return "ON";
		else 
			return "OFF";
	}

	public LocalDateTime getLd() {
		return ld;
	}

	public void setLd(LocalDateTime ld) {
		this.ld = ld;
	}
	
	

	@Override
	public String toString() {
		return "Device [id=" + id + ", name=" + name + ", state=" + state + ", ld=" + ld + "]";
	}

	
	
	
}
