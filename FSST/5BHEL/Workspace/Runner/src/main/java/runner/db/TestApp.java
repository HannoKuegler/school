package runner.db;

import java.util.ArrayList;

public class TestApp {

	public static void main(String[] args) {

		//Ausgabe
		ArrayList<Runner> runners = DAORunner.getAllRunnersSorted();
		for (Runner runner : runners) {
			System.out.println(runner);
		}
		System.out.println();
		
		// like um 1 bei mothe erhöhen
		DAORunner.incrementLikes(1);
		
		System.out.println();
		//Ausgabe
		runners = DAORunner.getAllRunnersSorted();
		for (Runner runner : runners) {
			System.out.println(runner);
		}
		
		System.out.println();
		
		// start a new run 
		DAORunner.createRun("Karmen", "13:03");
		
		
		System.out.println();
		//Ausgabe
		runners = DAORunner.getAllRunnersSorted();
		for (Runner runner : runners) {
			System.out.println(runner);
		}
		
		
		DAORunner.stopRun(12, "15:30", "mitn mothe spazieren");
		
		System.out.println();
		//Ausgabe
		runners = DAORunner.getAllRunnersSorted();
		for (Runner runner : runners) {
			System.out.println(runner);
		}
		
		
		
		
	}

}
