package runner.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;

import runner.db.DAORunner;
import runner.db.Runner;

public class ServerTh extends Thread {

	private Socket s;

	public ServerTh(Socket s) {
		super();
		this.s = s;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			PrintWriter pw = new PrintWriter(s.getOutputStream());

			pw.println("Mothe is da - Running Server 2525.025V58");
			pw.flush();

			while (true) {
				String in = br.readLine();
				System.out.println(in);
				if (in.startsWith("STARTRUN")) {
					
					try {
						String[] temp = in.split(" ");
						
						// LocalDateTime.now();
						if (!temp[1].isEmpty()) {
						DAORunner.createRun(temp[1], "23:00");

						ArrayList<Runner> swimmers = DAORunner.getAllRunners();
						Runner r = swimmers.get(swimmers.size()-1);
						
						String s = "Hallo "+temp[1]+", deine Lauf ID ist " + r.getId();
						
						pw.println(s);
						pw.flush();
						
						
						
						}
					} catch (Exception e) {
						// TODO: handle exception
						pw.println("Fehler beim starten");
						pw.flush();
					}
					
					
					

				} else if (in.startsWith("STOPRUN")) {

				} else if (in.startsWith("exit")) {
					pw.println("Lets exit - BYYEEEE");
					pw.flush();
					pw.close();
					br.close();
					s.close();
				} else {
					pw.println("Invalid Command");
					pw.flush();
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
