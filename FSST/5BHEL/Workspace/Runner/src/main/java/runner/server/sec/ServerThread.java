package runner.server.sec;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerThread extends Thread {

	private Socket s;

	public ServerThread(Socket s) {
		super();
		this.s = s;
	}

	@Override
	public void run() {
		
		try {
			
			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			PrintWriter pw = new PrintWriter(s.getOutputStream(), true);
			String command = "";
			while((command = br.readLine()) != null){
				if(command.equalsIgnoreCase("quit")){ 
					break;
				}
				System.out.println("Command:"+command);
			  }
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		
	}

	
	
}
