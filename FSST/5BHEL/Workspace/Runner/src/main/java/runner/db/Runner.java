package runner.db;

public class Runner {

	private int id;
	private String name;
	private double km;
	private String start, end, description;
	private int likes;

	public Runner(String name, double km, String start, String end, String description, int likes) {
		super();
		this.name = name;
		this.km = km;
		this.start = start;
		this.end = end;
		this.description = description;
		this.likes = likes;
	}
	
	public Runner(int id, String name, double km, String start, String end, String description, int likes) {
		super();
		this.id = id;
		this.name = name;
		this.km = km;
		this.start = start;
		this.end = end;
		this.description = description;
		this.likes = likes;
	}

	public Runner(String name, String start) {
		super();
		this.name = name;
		this.start = start;
	}
	
	@Override
	public String toString() {
		return "Runner [id=" + id + ", name=" + name + ", km=" + km + ", start=" + start + ", end=" + end
				+ ", description=" + description + ", likes=" + likes + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getKm() {
		return km;
	}

	public void setKm(double km) {
		this.km = km;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

}
