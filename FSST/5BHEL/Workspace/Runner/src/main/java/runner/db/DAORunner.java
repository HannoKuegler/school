package runner.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;

import com.mysql.cj.xdevapi.PreparableStatement;

public class DAORunner {

	public static ArrayList<Runner> getAllRunnersSorted() {

		try {
			Connection con = DbConnection.getConnection();
			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM runner";

			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<Runner> runners = new ArrayList();

			while (rs.next()) {

				int id = rs.getInt(1);
				String name = rs.getString(2);
				double km = rs.getDouble(3);
				String start = rs.getString(4);
				String end = rs.getString(5);
				String description = rs.getString(6);
				int likes = rs.getInt(7);

				Runner r = new Runner(id, name, km, start, end, description, likes);

				runners.add(r);

			}
			runners.sort(Comparator.comparing(Runner::getLikes).thenComparing(Runner::getName).reversed());
			return runners;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}
	
	public static ArrayList<Runner> getAllRunners() {

		try {
			Connection con = DbConnection.getConnection();
			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM runner";

			ResultSet rs = stmt.executeQuery(sql);

			ArrayList<Runner> runners = new ArrayList();

			while (rs.next()) {

				int id = rs.getInt(1);
				String name = rs.getString(2);
				double km = rs.getDouble(3);
				String start = rs.getString(4);
				String end = rs.getString(5);
				String description = rs.getString(6);
				int likes = rs.getInt(7);

				Runner r = new Runner(id, name, km, start, end, description, likes);

				runners.add(r);

			}
			return runners;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	public static void incrementLikes(int id) {

		System.out.println("PLLLLSSS INC ME");

		try {
			Connection con = DbConnection.getConnection();

			// UPDATE `runner`.`runner` SET `likes` = '2' WHERE (`id` = '3');

			String sql = "UPDATE runner SET likes = ? WHERE (id = " + id + ")";
			PreparedStatement pst = con.prepareStatement(sql);
			int likes = 0;

			for (Runner r : DAORunner.getAllRunnersSorted()) {

				if (r.getId() == id) {

					likes = r.getLikes() + 1;
					System.out.println(likes);

				}
				pst.setInt(1, likes);
				pst.executeUpdate();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void createRun(String name, String start) {

		try {
			Connection con = DbConnection.getConnection();
			String sql = "INSERT INTO runner (name, start) VALUES (?, ?)";
			PreparedStatement pst = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

			pst.setString(1, name);
			pst.setString(2, start);
			int id = 0;
			int ret = pst.executeUpdate();
			if (ret == 1) {
				ResultSet rs = pst.getGeneratedKeys();

				if (rs.next())
					id = rs.getInt(1);
			}
			System.out.println(id);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void stopRun(int id, String end, String description) {

		try {
			Connection con = DbConnection.getConnection();

			// UPDATE `runner`.`runner` SET `end` = '17:20', `description` = 'kaka' WHERE
			// (`id` = '9');

			String sql = "UPDATE runner SET end = ?, description = ? WHERE (`id` = '" + id + "')";
			PreparedStatement pst = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

			pst.setString(1, end);
			pst.setString(2, description);
			pst.executeUpdate();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
