<%@page import="runner.db.Runner"%>
<%@page import="runner.db.DAORunner"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Mothe ist am Boot</title>
</head>
<body>

	<h1>Schlauchboot</h1>

	<table>
		<tr>
			<th>Lauf ID</th>
			<th>Läufer Name</th>
			<th>Laufdauer</th>
			<th>km</th>
			<th>Beschreibung</th>
			<th>Likes</th>


		</tr>

		<%
		String id2 = request.getParameter("id");
		if (id2 != null) {
			int id2increment = Integer.parseInt(id2);
			DAORunner.incrementLikes(id2increment);

		}

		ArrayList<Runner> runners = DAORunner.getAllRunnersSorted();

		for (Runner runner : runners) {

			out.println("<tr>");

			out.println("<td>" + runner.getId() + "</td>");
			out.println("<td>" + runner.getName() + "</td>");

			String end = runner.getEnd();
			String start = runner.getStart();
			if (end != null) {

				String[] endA = end.split(":");
				String[] startA = start.split(":");

				int stunden = Integer.parseInt(endA[0]) - Integer.parseInt(startA[0]);
				int minuten = Integer.parseInt(endA[1]) - Integer.parseInt(startA[1]);

				out.println("<td>" + stunden + ":" + minuten + "</td>");
			} else {
				out.println("<td>00:00</td>");
			}
			out.println("<td>" + runner.getKm() + "</td>");
			out.println("<td>" + runner.getDescription() + "</td>");

			out.println("<td><a href='index.jsp?id=" + runner.getId() + "'>" + runner.getLikes()+"</td>");

			out.println("</tr>");

		}
		%>




	</table>


</body>
</html>