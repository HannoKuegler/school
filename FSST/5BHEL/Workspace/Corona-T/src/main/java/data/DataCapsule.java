package data;

import java.util.ArrayList;

import db.CoronaDao;
import db.Patient;

public class DataCapsule {

	public static ArrayList<Patient> getPatients(){
		return CoronaDao.getAllPatientsSortedByStatus();
	}
}
