package db;

public class Patient {
	
	private int id; 
	private String vorname, nachname, status, rolle;

	
	
	
	public Patient(String vorname, String nachname, String status, String rolle) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.status = status;
		this.rolle = rolle;
	}

	public Patient(int id, String vorname, String nachname, String status, String rolle) {
		super();
		this.id = id;
		this.vorname = vorname;
		this.nachname = nachname;
		this.status = status;
		this.rolle = rolle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Patient ["+id+" vorname=" + vorname + ", nachname=" + nachname + ", status=" + status + ", rolle=" + rolle
				+ "]";
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRolle() {
		return rolle;
	}

	public void setRolle(String rolle) {
		this.rolle = rolle;
	} 
	
	
	
}
