package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;



public class CoronaDao {

	public static void updateStatus(int id, String status) {
		//UPDATE `corona-tracker`.`patient` SET `status` = 'krank' WHERE (`id` = '9');
		Connection c;
		try {
			c = DbConnection.getConnection();
			Statement stmt = c.createStatement();
			String sql = "UPDATE `corona-tracker`.`patient` SET `status` = '"+status+"' WHERE (`id` = " + id + ")";

			stmt.executeUpdate(sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static ArrayList<Patient> getAllPatientsSortedByStatus() {
		// SELECT * FROM `corona-tracker`.patient ORDER BY status ASC;
		try {
			
			Connection c = DbConnection.getConnection();
			Statement stmt = c.createStatement();
			String sql = "SELECT * FROM `corona-tracker`.patient ORDER BY status ASC";
			
			
			ResultSet rs = stmt.executeQuery(sql);
			ArrayList<Patient> patients = new ArrayList<>(); 

			while(rs.next()) {
				int id = rs.getInt(1);
				String vorname = rs.getString(2);
				String nachname = rs.getString(3);
				String status = rs.getString(4);
				String rolle = rs.getString(5);
				Patient p = new Patient(id ,vorname, nachname, status, rolle); 
				patients.add(p);
				System.out.println(p);
			}
			
			return patients;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	public static void deletePatient(int id) {
		// DELETE FROM `corona-tracker`.`patient` WHERE (`id` = '6');

		Connection c;
		try {
			c = DbConnection.getConnection();
			Statement stmt = c.createStatement();
			String sql = "DELETE FROM `corona-tracker`.`patient` WHERE (`id` = " + id + ")";

			stmt.executeUpdate(sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void insertNetPatient(Patient p) {

		// INSERT INTO `corona-tracker`.`patient` (`vorname`, `nachname`, `status`,
		// `rolle`) VALUES ('Peter', 'Lippo', 'krank', 'Bürger');

		try {

			Connection c = DbConnection.getConnection();
			String sql = "INSERT INTO `corona-tracker`.`patient` (`vorname`, `nachname`, `status`, `rolle`) VALUES (?,?,?,?)";
			PreparedStatement pst = c.prepareStatement(sql);

			pst.setString(1, p.getVorname());
			pst.setString(2, p.getNachname());
			pst.setString(3, p.getStatus());
			pst.setString(4, p.getRolle());

			int u = pst.executeUpdate();

			if (u >= 0) {
				System.out.println("Erfolgreich insertet!");
			} else {
				System.out.println("Fehlerhafter Insert!");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
