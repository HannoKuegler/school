package socket;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import db.CoronaDao;
import db.Patient;

public class ServerThread extends Thread {

	private Socket s;

	public ServerThread(Socket s) {
		super();
		this.s = s;
	}

	@Override
	public void run() {
		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			PrintWriter pw = new PrintWriter(s.getOutputStream());

			pw.println("Welcome to Corona-Tacker! Stay healthy or get better soon!");
			pw.flush();

			while (true) {
				String in = br.readLine();
				System.out.println(in);
				if (in.startsWith("Register")) {
					
					try {
						String[] data = in.split(" ");
						String vorname = data[1];
						String nachname = data[2];
						Patient p = new Patient(vorname, nachname, "unbekannt", "Besucher");

						
						if (!vorname.isEmpty() && !nachname.isEmpty()) {
							CoronaDao.insertNetPatient(p);

							pw.println(p + " registert");
							pw.flush();
						} 
							
					} catch (Exception e) {
						// TODO: handle exception
						pw.println("Fehler beim reg");
						pw.flush();
					}
					

				} else if (in.startsWith("MyStatus")) {
					pw.println("Lets status");
					pw.flush();

					try {
						String[] data = in.split(" ");
						int id = Integer.parseInt(data[1]);
						String status = data[2];

						if (status.equals("unbekannt") || status.equals("genesen") || status.equals("krank") || status.equals("verstorben")
								|| status.equals("nicht identifiziert")) {

							CoronaDao.updateStatus(id, status);
							pw.println("Status super");
							pw.flush();
						} 
						
					} catch (Exception e) {
						// TODO: handle exception
						pw.println("Fehler Status");
						pw.flush();
					}
					

				} else if (in.startsWith("exit")) {
					pw.println("Lets exit - BYYEEEE");
					pw.flush();
					pw.close();
					br.close();
					s.close();
				} else {
					pw.println("Invalid Command");
					pw.flush();
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
