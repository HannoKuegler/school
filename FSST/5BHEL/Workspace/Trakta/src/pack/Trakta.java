package pack;

public class Trakta {

	private String vendor; 
	private int bj; 
	private double price;
	private int weight;
	private int power;
	
	@Override
	public String toString() {
		return "" + vendor + " (" + power + "PS) : " + bj + ", " + weight + "kg, €"
				+ price + "";
	}

	public Trakta(String vendor, int bj, double price, int weight, int power) {
		super();
		this.vendor = vendor;
		this.bj = bj;
		this.price = price;
		this.weight = weight;
		this.power = power;
	}
	public Trakta(Trakta t) {
		this.bj = t.getBj();
		this.power = t.getPower();
		this.price = t.getPrice();
		this.vendor = t.getVendor();
		this.weight = t.getWeight();
		
	}

	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public int getBj() {
		return bj;
	}

	public void setBj(int bj) {
		this.bj = bj;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	} 
	
	
	
	
}
