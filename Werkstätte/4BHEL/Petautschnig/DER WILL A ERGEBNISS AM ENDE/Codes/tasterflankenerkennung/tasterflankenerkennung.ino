/* Tastenlangdruckdetektion mit Arudino und myShield
 * Bei langem Tastendruck, außerhalb von 0,75s, wird die zaehleriable
 * in einzelnen Schritten in- bzw. dekrementiert.
 * Bei schnellem Tastendruck, innerhalb von 0,75s, wird die
 * zaehleriable exakt in- bzw. dekrementiert.
 * Paul Kleinlercher, 27.01.2021
 */

 

#define S1_PIN A2 // rechter Taster
#define S2_PIN A3 // linker Taster

 

// Tasterzustände in zaehleriablen fassen
boolean s1, s1_old, s2, s2_old;
// Zeitpunkte, in denen die Taster gedrückt bzw. losgelassen werden
unsigned long r_time, p_time = 0;
// Zeit in ms bis ein langer Tastendruck erkannt wird
int pressed_time = 750;
// Zu manipulierende zaehleriable
byte zaehler = 0;

 

void setup() {
// Pin Zuweisungen der Switches
pinMode(S1_PIN, OUTPUT);
pinMode(S2_PIN, OUTPUT);

 

Serial.begin(9600);
}

 


void loop() {
// letzte Tasterzustände abspeichern
s1_old=s1;
s2_old=s2;

 

// aktuelle Tasterzustände
  s1 = digitalRead(A2);
  s2 = digitalRead(A3);

 

// zur Stabilisierung
delay(1);

 

 if(s1){
   if(s1 && !s1_old) // +
   p_time = millis();
   else if((millis()-p_time)>pressed_time) { zaehler++;}
 }
 else if(!s1 && s1_old) { // + losgelassen
    r_time = millis();
    if((r_time-p_time)<pressed_time) // kurzer Tastendruck
    zaehler++;
    }

 

 if(s2){ 
   if( && !s2_old) // -
   p_time = millis();
  else if((millis()-p_time)>pressed_time){ 
    zaehler--;
    }
 }
 else if(!s2 && s2_old) { // - losgelassen
    r_time = millis();
    if((r_time-p_time)<pressed_time) // kurzer Tastendruck
    zaehler--; }

 

Serial.println(zaehler);
}

 



 
