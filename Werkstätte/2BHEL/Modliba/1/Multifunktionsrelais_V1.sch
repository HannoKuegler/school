<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="dots" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="e-klemmen">
<description>&lt;b&gt;Klemmen für Elektropläne&lt;/b&gt;&lt;p&gt;

Diese Bibliothek enthält Klemmen sowie Devices für Einspeisung und Erdung. Folgendes ist zu
beachten: &lt;p&gt;

&lt;b&gt;Einspeisungs-Devices&lt;/b&gt;&lt;p&gt;

Enthalten kein Package, da kein entsprechendes Bauteil existiert, das in einer Materialliste erscheinen sollte. &lt;p&gt;

&lt;b&gt;Erdungs-Devices&lt;/b&gt;&lt;p&gt;

Enthalten ein Package, da in einer Materialliste zumindest ein Bauteil mit Klemmmöglichkeit
erscheinen muss.&lt;p&gt;

&lt;b&gt;Klemmen&lt;/b&gt;&lt;p&gt;

Klemmennamen müssen im Schaltplan mit &lt;i&gt;X&lt;/i&gt; beginnen, damit Klemmenplan und Brückenplan richtig erzeugt werden. Deshalb ist ihr Prefix im Device auf X gesetzt. Bitte auch im Schaltplan keine
anderen Namen verwenden. Siehe auch: User-Language-Programm e-klemmenplan.ulp.&lt;p&gt;

&lt;b&gt;Brückenklemmen&lt;/b&gt;&lt;p&gt;

Brückenklemmen sind Klemmen, die zusätzlich zu den Drahtanschlüssen die Möglichkeit bieten, mit einem Brückenkamm eine Reihe von Klemmen zu verbinden. Mit dem User-Language-Programm
e-brueckenverwaltung.ulp werden solche Brücken definiert und als Liste ausgegeben. In dessen
Hilfe erfahren Sie Details. Dieses Programm setzt einige Dinge bei den verwendeten Bauteilen
voraus (nur wichtig, wenn Sie eigene Brückenklemmen definieren wollen):&lt;p&gt;

Der Device-Name muss &lt;i&gt;BRUECKE&lt;/i&gt; enthalten, andere Klemmen dürfen  &lt;i&gt;BRUECKE&lt;/i&gt;
nicht als Namensbestandteil enthalten.&lt;p&gt;

Die Pin-Namen der Klemmensymbole müssen 1 und 2 sein. Die damit verbundenen Pad-Namen
des Package müssen 1.1, 1.2; 2.1, 2.2 usw. sein. Dabei entspricht die Zahl vor dem Punkt dem
Gate-Namen (1, 2, 3..).&lt;p&gt;

Jedes Klemmensymbol muss gesondert definiert sein, da es für den Referenz-Text zwei Attribute verwendet, deren Platzhalter je Symbol unterschiedlich sind. Der Name der Attribute ist 51 und 52,
wenn es sich um Klemme 5 handelt. Der Parameter &lt;i&gt;display&lt;/i&gt; für diese Attribute (der im
Attribut-Bearbeitungsmenü im Feld &lt;i&gt;Anzeige&lt;/i&gt; eingestellt wird) muss im Schaltplan auf
&lt;i&gt;Off&lt;/i&gt; stehen, sonst werden die Referenz-Texte nicht an der richtigen Stelle dargestellt.
Wenn Sie das ULP zur Brückenverwaltung verwenden, geschieht das automatisch.&lt;p&gt;

&lt;p&gt;&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="KLEMME_1-1">
<description>Dummy</description>
<pad name="1.1" x="0" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="1.2" x="0" y="-2.54" drill="0.5" diameter="1" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="EINSPEISUNG_GLEICHSPANNUNG">
<wire x1="-7.62" y1="5.08" x2="8.89" y2="5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="5.08" x2="8.89" y2="-5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-6.35" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-7.62" y="-7.62" size="1.778" layer="94">EINSPEISUNG</text>
<text x="-2.921" y="1.651" size="1.778" layer="94">+</text>
<text x="1.651" y="2.794" size="1.778" layer="94" rot="R270">0V</text>
<pin name="+-EXT" x="-2.54" y="7.62" visible="off" length="short" direction="sup" rot="R270"/>
<pin name="0V-EXT" x="2.54" y="7.62" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="KLEMME_1-1">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.016" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="1.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="1.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EINSPEISUNG_GLEICHSPANNUNG" prefix="ESP" uservalue="yes">
<description>Einspeisung für Gleichspannung</description>
<gates>
<gate name="G$1" symbol="EINSPEISUNG_GLEICHSPANNUNG" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KLEMME_1-1" prefix="X" uservalue="yes">
<description>Klemme 1 Pol.</description>
<gates>
<gate name="1" symbol="KLEMME_1-1" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="KLEMME_1-1">
<connects>
<connect gate="1" pin="1.1" pad="1.1"/>
<connect gate="1" pin="1.2" pad="1.2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-lampen-signalisation">
<description>&lt;b&gt;Signalgeber für Elektropläne&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="LAMPE">
<description>Dummy</description>
<pad name="1" x="0" y="5.08" drill="0.2" diameter="0.6" shape="square"/>
<pad name="2" x="0" y="-5.08" drill="0.2" diameter="0.6" shape="square"/>
<text x="0" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="LAMPE">
<wire x1="0" y1="2.54" x2="0" y2="2.034" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.034" width="0.1524" layer="94"/>
<wire x1="-1.4352" y1="-1.4352" x2="1.4352" y2="1.4352" width="0.1524" layer="94"/>
<wire x1="1.4352" y1="-1.4352" x2="-1.4352" y2="1.4352" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="2.034" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-2.54" y="-7.62" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="X1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="X2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SIGNALLAMPE" prefix="P" uservalue="yes">
<description>Lampe</description>
<gates>
<gate name="G$1" symbol="LAMPE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LAMPE">
<connects>
<connect gate="G$1" pin="X1" pad="1"/>
<connect gate="G$1" pin="X2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-schalter">
<description>&lt;b&gt;Schalter für Elektropläne&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="EINTASTER">
<description>Dummy</description>
<pad name="14" x="0" y="-1.905" drill="0.8" shape="square"/>
<pad name="13" x="0" y="1.905" drill="0.8" shape="square"/>
<text x="0" y="2.54" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TASTER_SCHLIESSER">
<wire x1="-0.762" y1="0" x2="-0.889" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="-1.143" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="0" x2="-3.302" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.143" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="-4.191" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.937" y1="1.143" x2="-5.08" y2="1.143" width="0.1524" layer="94"/>
<wire x1="-3.937" y1="-1.143" x2="-5.08" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="1.524" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.651" width="0.254" layer="94"/>
<text x="-3.81" y="3.81" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-3.81" y="-2.54" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-3.81" y="-10.16" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-3.81" y="-5.08" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-3.81" y="-7.62" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="14" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="13" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TASTER_SCHLIESSER" prefix="S" uservalue="yes">
<description>Ein-Taster (Arbeitskontakt)</description>
<gates>
<gate name="G$1" symbol="TASTER_SCHLIESSER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EINTASTER">
<connects>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-schuetze-relais">
<description>&lt;b&gt;Schütze und Relais für Elektropläne&lt;/b&gt;&lt;p&gt;

Wenn das von Ihnen gewünschte Schütz in der vorliegenden Bibliothek nicht vorhanden ist, laden Sie
die Bibliothek &lt;u&gt;e-symbole.lbr&lt;/u&gt;, und starten Sie das User-Languag-Programm  &lt;u&gt;e-bauteil-erstellen.ulp&lt;/u&gt;.&lt;p&gt; 

&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MULTIFUNKTIONSRELAIS">
<description>Dummy</description>
<pad name="15" x="10.16" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="18" x="12.7" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="A1" x="0" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="A2" x="0" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="B1" x="2.54" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="A3" x="2.54" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="16" x="7.62" y="-12.7" drill="0.5" diameter="1" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="MULTIFUNKTIONSRELAIS">
<wire x1="5.715" y1="1.651" x2="2.54" y2="1.651" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.651" x2="-2.54" y2="1.651" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.651" x2="-3.175" y2="1.651" width="0.254" layer="94"/>
<wire x1="-3.175" y1="1.651" x2="-3.175" y2="-1.651" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.651" x2="-2.54" y2="-1.651" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-1.651" x2="2.54" y2="-1.651" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.651" x2="5.715" y2="-1.651" width="0.254" layer="94"/>
<wire x1="5.715" y1="-1.651" x2="5.715" y2="1.651" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="1.651" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.651" width="0.1524" layer="94"/>
<wire x1="-6.477" y1="1.651" x2="-3.175" y2="1.651" width="0.254" layer="94"/>
<wire x1="-6.477" y1="-1.651" x2="-6.477" y2="1.651" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.651" x2="-6.477" y2="-1.651" width="0.254" layer="94"/>
<text x="-7.62" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-7.62" y="0" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="A1" x="-2.54" y="5.08" visible="pad" length="short" direction="in" rot="R270"/>
<pin name="A2" x="-2.54" y="-5.08" visible="pad" length="short" direction="in" rot="R90"/>
<pin name="B1" x="2.54" y="5.08" visible="pad" length="short" direction="in" rot="R270"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.651" width="0.1524" layer="94"/>
<pin name="A3" x="2.54" y="-5.08" visible="pad" length="short" direction="in" rot="R90"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.651" width="0.1524" layer="94"/>
<rectangle x1="-6.35" y1="-1.524" x2="-3.302" y2="1.524" layer="94"/>
</symbol>
<symbol name="MF_WECHSLER">
<wire x1="-2.54" y1="-1.524" x2="-1.27" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="-1.651" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-1.524" width="0.1524" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="15" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="18" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="16" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MULTIFUNKTIONSRELAIS" prefix="K" uservalue="yes">
<description>Zeitrelais, 1-pol.</description>
<gates>
<gate name="G$1" symbol="MULTIFUNKTIONSRELAIS" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="MF_WECHSLER" x="17.78" y="0"/>
</gates>
<devices>
<device name="" package="MULTIFUNKTIONSRELAIS">
<connects>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="B1" pad="B1"/>
<connect gate="G$2" pin="15" pad="15"/>
<connect gate="G$2" pin="16" pad="16"/>
<connect gate="G$2" pin="18" pad="18"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="ESP1" library="e-klemmen" deviceset="EINSPEISUNG_GLEICHSPANNUNG" device=""/>
<part name="+T-X1/1" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="P1" library="e-lampen-signalisation" deviceset="SIGNALLAMPE" device=""/>
<part name="S1" library="e-schalter" deviceset="TASTER_SCHLIESSER" device=""/>
<part name="K1" library="e-schuetze-relais" deviceset="MULTIFUNKTIONSRELAIS" device=""/>
<part name="+T-X1/2" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="+R-X1/3" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="+R-X1/4" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="+T-X2/10" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="+R-X2/7" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="+R-X2/5" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="+R-X2/6" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
<part name="+T-X2/7" library="e-klemmen" deviceset="KLEMME_1-1" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="ESP1" gate="G$1" x="17.78" y="76.2" smashed="yes" rot="R270">
<attribute name="VALUE" x="13.97" y="82.55" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+T-X1/1" gate="1" x="43.18" y="68.58" smashed="yes">
<attribute name="PART" x="41.402" y="69.596" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="P1" gate="G$1" x="63.5" y="30.48" smashed="yes">
<attribute name="PART" x="60.96" y="33.02" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="60.96" y="30.48" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="60.96" y="22.86" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="60.96" y="27.94" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="60.96" y="25.4" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="S1" gate="G$1" x="43.18" y="58.42" smashed="yes">
<attribute name="PART" x="39.37" y="62.23" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="39.37" y="55.88" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="39.37" y="48.26" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="39.37" y="53.34" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="39.37" y="50.8" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="K1" gate="G$2" x="60.96" y="58.42" smashed="yes">
<attribute name="PART" x="57.15" y="60.96" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="K1" gate="G$1" x="45.72" y="30.48" smashed="yes">
<attribute name="PART" x="38.1" y="33.02" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="38.1" y="30.48" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+T-X1/2" gate="1" x="43.18" y="48.26" smashed="yes">
<attribute name="PART" x="41.402" y="49.276" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-X1/3" gate="1" x="43.18" y="40.64" smashed="yes">
<attribute name="PART" x="41.402" y="41.656" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-X1/4" gate="1" x="43.18" y="20.32" smashed="yes">
<attribute name="PART" x="41.402" y="21.336" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X2/10" gate="1" x="63.5" y="20.32" smashed="yes">
<attribute name="PART" x="61.722" y="21.336" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-X2/7" gate="1" x="63.5" y="48.26" smashed="yes" rot="MR0">
<attribute name="PART" x="65.278" y="49.276" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="+R-X2/5" gate="1" x="60.96" y="68.58" smashed="yes">
<attribute name="PART" x="59.182" y="69.596" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+R-X2/6" gate="1" x="58.42" y="48.26" smashed="yes">
<attribute name="PART" x="56.642" y="49.276" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X2/7" gate="1" x="63.5" y="40.64" smashed="yes">
<attribute name="PART" x="61.722" y="41.656" size="1.778" layer="95" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="+-EXT" class="0">
<segment>
<pinref part="ESP1" gate="G$1" pin="+-EXT"/>
<pinref part="+R-X2/5" gate="1" pin="1.2"/>
<wire x1="25.4" y1="78.74" x2="43.18" y2="78.74" width="0.1524" layer="91"/>
<wire x1="43.18" y1="78.74" x2="60.96" y2="78.74" width="0.1524" layer="91"/>
<wire x1="60.96" y1="78.74" x2="60.96" y2="71.12" width="0.1524" layer="91"/>
<pinref part="+T-X1/1" gate="1" pin="1.2"/>
<wire x1="43.18" y1="71.12" x2="43.18" y2="78.74" width="0.1524" layer="91"/>
<junction x="43.18" y="78.74"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="+T-X1/1" gate="1" pin="1.1"/>
<pinref part="S1" gate="G$1" pin="13"/>
<wire x1="43.18" y1="66.04" x2="43.18" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="14"/>
<pinref part="+T-X1/2" gate="1" pin="1.2"/>
<wire x1="43.18" y1="53.34" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="+T-X1/2" gate="1" pin="1.1"/>
<pinref part="+R-X1/3" gate="1" pin="1.2"/>
<wire x1="43.18" y1="45.72" x2="43.18" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="+R-X1/3" gate="1" pin="1.1"/>
<pinref part="K1" gate="G$1" pin="A1"/>
<wire x1="43.18" y1="38.1" x2="43.18" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="K1" gate="G$1" pin="A2"/>
<pinref part="+R-X1/4" gate="1" pin="1.2"/>
<wire x1="43.18" y1="25.4" x2="43.18" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="+R-X2/7" gate="1" pin="1.1"/>
<wire x1="63.5" y1="43.18" x2="63.5" y2="45.72" width="0.1524" layer="91"/>
<pinref part="+T-X2/7" gate="1" pin="1.2"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="K1" gate="G$2" pin="15"/>
<pinref part="+R-X2/5" gate="1" pin="1.1"/>
<wire x1="60.96" y1="63.5" x2="60.96" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="K1" gate="G$2" pin="18"/>
<pinref part="+R-X2/7" gate="1" pin="1.2"/>
<wire x1="63.5" y1="53.34" x2="63.5" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="K1" gate="G$2" pin="16"/>
<pinref part="+R-X2/6" gate="1" pin="1.2"/>
<wire x1="58.42" y1="53.34" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="X2"/>
<pinref part="+T-X2/10" gate="1" pin="1.2"/>
<wire x1="63.5" y1="25.4" x2="63.5" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="0V-EXT" class="0">
<segment>
<pinref part="+T-X2/10" gate="1" pin="1.1"/>
<wire x1="63.5" y1="17.78" x2="63.5" y2="15.24" width="0.1524" layer="91"/>
<wire x1="63.5" y1="15.24" x2="43.18" y2="15.24" width="0.1524" layer="91"/>
<wire x1="43.18" y1="15.24" x2="30.48" y2="15.24" width="0.1524" layer="91"/>
<wire x1="30.48" y1="15.24" x2="30.48" y2="73.66" width="0.1524" layer="91"/>
<pinref part="ESP1" gate="G$1" pin="0V-EXT"/>
<wire x1="30.48" y1="73.66" x2="25.4" y2="73.66" width="0.1524" layer="91"/>
<pinref part="+R-X1/4" gate="1" pin="1.1"/>
<wire x1="43.18" y1="17.78" x2="43.18" y2="15.24" width="0.1524" layer="91"/>
<junction x="43.18" y="15.24"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="X1"/>
<wire x1="63.5" y1="35.56" x2="63.5" y2="38.1" width="0.1524" layer="91"/>
<pinref part="+T-X2/7" gate="1" pin="1.1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
