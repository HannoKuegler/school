<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="dots" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Weiß" color="0" fill="0" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="e-schalter">
<description>&lt;b&gt;Schalter für Elektropläne&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="KIPPSCHALTER_AUS">
<description>Dummy</description>
<pad name="11" x="0" y="3.175" drill="0.8" shape="square"/>
<pad name="12" x="0" y="-3.175" drill="0.8" shape="square"/>
<text x="0" y="2.54" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SCHALTER_SCHLIESSER_HANDBETAETIGT">
<wire x1="-5.08" y1="1.143" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="0" x2="-3.429" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="1.524" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.524" width="0.254" layer="94"/>
<text x="-3.81" y="3.81" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-3.81" y="-2.54" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-3.81" y="-10.16" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-3.81" y="-5.08" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-3.81" y="-7.62" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="CS1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="S1" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SCHALTER_SCHLIESSER_HANDBETAETIGT" prefix="S" uservalue="yes">
<description>Schließer</description>
<gates>
<gate name="G$1" symbol="SCHALTER_SCHLIESSER_HANDBETAETIGT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="KIPPSCHALTER_AUS">
<connects>
<connect gate="G$1" pin="CS1" pad="11"/>
<connect gate="G$1" pin="S1" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-klemmen">
<description>&lt;b&gt;Klemmen für Elektropläne&lt;/b&gt;&lt;p&gt;

Diese Bibliothek enthält Klemmen sowie Devices für Einspeisung und Erdung. Folgendes ist zu
beachten: &lt;p&gt;

&lt;b&gt;Einspeisungs-Devices&lt;/b&gt;&lt;p&gt;

Enthalten kein Package, da kein entsprechendes Bauteil existiert, das in einer Materialliste erscheinen sollte. &lt;p&gt;

&lt;b&gt;Erdungs-Devices&lt;/b&gt;&lt;p&gt;

Enthalten ein Package, da in einer Materialliste zumindest ein Bauteil mit Klemmmöglichkeit
erscheinen muss.&lt;p&gt;

&lt;b&gt;Klemmen&lt;/b&gt;&lt;p&gt;

Klemmennamen müssen im Schaltplan mit &lt;i&gt;X&lt;/i&gt; beginnen, damit Klemmenplan und Brückenplan richtig erzeugt werden. Deshalb ist ihr Prefix im Device auf X gesetzt. Bitte auch im Schaltplan keine
anderen Namen verwenden. Siehe auch: User-Language-Programm e-klemmenplan.ulp.&lt;p&gt;

&lt;b&gt;Brückenklemmen&lt;/b&gt;&lt;p&gt;

Brückenklemmen sind Klemmen, die zusätzlich zu den Drahtanschlüssen die Möglichkeit bieten, mit einem Brückenkamm eine Reihe von Klemmen zu verbinden. Mit dem User-Language-Programm
e-brueckenverwaltung.ulp werden solche Brücken definiert und als Liste ausgegeben. In dessen
Hilfe erfahren Sie Details. Dieses Programm setzt einige Dinge bei den verwendeten Bauteilen
voraus (nur wichtig, wenn Sie eigene Brückenklemmen definieren wollen):&lt;p&gt;

Der Device-Name muss &lt;i&gt;BRUECKE&lt;/i&gt; enthalten, andere Klemmen dürfen  &lt;i&gt;BRUECKE&lt;/i&gt;
nicht als Namensbestandteil enthalten.&lt;p&gt;

Die Pin-Namen der Klemmensymbole müssen 1 und 2 sein. Die damit verbundenen Pad-Namen
des Package müssen 1.1, 1.2; 2.1, 2.2 usw. sein. Dabei entspricht die Zahl vor dem Punkt dem
Gate-Namen (1, 2, 3..).&lt;p&gt;

Jedes Klemmensymbol muss gesondert definiert sein, da es für den Referenz-Text zwei Attribute verwendet, deren Platzhalter je Symbol unterschiedlich sind. Der Name der Attribute ist 51 und 52,
wenn es sich um Klemme 5 handelt. Der Parameter &lt;i&gt;display&lt;/i&gt; für diese Attribute (der im
Attribut-Bearbeitungsmenü im Feld &lt;i&gt;Anzeige&lt;/i&gt; eingestellt wird) muss im Schaltplan auf
&lt;i&gt;Off&lt;/i&gt; stehen, sonst werden die Referenz-Texte nicht an der richtigen Stelle dargestellt.
Wenn Sie das ULP zur Brückenverwaltung verwenden, geschieht das automatisch.&lt;p&gt;

&lt;p&gt;&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="KLEMME_1-10">
<description>Dummy</description>
<pad name="1.1" x="0" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="1.2" x="0" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="2.1" x="2.54" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="2.2" x="2.54" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="3.1" x="5.08" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="3.2" x="5.08" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="4.1" x="7.62" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="4.2" x="7.62" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="5.1" x="10.16" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="5.2" x="10.16" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="6.1" x="12.7" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="6.2" x="12.7" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="7.1" x="15.24" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="7.2" x="15.24" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="8.1" x="17.78" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="8.2" x="17.78" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="9.1" x="20.32" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="9.2" x="20.32" y="-2.54" drill="0.5" diameter="1" shape="square"/>
<pad name="10.1" x="22.86" y="-12.7" drill="0.5" diameter="1" shape="square"/>
<pad name="10.2" x="22.86" y="-2.54" drill="0.5" diameter="1" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="EINSPEISUNG_GLEICHSPANNUNG">
<wire x1="-7.62" y1="5.08" x2="8.89" y2="5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="5.08" x2="8.89" y2="-5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-6.35" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<text x="-7.62" y="-7.62" size="1.778" layer="94">EINSPEISUNG</text>
<text x="-2.921" y="1.651" size="1.778" layer="94">+</text>
<text x="1.651" y="2.794" size="1.778" layer="94" rot="R270">0V</text>
<pin name="+-EXT" x="-2.54" y="7.62" visible="off" length="short" direction="sup" rot="R270"/>
<pin name="0V-EXT" x="2.54" y="7.62" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="KLEMME_1-1">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="1.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="1.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE </text>
</symbol>
<symbol name="KLEMME_2">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="2.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="2.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_3">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.524" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="3.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="3.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.524" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_4">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="4.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="4.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_5">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="5.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_6">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="6.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_7">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="7.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="7.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_8">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="8.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="8.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_9">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.016" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="9.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="9.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.524" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
<symbol name="KLEMME_10">
<wire x1="0" y1="2.54" x2="0" y2="0.889" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.889" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.8054" width="0.1524" layer="94"/>
<text x="-1.778" y="1.524" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="10.1" x="0" y="-2.54" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="10.2" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
<text x="-1.778" y="-1.016" size="1.778" layer="95" rot="R180">&gt;GATE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="EINSPEISUNG_GLEICHSPANNUNG" prefix="ESP" uservalue="yes">
<description>Einspeisung für Gleichspannung</description>
<gates>
<gate name="G$1" symbol="EINSPEISUNG_GLEICHSPANNUNG" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KLEMME_1-10" prefix="X" uservalue="yes">
<description>Klemme 10 Pol.</description>
<gates>
<gate name="1" symbol="KLEMME_1-1" x="-48.26" y="0" swaplevel="1"/>
<gate name="2" symbol="KLEMME_2" x="-35.56" y="0" swaplevel="2"/>
<gate name="3" symbol="KLEMME_3" x="-22.86" y="0" swaplevel="3"/>
<gate name="4" symbol="KLEMME_4" x="-10.16" y="0" swaplevel="4"/>
<gate name="5" symbol="KLEMME_5" x="2.54" y="0" swaplevel="5"/>
<gate name="6" symbol="KLEMME_6" x="15.24" y="0" swaplevel="6"/>
<gate name="7" symbol="KLEMME_7" x="27.94" y="0" swaplevel="7"/>
<gate name="8" symbol="KLEMME_8" x="40.64" y="0" swaplevel="8"/>
<gate name="9" symbol="KLEMME_9" x="53.34" y="0" swaplevel="9"/>
<gate name="10" symbol="KLEMME_10" x="66.04" y="0" swaplevel="10"/>
</gates>
<devices>
<device name="" package="KLEMME_1-10">
<connects>
<connect gate="1" pin="1.1" pad="1.1"/>
<connect gate="1" pin="1.2" pad="1.2"/>
<connect gate="10" pin="10.1" pad="10.1"/>
<connect gate="10" pin="10.2" pad="10.2"/>
<connect gate="2" pin="2.1" pad="2.1"/>
<connect gate="2" pin="2.2" pad="2.2"/>
<connect gate="3" pin="3.1" pad="3.1"/>
<connect gate="3" pin="3.2" pad="3.2"/>
<connect gate="4" pin="4.1" pad="4.1"/>
<connect gate="4" pin="4.2" pad="4.2"/>
<connect gate="5" pin="5.1" pad="5.1"/>
<connect gate="5" pin="5.2" pad="5.2"/>
<connect gate="6" pin="6.1" pad="6.1"/>
<connect gate="6" pin="6.2" pad="6.2"/>
<connect gate="7" pin="7.1" pad="7.1"/>
<connect gate="7" pin="7.2" pad="7.2"/>
<connect gate="8" pin="8.1" pad="8.1"/>
<connect gate="8" pin="8.2" pad="8.2"/>
<connect gate="9" pin="9.1" pad="9.1"/>
<connect gate="9" pin="9.2" pad="9.2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-lampen-signalisation">
<description>&lt;b&gt;Signalgeber für Elektropläne&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="LAMPE">
<description>Dummy</description>
<pad name="1" x="0" y="5.08" drill="0.2" diameter="0.6" shape="square"/>
<pad name="2" x="0" y="-5.08" drill="0.2" diameter="0.6" shape="square"/>
<text x="0" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="LAMPE">
<wire x1="0" y1="2.54" x2="0" y2="2.034" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.034" width="0.1524" layer="94"/>
<wire x1="-1.4352" y1="-1.4352" x2="1.4352" y2="1.4352" width="0.1524" layer="94"/>
<wire x1="1.4352" y1="-1.4352" x2="-1.4352" y2="1.4352" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="2.034" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-2.54" y="-7.62" size="1.778" layer="96" rot="R180">&gt;FUNKTION</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R180">&gt;TYPE</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R180">&gt;HERSTELLER</text>
<pin name="X1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="X2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SIGNALLAMPE" prefix="P" uservalue="yes">
<description>Lampe</description>
<gates>
<gate name="G$1" symbol="LAMPE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LAMPE">
<connects>
<connect gate="G$1" pin="X1" pad="1"/>
<connect gate="G$1" pin="X2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-sps-logo">
<description>&lt;a href="http://www.element14.com/community/people/HeiJo"&gt; Ersteller &lt;/a&gt;&lt;br&gt;
&lt;a href="http://www.element14.com/community/message/58180#58180/l/re-siemens-logo-kontaktspiegel-mit-eagle-elektrocad-library"&gt; Forumbeitrag &lt;/a&gt;&lt;br&gt;
4. 12.08.2012 22:04 (als Antwort auf: ALF_Z)&lt;br&gt;
Re: Siemens Logo! Kontaktspiegel mit Eagle Elektrocad library&lt;br&gt;
PS Das Lib kann natürlich jeder verwenden der mag !!!&lt;br&gt;
&lt;a href="http://www.element14.com/community/servlet/JiveServlet/downloadBody/49001-102-1-256331/logo.zip"&gt; Download &lt;/a&gt;&lt;br&gt;</description>
<packages>
<package name="SIEMENS_LOGO_230RCE">
<description>Dummy</description>
<pad name="L1" x="-5.08" y="1.27" drill="0.8" shape="square"/>
<pad name="N" x="-2.54" y="1.27" drill="0.8" shape="square"/>
<pad name="I1" x="0" y="1.27" drill="0.8" shape="square"/>
<pad name="I2" x="2.54" y="1.27" drill="0.8" shape="square"/>
<pad name="I3" x="5.08" y="1.27" drill="0.8" shape="square"/>
<pad name="I4" x="7.62" y="1.27" drill="0.8" shape="square"/>
<pad name="I5" x="10.16" y="1.27" drill="0.8" shape="square"/>
<pad name="I6" x="12.7" y="1.27" drill="0.8" shape="square"/>
<pad name="I7" x="15.24" y="1.27" drill="0.8" shape="square"/>
<pad name="I8" x="17.78" y="1.27" drill="0.8" shape="square"/>
<pad name="Q1.1" x="-5.08" y="-2.54" drill="0.8" shape="square"/>
<pad name="Q1.2" x="-2.54" y="-2.54" drill="0.8" shape="square"/>
<pad name="Q2.1" x="0" y="-2.54" drill="0.8" shape="square" rot="R180"/>
<pad name="Q2.2" x="2.54" y="-2.54" drill="0.8" shape="square" rot="R180"/>
<pad name="Q3.1" x="5.08" y="-2.54" drill="0.8" shape="square"/>
<pad name="Q3.2" x="7.62" y="-2.54" drill="0.8" shape="square"/>
<pad name="Q4.1" x="10.16" y="-2.54" drill="0.8" shape="square" rot="R180"/>
<pad name="Q4.2" x="12.7" y="-2.54" drill="0.8" shape="square" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="EINGANG_1-4">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.127" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-2.54" y="-1.27" size="1.27" layer="95" rot="R180"></text>
<pin name="IN" x="0" y="5.08" visible="pad" length="short" direction="in" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="KONTAKT">
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.524" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.778" width="0.1524" layer="94"/>
<text x="-1.905" y="-1.27" size="1.27" layer="95" rot="R180"></text>
<text x="-2.54" y="1.27" size="1.778" layer="95" rot="R180">&gt;PART</text>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="out" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="out" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="EINGANG_5-8">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.127" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.127" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.127" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="-2.54" y="-1.27" size="1.27" layer="95" rot="R180"></text>
<pin name="IN" x="0" y="5.08" visible="pad" length="short" direction="in" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="SPANNUNGSVERSORGUNG_24V">
<wire x1="-22.86" y1="12.7" x2="27.94" y2="12.7" width="0.127" layer="94"/>
<wire x1="27.94" y1="12.7" x2="27.94" y2="-12.7" width="0.254" layer="94"/>
<wire x1="27.94" y1="-12.7" x2="-22.86" y2="-12.7" width="0.127" layer="94"/>
<wire x1="-22.86" y1="12.7" x2="-22.86" y2="-12.7" width="0.254" layer="94"/>
<text x="1.27" y="25.4" size="1.778" layer="95" rot="R180">&gt;PART</text>
<text x="26.67" y="25.4" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="M" x="-15.24" y="25.4" visible="pad" length="short" direction="pwr" rot="R270"/>
<pin name="L+" x="-17.78" y="25.4" visible="pad" length="short" direction="pwr" rot="R270"/>
<wire x1="-22.86" y1="12.7" x2="-22.86" y2="17.78" width="0.254" layer="94"/>
<wire x1="-22.86" y1="17.78" x2="27.94" y2="17.78" width="0.127" layer="94"/>
<wire x1="27.94" y1="17.78" x2="27.94" y2="12.7" width="0.254" layer="94"/>
<wire x1="-22.86" y1="17.78" x2="-22.86" y2="22.86" width="0.254" layer="94"/>
<wire x1="-22.86" y1="22.86" x2="27.94" y2="22.86" width="0.254" layer="94"/>
<wire x1="27.94" y1="22.86" x2="27.94" y2="17.78" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-12.7" x2="-22.86" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-17.78" x2="27.94" y2="-17.78" width="0.127" layer="94"/>
<wire x1="27.94" y1="-17.78" x2="27.94" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-17.78" x2="-22.86" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-22.86" x2="27.94" y2="-22.86" width="0.254" layer="94"/>
<wire x1="27.94" y1="-22.86" x2="27.94" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-8.128" x2="-20.32" y2="8.128" width="0.0508" layer="94"/>
<wire x1="-20.32" y1="8.128" x2="-2.032" y2="8.128" width="0.0508" layer="94"/>
<wire x1="-2.032" y1="8.128" x2="-2.032" y2="-8.128" width="0.0508" layer="94"/>
<wire x1="-2.032" y1="-8.128" x2="-20.32" y2="-8.128" width="0.0508" layer="94"/>
<wire x1="3.048" y1="3.556" x2="1.016" y2="1.524" width="0.0508" layer="94"/>
<wire x1="1.016" y1="1.524" x2="3.048" y2="-0.508" width="0.0508" layer="94"/>
<wire x1="3.048" y1="-0.508" x2="3.048" y2="3.556" width="0.0508" layer="94"/>
<wire x1="4.064" y1="4.572" x2="8.128" y2="4.572" width="0.0508" layer="94"/>
<wire x1="8.128" y1="4.572" x2="6.096" y2="6.604" width="0.0508" layer="94"/>
<wire x1="6.096" y1="6.604" x2="4.064" y2="4.572" width="0.0508" layer="94"/>
<wire x1="4.064" y1="-1.524" x2="8.128" y2="-1.524" width="0.0508" layer="94"/>
<wire x1="8.128" y1="-1.524" x2="6.096" y2="-3.556" width="0.0508" layer="94"/>
<wire x1="6.096" y1="-3.556" x2="4.064" y2="-1.524" width="0.0508" layer="94"/>
<wire x1="9.144" y1="3.556" x2="9.144" y2="-0.508" width="0.0508" layer="94"/>
<wire x1="9.144" y1="-0.508" x2="11.176" y2="1.524" width="0.0508" layer="94"/>
<wire x1="11.176" y1="1.524" x2="9.144" y2="3.556" width="0.0508" layer="94"/>
<text x="0.508" y="-8.89" size="1.27" layer="94">ESC</text>
<text x="8.128" y="-8.89" size="1.27" layer="94">OK</text>
<wire x1="0" y1="-7.112" x2="0" y2="-9.144" width="0.0508" layer="94"/>
<wire x1="0" y1="-9.144" x2="4.572" y2="-9.144" width="0.0508" layer="94"/>
<wire x1="4.572" y1="-9.144" x2="4.572" y2="-7.112" width="0.0508" layer="94"/>
<wire x1="4.572" y1="-7.112" x2="0" y2="-7.112" width="0.0508" layer="94"/>
<wire x1="7.112" y1="-7.112" x2="7.112" y2="-9.144" width="0.0508" layer="94"/>
<wire x1="7.112" y1="-9.144" x2="11.684" y2="-9.144" width="0.0508" layer="94"/>
<wire x1="11.684" y1="-9.144" x2="11.684" y2="-7.112" width="0.0508" layer="94"/>
<wire x1="11.684" y1="-7.112" x2="7.112" y2="-7.112" width="0.0508" layer="94"/>
<wire x1="26.416" y1="9.652" x2="21.336" y2="9.652" width="0.0508" layer="94"/>
<wire x1="21.336" y1="9.652" x2="21.336" y2="-10.668" width="0.0508" layer="94"/>
<wire x1="21.336" y1="-10.668" x2="25.4" y2="-10.668" width="0.0508" layer="94"/>
<wire x1="25.4" y1="-10.668" x2="26.416" y2="-9.652" width="0.0508" layer="94"/>
<wire x1="26.416" y1="-9.652" x2="26.416" y2="-2.794" width="0.0508" layer="94"/>
<wire x1="26.416" y1="-2.794" x2="26.924" y2="-2.794" width="0.0508" layer="94"/>
<wire x1="26.924" y1="-2.794" x2="26.924" y2="-0.762" width="0.0508" layer="94"/>
<wire x1="26.924" y1="-0.762" x2="26.416" y2="-0.762" width="0.0508" layer="94"/>
<wire x1="26.416" y1="-0.762" x2="26.416" y2="9.652" width="0.0508" layer="94"/>
<wire x1="26.416" y1="-2.794" x2="26.416" y2="-0.762" width="0.0508" layer="94"/>
<wire x1="25.654" y1="-0.762" x2="25.908" y2="-0.762" width="0.0508" layer="94"/>
<wire x1="25.908" y1="-0.762" x2="25.908" y2="-2.794" width="0.0508" layer="94"/>
<wire x1="25.908" y1="-2.794" x2="25.654" y2="-2.794" width="0.0508" layer="94"/>
<wire x1="25.654" y1="-2.794" x2="25.654" y2="-0.762" width="0.0508" layer="94"/>
<wire x1="17.78" y1="0.508" x2="20.32" y2="0.508" width="0.0508" layer="94"/>
<wire x1="20.32" y1="0.508" x2="20.32" y2="-0.508" width="0.0508" layer="94"/>
<wire x1="20.32" y1="-0.508" x2="19.812" y2="-1.016" width="0.0508" layer="94"/>
<wire x1="19.812" y1="-1.016" x2="17.78" y2="-1.016" width="0.0508" layer="94"/>
<wire x1="17.78" y1="-1.016" x2="17.78" y2="0.508" width="0.0508" layer="94"/>
<polygon width="0.0508" layer="94">
<vertex x="20.193" y="-0.381"/>
<vertex x="19.812" y="-0.762"/>
<vertex x="19.812" y="0"/>
</polygon>
<text x="18.034" y="-0.635" size="0.762" layer="94" ratio="30">SD</text>
<text x="18.034" y="0.762" size="0.762" layer="94" ratio="30">X50</text>
<text x="-18.796" y="-12.192" size="1.6764" layer="94" ratio="30">LOGO!</text>
<wire x1="2.54" y1="-6.096" x2="9.652" y2="-6.096" width="0.1524" layer="94"/>
<circle x="-17.78" y="-19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-18.3896" y1="-18.5928" x2="-17.3228" y2="-19.6596" width="0.0508" layer="94"/>
<wire x1="-18.2372" y1="-18.4404" x2="-17.1704" y2="-19.5072" width="0.0508" layer="94"/>
<circle x="-15.24" y="-19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-15.8496" y1="-18.5928" x2="-14.7828" y2="-19.6596" width="0.0508" layer="94"/>
<wire x1="-15.6972" y1="-18.4404" x2="-14.6304" y2="-19.5072" width="0.0508" layer="94"/>
<circle x="-10.16" y="-19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-10.7696" y1="-18.5928" x2="-9.7028" y2="-19.6596" width="0.0508" layer="94"/>
<wire x1="-10.6172" y1="-18.4404" x2="-9.5504" y2="-19.5072" width="0.0508" layer="94"/>
<circle x="-7.62" y="-19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-8.2296" y1="-18.5928" x2="-7.1628" y2="-19.6596" width="0.0508" layer="94"/>
<wire x1="-8.0772" y1="-18.4404" x2="-7.0104" y2="-19.5072" width="0.0508" layer="94"/>
<circle x="-2.54" y="-19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-3.1496" y1="-18.5928" x2="-2.0828" y2="-19.6596" width="0.0508" layer="94"/>
<wire x1="-2.9972" y1="-18.4404" x2="-1.9304" y2="-19.5072" width="0.0508" layer="94"/>
<circle x="0" y="-19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-0.6096" y1="-18.5928" x2="0.4572" y2="-19.6596" width="0.0508" layer="94"/>
<wire x1="-0.4572" y1="-18.4404" x2="0.6096" y2="-19.5072" width="0.0508" layer="94"/>
<circle x="5.08" y="-19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="4.4704" y1="-18.5928" x2="5.5372" y2="-19.6596" width="0.0508" layer="94"/>
<wire x1="4.6228" y1="-18.4404" x2="5.6896" y2="-19.5072" width="0.0508" layer="94"/>
<circle x="7.62" y="-19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="7.0104" y1="-18.5928" x2="8.0772" y2="-19.6596" width="0.0508" layer="94"/>
<wire x1="7.1628" y1="-18.4404" x2="8.2296" y2="-19.5072" width="0.0508" layer="94"/>
<circle x="12.446" y="-19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="11.8364" y1="-18.5928" x2="12.9032" y2="-19.6596" width="0.0508" layer="94"/>
<wire x1="11.9888" y1="-18.4404" x2="13.0556" y2="-19.5072" width="0.0508" layer="94"/>
<circle x="-17.78" y="19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-18.3896" y1="19.5072" x2="-17.3228" y2="18.4404" width="0.0508" layer="94"/>
<wire x1="-18.2372" y1="19.6596" x2="-17.1704" y2="18.5928" width="0.0508" layer="94"/>
<circle x="-15.24" y="19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-15.8496" y1="19.5072" x2="-14.7828" y2="18.4404" width="0.0508" layer="94"/>
<wire x1="-15.6972" y1="19.6596" x2="-14.6304" y2="18.5928" width="0.0508" layer="94"/>
<circle x="-12.7" y="19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-13.3096" y1="19.5072" x2="-12.2428" y2="18.4404" width="0.0508" layer="94"/>
<wire x1="-13.1572" y1="19.6596" x2="-12.0904" y2="18.5928" width="0.0508" layer="94"/>
<circle x="-10.16" y="19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-10.7696" y1="19.5072" x2="-9.7028" y2="18.4404" width="0.0508" layer="94"/>
<wire x1="-10.6172" y1="19.6596" x2="-9.5504" y2="18.5928" width="0.0508" layer="94"/>
<circle x="-7.62" y="19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-8.2296" y1="19.5072" x2="-7.1628" y2="18.4404" width="0.0508" layer="94"/>
<wire x1="-8.0772" y1="19.6596" x2="-7.0104" y2="18.5928" width="0.0508" layer="94"/>
<circle x="-5.08" y="19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-5.6896" y1="19.5072" x2="-4.6228" y2="18.4404" width="0.0508" layer="94"/>
<wire x1="-5.5372" y1="19.6596" x2="-4.4704" y2="18.5928" width="0.0508" layer="94"/>
<circle x="-2.54" y="19.05" radius="0.762" width="0.127" layer="94"/>
<circle x="0" y="19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-0.6096" y1="19.5072" x2="0.4572" y2="18.4404" width="0.0508" layer="94"/>
<wire x1="-0.4572" y1="19.6596" x2="0.6096" y2="18.5928" width="0.0508" layer="94"/>
<circle x="2.54" y="19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="1.9304" y1="19.5072" x2="2.9972" y2="18.4404" width="0.0508" layer="94"/>
<wire x1="2.0828" y1="19.6596" x2="3.1496" y2="18.5928" width="0.0508" layer="94"/>
<circle x="5.08" y="19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="4.4704" y1="19.5072" x2="5.5372" y2="18.4404" width="0.0508" layer="94"/>
<wire x1="4.6228" y1="19.6596" x2="5.6896" y2="18.5928" width="0.0508" layer="94"/>
<circle x="7.62" y="19.05" radius="0.762" width="0.127" layer="94"/>
<wire x1="-3.1496" y1="19.5072" x2="-2.0828" y2="18.4404" width="0.0508" layer="94"/>
<wire x1="-2.9972" y1="19.6596" x2="-1.9304" y2="18.5928" width="0.0508" layer="94"/>
<text x="-21.336" y="18.542" size="0.762" layer="94" ratio="30">X10</text>
<text x="-20.828" y="-19.558" size="0.762" layer="94" ratio="30">X11</text>
<wire x1="-17.78" y1="-19.812" x2="-17.78" y2="-20.574" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-20.574" x2="-17.018" y2="-20.574" width="0.254" layer="94"/>
<wire x1="-17.018" y1="-20.574" x2="-16.256" y2="-20.066" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-19.812" x2="-15.24" y2="-20.574" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-20.574" x2="-16.002" y2="-20.574" width="0.254" layer="94"/>
<text x="-17.272" y="-21.844" size="0.8128" layer="94" ratio="30">Q1</text>
<wire x1="-10.16" y1="-19.812" x2="-10.16" y2="-20.574" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-20.574" x2="-9.398" y2="-20.574" width="0.254" layer="94"/>
<wire x1="-9.398" y1="-20.574" x2="-8.636" y2="-20.066" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-19.812" x2="-7.62" y2="-20.574" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-20.574" x2="-8.382" y2="-20.574" width="0.254" layer="94"/>
<text x="-9.652" y="-21.844" size="0.8128" layer="94" ratio="30">Q2</text>
<wire x1="-2.54" y1="-19.812" x2="-2.54" y2="-20.574" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-20.574" x2="-1.778" y2="-20.574" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-20.574" x2="-1.016" y2="-20.066" width="0.254" layer="94"/>
<wire x1="0" y1="-19.812" x2="0" y2="-20.574" width="0.254" layer="94"/>
<wire x1="0" y1="-20.574" x2="-0.762" y2="-20.574" width="0.254" layer="94"/>
<text x="-2.032" y="-21.844" size="0.8128" layer="94" ratio="30">Q3</text>
<wire x1="5.08" y1="-19.812" x2="5.08" y2="-20.574" width="0.254" layer="94"/>
<wire x1="5.08" y1="-20.574" x2="5.842" y2="-20.574" width="0.254" layer="94"/>
<wire x1="5.842" y1="-20.574" x2="6.604" y2="-20.066" width="0.254" layer="94"/>
<wire x1="7.62" y1="-19.812" x2="7.62" y2="-20.574" width="0.254" layer="94"/>
<wire x1="7.62" y1="-20.574" x2="6.858" y2="-20.574" width="0.254" layer="94"/>
<text x="5.588" y="-21.844" size="0.8128" layer="94" ratio="30">Q4</text>
<text x="-18.796" y="-20.828" size="0.8128" layer="94" ratio="30">1</text>
<text x="-14.732" y="-20.828" size="0.8128" layer="94" ratio="30">2</text>
<text x="-11.176" y="-20.828" size="0.8128" layer="94" ratio="30">1</text>
<text x="-7.112" y="-20.828" size="0.8128" layer="94" ratio="30">2</text>
<text x="-3.556" y="-20.828" size="0.8128" layer="94" ratio="30">1</text>
<text x="0.508" y="-20.828" size="0.8128" layer="94" ratio="30">2</text>
<text x="4.064" y="-20.828" size="0.8128" layer="94" ratio="30">1</text>
<text x="8.128" y="-20.828" size="0.8128" layer="94" ratio="30">2</text>
<text x="-18.288" y="20.574" size="0.8128" layer="94" ratio="30">L+</text>
<text x="-15.621" y="20.574" size="0.8128" layer="94" ratio="30">M</text>
<text x="-13.208" y="20.574" size="0.8128" layer="94" ratio="30">I1</text>
<text x="-10.668" y="20.574" size="0.8128" layer="94" ratio="30">I2</text>
<text x="-8.128" y="20.574" size="0.8128" layer="94" ratio="30">I3</text>
<text x="-5.588" y="20.574" size="0.8128" layer="94" ratio="30">I4</text>
<text x="-3.048" y="20.574" size="0.8128" layer="94" font="fixed" ratio="30">I5</text>
<text x="-0.508" y="20.574" size="0.8128" layer="94" font="fixed" ratio="30">I6</text>
<text x="2.032" y="20.574" size="0.8128" layer="94" font="fixed" ratio="30">I7</text>
<text x="4.572" y="20.574" size="0.8128" layer="94" font="fixed" ratio="30">I8</text>
<text x="-18.542" y="16.002" size="0.8128" layer="94" ratio="30">DC 12/24V           INPUT 8xDC</text>
<text x="-18.542" y="-16.51" size="0.8128" layer="94" ratio="30">OUTPUT 4xRELAY/10A</text>
<text x="-9.398" y="-12.192" size="0.8128" layer="94" ratio="30">24RCE</text>
<text x="18.542" y="-12.192" size="0.6096" layer="94" ratio="30">6ED1 052-1FB00-0BA7</text>
<text x="5.334" y="11.684" size="0.5842" layer="94" ratio="30">WARNING: DO NOT DISCONNECT WHILE CIRCUT IS LIVE</text>
<text x="5.334" y="10.414" size="0.5842" layer="94" ratio="30">UNLESS AREA IS KNOWN TO BE NON-HAZARDOUS</text>
<text x="11.684" y="-21.844" size="0.8128" layer="94" ratio="30">PE</text>
<text x="16.764" y="-19.304" size="0.6096" layer="94" ratio="30">LAN</text>
<text x="14.986" y="-20.828" size="0.6096" layer="94" ratio="30">MAC ADDRESS</text>
<text x="14.478" y="-22.352" size="0.6096" layer="94" ratio="30">00-1C-06-00-00-00</text>
<text x="24.638" y="-21.336" size="0.6096" layer="94" ratio="30">LINK</text>
<text x="24.638" y="-19.812" size="0.6096" layer="94" ratio="30">RX/TX</text>
<rectangle x1="23.241" y1="-19.939" x2="24.003" y2="-19.177" layer="94"/>
<rectangle x1="23.241" y1="-21.463" x2="24.003" y2="-20.701" layer="94"/>
<rectangle x1="23.368" y1="-21.336" x2="23.876" y2="-20.828" layer="100"/>
<rectangle x1="23.368" y1="-19.812" x2="23.876" y2="-19.304" layer="100"/>
<text x="12.7" y="-14.986" size="0.6096" layer="94" ratio="30">1</text>
<text x="11.684" y="-14.986" size="0.6096" layer="94" ratio="30">X</text>
<text x="11.684" y="-16.002" size="0.6096" layer="94" ratio="30">3</text>
<text x="12.7" y="-16.002" size="0.6096" layer="94" ratio="30">4</text>
<wire x1="12.446" y1="-14.351" x2="12.446" y2="-16.129" width="0.0508" layer="94"/>
<wire x1="11.557" y1="-15.24" x2="13.335" y2="-15.24" width="0.0508" layer="94"/>
<rectangle x1="15.494" y1="-16.256" x2="26.67" y2="-14.224" layer="94"/>
<text x="-18.796" y="9.398" size="1.778" layer="94" font="vector" ratio="22">SIEMENS</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LOGO_24RCE" prefix="-A">
<gates>
<gate name="I1" symbol="EINGANG_1-4" x="48.26" y="-20.32" addlevel="can"/>
<gate name="I2" symbol="EINGANG_1-4" x="55.88" y="-20.32" addlevel="can"/>
<gate name="I3" symbol="EINGANG_1-4" x="63.5" y="-20.32" addlevel="can"/>
<gate name="I4" symbol="EINGANG_1-4" x="71.12" y="-20.32" addlevel="can"/>
<gate name="Q1" symbol="KONTAKT" x="48.26" y="15.24" addlevel="can"/>
<gate name="Q2" symbol="KONTAKT" x="55.88" y="15.24" addlevel="can"/>
<gate name="Q3" symbol="KONTAKT" x="71.12" y="15.24" addlevel="can"/>
<gate name="Q4" symbol="KONTAKT" x="63.5" y="15.24" addlevel="can"/>
<gate name="I5" symbol="EINGANG_5-8" x="48.26" y="-2.54" addlevel="can"/>
<gate name="I6" symbol="EINGANG_5-8" x="55.88" y="-2.54" addlevel="can"/>
<gate name="I7" symbol="EINGANG_5-8" x="63.5" y="-2.54" addlevel="can"/>
<gate name="I8" symbol="EINGANG_5-8" x="71.12" y="-2.54" addlevel="can"/>
<gate name="G$1" symbol="SPANNUNGSVERSORGUNG_24V" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SIEMENS_LOGO_230RCE">
<connects>
<connect gate="G$1" pin="L+" pad="L1"/>
<connect gate="G$1" pin="M" pad="N"/>
<connect gate="I1" pin="IN" pad="I1"/>
<connect gate="I2" pin="IN" pad="I2"/>
<connect gate="I3" pin="IN" pad="I3"/>
<connect gate="I4" pin="IN" pad="I4"/>
<connect gate="I5" pin="IN" pad="I5"/>
<connect gate="I6" pin="IN" pad="I6"/>
<connect gate="I7" pin="IN" pad="I7"/>
<connect gate="I8" pin="IN" pad="I8"/>
<connect gate="Q1" pin="1" pad="Q1.1"/>
<connect gate="Q1" pin="2" pad="Q1.2"/>
<connect gate="Q2" pin="1" pad="Q2.1"/>
<connect gate="Q2" pin="2" pad="Q2.2"/>
<connect gate="Q3" pin="1" pad="Q3.1"/>
<connect gate="Q3" pin="2" pad="Q3.2"/>
<connect gate="Q4" pin="1" pad="Q4.1"/>
<connect gate="Q4" pin="2" pad="Q4.2"/>
</connects>
<technologies>
<technology name="">
<attribute name="FUNKTION" value="" constant="no"/>
<attribute name="HERSTELLER" value="Siemens"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="e-elektro-zeichnungsrahmen">
<description>&lt;b&gt;Rahmen für Elektropläne&lt;/b&gt;&lt;p&gt;
Die vordefinierten Rahmen eignen sich für &lt;u&gt;Kontakt- und Signalreferenzen&lt;/u&gt;, die ab Version 5.0 möglich sind.&lt;p&gt;

Wird ein Rahmen mit vordefinerten &lt;u&gt;Attributen&lt;/u&gt;, z. B. RAHMEN_A4_8Z-19S, in eine neue Schaltung
geholt, dann sind zunächst die Platzhaltertexte (&gt;ATTRIBUTNAME) sichtbar, weil diese Attribute noch nicht
angelegt sind. Verwenden Sie das User-Language-Programm &lt;u&gt;e-attributverwaltung.ulp&lt;/u&gt;, um auf
bequeme Weise die noch nicht definierten Attribute zu generieren.&lt;p&gt;
&lt;author&gt;Autor librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="RAHMEN_A4_8Z-19S_A4">
<frame x1="0" y1="0" x2="287" y2="190" columns="16" rows="8" layer="94" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD_HTL">
<wire x1="0" y1="0" x2="64.77" y2="0" width="0.1016" layer="94"/>
<wire x1="86.36" y1="12.065" x2="64.77" y2="12.065" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="64.77" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="12.065" width="0.1016" layer="94"/>
<wire x1="86.36" y1="12.065" x2="86.36" y2="5.08" width="0.1016" layer="94"/>
<wire x1="64.77" y1="5.08" x2="64.77" y2="0" width="0.1016" layer="94"/>
<wire x1="64.77" y1="0" x2="86.36" y2="0" width="0.1016" layer="94"/>
<wire x1="64.77" y1="12.065" x2="64.77" y2="5.08" width="0.1016" layer="94"/>
<wire x1="64.77" y1="12.065" x2="0" y2="12.065" width="0.1016" layer="94"/>
<wire x1="64.77" y1="5.08" x2="86.36" y2="5.08" width="0.1016" layer="94"/>
<wire x1="86.36" y1="5.08" x2="86.36" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="12.065" x2="0" y2="16.51" width="0.1016" layer="94"/>
<wire x1="86.36" y1="31.75" x2="40.64" y2="31.75" width="0.1016" layer="94"/>
<wire x1="40.64" y1="31.75" x2="0" y2="31.75" width="0.1016" layer="94"/>
<wire x1="86.36" y1="31.75" x2="86.36" y2="16.51" width="0.1016" layer="94"/>
<wire x1="0" y1="16.51" x2="40.64" y2="16.51" width="0.1016" layer="94"/>
<wire x1="40.64" y1="16.51" x2="86.36" y2="16.51" width="0.1016" layer="94"/>
<wire x1="0" y1="16.51" x2="0" y2="21.59" width="0.1016" layer="94"/>
<wire x1="0" y1="21.59" x2="0" y2="31.75" width="0.1016" layer="94"/>
<wire x1="86.36" y1="16.51" x2="86.36" y2="12.065" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.032" layer="94" font="vector">Datum:</text>
<text x="66.04" y="1.27" size="2.032" layer="94" font="vector">Seite:</text>
<text x="76.2" y="1.27" size="2.032" layer="94" font="vector">&gt;SHEET</text>
<text x="66.04" y="7.62" size="2.032" layer="94" font="vector">Version:</text>
<text x="1.27" y="13.335" size="2.032" layer="94" font="vector">Dateiname:</text>
<text x="1.27" y="7.62" size="2.032" layer="94" font="vector">Projektname:</text>
<text x="25.4" y="13.335" size="2.032" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="42.545" y="17.78" size="2.159" layer="94" font="vector">HTL | MÖSSINGERSTRASSE</text>
<polygon width="0" layer="94">
<vertex x="42.2591" y="21.685"/>
<vertex x="47.9343" y="24.0226" curve="-15.518484"/>
<vertex x="68.8171" y="29.6571" curve="-38.339913"/>
<vertex x="73.8618" y="28.6006"/>
<vertex x="74.8004" y="28.8354" curve="-21.727164"/>
<vertex x="83.2474" y="29.3049" curve="-91.727553"/>
<vertex x="85.2418" y="26.6051" curve="-42.039893"/>
<vertex x="82.296" y="21.59"/>
<vertex x="76.3449" y="21.5889" curve="45.002249"/>
<vertex x="77.1468" y="24.6103" curve="90.001852"/>
<vertex x="74.0524" y="26.5466"/>
<vertex x="73.7445" y="26.4877" curve="-45.000011"/>
<vertex x="71.7779" y="21.7399"/>
<vertex x="71.628" y="21.59"/>
<vertex x="65.2706" y="21.6037" curve="18.915156"/>
<vertex x="65.9186" y="24.4094" curve="90"/>
<vertex x="64.1588" y="26.5704" curve="20.762994"/>
<vertex x="47.817" y="23.2009" curve="-4.636247"/>
<vertex x="43.0321" y="21.7127" curve="-49.715671"/>
</polygon>
<wire x1="40.64" y1="16.51" x2="40.64" y2="21.59" width="0.1016" layer="94"/>
<wire x1="40.64" y1="21.59" x2="40.64" y2="31.75" width="0.1016" layer="94"/>
<wire x1="0" y1="21.59" x2="40.64" y2="21.59" width="0.1016" layer="94"/>
<text x="1.27" y="29.21" size="2.032" layer="94" font="vector">Name:</text>
<text x="1.27" y="17.78" size="2.032" layer="94" font="vector">Klasse:</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RAHMEN_ELEKTRO_HTL-KLU" prefix="RAHMEN">
<description>Zeichnungsrahmen DIN A4, 8 Zeilen, 19 Spalten ohne Dokumentenfeld</description>
<gates>
<gate name="G$1" symbol="RAHMEN_A4_8Z-19S_A4" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="DOCFIELD_HTL" x="200.66" y="0" addlevel="always"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="S5" library="e-schalter" deviceset="SCHALTER_SCHLIESSER_HANDBETAETIGT" device=""/>
<part name="S4" library="e-schalter" deviceset="SCHALTER_SCHLIESSER_HANDBETAETIGT" device=""/>
<part name="ESP1" library="e-klemmen" deviceset="EINSPEISUNG_GLEICHSPANNUNG" device=""/>
<part name="+T-X1" library="e-klemmen" deviceset="KLEMME_1-10" device=""/>
<part name="P1" library="e-lampen-signalisation" deviceset="SIGNALLAMPE" device=""/>
<part name="P2" library="e-lampen-signalisation" deviceset="SIGNALLAMPE" device=""/>
<part name="P3" library="e-lampen-signalisation" deviceset="SIGNALLAMPE" device=""/>
<part name="S3" library="e-schalter" deviceset="SCHALTER_SCHLIESSER_HANDBETAETIGT" device=""/>
<part name="+T-X2" library="e-klemmen" deviceset="KLEMME_1-10" device=""/>
<part name="-A1" library="e-sps-logo" deviceset="LOGO_24RCE" device=""/>
<part name="RAHMEN1" library="e-elektro-zeichnungsrahmen" deviceset="RAHMEN_ELEKTRO_HTL-KLU" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="220.98" y="7.112" size="2.54" layer="91">LOGO! Lauflicht</text>
<text x="220.98" y="1.27" size="2.54" layer="91">2019-05-20</text>
</plain>
<instances>
<instance part="S5" gate="G$1" x="142.24" y="132.08" smashed="yes">
<attribute name="PART" x="138.43" y="135.89" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="138.43" y="129.54" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="138.43" y="121.92" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="138.43" y="127" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="138.43" y="124.46" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="S4" gate="G$1" x="157.48" y="132.08" smashed="yes">
<attribute name="PART" x="153.67" y="135.89" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="153.67" y="129.54" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="153.67" y="121.92" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="153.67" y="127" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="153.67" y="124.46" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="ESP1" gate="G$1" x="58.42" y="147.32" smashed="yes" rot="R270">
<attribute name="VALUE" x="54.61" y="153.67" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+T-X1" gate="1" x="142.24" y="142.24" smashed="yes">
<attribute name="PART" x="140.462" y="143.764" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="140.462" y="141.224" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="P1" gate="G$1" x="182.88" y="111.76" smashed="yes">
<attribute name="PART" x="180.34" y="114.3" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="180.34" y="111.76" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="180.34" y="104.14" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="180.34" y="109.22" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="180.34" y="106.68" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P2" gate="G$1" x="193.04" y="111.76" smashed="yes">
<attribute name="PART" x="190.5" y="114.3" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="190.5" y="111.76" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="190.5" y="104.14" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="190.5" y="109.22" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="190.5" y="106.68" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P3" gate="G$1" x="203.2" y="111.76" smashed="yes">
<attribute name="PART" x="200.66" y="114.3" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="200.66" y="111.76" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="200.66" y="104.14" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="200.66" y="109.22" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="200.66" y="106.68" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="S3" gate="G$1" x="170.18" y="132.08" smashed="yes">
<attribute name="PART" x="166.37" y="135.89" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="166.37" y="129.54" size="1.778" layer="96" rot="R180"/>
<attribute name="FUNKTION" x="166.37" y="121.92" size="1.778" layer="96" rot="R180"/>
<attribute name="TYPE" x="166.37" y="127" size="1.778" layer="96" rot="R180"/>
<attribute name="HERSTELLER" x="166.37" y="124.46" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="+T-X1" gate="2" x="142.24" y="121.92" smashed="yes">
<attribute name="PART" x="140.462" y="123.444" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="140.462" y="120.904" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X1" gate="3" x="157.48" y="142.24" smashed="yes">
<attribute name="PART" x="155.956" y="143.764" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="155.956" y="141.224" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X1" gate="4" x="157.48" y="121.92" smashed="yes">
<attribute name="PART" x="155.702" y="123.444" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="155.702" y="120.904" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X1" gate="5" x="170.18" y="142.24" smashed="yes">
<attribute name="PART" x="168.402" y="143.764" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="168.402" y="141.224" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X1" gate="6" x="170.18" y="121.92" smashed="yes">
<attribute name="PART" x="168.402" y="123.444" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="168.402" y="120.904" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="-A1" gate="G$1" x="93.98" y="106.68" smashed="yes">
<attribute name="PART" x="95.25" y="132.08" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="120.65" y="132.08" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="-A1" gate="I1" x="142.24" y="111.76" smashed="yes">
<attribute name="PART" x="139.7" y="114.3" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="-A1" gate="I2" x="157.48" y="111.76" smashed="yes">
<attribute name="PART" x="154.94" y="114.3" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="-A1" gate="I3" x="170.18" y="111.76" smashed="yes">
<attribute name="PART" x="167.64" y="114.3" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="-A1" gate="Q1" x="182.88" y="132.08" smashed="yes">
<attribute name="PART" x="180.34" y="133.35" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="-A1" gate="Q2" x="193.04" y="132.08" smashed="yes">
<attribute name="PART" x="190.5" y="133.35" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="-A1" gate="Q3" x="203.2" y="132.08" smashed="yes">
<attribute name="PART" x="200.66" y="133.35" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X2" gate="10" x="193.04" y="99.06" smashed="yes">
<attribute name="PART" x="191.262" y="100.584" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="191.262" y="98.044" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X2" gate="9" x="203.2" y="121.92" smashed="yes">
<attribute name="PART" x="201.422" y="122.936" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="201.422" y="120.396" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X2" gate="8" x="193.04" y="121.92" smashed="yes">
<attribute name="PART" x="191.262" y="123.444" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="191.262" y="120.904" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="+T-X2" gate="7" x="182.88" y="121.92" smashed="yes">
<attribute name="PART" x="181.102" y="123.444" size="1.778" layer="95" rot="R180"/>
<attribute name="GATE" x="181.102" y="120.904" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="RAHMEN1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="RAHMEN1" gate="G$2" x="200.66" y="0" smashed="yes">
<attribute name="SHEET" x="276.86" y="1.27" size="2.032" layer="94" font="vector"/>
<attribute name="DRAWING_NAME" x="226.06" y="13.335" size="2.032" layer="94" font="vector"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="S5" gate="G$1" pin="S1"/>
<pinref part="+T-X1" gate="2" pin="2.2"/>
<wire x1="142.24" y1="124.46" x2="142.24" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="S4" gate="G$1" pin="CS1"/>
<pinref part="+T-X1" gate="3" pin="3.1"/>
<wire x1="157.48" y1="139.7" x2="157.48" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="S4" gate="G$1" pin="S1"/>
<pinref part="+T-X1" gate="4" pin="4.2"/>
<wire x1="157.48" y1="124.46" x2="157.48" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="CS1"/>
<pinref part="+T-X1" gate="5" pin="5.1"/>
<wire x1="170.18" y1="139.7" x2="170.18" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="S1"/>
<pinref part="+T-X1" gate="6" pin="6.2"/>
<wire x1="170.18" y1="124.46" x2="170.18" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="S5" gate="G$1" pin="CS1"/>
<pinref part="+T-X1" gate="1" pin="1.1"/>
<wire x1="142.24" y1="139.7" x2="142.24" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+-EXT" class="0">
<segment>
<pinref part="ESP1" gate="G$1" pin="+-EXT"/>
<pinref part="+T-X1" gate="5" pin="5.2"/>
<wire x1="66.04" y1="149.86" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
<wire x1="76.2" y1="149.86" x2="142.24" y2="149.86" width="0.1524" layer="91"/>
<wire x1="142.24" y1="149.86" x2="157.48" y2="149.86" width="0.1524" layer="91"/>
<wire x1="157.48" y1="149.86" x2="170.18" y2="149.86" width="0.1524" layer="91"/>
<wire x1="170.18" y1="149.86" x2="170.18" y2="144.78" width="0.1524" layer="91"/>
<pinref part="+T-X1" gate="3" pin="3.2"/>
<wire x1="157.48" y1="144.78" x2="157.48" y2="149.86" width="0.1524" layer="91"/>
<junction x="157.48" y="149.86"/>
<pinref part="+T-X1" gate="1" pin="1.2"/>
<wire x1="142.24" y1="144.78" x2="142.24" y2="149.86" width="0.1524" layer="91"/>
<junction x="142.24" y="149.86"/>
<pinref part="-A1" gate="G$1" pin="L+"/>
<wire x1="76.2" y1="132.08" x2="76.2" y2="149.86" width="0.1524" layer="91"/>
<junction x="76.2" y="149.86"/>
<pinref part="-A1" gate="Q3" pin="1"/>
<wire x1="170.18" y1="149.86" x2="182.88" y2="149.86" width="0.1524" layer="91"/>
<wire x1="182.88" y1="149.86" x2="193.04" y2="149.86" width="0.1524" layer="91"/>
<wire x1="193.04" y1="149.86" x2="203.2" y2="149.86" width="0.1524" layer="91"/>
<wire x1="203.2" y1="149.86" x2="203.2" y2="137.16" width="0.1524" layer="91"/>
<junction x="170.18" y="149.86"/>
<pinref part="-A1" gate="Q2" pin="1"/>
<wire x1="193.04" y1="137.16" x2="193.04" y2="149.86" width="0.1524" layer="91"/>
<junction x="193.04" y="149.86"/>
<pinref part="-A1" gate="Q1" pin="1"/>
<wire x1="182.88" y1="137.16" x2="182.88" y2="149.86" width="0.1524" layer="91"/>
<junction x="182.88" y="149.86"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="X1"/>
<pinref part="+T-X2" gate="9" pin="9.1"/>
<wire x1="203.2" y1="116.84" x2="203.2" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="P2" gate="G$1" pin="X1"/>
<pinref part="+T-X2" gate="8" pin="8.1"/>
<wire x1="193.04" y1="116.84" x2="193.04" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="X1"/>
<pinref part="+T-X2" gate="7" pin="7.1"/>
<wire x1="182.88" y1="116.84" x2="182.88" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="+T-X2" gate="10" pin="10.2"/>
<pinref part="P2" gate="G$1" pin="X2"/>
<wire x1="193.04" y1="101.6" x2="193.04" y2="104.14" width="0.1524" layer="91"/>
<wire x1="193.04" y1="104.14" x2="193.04" y2="106.68" width="0.1524" layer="91"/>
<wire x1="193.04" y1="104.14" x2="203.2" y2="104.14" width="0.1524" layer="91"/>
<junction x="193.04" y="104.14"/>
<pinref part="P3" gate="G$1" pin="X2"/>
<wire x1="203.2" y1="104.14" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<pinref part="P1" gate="G$1" pin="X2"/>
<wire x1="182.88" y1="106.68" x2="182.88" y2="104.14" width="0.1524" layer="91"/>
<wire x1="182.88" y1="104.14" x2="193.04" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="0V-EXT" class="0">
<segment>
<pinref part="+T-X2" gate="10" pin="10.1"/>
<wire x1="193.04" y1="96.52" x2="193.04" y2="93.98" width="0.1524" layer="91"/>
<wire x1="193.04" y1="93.98" x2="129.54" y2="93.98" width="0.1524" layer="91"/>
<wire x1="129.54" y1="93.98" x2="129.54" y2="144.78" width="0.1524" layer="91"/>
<pinref part="ESP1" gate="G$1" pin="0V-EXT"/>
<wire x1="129.54" y1="144.78" x2="78.74" y2="144.78" width="0.1524" layer="91"/>
<pinref part="-A1" gate="G$1" pin="M"/>
<wire x1="78.74" y1="144.78" x2="66.04" y2="144.78" width="0.1524" layer="91"/>
<wire x1="78.74" y1="132.08" x2="78.74" y2="144.78" width="0.1524" layer="91"/>
<junction x="78.74" y="144.78"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="-A1" gate="I1" pin="IN"/>
<pinref part="+T-X1" gate="2" pin="2.1"/>
<wire x1="142.24" y1="116.84" x2="142.24" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="-A1" gate="I2" pin="IN"/>
<pinref part="+T-X1" gate="4" pin="4.1"/>
<wire x1="157.48" y1="116.84" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="-A1" gate="I3" pin="IN"/>
<pinref part="+T-X1" gate="6" pin="6.1"/>
<wire x1="170.18" y1="116.84" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="+T-X2" gate="7" pin="7.2"/>
<pinref part="-A1" gate="Q1" pin="2"/>
<wire x1="182.88" y1="124.46" x2="182.88" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="+T-X2" gate="8" pin="8.2"/>
<pinref part="-A1" gate="Q2" pin="2"/>
<wire x1="193.04" y1="124.46" x2="193.04" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="+T-X2" gate="9" pin="9.2"/>
<pinref part="-A1" gate="Q3" pin="2"/>
<wire x1="203.2" y1="124.46" x2="203.2" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
