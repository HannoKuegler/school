import at.htlklu.schnittstellen.SerielleSchnittstelle;

public class TestApplication {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		final SerielleSchnittstelle serial = new SerielleSchnittstelle();
		serial.setPortName("COM6");
		serial.setBaudRate(9600);
		serial.sendString("LEDON\r\n");
		serial.disconnect();
	}

}
