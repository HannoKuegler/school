String inputString = ""; // a String to hold incoming data
bool stringComplete = false; // whether the string is complete

void setup() {

  Serial.begin(9600);
  inputString.reserve(200);
  pinMode(3, OUTPUT);
  pinMode(A1, INPUT);
  digitalWrite(3, LOW);
  pinMode(10, OUTPUT);
  digitalWrite(10, LOW);
  //buzzer 
  pinMode(11, OUTPUT);
}

void loop() {

  if (stringComplete) {
    //Serial.println("Empfangen: "+inputString);

    if (inputString == "LEDON") {
      ledOn();
    }
    if (inputString == "LEDOFF") {
      ledOff();
    }
    if (inputString == "GETLIGHT") {
      getLight();
    }
    if (inputString == "BUZZ") {
      buzz();
    }
    if (inputString == "NOBUZZ") {
      analogWrite(11, 0);
    }

    inputString = "";
    stringComplete = false;
  }
}
void getLight() {
  int licht = analogRead(A1);
  Serial.print(licht);
  Serial.print("\n\r");
}
void buzz() {

  int buzzer = 11;
  analogWrite(buzzer, 20);

}

void ledOn() {
  digitalWrite(3, HIGH);
}
void ledOff() {
  digitalWrite(3, LOW);
}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char) Serial.read();
    if (inChar != '\n' && inChar != '\r') {
      inputString += inChar;
    } else {
      stringComplete = true;
    }

  }
}
